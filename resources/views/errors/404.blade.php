@extends('site.sitemaster')
@section('maincontent')
<!-- Page Content -->
<h3 class="text-center">LOST YOUR WAY? No worries, we got you.</h3>
@include('site.fragments.searchbox_mini')

<h2 class="text-center" style='font-size:30px'>404 Error</h2>
<h3 class='text-center'>The page you were looking for couldn't be found</h3>

@stop
