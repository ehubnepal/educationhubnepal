@extends('admin.adminmaster')
@section('maincontent')
<!-- Main content -->
<section class="content">
    @if(isset($brand))
        {!! Form::open(['url'=>'admin/brand/update','files'=>'true','method'=>'post']) !!}
        {!! Form::hidden('id',$brand->id) !!}
        @else
        {!! Form::open(['url'=>'admin/brand','files'=>'true','method'=>'post']) !!}
        @endif
        <div class="row">
            <div class="col-md-12"> 
                <!-- /.box-body -->
                <div class="box-footer text-right">
                    <button type="submit" class="btn btn-success">Save</button>
                    <button type="reset" class="btn btn-primary">Reset</button>
                    <a href="{{ url('admin/brand/') }}" class="btn btn-danger">Cancel</a> </div>
                <!-- /.box --> 
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"> Add new Brand </h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <?php $brandname = isset($brand->brandname)?$brand->brandname:'';?>
                          {!! Form::label('title','Title * ') !!}
                          {!! Form::text('brandname', $brandname,['class'=>'form-control','id'=>'brandname','placeholder'=>'Title','required'=>'required']) !!}
                        </div>
                        <div class="form-group">
                            <?php $description = isset($brand->description)?$brand->description:'';?>
                            {!! Form::label('description','Description ') !!}
                            {!! Form::textarea('description', $description, ['class'=>'form-control','id'=>'description','placeholder'=>'Description']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('logo','Logo ') !!}
                            {!! Form::file('logo',['class'=>'form-control','id'=>'logo']) !!}
                            <?php if(!empty($brand->media)){
                                if(@getimagesize(url($brand->media[0]->thumb_url))){
                                    echo '<br/><img src="'.url($brand->media[0]->thumb_url).'" />';
                                    echo '<a href="'.url('admin/brand/'.$brand->id.'/removelogo').'" class="text-danger"><i class="fa fa-trash"></i> Remove logo</a>';
                                }
                            }?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            Publish option
                        </h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <label> 
                                <?php $published = isset($brand->published)&& $brand->published==1?'checked="checked"':'';?>
                                <input type='checkbox' name='published' value='1' class='flat-red' <?php echo $published;?> /> Published
                            </label><br>
                            <label> 
                        </div>
                    </div>
                    <!-- -->
                </div>
            </div>
        </div>
    </form>
</section>
@stop