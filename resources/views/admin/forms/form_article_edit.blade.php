@extends('admin.adminmaster')
@section('maincontent')
<div class="box-body"><h1> Edit: {{$post->title}}</h1></div>

</hr>

{!! Form::model($post,['method'=>'PATCH', 'files' => true, 'action'=>['Admin\ArticleController@update',$post->post_id]]) !!}
@include('admin.forms.form_article',['ButtonText'=>'Update Article'])
@include('admin.partials.flash')
{!! Form::close() !!}
<!-- validation and error handaling goes here -->
@include('errors.error_list')

@stop