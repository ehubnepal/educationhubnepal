<div class="row">
<div class="col-md-12">
<div class="box-body">
<div class="form-group">
{!! Form::label('name','Name:') !!}
{!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Enter title']) !!}
{!! Form::label('slug','Slug:') !!}
{!! Form::text('slug',null,['class'=>'form-control','placeholder'=>'Enter Slug']) !!}
{!! Form::label('title','Category Title:') !!}
{!! Form::text('title',null,['class'=>'form-control','placeholder'=>'Enter title']) !!}
</div>
<div class="form-group">
{!! Form::label('desc','Description:') !!}
{!! Form::textarea('desc',null,['class'=>'form-control','placeholder'=>'Short Description About Category','rows'=>'3']) !!}
	</div>
	<div class="form-group">
<?php if(isset($category)){
			
		
		if($category->published==1){
			echo '<label for="check"><input checked="active" name="published" type="checkbox" id="check" value="1"> Published</label>';
		}
		else{
			echo '<label for="check"><input checked="active" name="published" type="checkbox" id="check" value="0"> Published</label>';
		}
	}

		else{
			
			echo '<label for="check"><input checked="active" name="published" type="checkbox" id="check" value="1"> Published</label>';
		}

?>
</div>
<div class="form-group">
    {!! Form::label('image','Banner Image') !!}
    {!! Form::file('image', null) !!}
    <?php if(isset($category->image)):?>
    <img src="{!!url('public/admin/img/blog/'.$category->image) !!}" class="img-circle" height="100px" width="100px">
<?php endif; ?>
</div>
<div class="form-group">
{!! Form::submit($ButtonText,['class'=>'btn btn-primary form-control']) !!}

</div>
</div>
</div>
</div>