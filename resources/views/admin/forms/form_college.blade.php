@extends('admin.adminmaster')
@section('maincontent')
<!-- Main content -->
<section class="content">
    @if(isset($college))
    {!! Form::open(['url'=>'admin/college/update','files'=>'true','method'=>'post']) !!}
    {!! Form::hidden('id',$college->id) !!}
    @else
    {!! Form::open(['url'=>'admin/college','files'=>'true','method'=>'post']) !!}
    @endif
    <div class="row">
        <div class="col-md-12"> 
            <!-- /.box-body -->
            <div class="box-footer text-right">
                <button type="submit" class="btn btn-success">Save</button>
                <button type="reset" class="btn btn-primary">Reset</button>
                @if(isset($college))
                <a href="{{ url('admin/collegeprograms/'.$college->id) }}" class="btn btn-info">Edit Programs</a>
                @endif
                <a href="{{ url('admin/college') }}" class="btn btn-danger">Cancel</a> </div>
            <!-- /.box --> 
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"> <?php isset($college) ? print 'Edit college (' . $college->cname . ')' : print 'Add new college'; ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    <div class="form-group">
                        <?php $cname = isset($college->cname) ? $college->cname : ''; ?>
                        {!! Form::label('cname','College * ') !!}
                        {!! Form::text('cname', $cname,['class'=>'form-control','id'=>'model','placeholder'=>'Model name','required'=>'required']) !!}
                    </div>
                    <div class="form-group">
                        <?php $url = isset($college->url) ? $college->url : ''; ?>
                        {!! Form::label('url','Alias * ') !!}
                        {!! Form::text('url', $url,['class'=>'form-control','id'=>'url','placeholder'=>'Url','required'=>'required']) !!}
                    </div>
                    <div class="form-group">

                        <?php $address = isset($college->address) ? $college->address : ''; ?>
                        {!! Form::label('address','Address * ') !!}
                        {!! Form::text('address', $address,['class'=>'form-control','id'=>'address','placeholder'=>'Enter address']) !!}
                    </div>
                    <div class="form-group">
                        <?php $longitude = isset($college->longitude) ? $college->longitude : ''; ?>
                        {!! Form::label('longitude','Longitude ') !!}
                        {!! Form::text('longitude', $longitude,['class'=>'form-control','id'=>'longitude','placeholder'=>'Autopopulate longitude']) !!}
                    </div>
                    <div class="form-group">
                        <?php $latitude = isset($college->latitude) ? $college->latitude : ''; ?>
                        {!! Form::label('latitude','Latitude ') !!}
                        {!! Form::text('latitude', $latitude,['class'=>'form-control','id'=>'latitude','placeholder'=>'Autopopulate latitude']) !!}
                    </div>
                    <div class="form-group">
                        <?php $district = isset($college->district) ? $college->district : ''; ?>
                        {!! Form::label('district','District ') !!}
                        {!! Form::text('district', $district,['class'=>'form-control','id'=>'district','placeholder'=>'Enter district']) !!}
                    </div>
                    <div class="form-group">
                        <?php
                        $affiliated_by = array();
                        if (isset($college->affiliation_ids)):
                            $affiliated_by = @json_decode($college->affiliation_ids);
                        endif;
                        $affiliated_by = is_array($affiliated_by) ? $affiliated_by : array();
                        ?>
                        {!! Form::label('affiliations','Affiliated with ') !!}
                        @if(isset($affiliations))
                        <select class="form-control" name="affiliation_ids[]" multiple>
                            <?php
                            foreach ($affiliations as $affiliation):
                                $selected = in_array($affiliation->id, $affiliated_by) ? 'selected="selected"' : '';
                                ?>
                                <option value="{{$affiliation->id}}" {{$selected}}>{{$affiliation->aname}}</option>
                            <?php endforeach; ?>
                        </select>
                        @endif
                    </div>
                    <div class="form-group">
                        <?php $country = "Nepal" ?>
                        {!! Form::label('country','Country ') !!}
                        {!! Form::text('country', $country,['class'=>'form-control','id'=>'country','placeholder'=>'Country']) !!}
                    </div>
                    <div class="form-group">
                        <?php $description = isset($college->description) ? $college->description : ''; ?>
                        {!! Form::label('description','Description ') !!}
                        {!! Form::textarea('description', $description, ['class'=>'form-control','id'=>'description','placeholder'=>'Description']) !!}
                    </div>
                    <div class="form-group">
                        <?php $facilities = isset($college->facilities) ? $college->facilities : ''; ?>
                        {!! Form::label('facilities','Facilities ') !!}
                        {!! Form::textarea('facilities', $facilities, ['class'=>'form-control','id'=>'facilities','placeholder'=>'Facilities']) !!}
                    </div>
                    <div class="form-group">
                        <label> 
                            <?php $published = isset($college->published) && $college->published == 1 ? 'checked="checked"' : ''; ?>
                            <input type='checkbox' name='published' value='1' class='flat-red' <?php echo $published; ?> /> Published
                        </label><br>
                        <label> 
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"> Contact option </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <?php $phone = isset($college->phone) ? $college->phone : ''; ?>
                        {!! Form::label('phone','Phone ') !!}
                        {!! Form::text('phone', $phone,['class'=>'form-control','id'=>'phone','placeholder'=>'Enter Phone']) !!}
                    </div>
                    <div class="form-group">
                        <?php $fax = isset($college->fax) ? $college->fax : ''; ?>
                        {!! Form::label('fax','Fax ') !!}
                        {!! Form::text('fax', $fax,['class'=>'form-control','id'=>'fax','placeholder'=>'Enter fax']) !!}
                    </div>
                    <div class="form-group">
                        <?php $website = isset($college->website) ? $college->website : ''; ?>
                        {!! Form::label('website','Website ') !!}
                        {!! Form::text('website', $website,['class'=>'form-control','id'=>'website','placeholder'=>'Enter website']) !!}
                    </div>
                    <div class="form-group">
                        <?php $pobox = isset($college->pobox) ? $college->pobox : ''; ?>
                        {!! Form::label('pobox','POBox ') !!}
                        {!! Form::text('pobox', $pobox,['class'=>'form-control','id'=>'pobox','placeholder'=>'Enter pobox']) !!}
                    </div>
                    <div class="form-group">
                        <?php $email = isset($college->email) ? $college->email : ''; ?>
                        {!! Form::label('email','Email ') !!}
                        {!! Form::text('email', $email,['class'=>'form-control','id'=>'email','placeholder'=>'Enter email']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('collegelogo','Logo ') !!}
                        {!! Form::file('collegelogo',['class'=>'form-control','id'=>'collegelogo']) !!}
                        <?php
                        if (!empty($college->full_url)) {
                            if (@getimagesize(url($college->full_url))) {
                                echo '<br/><img src="' . url($college->full_url) . '" />';
                                echo '<a href="' . url('admin/college/' . $college->id . '/removelogo') . '" class="text-danger"><i class="fa fa-trash"></i> Remove logo</a>';
                            }
                        }
                        ?>
                    </div>
                    <hr/>
                    <div class="form-group">
                        {!! Form::label('collegeimage','Images ') !!} &nbsp;<span class="loading" style="display:none"> | Loading ...</span>
                        <div class="row">
                            <div class="col-sm-8">
                                {!! Form::file('collegeimage',['class'=>'form-control','id'=>'collegeimage']) !!}
                            </div>
                            <div class="col-sm-4">
                                <input type="checkbox" name="is_banner" value="1" /> Is banner
                            </div>
                        </div>
                        <br/>
                        @if (!empty($college->media))
                        @foreach ($college->media as $media)
                        @if (@getimagesize(url($media->full_url)))
                        <div style="margin-bottom:10px;border-bottom:1px dashed #ccc;" class="row">
                            <div class="col-sm-4">
                                <img src="{{url($media->thumb_url)}}" />
                                <br/>

                            </div>
                            <div class="col-sm-8">
                                Is banner : <input type="checkbox" class="image_is_banner" data="{{$media->refid}}" <?php $media->is_banner == 1 ? print 'checked' : ''; ?>/> <br/>
                                Published : <input type="checkbox" class="image_is_published" data="{{$media->refid}}" <?php $media->published == 1 ? print 'checked' : ''; ?>/> <br/>
                                Ordering : <input type="text" class="image_ordering" data="{{$media->refid}}" value = "{{$media->ordering}}"/><br/>
                                <a href="{{url('admin/college/' . $media->id . '/removeimage')}}" class="text-danger"><i class="fa fa-trash"></i> Remove image</a>
                                
                            </div>
                        </div>
                        @endif
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
</section>
@stop

@section('scripts')
<script src="{{url('public/admin/js/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
var __base_url = "{{url('/')}}";
$(function () {
    CKEDITOR.replace('description');
    CKEDITOR.replace('facilities');
});
$('.image_is_banner').click(function () {
    var url = $(this).is(":checked") ? __base_url + '/admin/college/setimagebanner/'+jQuery(this).attr('data') : __base_url + '/admin/college/unsetimagebanner/'+jQuery(this).attr('data');
    sendRequest(url);
});

$('.image_is_published').click(function () {
    var url = $(this).is(":checked") ? __base_url + '/admin/college/setimagepublish/'+$(this).attr('data') : __base_url + '/admin/college/setimageunpublish/'+$(this).attr('data');
    sendRequest(url);
});

$('.image_ordering').change(function () {
    var url = __base_url + '/admin/college/setimageordering/'+$(this).attr('data')+'/'+$(this).val();
    sendRequest(url);
});

function sendRequest(url) {
    jQuery('.loading').show();
    jQuery.ajax({
        url: url, type: "get", complete: function (data) {
            jQuery('.loading').hide();
        }
    });
}
</script>
@stop

