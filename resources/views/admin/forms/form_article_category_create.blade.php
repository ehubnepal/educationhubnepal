@extends('admin.adminmaster')
@section('maincontent')
<div class="box-body"><h1> Add A New Category</h1></div>
{!! Form::open(array('url'=>'admin/blog/categories','files'=>true)) !!}

@include('admin.forms.form_article_category',['ButtonText'=>'Add Category'])
{!! Form::close() !!}

@stop