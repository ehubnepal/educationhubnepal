@extends('admin.adminmaster')
@section('maincontent')
<div class="box-body"><h1> Add A New Tag</h1></div>
{!! Form::open(array('url'=>'admin/blog/tags')) !!}

@include('admin.forms.form_article_tag',['ButtonText'=>'Add Tag'])
{!! Form::close() !!}

@stop