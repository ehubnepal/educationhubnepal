@extends('admin.adminmaster')
@section('maincontent')
<!-- Main content -->
<section class="content">
    @if(isset($faculty))
    {!! Form::open(['url'=>'admin/faculty/update','files'=>'true','method'=>'post']) !!}
    {!! Form::hidden('id',$faculty->id) !!}
    @else
    {!! Form::open(['url'=>'admin/faculty','files'=>'true','method'=>'post']) !!}
    @endif
    <div class="row">
        <div class="col-md-12"> 
            <!-- /.box-body -->
            <div class="box-footer text-right">
                <button type="submit" class="btn btn-success">Save</button>
                <button type="reset" class="btn btn-primary">Reset</button>
                <a href="{{ url('admin/faculty') }}" class="btn btn-danger">Cancel</a> </div>
            <!-- /.box --> 
        </div>
    </div>
    <?php
//    echo '<pre>';
//    print_r($affiliation);
//    echo '</pre>';
    ?>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"> <?php isset($faculty)?print 'Edit ':print 'Add new ';?> Faculty </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <?php $faculty_ = isset($faculty->faculty) ? $faculty->faculty : ''; ?>
                        {!! Form::label('faculty','Faculty * ') !!}
                        {!! Form::text('faculty', $faculty_,['class'=>'form-control','id'=>'faculty','placeholder'=>'eg. Faculty of management','required'=>'required']) !!}
                    </div>
                   <div class="form-group">
                        {!! Form::label('affiliations','University / Affiliation') !!}
                        @if(isset($affiliations))
                        <select class="form-control" name="affiliation_id" >
                            <?php
                            foreach ($affiliations as $affiliation):
                                $selected = isset($faculty->affiliation_id) && $faculty->affiliation_id == $affiliation->id ? 'selected="selected"' : '';
                                ?>
                                <option value="{{$affiliation->id}}" {{$selected}}>{{$affiliation->aname}}</option>
                            <?php endforeach; ?>
                        </select>
                        @endif
                    </div>
                    <div class="form-group">
                        <?php $logo = isset($faculty->logo) ? $faculty->logo : ''; ?>
                        {!! Form::label('logo','Logo ') !!}
                        {!! Form::text('logo', $logo,['class'=>'form-control','id'=>'logo','placeholder'=>'eg. fa fa-hat']) !!}
                    </div>
                    <div class="form-group">
                        <?php $url = isset($faculty->url) ? $faculty->url : ''; ?>
                        {!! Form::label('url','URL ') !!}
                        {!! Form::text('url', $url,['class'=>'form-control','id'=>'url','placeholder'=>'Custom url']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"> Publish option </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label> 
                            <?php $published = isset($faculty->published) && $faculty->published == 1 ? 'checked="checked"' : ''; ?>
                            <input type='checkbox' name='published' value='1' class='flat-red' <?php echo $published; ?> /> Published
                        </label><br>
                        <label> 
                    </div>
                </div>
            </div>
        </div>
    </div>
{!! Form::close() !!}
</section>
@stop


