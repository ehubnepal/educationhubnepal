@extends('admin.adminmaster')
@section('maincontent')
<!-- Main content -->
<section class="content">
    {!! Form::open(['url'=>'admin/bikefeature/update','files'=>'true','method'=>'post']) !!}
    {!! Form::hidden('id',$bikefeature->id) !!}
    <div class="row">
        <div class="col-md-12"> 
            <!-- /.box-body -->
            <div class="box-footer text-right">
                <button type="submit" class="btn btn-success">Save</button>
                <button type="reset" class="btn btn-primary">Reset</button>
                <a href="{{ url('admin/bikemodel') }}" class="btn btn-danger">Cancel</a> </div>
            <!-- /.box --> 
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"> Features </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        {!! Form::label('displacement','Displacement ') !!}
                        {!! Form::text('displacement', $bikefeature->displacement,['class'=>'form-control','id'=>'displacement','placeholder'=>'eg 150 cc']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('gearbox','Gearbox ') !!}
                        {!! Form::text('gearbox', $bikefeature->gearbox,['class'=>'form-control','id'=>'gearbox','placeholder'=>'eg 150 cc']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('seat_height','Seat Height ') !!}
                        {!! Form::text('seat_height', $bikefeature->seat_height,['class'=>'form-control','id'=>'seat_height','placeholder'=>'Seat Height']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('ground_clearance','Ground Clearance') !!}
                        {!! Form::text('ground_clearance', $bikefeature->ground_clearance,['class'=>'form-control','id'=>'ground_clearance','placeholder'=>'Ground Clearance']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tank_capacity','Tank Capacity') !!}
                        {!! Form::text('tank_capacity', $bikefeature->tank_capacity,['class'=>'form-control','id'=>'tank_capacity','placeholder'=>'eg 5 liters']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('top_speed','Top Speed') !!}
                        {!! Form::text('top_speed', $bikefeature->top_speed,['class'=>'form-control','id'=>'top_speed','placeholder'=>'eg 106 kmph']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('mileage','Mileage') !!}
                        {!! Form::text('mileage', $bikefeature->mileage,['class'=>'form-control','id'=>'mileage','placeholder'=>'eg 60 kmpl']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('brake','Brake') !!}
                        {!! Form::text('brake', $bikefeature->brake,['class'=>'form-control','id'=>'brake','placeholder'=>'Brake']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tyre','Tyre') !!}
                        {!! Form::text('tyre', $bikefeature->tyre,['class'=>'form-control','id'=>'tyre','placeholder'=>'tyre']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('engine','Engine') !!}
                        {!! Form::text('engine', $bikefeature->engine,['class'=>'form-control','id'=>'engine','placeholder'=>'engine']) !!}
                    </div>
                </div>
                <!-- --> 

            </div>
        </div>

        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title text-info"> {{$bikemodel->model}} </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php
                    if (!empty($bikemodel->media)) {
                        if (@getimagesize(url($bikemodel->media[0]->thumb_url))) {
                            echo '<img src="' . url($bikemodel->media[0]->thumb_url) . '" /><hr/>';
                        }
                    }
                    ?>
                    Brand : {{$bikemodel->brand}}<hr/>
                    Ad Count : {{$bikemodel->adCount}}<hr/>
                </div>
            </div>
        </div>
    </div>
</form>
</section>
@stop
