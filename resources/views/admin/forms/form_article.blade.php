
<div class="row">
<div class="col-md-12">
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js" type="text/javascript"></script>
<script src="{{url('public/admin/js/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
$(function () {
    CKEDITOR.replace('content');
    CKEDITOR.replace('desc');
});
</script>

<div class="box-body">
<div class="form-group">
{!! Form::label('title','Title:') !!}
{!! Form::text('title',null,['class'=>'form-control','placeholder'=>'Enter title']) !!}
{!! Form::label('slug','Slug:') !!}
{!! Form::text('slug',null,['class'=>'form-control','placeholder'=>'Enter Slug']) !!}
{!! Form::label('meta','Meta Description:') !!}
{!! Form::text('meta',null,['class'=>'form-control','placeholder'=>'Meta Description', 'rows'=>'3']) !!}
</div>
<div class="form-group">
{!! Form::label('desc','Description:') !!}
{!! Form::textarea('desc',null,['class'=>'form-control','placeholder'=>'Short Description About Your Article','rows'=>'3']) !!}


	</div>
<div class="form-group">
{!! Form::label('canonical','Canonical:') !!}
{!! Form::text('canonical',null,['class'=>'form-control','placeholder'=>'Enter Canonical URL']) !!}


	</div>
<div class="form-group">
{!! Form::label('content','Content') !!}
{!! Form::textarea('content',null,['class'=>'form-control','placeholder'=>'Paste your post here']) !!}
</div>
<!--<div class='form-group'>
{!! Form::label('publised_at','Published On:') !!}
{!! Form::input('date','published_at', date('Y-m-d H:i:s'), ['class'=>'form-control']) !!}
	</div>-->
<!-- tag form-->
@if(isset($post))
<div class="form-group">
	{!! Form::label('tag_list','Tags:') !!}
	{!! Form::select('tag_list[]', $tags,$post_tags,['id'=>'tag_list', 'class'=>'form-control','multiple']) !!}
	
</div>
<div class="form-group">
{!! Form::label('categories','Categories') !!}
{!! Form::select('categories[]', $categories, $post_categories,['id'=>'cat_list', 'class'=>'form-control','multiple']) !!}

</div>
<div class="form-group">
{!! Form::label('created_at','Created Date') !!}
{!! Form::date('created_at', date('Y-m-d',strtotime($post->created_at)), ['class' => 'form-control', 'placeholder' => 'Date']) !!}
</div>
@else
<div class="form-group">
	{!! Form::label('tag_list','Tags:') !!}
	{!! Form::select('tag_list[]', $tags,null,['id'=>'tag_list', 'class'=>'form-control','multiple']) !!}
	
</div>
<div class="form-group">
{!! Form::label('categories','Categories') !!}
{!! Form::select('categories[]', $categories, null,['id'=>'cat_list', 'class'=>'form-control','multiple']) !!}

</div>
<div class="form-group">
{!! Form::label('created_at','Created Date') !!}
{!! Form::date('created_at', null, ['class' => 'form-control', 'placeholder' => 'Date']) !!}
</div>
@endif


<div class="form-group">
<?php if(isset($post)){
			if($post->active==1){
				echo '<label for="check"><input checked="active" name="active" type="checkbox" id="check" value="1"> Published</label>';
			}
			else{
				echo '<label for="check"><input name="active" type="checkbox" value="0" id="check"> Published</label>';
				}
		}
		else{
			echo '<label for="check"><input checked="active" name="active" type="checkbox" id="check" value="1"> Published</label>';
		}

?>
</div>
<div class="form-group">
<?php if(isset($post)){
			if($post->home_page==1){
				echo '<label for="check"><input checked="active" name="home_page" type="checkbox" id="check" value="1">Publish in Homepage</label>';
			}
			else{
				echo '<label for="check"><input name="home_page" type="checkbox" value="0" id="check"> Publish in Homepage</label>';
				}
		}
		else{
			echo '<label for="check"><input checked="active" name="home_page" type="checkbox" id="check" value="1"> Publish in Homepage</label>';
		}

?>
</div>
	<div class="form-group">
    {!! Form::label('image','Banner Image') !!}
    {!! Form::file('image', null) !!}
    <?php if(isset($post->image)):?>
    <img src="{!!url('public/admin/img/blog/'.$post->image) !!}" class="img-circle" height="100px" width="100px">
<?php endif; ?>
</div>
<div class="form-group">
{!! Form::submit($ButtonText,['class'=>'btn btn-primary form-control']) !!}
</div>
</div>

<script type="text/javascript">

$('#tag_list').select2({
	'placeholder':'Choose Tags',
	tags:true,
	tokenSeparators:[",", " "],
	createTag:function(newTag){
		return{
			id:'new:' + newTag.term,
			text:newTag.term + '(new)'
		};
	}
});
$('#cat_list').select2({
	'placeholder':'Choose Categories Or Insert New',
	tags:true,
	
});

</script>
</div>
</div>