@extends('admin.adminmaster')
@section('maincontent')
<!-- Main content -->
<section class="content">
    @if(isset($affiliation))
    {!! Form::open(['url'=>'admin/affiliation/update','files'=>'true','method'=>'post']) !!}
    {!! Form::hidden('id',$affiliation->id) !!}
    @else
    {!! Form::open(['url'=>'admin/affiliation','files'=>'true','method'=>'post']) !!}
    @endif
    <div class="row">
        <div class="col-md-12"> 
            <!-- /.box-body -->
            <div class="box-footer text-right">
                <button type="submit" class="btn btn-success">Save</button>
                <button type="reset" class="btn btn-primary">Reset</button>
                <a href="{{ url('admin/affiliation') }}" class="btn btn-danger">Cancel</a> </div>
            <!-- /.box --> 
        </div>
    </div>
    <?php
//    echo '<pre>';
//    print_r($affiliation);
//    echo '</pre>';
    ?>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"> Add new University / Affiliation </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <?php $aname = isset($affiliation->aname) ? $affiliation->aname : ''; ?>
                        {!! Form::label('aname','University * ') !!}
                        {!! Form::text('aname', $aname,['class'=>'form-control','id'=>'affiliation','placeholder'=>'Unieversity/affiliation','required'=>'required']) !!}
                    </div>
                    <div class="form-group">

                        <?php $established = isset($affiliation->established) ? $affiliation->established : ''; ?>
                        {!! Form::label('established','Established * ') !!}
                        {!! Form::text('established', $established,['class'=>'form-control','id'=>'established','placeholder'=>'Enter year']) !!}
                    </div>
                    <div class="form-group">
                        <?php $educationbodytype = isset($affiliation->education_body_type) ? $affiliation->education_body_type : ''; ?>
                        {!! Form::label('educationbodytype','Education body type ') !!}
                        {!! Form::text('education_body_type', $educationbodytype,['class'=>'form-control','id'=>'educationbodytype','placeholder'=>'Education body type']) !!}
                    </div>
                    <div class="form-group">
                        <?php $languageofeducation = isset($affiliation->language_of_education) ? $affiliation->language_of_education : ''; ?>
                        {!! Form::label('languageofeducation','Language of Education ') !!}
                        {!! Form::text('language_of_education', $languageofeducation,['class'=>'form-control','id'=>'languageofeducation','placeholder'=>'Language of education']) !!}
                    </div>
                    <div class="form-group">
                        <?php $vicechancellor = isset($affiliation->vice_chancellor) ? $affiliation->vice_chancellor : ''; ?>
                        {!! Form::label('vicechancellor','Vice Chancellor ') !!}
                        {!! Form::text('vice_chancellor', $vicechancellor,['class'=>'form-control','id'=>'vicechancellor','placeholder'=>'Vice Chancellor']) !!}
                    </div>
                    <div class="form-group">
                        <?php $description = isset($affiliation->description) ? $affiliation->description : ''; ?>
                        {!! Form::label('description','Description ') !!}
                        {!! Form::textarea('description', $description, ['class'=>'form-control','id'=>'description','placeholder'=>'Description']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('universitylogo','Logo ') !!}
                        {!! Form::file('universitylogo',['class'=>'form-control','id'=>'universitylogo']) !!}
                        <?php
                        if (isset($affiliation->thumb_url)) {
                            if (@getimagesize(url($affiliation->thumb_url))) {
                                echo '<br/><img src="' . url($affiliation->thumb_url) . '" />';
                                echo '<a href="' . url('admin/affiliation/' . $affiliation->id . '/removelogo') . '" class="text-danger"><i class="fa fa-trash"></i> Remove logo</a>';
                            }
                        }
                        ?>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"> Publish option </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label> 
                            <?php $published = isset($affiliation->published) && $affiliation->published == 1 ? 'checked="checked"' : ''; ?>
                            <input type='checkbox' name='published' value='1' class='flat-red' <?php echo $published; ?> /> Published
                        </label><br>
                        <label> 
                    </div>
                    <hr/>
                    <div class="form-group">
                        {!! Form::label('image','Images ') !!} &nbsp;<span class="loading" style="display:none"> | Loading ...</span>
                        <div class="row">
                            <div class="col-sm-8">
                                {!! Form::file('universityimage',['class'=>'form-control','id'=>'universityimage']) !!}
                            </div>
                            <div class="col-sm-4">
                                <input type="checkbox" name="is_banner" value="1" /> Is banner
                            </div>
                        </div>
                        <br/>
                        @if (!empty($affiliation->media))
                        @foreach ($affiliation->media as $media)
                        @if (@getimagesize(url($media->full_url)))
                        <div style="margin-bottom:10px;border-bottom:1px dashed #ccc;" class="row">
                            <div class="col-sm-4">
                                <img src="{{url($media->thumb_url)}}" />
                                <br/>

                            </div>
                            <div class="col-sm-8">
                                Is banner : <input type="checkbox" class="image_is_banner" data="{{$media->refid}}" <?php $media->is_banner == 1 ? print 'checked' : ''; ?>/> <br/>
                                Published : <input type="checkbox" class="image_is_published" data="{{$media->refid}}" <?php $media->published == 1 ? print 'checked' : ''; ?>/> <br/>
                                Ordering : <input type="text" class="image_ordering" data="{{$media->refid}}" value = "{{$media->ordering}}"/><br/>
                                <a href="{{url('admin/affiliation/' . $media->id . '/removeimage')}}" class="text-danger"><i class="fa fa-trash"></i> Remove image</a>
                                
                            </div>
                        </div>
                        @endif
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
{!! Form::close() !!}
</section>
@stop

@section('scripts')
<script src="{{url('public/admin/js/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
var __base_url = "{{url('/')}}";
$(function () {
    CKEDITOR.replace('description');
});
$('.image_is_banner').click(function () {
    var url = $(this).is(":checked") ? __base_url + '/admin/affiliation/setimagebanner/'+jQuery(this).attr('data') : __base_url + '/admin/affiliation/unsetimagebanner/'+jQuery(this).attr('data');
    sendRequest(url);
});

$('.image_is_published').click(function () {
    var url = $(this).is(":checked") ? __base_url + '/admin/affiliation/setimagepublish/'+$(this).attr('data') : __base_url + '/admin/affiliation/setimageunpublish/'+$(this).attr('data');
    sendRequest(url);
});

$('.image_ordering').change(function () {
    var url = __base_url + '/admin/affiliation/setimageordering/'+$(this).attr('data')+'/'+$(this).val();
    sendRequest(url);
});

function sendRequest(url) {
    jQuery('.loading').show();
    jQuery.ajax({
        url: url, type: "get", complete: function (data) {
            jQuery('.loading').hide();
        }
    });
}
</script>
@stop
