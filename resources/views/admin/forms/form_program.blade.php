@extends('admin.adminmaster')
@section('maincontent')
<!-- Main content -->
<section class="content">
    @if(isset($program))
    {!! Form::open(['url'=>'admin/program/update','files'=>'true','method'=>'post']) !!}
    {!! Form::hidden('id',$program->id) !!}
    @else
    {!! Form::open(['url'=>'admin/program','files'=>'true','method'=>'post']) !!}
    @endif
    <div class="row">
        <div class="col-md-12"> 
            <!-- /.box-body -->
            <div class="box-footer text-right">
                <button type="submit" class="btn btn-success">Save</button>
                <button type="reset" class="btn btn-primary">Reset</button>
                <a href="{{ url('admin/program') }}" class="btn btn-danger">Cancel</a> </div>
            <!-- /.box --> 
        </div>
    </div>
    <?php
//    echo '<pre>';
//    print_r($affiliation);
//    echo '</pre>';
    ?>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"> Add new Program </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <?php $pname = isset($program->pname) ? $program->pname : ''; ?>
                        {!! Form::label('pname','Program * ') !!}
                        {!! Form::text('pname', $pname,['class'=>'form-control','id'=>'pname','placeholder'=>'Program','required'=>'required']) !!}
                    </div>
                    <div class="form-group">
                        <?php $url = isset($program->url) ? $program->url : ''; ?>
                        {!! Form::label('url','Url * ') !!}
                        {!! Form::text('url', $url,['class'=>'form-control','id'=>'url','placeholder'=>'Url','required'=>'required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('affiliations','Affiliated with ') !!} <span class="loading" style="display:none">Loading...</span>
                        @if(isset($affiliations))
                        <select class="form-control" name="affiliation_id" id="affiliation_id">
                            <?php
                            foreach ($affiliations as $affiliation):
                                $selected = isset($program->affiliation_id) && $program->affiliation_id == $affiliation->id ? 'selected="selected"' : '';
                                ?>
                                <option value="{{$affiliation->id}}" {{$selected}}>{{$affiliation->aname}}</option>
                            <?php endforeach; ?>
                        </select>
                        @endif
                    </div>

                    <div class="form-group">
                        {!! Form::label('faculty','Faculty ') !!}
                        @if(isset($faculties))
                        <select class="form-control" name="faculty_id" id="faculty_id">
                            <?php
                            foreach ($faculties as $faculty):
                                $selected = isset($program->faculty_id) && $program->faculty_id == $faculty->id ? 'selected="selected"' : '';
                                ?>
                                <option value="{{$faculty->id}}" {{$selected}}>{{$faculty->faculty}}</option>
                            <?php endforeach; ?>
                            <option value="-1">n/a</option>
                        </select>
                        @endif
                    </div>
                    
                    <div class="form-group">
                        {!! Form::label('programcategory','Category ') !!}
                        @if(isset($faculties))
                        <select class="form-control" name="programcategory[]" id="programcategory" multiple="">
                            <?php
                            if(isset($program_category)):
                            foreach ($program_category as $category):
                                $selected = isset($program->programcategory) && in_array($category->id, $program->programcategory) ? 'selected="selected"' : '';
                                ?>
                                <option value="{{$category->id}}" {{$selected}}>{{$category->category}}</option>
                            <?php endforeach;
                            endif;?>
                            <option value="-1">n/a</option>
                        </select>
                        @endif
                    </div>

                    <div class="form-group">
                        <?php $level_ = isset($program->level_id) ? $program->level_id : ''; ?>
                        {!! Form::label('level','Level * ') !!}
                        <select name="level_id" class="form-control">
                            <?php
                            //$levels = array('school', 'intermediate', '+2', 'diploma', 'bachelors', 'masters', 'PhD', 'other');
                            foreach ($levels as $level):
                                $selected = $level->id == $level_ ? 'selected="selected"' : '';
                                echo "<option value='" . $level->id . "' " . $selected . ">" . $level->lname . "</option>";
                            endforeach;
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <?php $duration = isset($program->duration) ? $program->duration : ''; ?>
                        {!! Form::label('duration','Duration') !!}
                        {!! Form::text('duration', $duration,['class'=>'form-control','id'=>'duration','placeholder'=>'eg: 4 years']) !!}
                    </div>
                    <div class="form-group">
                        <?php $cost_range = isset($program->cost_range) ? $program->cost_range : ''; ?>
                        {!! Form::label('cost_range','Cost range ') !!}
                        {!! Form::text('cost_range', $cost_range,['class'=>'form-control','id'=>'cost_range','placeholder'=>'eg: Rs 2,00,000 to Rs 3,00,000']) !!}
                    </div>
                    <div class="form-group">
                        <?php $academic_year = isset($program->academic_year) ? $program->academic_year : ''; ?>
                        {!! Form::label('academic_year','Academic year') !!}
                        {!! Form::text('academic_year', $academic_year,['class'=>'form-control','id'=>'academic_year','placeholder'=>'eg. semester, year']) !!}
                    </div>
                    <div class="form-group">
                        <?php $evaluation_method_ = isset($program->evaluation_method) ? $program->evaluation_method : ''; ?>
                        {!! Form::label('evaluation_method','Evaluation method ') !!}
                        <select name="evaluation_method" class="form-control">
                            <?php
                            $evaluation_methods = array('gpa', 'cgpa', 'percentage', 'otehr');
                            foreach ($evaluation_methods as $evaluation_method):
                                $selected = $evaluation_method == $evaluation_method_ ? 'selected="selected"' : '';
                                echo "<option value='" . $evaluation_method . "' " . $selected . ">" . $evaluation_method . "</option>\n";
                            endforeach;
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <?php $program_since = isset($program->program_since) ? $program->program_since : ''; ?>
                        {!! Form::label('pname','Program since ') !!}
                        {!! Form::text('program_since', $program_since,['class'=>'form-control','id'=>'program_since','placeholder'=>'Program since']) !!}
                    </div>
                    <div class="form-group">
                        <?php $weight = isset($program->weight) ? $program->weight : ''; ?>
                        {!! Form::label('weight','Program Weight ') !!}
                        {!! Form::text('weight', $weight,['class'=>'form-control','id'=>'program_since','placeholder'=>'Program Weight']) !!}
                    </div>
                    
                    <div class="form-group">
                        <?php $eligibility = isset($program->eligibility) ? $program->eligibility : ''; ?>
                        {!! Form::label('eligibility','Eligibility') !!}
                        {!! Form::textarea('eligibility', $eligibility,['class'=>'form-control','id'=>'eligibility','placeholder'=>'Eligibility']) !!}
                    </div>
                    <div class="form-group">
                        <?php $curriculum = isset($program->curriculum) ? $program->curriculum : ''; ?>
                        {!! Form::label('curriculum','Curriculum') !!}
                        {!! Form::textarea('curriculum', $curriculum,['class'=>'form-control','id'=>'curriculum','placeholder'=>'Curriculum']) !!}
                    </div>
                    

                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"> Publish option </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label> 
                            <?php $published = isset($program->published) && $program->published == 1 ? 'checked="checked"' : ''; ?>
                            <input type='checkbox' name='published' value='1' class='flat-red' <?php echo $published; ?> /> Published
                        </label><br>
                        <label> 
                    </div>
                    <div class="form-group">
                        <?php $course_description = isset($program->course_description) ? $program->course_description : ''; ?>
                        {!! Form::label('course_description','Course Description ') !!}
                        {!! Form::textarea('course_description', $course_description, ['class'=>'form-control','id'=>'course_description','placeholder'=>'Course Description']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</section>
@stop

@section('scripts')
<script src="{{url('public/admin/js/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
var __base_url = "{{url('/')}}";
$(function () {
    CKEDITOR.replace('curriculum');
    CKEDITOR.replace('course_description');
    CKEDITOR.replace('eligibility');
});

$('#affiliation_id').change(function(){
    var affiliation_id = $(this).val();
    $('.loading').show();
    $.ajax({
        url: __base_url+'/admin/program/'+affiliation_id+'/getFaculties',
        type:"get",
        success:function(data){
            $('#faculty_id').html(data);
            $('.loading').hide();
        }
    });
});
</script>
@stop

