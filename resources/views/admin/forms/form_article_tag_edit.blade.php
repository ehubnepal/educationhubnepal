@extends('admin.adminmaster')
@section('maincontent')
<div class="box-body"><h1> Edit A Tag</h1></div>
{!! Form::model($tag,['method'=>'PATCH', 'action'=>['Admin\ArticleTagController@update',$tag->tag_id]]) !!}
@include('admin.forms.form_article_tag',['ButtonText'=>'Update Tag'])
{!! Form::close() !!}

@stop