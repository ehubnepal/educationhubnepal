<div class="row">
<div class="col-md-12">
<div class="box-body">
<div class="form-group">
{!! Form::label('name','Name:') !!}
{!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Enter Tag Name']) !!}
</div>
<div class="form-group">
{!! Form::submit($ButtonText,['class'=>'btn btn-primary form-control']) !!}

</div>
</div>
</div>
</div>