@extends('admin.adminmaster')
@section('maincontent')
<div class="box-body"><h1> Add A New Article</h1></div>

</hr>

{!! Form::open(array('url'=>'admin/blog/posts','files' => true)) !!}
@include('admin.forms.form_article',['ButtonText'=>'Add Article'])
@include('admin.partials.flash')
{!! Form::close() !!}
<!-- validation and error handaling goes here -->
@include('errors.error_list')

@stop