@extends('admin.adminmaster')
@section('maincontent')
<div class="box-body"><h1> Edit A Category</h1></div>
{!! Form::model($category,['method'=>'PATCH', 'files'=>true, 'action'=>['Admin\ArticleCategoryController@update',$category->category_id]]) !!}
@include('admin.forms.form_article_category',['ButtonText'=>'Update Category'])
{!! Form::close() !!}

@stop