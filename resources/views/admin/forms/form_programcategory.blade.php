@extends('admin.adminmaster')
@section('maincontent')
<!-- Main content -->
<section class="content">
    @if(isset($programcategory))
    {!! Form::open(['url'=>'admin/programcategory/update','files'=>'true','method'=>'post']) !!}
    {!! Form::hidden('id',$programcategory->id) !!}
    @else
    {!! Form::open(['url'=>'admin/programcategory','files'=>'true','method'=>'post']) !!}
    @endif
    <div class="row">
        <div class="col-md-12"> 
            <!-- /.box-body -->
            <div class="box-footer text-right">
                <button type="submit" class="btn btn-success">Save</button>
                <button type="reset" class="btn btn-primary">Reset</button>
                <a href="{{ url('admin/programcategory') }}" class="btn btn-danger">Cancel</a> </div>
            <!-- /.box --> 
        </div>
    </div>
    <?php
//    echo '<pre>';
//    print_r($affiliation);
//    echo '</pre>';
    ?>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"> Add new Category </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <?php $category = isset($programcategory->category) ? $programcategory->category : ''; ?>
                        {!! Form::label('category','Category * ') !!}
                        {!! Form::text('category', $category,['class'=>'form-control','id'=>'affiliation','placeholder'=>'eg. Arts','required'=>'required']) !!}
                    </div>
                   <div class="form-group">
                        <?php $url = isset($programcategory->url) ? $programcategory->url : ''; ?>
                        {!! Form::label('url','Url *') !!}
                        {!! Form::text('url', $url,['class'=>'form-control','id'=>'url','placeholder'=>'eg. science','required'=>'required']) !!}
                    </div>
                    <div class="form-group">
                        <?php $logo = isset($programcategory->logo) ? $programcategory->logo : ''; ?>
                        {!! Form::label('logo','Logo ') !!}
                        {!! Form::text('logo', $logo,['class'=>'form-control','id'=>'logo','placeholder'=>'eg. testtube.png']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"> Publish option </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label> 
                            <?php $published = isset($programcategory->published) && $programcategory->published == 1 ? 'checked="checked"' : ''; ?>
                            <input type='checkbox' name='published' value='1' class='flat-red' <?php echo $published; ?> /> Published
                        </label><br>
                        <label> 
                    </div>
                </div>
            </div>
        </div>
    </div>
{!! Form::close() !!}
</section>
@stop


