
<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Admin login screen</title>
        <link rel="stylesheet" href="{{url('public/admin/auth/css/style.css')}}">
  </head>

  <body>

    <div class="wrapper">
	<div class="container">
		<h1>Welcome</h1>
		
		<form class="form" method = 'POST' action="{{url('admin/login')}}">
			<input type="text" name="username" placeholder="Username">
			<input type="password" name = "password" placeholder="Password">
                        <input type="submit"  value="Login">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        
		</form>
	</div>
	
	<ul class="bg-bubbles">
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
	</ul>
</div>
    <script src='{{url('public/admin/auth/js/jQuery-2.1.4.min.js')}}'></script>
    <script src='{{url('public/admin/auth/js/index.js')}}'></script>
</body>
</html>
