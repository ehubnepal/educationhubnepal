@extends('admin.adminmaster')
@section('maincontent')
<?php
//echo '<pre>';
//print_r($brands);
//echo '</pre>';
?>
<section class="content actionbuttonbox">

    <div class="row">
        <div class="col-xs-12">

            <div class="box text-right actionbutton clearfix padding">

                <div class="col-sm-4">
                    {!! Form::open(['url'=>'admin/college/search','files'=>'true','method'=>'get']) !!}
                        <input type="search" placeholder="search" name="search" value="<?php isset($query)?print $query:'';?>" class="form-control">
                    {!!Form::close()!!}
                </div>
                <div class="col-sm-8">
                    <a class="btn btn-success" href="{{ url('admin/college/create')}}"> <i class="fa fa-plus"></i> Add new </a> 
                    <a class="btn text-success" id="publishMenu"><i class="fa fa-circle"></i> Publish</a> 
                    <a class="btn text-danger" id="unpublishMenu"><i class="fa fa-circle-o"></i> Unpublish</a>  
                </div>
            </div>
        </div>
    </div>
   
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">All Colleges</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th><input class="checkall" type="checkbox" onclick="checkAll(this)"></th>
                                <th>ID</th>
                                <th>College</th>
                                <th>Logo</th>
                                <th>Address</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            @foreach($colleges as $college)
                            <tr>
                                <td><input class="checksingle" type="checkbox" data-id="{{$college->id}}"></td>
                                <td> <a href="{{ url('admin/college/'.$college->id)}}">{{$college->id}}</a></td>
                                <td> <a href="{{ url('admin/college/'.$college->id)}}">{{$college->cname}} </a></td>
                                <td>
                                        <?php 
                                           // if (getimagesize(url($college->logo))) {
                                                echo '<img src="' . url($college->full_url) . '" style="max-height:40px;max-width:100px"/>';
                                           // }
                                        ?>
                                    </td>
                                <td>{{$college->address}}</td>
                                <?php
                                if ($college->published == 0):
                                    $publishLink = '<a href="' . url('admin/college/publish/' . $college->id) . '" class="text-danger" ><i class="fa fa-circle"></i> Unublished</a>';
                                else:
                                    $publishLink = '<a href="' . url('admin/college/unpublish/' . $college->id) . '" class="text-success" ><i class="fa fa-circle"></i> Published</a>';
                                endif;
                                ?>
                                <td>{!! $publishLink !!}</td>
                                <td><a href="{{url('admin/college/'.$college->id.'/edit')}}" class="text-info"><i class="fa fa-pencil"></i> Edit</a> |
                                    <a href="{{url('admin/college/'.$college->id.'/delete')}}" class="text-danger"><i class="fa fa-trash"></i> Delete</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                     {!! $colleges->links() !!}
                </div>
                <!-- /.box-body --> 
            </div>
            <!-- /.box --> 
        </div>
    </div>
</section>
<!-- /.content --> 
@stop
