@extends('admin.adminmaster')
@section('maincontent')
<?php
//echo '<pre>';
//print_r($brands);
//echo '</pre>';
?>
<section class="content actionbuttonbox">
    <div class="row">
        <div class="col-xs-12">
            <div class="box text-right actionbutton clearfix padding">
                <div class="col-sm-4">
                    <input type="search" placeholder="search" class="form-control search" autofocus="true">
                </div>
                <div class="col-sm-8">
                    <a class="btn btn-success" href="{{ url('admin/affiliation/create')}}"> <i class="fa fa-plus"></i> Add new </a> 
                    <a class="btn text-success" id="publishMenu"><i class="fa fa-circle"></i> Publish</a> 
                    <a class="btn text-danger" id="unpublishMenu"><i class="fa fa-circle-o"></i> Unpublish</a>  
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">All Universities</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                        <th><input class="checkall" type="checkbox" onclick="checkAll(this)"></th>
                        <th>ID</th>
                        <th>University/Affiliation</th>
                        <th>Logo</th>
                        <th>Status</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach($affiliations as $affiliation)
                            <tr>
                                <td><input class="checksingle" type="checkbox" data-id="{{$affiliation->id}}"></td>
                                <td> <a href="{{ url('admin/affiliation/'.$affiliation->id)}}">{{$affiliation->id}}</a></td>
                                <td> <a href="{{ url('admin/affiliation/'.$affiliation->id)}}">
                                        <?php
                                        // if (@getimagesize(url($affiliation->thumb_url))) {
                                        if (!empty($affiliation->thumb_url)):
                                            echo '<img src="' . url($affiliation->thumb_url) . '" style="max-height:40px;max-width:100px"/>';
                                        endif;
//  }
                                        ?>
                                        {{$affiliation->aname}} 
                                    </a></td>
                                <?php
                                if ($affiliation->published == 0):
                                    $publishLink = '<a href="' . url('admin/affiliation/publish/' . $affiliation->id) . '" class="text-danger" ><i class="fa fa-circle"></i> Unublished</a>';
                                else:
                                    $publishLink = '<a href="' . url('admin/affiliation/unpublish/' . $affiliation->id) . '" class="text-success" ><i class="fa fa-circle"></i> Published</a>';
                                endif;
                                ?>
                                <td>{!! $publishLink !!}</td>
                                <td><a href="{{url('admin/affiliation/'.$affiliation->id.'/edit')}}" class="text-info"><i class="fa fa-pencil"></i> Edit</a> |
                                    <a href="{{url('admin/affiliation/'.$affiliation->id.'/delete')}}" class="text-danger"><i class="fa fa-trash"></i> Delete</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {!! $affiliations->links() !!}
                </div>
                <!-- /.box-body --> 
            </div>
            <!-- /.box --> 
        </div>
    </div>
</section>
<!-- /.content --> 
@stop

@section('scripts')
<script type='text/javascript'>
    $('.search').keyup(function () {
        var val = $(this).val();
        $('tbody>tr').hide();
        $('tbody>tr').each(function () {
            if ($(this).text().toLowerCase().indexOf(val.toLowerCase()) > -1) {
                $(this).show();
            }
        });
    });
</script>
@stop
