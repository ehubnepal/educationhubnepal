@extends('admin.adminmaster')
@section('maincontent')

<section class="content actionbuttonbox">
    <div class="row">
        <div class="col-xs-12">
            <div class="box text-right actionbutton clearfix padding">
                <div class="col-sm-4">
                     {!! Form::open(['url'=>'admin/faculty/search','files'=>'true','method'=>'get']) !!}
                        <input type="search" placeholder="search" name="search" value="<?php isset($query)?print $query:'';?>" class="form-control" autofocus="true">
                    {!!Form::close()!!}
                </div>
                <div class="col-sm-8">
                    <a class="btn btn-success" href="{{ url('admin/faculty/create')}}"> <i class="fa fa-plus"></i> Add new </a> 
                    <a class="btn text-success" id="publishMenu"><i class="fa fa-circle"></i> Publish</a> 
                    <a class="btn text-danger" id="unpublishMenu"><i class="fa fa-circle-o"></i> Unpublish</a>  
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Faculties in all University</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                        <th><input class="checkall" type="checkbox" onclick="checkAll(this)"></th>
                        <th>ID</th>
                        <th>Faculty</th>
                        <th>University/Affiliation</th>
                        <th>Status</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach($faculties as $faculty)
                            <tr>
                                <td><input class="checksingle" type="checkbox" data-id="{{$faculty->id}}"></td>
                                <td> <a href="{{ url('admin/faculty/'.$faculty->id)}}">{{$faculty->id}}</a></td>

                                <td> <a href="{{ url('admin/faculty/'.$faculty->id)}}">
                                        {{$faculty->faculty}} 
                                    </a>
                                </td>
                                <td> 
	                                @if(!empty($faculty->affiliation))
	                                <a href="{{ url('admin/affiliation/'.$faculty->affiliation->id)}}">{{$faculty->affiliation->aname}}</a>
	                                @else
	                                n/a
	                                @endif
                                </td>
                                <?php
                                if ($faculty->published == 0):
                                    $publishLink = '<a href="' . url('admin/faculty/publish/' . $faculty->id) . '" class="text-danger" ><i class="fa fa-circle"></i> Unublished</a>';
                                else:
                                    $publishLink = '<a href="' . url('admin/faculty/unpublish/' . $faculty->id) . '" class="text-success" ><i class="fa fa-circle"></i> Published</a>';
                                endif;
                                ?>
                                <td>{!! $publishLink !!}</td>
                                <td>
                                    <a href="{{url('admin/faculty/'.$faculty->id.'/edit')}}" class="text-info"><i class="fa fa-pencil"></i> Edit</a> |
                                    <a href="{{url('admin/faculty/'.$faculty->id.'/delete')}}" class="text-danger"><i class="fa fa-trash"></i> Delete</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {!! $faculties->links() !!}
                </div>
                <!-- /.box-body --> 
            </div>
            <!-- /.box --> 
        </div>
    </div>
</section>
<!-- /.content --> 
@stop
