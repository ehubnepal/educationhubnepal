@extends('admin.adminmaster')
@section('maincontent')

<section class="content actionbuttonbox">

    <div class="row">
        <div class="col-xs-12">
            <div class="box text-right actionbutton clearfix padding">
                <div class="col-sm-4">
                 {!! Form::open(['url'=>'admin/program/search','files'=>'true','method'=>'get']) !!}
                        <input type="search" placeholder="search" name="search" value="<?php isset($query)?print $query:'';?>" class="form-control">
                    {!!Form::close()!!}
                </div>
                <div class="col-sm-8">
                    <a class="btn btn-success" href="{{ url('admin/blog/categories/create')}}"> <i class="fa fa-plus"></i> Add new </a> 
                    <a class="btn text-success" id="publishMenu"><i class="fa fa-circle"></i> Publish</a> 
                    <a class="btn text-danger" id="unpublishMenu"><i class="fa fa-circle-o"></i> Unpublish</a>  
                </div>
            </div>
        </div>
    </div>
   
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">All Blog Categories</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th><input class="checkall" type="checkbox" onclick="checkAll(this)"></th>
                                <th>ID</th>
                                <th>Category Name</th>
                                <th>Status</th>
                                <th>Action</th>
                                <th></th>
                            </tr>
                            @foreach($categories as $category)
                            <tr>
                                <td><input class="checksingle" type="checkbox" data-id="{{$category->category_id}}"></td>
                                <td> <a href="{{ url('admin/blog/posts/edit/'.$category->category_id)}}">{{$category->category_id}}</a></td>
                                <td> <a href="{{ url('admin/blog/posts/edit/'.$category->category_id)}}">{{$category->name}} </a></td>
                                <td>@if(!empty($post->categories))@foreach($post->categories as $category)<span>{{$category->name}} </span>@endforeach @endif</td>
                                <?php
                                if ($category->published == 0):
                                    $publishLink = '<a href="' . url('admin/blog/categories/publish/' . $category->category_id) . '" class="text-danger" ><i class="fa fa-circle"></i> Unublished</a>';
                                else:
                                    $publishLink = '<a href="' . url('admin/blog/categories/unpublish/' . $category->category_id) . '" class="text-success" ><i class="fa fa-circle"></i> Published</a>';
                                endif;
                                ?>
                                <td>{!! $publishLink !!}</td>
                                <td><a href="{{url('admin/blog/categories/edit/'.$category->category_id)}}" class="text-info"><i class="fa fa-pencil"></i> Edit</a> |
                                    <a href="{{url('admin/blog/categories/delete/'.$category->category_id)}}" class="text-danger"><i class="fa fa-trash"></i> Delete</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                     
                </div>
                <!-- /.box-body --> 
            </div>
            <!-- /.box --> 
        </div>
    </div>
</section>
<!-- /.content --> 
@stop

