@extends('admin.adminmaster')
@section('maincontent')
<?php
//echo '<pre>';
//print_r($brands);
//echo '</pre>';
?>
<section class="content actionbuttonbox">

    <div class="row">
        <div class="col-xs-12">
            <div class="box text-right actionbutton clearfix padding">
                <div class="col-sm-4">
                 {!! Form::open(['url'=>'admin/program/search','files'=>'true','method'=>'get']) !!}
                        <input type="search" placeholder="search" name="search" value="<?php isset($query)?print $query:'';?>" class="form-control">
                    {!!Form::close()!!}
                </div>
                <div class="col-sm-8">
                    <a class="btn btn-success" href="{{ url('admin/blog/posts/create')}}"> <i class="fa fa-plus"></i> Add new </a> 
                    <a class="btn text-success" id="publishMenu"><i class="fa fa-circle"></i> Publish</a> 
                    <a class="btn text-danger" id="unpublishMenu"><i class="fa fa-circle-o"></i> Unpublish</a>  
                </div>
            </div>
        </div>
    </div>
   
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">All Blog Posts</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th><input class="checkall" type="checkbox" onclick="checkAll(this)"></th>
                                <th>ID</th>
                                <th>Post</th>
                                <th>Categories</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            @foreach($posts as $post)
                            <tr>
                                <td><input class="checksingle" type="checkbox" data-id="{{$post->post_id}}"></td>
                                <td> <a href="{{ url('admin/blog/posts/edit/'.$post->post_id)}}">{{$post->id}}</a></td>
                                <td> <a href="{{ url('admin/blog/posts/edit/'.$post->post_id)}}">{{$post->title}} </a></td>
                                <td>@if(!empty($post->categories))@foreach($post->categories as $category)<span>{{$category->name}} </span>@endforeach @endif</td>
                                <?php
                                if ($post->active == 0):
                                    $publishLink = '<a href="' . url('admin/blog/posts/publish/' . $post->post_id) . '" class="text-danger" ><i class="fa fa-circle"></i> Unublished</a>';
                                else:
                                    $publishLink = '<a href="' . url('admin/blog/posts/unpublish/' . $post->post_id) . '" class="text-success" ><i class="fa fa-circle"></i> Published</a>';
                                endif;
                                ?>
                                <td>{!! $publishLink !!}</td>
                                <td><a href="{{url('admin/blog/posts/edit/'.$post->post_id)}}" class="text-info"><i class="fa fa-pencil"></i> Edit</a> |
                                    <a href="{{url('admin/blog/posts/delete/'.$post->post_id)}}" class="text-danger"><i class="fa fa-trash"></i> Delete</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                     {!! $posts->render() !!}
                </div>
                <!-- /.box-body --> 
            </div>
            <!-- /.box --> 
        </div>
    </div>
</section>
<!-- /.content --> 
@stop
