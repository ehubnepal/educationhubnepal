@extends('admin.adminmaster')
@section('maincontent')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">All Search Queries</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                        <th>Query</th>
                        <th>Type</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach($queries as $query)
                            <tr>
                                <td>{{$query->query}}({{$query->count}})</td>
                                <td>{{$query->type}}</td>
                                <td><a href='{{url('admin/searchqueries/delete?query='.$query->query)}}' class='text-danger'><i class='fa fa-trash'></i> Delete</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {!! $queries->links() !!} 
                </div>
                <!-- /.box-body --> 
            </div>
            <!-- /.box --> 
        </div>
    </div>
</section>
<!-- /.content --> 
@stop
