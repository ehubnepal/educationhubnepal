@extends('admin.adminmaster')
@section('maincontent')
<!--<section class="content actionbuttonbox">
    <div class="row">
        <div class="col-xs-12">
            <div class="box text-right actionbutton clearfix padding">
                <div class="col-sm-4">
                    <input type="search" placeholder="search" class="form-control search" autofocus="true">
                </div>
                <div class="col-sm-8">
                    <a class="btn text-success" id="publishMenu"><i class="fa fa-circle"></i> Publish</a> 
                    <a class="btn text-danger" id="unpublishMenu"><i class="fa fa-circle-o"></i> Unpublish</a>  
                </div>
            </div>
        </div>
    </div>
</section>-->
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">All reviews</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                        <th>Date</th>
                        <th>College</th>
                        <th>User</th>
                        <th>Review</th>
                        <th>Status</th>
                        <th><i class="fa fa-gears"></i></th>
                        </thead>
                        <tbody>
                            @foreach($collegeReviews as $review)
                            <tr>
                                <td>{{$review->created_at}}</td>
                                <td>
                                    <a href="{{ url('admin/college/'.$review->college_id)}}">{{$review->cname}}</a>
                                </td>
                                <td><a href="{{url('admin/user/'.$review->user_id)}}">{{ $review->fullname }}</a></td>
                                <td> {{ $review->review }}</td>
                                <td>
                                    @if($review->published==1)
                                    <a class="text text-success" href="collegereviews/unpublish/{{$review->id}}"><i class="fa fa-circle"></i> Published</a>
                                    @else
                                    <a class="text text-danger" href="collegereviews/publish/{{$review->id}}"><i class="fa fa-circle-o"></i> Published</a>
                                    @endif
                                </td>
                                <td></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {!! $collegeReviews->links() !!} 
                </div>
                <!-- /.box-body --> 
            </div>
            <!-- /.box --> 
        </div>
    </div>
</section>
<!-- /.content --> 
@stop
