@extends('admin.adminmaster')
@section('maincontent')
<?php
//echo '<pre>';
//print_r($brands);
//echo '</pre>';
?>
<section class="content actionbuttonbox">
    <div class="row">
        <div class="col-xs-12">
            <div class="box text-right actionbutton clearfix padding">
                <div class="col-sm-4">
                    <input type="search" placeholder="search" class="form-control search" autofocus="true">
                </div>
                <div class="col-sm-8">
                    <a class="btn text-success" id="publishMenu"><i class="fa fa-circle"></i> Publish</a> 
                    <a class="btn text-danger" id="unpublishMenu"><i class="fa fa-circle-o"></i> Unpublish</a>  
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">All users(Members)</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                        <th><input class="checkall" type="checkbox" onclick="checkAll(this)"></th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Social</th>
                        <th>Status</th>
                        <th>Action</th>
                        <th><i class="fa fa-gears"></i></th>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>
                                    <input class="checksingle" type="checkbox" data-id="{{$user->id}}">
                                    <a href="{{ url('admin/user/'.$user->id)}}">{{$user->id}}</a>
                                </td>
                                <td>
                                    @if(!empty($user->basicinfo->social_image))
                                    <img src="{{$user->basicinfo->social_image}}" style="max-width:50px" alt="{{$user->basicinfo->fullname}}"/>
                                    @endif
                                    <a href="{{ url('admin/user/'.$user->id)}}">{{$user->basicinfo->fullname}}</a>
                                </td>
                                <td> {{ $user->email }}</td>
                                <td><?php switch ($user->role):
    case 'admin':
        ?>
                                            <span class='text-danger'><i class='fa fa-key'></i> Admin</span>
                                            <?php break;
                                        case 'maintainer':
                                            ?>
                                            <span class='text-warning'><i class='fa fa-wrench'></i> Maintainer</span>
                                            <?php break;
                                        case 'member':
                                            ?>
                                            <span class='text-info'><i class='fa fa-user'></i> Member</span>
                                            <?php
                                            break;
                                        default:
                                            break;
                                    endswitch;
                                    ?></td>
                                <td><?php switch ($user->oauth):
                                        case 'google':
                                            ?>
                                            <span class='text-danger'><i class='fa fa-google-plus-square'></i> google</span>
                                            <?php break;
                                        case 'facebook':
                                            ?>
                                            <span class='text-info'><i class='fa fa-facebook-official'></i> facebook</span>
                                            <?php
                                            break;
                                        default:
                                            break;
                                    endswitch;
                                    ?>
                                </td>
                                <td>
                                    <?php switch ($user->status):
                                        case 'confirmed':
                                            ?>
                                            <span class='text-success'><i class='fa fa-check'></i> Verified</span>
        <?php break;
    case 'pending':
        ?>
                                            <span class='text-warning'><i class='fa fa-clock-o'></i> Pending</span>
        <?php break;
    case 'blocked':
        ?>
                                            <span class='text-danger'><i class='fa fa-ban'></i> Blocked</span>
        <?php break;
endswitch;
?>
                                </td>
                                @if($user->role=='admin')
                                <td>N/A</td>
                                @else
                                <td>
                                    <a href="{{url('admin/user/'.$user->id.'/edit')}}" class="text-info"><i class="fa fa-pencil"></i> Edit</a> |
                                    <a href="{{url('admin/user/'.$user->id.'/alertdelete')}}" class="text-danger"><i class="fa fa-trash"></i> Delete</a>
                                </td>
                                <td>
                                    @if($user->role=="maintainer")
                                    <a href="{{url('admin/user/'.$user->id.'/unsetmaintainer')}}" class="text-warning"><i class="fa fa-user"></i> Unset maintainer</a>
                                    @elseif($user->role=="member")
                                    <a href="{{url('admin/user/'.$user->id.'/setmaintainer')}}" class="text-success"><i class="fa fa-user"></i> Set maintainer</a>
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {!! $users->links() !!} 
                </div>
                <!-- /.box-body --> 
            </div>
            <!-- /.box --> 
        </div>
    </div>
</section>
<!-- /.content --> 
@stop

@section('scripts')
<script type='text/javascript'>
    $('.search').keyup(function () {
        var val = $(this).val();
        $('tbody>tr').hide();
        $('tbody>tr').each(function () {
            if ($(this).text().toLowerCase().indexOf(val.toLowerCase()) > -1) {
                $(this).show();
            }
        });
    });
</script>
@stop
