@extends('admin.adminmaster')
@section('maincontent')
<?php
//echo '<pre>';
//print_r($brands);
//echo '</pre>';
?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">All Notifications</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th><input class="checkall" type="checkbox" onclick="checkAll(this)"></th>
                                <th>Notification</th>
                                <th>User</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                            @foreach($notifications as $notification)
                            <tr>
                                <td><input class="checksingle" type="checkbox" data-id="{{$notification->id}}"></td>
                                <td> 
                                    @if($notification->visited==0)
                                    <i class="fa fa-star text-warning"></i> 
                                    @endif
                                    @if($notification->type=="college")
                                    <a target="_blank" href="{{url('superuser/college/'.$notification->reference_id.'/edit?_user=admin&_id='.$notification->id)}}">{{$notification->message}}</a>
                                    @elseif($notification->type=="program")
                                    <a target="_blank" href="{{url('superuser/program/'.$notification->reference_id.'/edit?_user=admin&_id='.$notification->id)}}">{{$notification->message}}</a>
                                    @else
                                    <a target="_blank" href="{{url('superuser/collegeprogram/'.$notification->reference_id.'/'.$notification->reference_id2.'/edit?_user=admin&_id='.$notification->id)}}">{{$notification->message}}</a>
                                    @endif
                                </td>
                                <td> <a href="{{ url('admin/user/'.$notification->user_id)}}">
                                        @if(!empty($notification->social_image))
                                        <img src="{{$notification->social_image}}" style="max-width: 80px" /> 
                                        @endif
                                         {{$notification->fullname}}
                                    </a></td>
                                <td> {{$notification->updated_at}} </td>
                                <td>
                                    <a href='{{url('admin/editnotification/'.$notification->id.'/delete')}}' class="text-danger"><i class="fa fa-trash"></i> Delete</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                     {!! $notifications->links() !!}
                </div>
                <!-- /.box-body --> 
            </div>
            <!-- /.box --> 
        </div>
    </div>
</section>
<!-- /.content --> 
@stop
