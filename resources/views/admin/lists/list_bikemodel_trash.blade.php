@extends('admin.adminmaster')
@section('maincontent')
<section class="content actionbuttonbox">
    <div class="row">
        <div class="col-xs-12">
            <div class="box text-right actionbutton clearfix padding">

                <div class="col-sm-4"><form>
                        <input type="search" placeholder="search" class="form-control">
                    </form></div>
                <div class="col-sm-8">
                    <a class="btn btn-danger" href="#"> <i class="fa fa-trash-o"></i> Delete all </a> 
                    <a class="btn btn-info" href="{{ url('admin/bikemodel')}}"> <i class="fa fa-arrow-left"></i> Back </a> 
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Trashed Bike Model</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th><input class="checkall" type="checkbox" onclick="checkAll(this)"></th>
                                <th>ID</th>
                                <th>Model</th>
                                <th>Image</th>
                                <th>Bike count</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            @foreach($models as $model)
                            <tr>
                                <td><input class="checksingle" type="checkbox" data-id="{{$model->id}}"></td>
                                <td>{{$model->id}}</td>
                                <td> <a href="{{ url('admin/bikemodel/'.$model->id)}}">{{$model->model}} ({{$model->brand}}) </a> </td>
                                <td>
                                    <?php
                                    if (!empty($model->media)) {
                                        if (@getimagesize(url($model->media[0]->thumb_url))) {
                                            echo '<img src="' . url($model->media[0]->thumb_url) . '" style="max-height:40px;max-width:100px"/>';
                                        }
                                    }
                                    ?>
                                </td>
                                <td>{{$model->adCount}}</td>
                                <?php
                                if ($model->published == 0):
                                    $publishLink = '<a href="' . url('admin/bikemodel/publish/' . $model->id) . '" class="text-danger" ><i class="fa fa-circle"></i> Unublished</a>';
                                else:
                                    $publishLink = '<a href="' . url('admin/bikemodel/unpublish/' . $model->id) . '" class="text-success" ><i class="fa fa-circle"></i> Published</a>';
                                endif;
                                ?>
                                <td>{!! $publishLink !!}</td>
                                <td>
                                    <a href="{{url('admin/bikemodel/'.$model->id.'/untrash')}}" class="text-success"><i class="fa fa-recycle"></i> Recover</a> | 
                                    <a href="{{url('admin/bikemodel/'.$model->id.'/delete')}}" class="text-danger"><i class="fa fa-trash"></i> Delete</a>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body --> 
            </div>
            <!-- /.box --> 
        </div>
    </div>
</section>
@stop
