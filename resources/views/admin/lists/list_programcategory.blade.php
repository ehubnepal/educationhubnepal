@extends('admin.adminmaster')
@section('maincontent')

<section class="content actionbuttonbox">
    <div class="row">
        <div class="col-xs-12">
            <div class="box text-right actionbutton clearfix padding">
                <div class="col-sm-4">
                    <input type="search" placeholder="search" class="form-control search" autofocus="true">
                </div>
                <div class="col-sm-8">
                    <a class="btn btn-success" href="{{ url('admin/programcategory/create')}}"> <i class="fa fa-plus"></i> Add new </a> 
                    <a class="btn text-success" id="publishMenu"><i class="fa fa-circle"></i> Publish</a> 
                    <a class="btn text-danger" id="unpublishMenu"><i class="fa fa-circle-o"></i> Unpublish</a>  
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">All Program Categories</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                        <th><input class="checkall" type="checkbox" onclick="checkAll(this)"></th>
                        <th>ID</th>
                        <th>Category</th>
                        <th>Status</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach($programcategory->all as $category)
                            <tr>
                                <td><input class="checksingle" type="checkbox" data-id="{{$category->id}}"></td>
                                <td> <a href="{{ url('admin/programcategory/'.$category->id)}}">{{$category->id}}</a></td>
                                <td> <a href="{{ url('admin/programcategory/'.$category->id)}}">
                                        {{$category->category}} 
                                    </a>
                                    </td>
                                <?php
                                if ($category->published == 0):
                                    $publishLink = '<a href="' . url('admin/programcategory/publish/' . $category->id) . '" class="text-danger" ><i class="fa fa-circle"></i> Unublished</a>';
                                else:
                                    $publishLink = '<a href="' . url('admin/programcategory/unpublish/' . $category->id) . '" class="text-success" ><i class="fa fa-circle"></i> Published</a>';
                                endif;
                                ?>
                                <td>{!! $publishLink !!}</td>
                                <td>
                                    <a href="{{url('admin/programcategory/'.$category->id.'/edit')}}" class="text-info"><i class="fa fa-pencil"></i> Edit</a> |
                                    <a href="{{url('admin/programcategory/'.$category->id.'/delete')}}" class="text-danger"><i class="fa fa-trash"></i> Delete</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {!! $programcategory->links() !!}
                </div>
                <!-- /.box-body --> 
            </div>
            <!-- /.box --> 
        </div>
    </div>
</section>
<!-- /.content --> 
@stop

@section('scripts')
<script type='text/javascript'>
    $('.search').keyup(function () {
        var val = $(this).val();
        $('tbody>tr').hide();
        $('tbody>tr').each(function () {
            if ($(this).text().toLowerCase().indexOf(val.toLowerCase()) > -1) {
                $(this).show();
            }
        });
    });
</script>
@stop