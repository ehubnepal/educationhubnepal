@extends('admin.adminmaster')
@section('maincontent')

@include('admin.forms.form_collegeprograms',array('college_id'=>$college_id,'programs'=>$programs,'collegeprograms'=>$collegeprograms))
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">All Programs</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th><input class="checkall" type="checkbox" onclick="checkAll(this)"></th>
                                <th>ID</th>
                                <th>Programs</th>
                                <th>Affiliated by</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            @foreach($collegeprograms as $collegeprogram)
                            <tr>
                                <td><input class="checksingle" type="checkbox" data-id="{{$collegeprogram->id}}"></td>
                                <td> <a href="{{ url('admin/program/'.$collegeprogram->program_id)}}">{{$collegeprogram->id}}</a></td>
                                <td> <a href="{{ url('admin/program/'.$collegeprogram->program_id)}}">{{$collegeprogram->pname}} </a></td>
                                <td> <a href="#">{{$collegeprogram->affiliation->aname}} </a></td>
                                <?php
                                if ($collegeprogram->published == 0):
                                    $publishLink = '<a href="' . url('admin/collegeprograms/publish/' . $collegeprogram->id) . '" class="text-danger" ><i class="fa fa-circle"></i> Unpublished</a>';
                                else:
                                    $publishLink = '<a href="' . url('admin/collegeprograms/unpublish/' . $collegeprogram->id) . '" class="text-success" ><i class="fa fa-circle"></i> Published</a>';
                                endif;
                                ?>
                                <td>{!! $publishLink !!}</td>
                                <td><a href="{{url('admin/collegeprograms/'.$college_id.'/'.$collegeprogram->program_id.'/edit')}}" class="text-info"><i class="fa fa-pencil"></i> Edit</a> |
                                    <a href="{{url('admin/collegeprograms/'.$collegeprogram->id.'/delete')}}" class="text-danger"><i class="fa fa-trash"></i> Delete</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                </div>
                <!-- /.box-body --> 
            </div>
            <!-- /.box --> 
        </div>
    </div>
</section>
<!-- /.content --> 
@stop
