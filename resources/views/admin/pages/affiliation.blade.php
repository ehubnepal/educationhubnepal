@extends('admin.adminmaster')
@section('maincontent')
<?php
//echo '<pre>';
//print_r($affiliation);
//echo '</pre>';
?>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title text-danger">
                        {{$affiliation->aname}}  <small><a href='{{url('admin/affiliation')}}' class='text-info'>[go back]</a></small>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                                        <strong>Established :</strong> {{$affiliation->established}} <br/>
                                        <strong>Education Body Type :</strong> {{$affiliation->education_body_type}} <br/>
                                        <strong>Language of Education :</strong> {{$affiliation->language_of_education}} <br/>
                                        <strong>Vice Chancellor :</strong> {{$affiliation->vice_chancellor}} <br/>
                   
                    <hr/>
                    {!! $affiliation->description !!}
                    <hr/>

                    <h4 class="text-danger">Programs offered</h4>
                    <!-- /.box-header -->
                    <div class="table-responsive no-padding">
                        <table class="table table-hover">
                                <tr>
                                    <th>ID</th>
                                    <th>Program</th>
                                    <th>Status</th>
                                </tr>
                                <tr>
                                    <td colspan='3'>
                                        <input type="search" placeholder="search" class="form-control search" autofocus="true">
                                    </td>
                                </tr>
                                <tbody>
                                @foreach($affiliation->programs as $program)
                                <tr class='rows'>
                                    <td> <a href="{{ url('admin/program/'.$program->id)}}">{{$program->id}}</a></td>
                                    <td> <a href="{{ url('admin/program/'.$program->id)}}">{{$program->pname}} </a></td>
                                    <?php
                                    if ($program->published == 0):
                                        $publishLink = '<a href="' . url('admin/program/publish/' . $program->id) . '" class="text-danger" ><i class="fa fa-circle"></i> Unublished</a>';
                                    else:
                                        $publishLink = '<a href="' . url('admin/program/unpublish/' . $program->id) . '" class="text-success" ><i class="fa fa-circle"></i> Published</a>';
                                    endif;
                                    ?>
                                    <td>{!! $publishLink !!}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                    <!-- /.box-body --> 
                    <!-- endprograms offered -->
                    <!-- endprograms offered -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content --> 
@stop

@section('scripts')
<script type='text/javascript'>
    $('.search').keyup(function () {
        var val = $(this).val();
        $('tbody>tr.rows').hide();
        $('tbody>tr,rows').each(function () {
            if ($(this).text().toLowerCase().indexOf(val.toLowerCase()) > -1) {
                $(this).show();
            }
        });
    });
</script>
@stop
