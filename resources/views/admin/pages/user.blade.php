@extends('admin.adminmaster')
@section('maincontent')

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title text-danger">
                        {{$user->member->fullname}}  <small><a href='{{url('admin/user')}}' class='text-info'>[go back]</a></small>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if(!empty($user->member->social_image))
                        <img src='{{$user->member->social_image}}' style='max-widthL80px' />
                         <hr/>
                    @endif
                                        <strong>Name :</strong> {{$user->member->fullname}} <br/>
                                        <strong>User name :</strong> {{$user->username}} <br/>
                                        <strong>Email :</strong> {{$user->email}} <br/>
                                        <strong>Address :</strong> {{$user->member->address}} <br/>
                                        <strong>Phone :</strong> {{$user->member->phone}} <br/>
                                        <strong>Login in with :</strong> {{$user->oauth}} <br/>
                                        <strong>Social Id :</strong> {{$user->social_id}} <br/> <br/>
                                        <strong>Account Privilege :</strong> {{$user->role}} <br/>
                                        <strong>Account status :</strong> {{$user->status}} <br/><br/>
                                        <i><strong>Skype :</strong> {{$user->member->skype}}</i> <br/>
                                        <i><strong>Facebook :</strong> {{$user->member->facebook}}</i> <br/>
                                        <i><strong>Twitter :</strong> {{$user->member->twitter}}</i> <br/>
                    </div>
                    <!-- /.box-body --> 
                    <!-- endprograms offered -->
                    <!-- endprograms offered -->
                </div>
            </div>
        </div>
</section>
<!-- /.content --> 
@stop

