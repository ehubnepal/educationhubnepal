@extends('admin.adminmaster')
@section('maincontent')
<?php
//echo '<pre>';
//print_r($brands);
//echo '</pre>';
?>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title text-danger">
                        <img src="{{url($college->full_url)}}" style="max-height:60px"/>
                        {{$college->cname}}
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    Address : {{$college->address}} <br/>
                    District : {{$college->district}} <br/>
                    Phone : {{$college->phone}} <br/>
                    Fax : {{$college->fax}} <br/>
                    Website : {{$college->website}} <br/>
                    Email : {{$college->email}} <br/>
                    <hr/>
                    {!! $college->description !!}
                    <hr/>
                    <h4 class="text-danger">Programs offered</h4>
                    <!-- programs offered -->
                    <div class="row">
                        @foreach($college->programs as $program)
                        <div class="col-md-12">
                            <div class="box collapsed-box  box-success" style="margin-bottom: 0px">
                                <div class="box-header with-border">
                                    <h4 class="box-title text-success" style="font-size:15px">{{$program->pname}} <span class="text-info"> | Affiliated to : <a  href="#">{{$program->affiliation->aname}} </a> </span></h4>

                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                    <!-- /.box-tools -->
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table class="table table-hover">
                                        {!! $program->detail !!}
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <div class="clearfix"></div>
                        @endforeach;
                    </div>
                    <!-- endprograms offered -->
                </div>
            </div>
        </div>
    </div>


</section>
<!-- /.content --> 
@stop
