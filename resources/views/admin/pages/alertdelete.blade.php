@extends('admin.adminmaster')
@section('maincontent')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Application buttons -->
            <div class="box text-center box-info">
                <div class="box-header">
                    <h3 class="box-title">By deleting this user, you will also deleted following related information</h3>
                </div>
                <div class="box-body">

                    <a class="btn btn-app" href="javascript:void(0)">
                        {{$collegeReviewCount}}
                        <br/>
                        <i class="fa fa-comment"></i> College Reviews
                    </a>
                    <a class="btn btn-app" href="javascript:void(0)">
                        {{$programReviewCount}}
                        <br/>
                        <i class="fa fa-comment"></i> Program Reviews
                    </a>
                    <a class="btn btn-app" href="javascript:void(0)">
                        {{$maintainerCollegeCount}}
                        <br/>
                        <i class="fa fa-wrench"></i> College
                    </a>
                    <a class="btn btn-app" href="javascript:void(0)">
                        {{$maintainerProgramCount}}
                        <br/>
                        <i class="fa fa-wrench"></i> Program
                    </a>
                    <a class="btn btn-app" href="javascript:void(0)">
                        {{$maintainerCollegeProgramCount}}
                        <br/>
                        <i class="fa fa-wrench"></i> College program
                    </a>
                    <div class="box box-solid">
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </div>
        </div>
        <div class="col-md-12">
            <!-- Application buttons -->
            <div class="box text-center box-info">
                <div class="box-header">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <button class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;"><i class="fa fa-minus"></i></button>
                    </div><!-- /. tools -->

                    <i class="fa fa-gears"></i>
                    <h3 class="box-title">
                        <a href="{{url('admin/user/'.$userId.'/delete')}}" class="btn-lg btn-danger">I understand. Delete</a>
                    </h3>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
