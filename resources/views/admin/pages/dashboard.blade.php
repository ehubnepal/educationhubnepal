@extends('admin.adminmaster')
@section('maincontent')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Application buttons -->
            <div class="box text-center box-info">
                <div class="box-header">
                    <h3 class="box-title">Menu</h3>
                </div>
                <div class="box-body">
                    <!--<p>Add the classes <code>.btn.btn-app</code> to an <code>&lt;a></code> tag to achieve the following:</p>-->

                    <a class="btn btn-app" href="{{ url('admin/affiliation')}}">
                        <i class="fa fa-university text-info"></i> University
                    </a>
                    <a class="btn btn-app" href="{{ url('admin/college')}}">
                        <i class="fa fa-home text-success"></i> College
                    </a>
                    <a class="btn btn-app" href="{{ url('admin/program')}}">
                        <i class="fa fa-graduation-cap text-warning"></i> Programs
                    </a>
                    <a class="btn btn-app" href="{{ url('admin/faculty')}}">
                        <i class="fa fa-pencil text-danger"></i> Faculties
                    </a>
                    <a class="btn btn-app" href="{{ url('admin/category')}}">
                        <i class="fa fa-book text-info"></i> Categories
                    </a>
                    <br/>
                    <a class="btn btn-app" href="{{ url('admin/user')}}">
                        <i class="fa fa-user text-success"></i> Users
                    </a>
                    <a class="btn btn-app" href="{{ url('admin/editnotification')}}">
                        <i class="fa fa-bell text-danger"></i>Edit Notifications
                    </a>
                    <div class="box box-solid">
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </div>
        </div>
        <div class="col-md-12">
            <!-- Application buttons -->
            <div class="box text-center box-info">
                <div class="box-header">
                    <i class="fa fa-user"></i>
                    <h3 class="box-title">
                        New Users In Past 30 Days &nbsp;|&nbsp;&nbsp;<span class='text-danger'><i class="fa fa-google-plus-square"></i> Google</span> &nbsp; &nbsp;<span class='text-info'><i class="fa fa-facebook-official"></i> Facebook</span> &nbsp; &nbsp;<span class='text-success'><i class="fa fa-graduation-cap"></i> EHN</span>
                    </h3>
                    <div class="chart">
                        <canvas id="lineChart" style="height:250px"></canvas>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('scripts')
<script src="{{ url('public/admin/js/plugins/chartjs/Chart.min.js') }}"></script>
<script type='text/javascript'>
var areaChartData = {
    labels: <?php echo $users->index; ?>,
    datasets: [
        {
            label: "Google",
            fillColor: "rgba(169,68,66,1)",
            strokeColor: "rgba(169,68,66,1)",
            pointColor: "rgba(169,68,66,1)",
            pointStrokeColor: "#a94442",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(169,68,66,1)",
            data: <?php echo $users->google; ?>},
        {
            label: "Facebook",
            fillColor: "rgba(60,141,188,0.9)",
            strokeColor: "rgba(60,141,188,0.8)",
            pointColor: "#3b8bba",
            pointStrokeColor: "rgba(60,141,188,1)",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(60,141,188,1)",
            data: <?php echo $users->facebook; ?>},
        {
            label: "EHN",
            fillColor: "rgba(60,118,61,1)",
            strokeColor: "rgba(60,118,61,1)",
            pointColor: "#3c763d",
            pointStrokeColor: "rgba(60,118,61,1)",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(60,118,61,1)",
            data: <?php echo $users->ehn; ?>}
    ]
};

var areaChartOptions = {
    //Boolean - If we should show the scale at all
    showScale: true,
    //Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines: false,
    //String - Colour of the grid lines
    scaleGridLineColor: "rgba(0,0,0,.05)",
    //Number - Width of the grid lines
    scaleGridLineWidth: 1,
    //Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,
    //Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines: true,
    //Boolean - Whether the line is curved between points
    bezierCurve: true,
    //Number - Tension of the bezier curve between points
    bezierCurveTension: 0.3,
    //Boolean - Whether to show a dot for each point
    pointDot: true,
    //Number - Radius of each point dot in pixels
    pointDotRadius: 4,
    //Number - Pixel width of point dot stroke
    pointDotStrokeWidth: 1,
    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
    pointHitDetectionRadius: 20,
    //Boolean - Whether to show a stroke for datasets
    datasetStroke: true,
    //Number - Pixel width of dataset stroke
    datasetStrokeWidth: 2,
    //Boolean - Whether to fill the dataset with a color
    datasetFill: true,
    //String - A legend template
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
    //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio: true,
    //Boolean - whether to make the chart responsive to window resizing
    responsive: true
};

//-------------
//- LINE CHART -
//--------------
var lineChartCanvas = $("#lineChart").get(0).getContext("2d");
var lineChart = new Chart(lineChartCanvas);
var lineChartOptions = areaChartOptions;
lineChartOptions.datasetFill = false;
lineChart.Line(areaChartData, lineChartOptions);
</script>
@stop
