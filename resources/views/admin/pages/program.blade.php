@extends('admin.adminmaster')
@section('maincontent')
<?php
//echo '<pre>';
//print_r($affiliation);
//echo '</pre>';
?>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title text-danger">
                        {{$program->pname}} 
                    </h3>
                </div>
                <div class="box-body">
                    <p><strong>Affiliated to :</strong><a href="{{url('admin/affiliation/'.$program->affiliation->id)}}"> {{$program->affiliation->aname}} </a></p>

                    <!-- program detail -->
                    <div class="row">
                        <?php $program_details = array("Course Description"=>$program->course_description,"Eligibility Criteria"=>$program->eligibility,"Curriculum"=>$program->curriculum); ?>
                        @foreach($program_details as $key=>$program_detail)
                        <div class="col-md-12">
                            <div class="box collapsed-box  box-success" style="margin-bottom: 0px">
                                <div class="box-header with-border">
                                    <h4 class="box-title text-success" style="font-size:15px">{{$key}}</h4>

                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                    <!-- /.box-tools -->
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table class="table table-hover">
                                        {!! $program_detail !!}
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <div class="clearfix"></div>
                        @endforeach
                    </div>
                    <!-- end program detail -->
                    <p class="text-info">Colleges offering this program</p>
                    <!-- /.box-header -->
                    <div class="table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody>
                                <tr>
                                    <th><input class="checkall" type="checkbox" onclick="checkAll(this)"></th>
                                    <th>ID</th>
                                    <th>College</th>
                                    <th>Logo</th>
                                    <th>Address</th>
                                    <th>Status</th>
                                </tr>
                                @foreach($program->colleges as $college)
                                <tr>
                                    <td><input class="checksingle" type="checkbox" data-id="{{$college->id}}"></td>
                                    <td> <a href="{{ url('admin/college/'.$college->id)}}">{{$college->id}}</a></td>
                                    <td> <a href="{{ url('admin/college/'.$college->id)}}">{{$college->cname}} </a></td>
                                    <td>
                                        <?php
                                        // if (getimagesize(url($college->logo))) {
                                        echo '<img src="' . url($college->full_url) . '" style="max-height:40px;max-width:100px"/>';
                                        // }
                                        ?>
                                    </td>
                                    <td>{{$college->address}}</td>
                                    <?php
                                    if ($college->published == 0):
                                        $publishLink = '<a href="' . url('admin/college/publish/' . $college->id) . '" class="text-danger" ><i class="fa fa-circle"></i> Unublished</a>';
                                    else:
                                        $publishLink = '<a href="' . url('admin/college/unpublish/' . $college->id) . '" class="text-success" ><i class="fa fa-circle"></i> Published</a>';
                                    endif;
                                    ?>
                                    <td>{!! $publishLink !!}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                    <!-- /.box-body --> 
                    <!-- endprograms offered -->
                </div>
            </div>
        </div>
    </div>


</section>
<!-- /.content --> 
@stop
