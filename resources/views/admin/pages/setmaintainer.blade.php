@extends('admin.adminmaster')
@section('maincontent')

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title text-danger">
                        {{$user->member->fullname}}  <small><a href='{{url('admin/user')}}' class='text-info'>[go back]</a></small>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class='col-md-3'>
                            <strong>Email :</strong> {{$user->email}} <br/>
                            <strong>Login in with :</strong> {{$user->oauth}} <br/>
                            <strong>Account Privilege :</strong> {{$user->role}} <br/>
                            <strong>Account status :</strong> {{$user->status}} 
                        </div>
                        <div class='col-md-3'>
                            <strong>User name :</strong> {{$user->username}} <br/>
                            <strong>Address :</strong> {{$user->member->address}} <br/>
                            <strong>Phone :</strong> {{$user->member->phone}} <br/>
                            <strong>Social Id :</strong> {{$user->social_id}}
                        </div>
                        <div class='col-md-3'>
                            <i><strong>Facebook :</strong> {{$user->member->facebook}}</i> <br/>
                            <i><strong>Twitter :</strong> {{$user->member->twitter}}</i> <br/>
                            <i><strong>Skype :</strong> {{$user->member->skype}}</i>
                        </div>
                        <div class="col-md-3">
                            @if(!empty($user->member->social_image))
                            <img src='{{$user->member->social_image}}' style='max-widthL80px' />
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title text-danger">
                        Assign college and programs
                    </h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class='col-md-6'>
                            <div class="form-group">
                                <div class='col-sm-10'><input placeholder="Search college" class='form-control' id='college_input' /></div>
                                <div class='col-sm-2'><button id='searchcollege'>Find</button></div>
                            </div>
                            <div class="form-group">
                                <div class='col-sm-10'><input placeholder="Search program" class='form-control' id='program_input' /></div>
                                <div class='col-sm-2'><button id='searchprogram'>Find</button></div>
                            </div>
                            <div class="clearfix"></div>
                            <br/>
                            <p>Search Result</p><hr/>
                            <div id="search_result" class="col-sm-10"></div>
                        </div>
                        <div class="col-md-6">
                            @if(!empty($user->colleges))
                            <strong><p>Assigned Colleges</p></strong>
                            <table class="table table-hover">
                                <thead>
                                <th>Logo</th>
                                <th>College</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                    @foreach($user->colleges as $college)
                                    <tr>
                                        <td>
                                            @if(!empty($college->full_url))
                                            <img src="{{url($college->full_url)}}" style="max-width:80px" />
                                            <br/>
                                            @endif
                                        </td>
                                        <td>
                                            <strong> {{$college->cname}} </strong>
                                            <br/>
                                            <i> {{$college->address}}</i>
                                            <br/>
                                            @foreach($college->programs as $program)
                                            <input type="checkbox" {{$program->checked}} data-program="{{$program->id}}" data-college="{{$college->id}}" class="collegeprogram" /> {{$program->pname}} <br/>
                                            @endforeach
                                        </td>
                                        <td><label class="label label-danger" onclick="unsetcollege({{$college->id}}, this)"><i class="fa fa-trash"> </i> Remove</label></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @endif

                            @if(!empty($user->programs))
                            <hr/>
                            <strong><p>Assigned Programs</p></strong>
                            <hr/>
                            <table class="table table-hover">
                                <thead>
                                <th>Program</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                    @foreach($user->programs as $program)
                                    <tr>
                                        <td>
                                            <strong> {{$program->pname}} </strong>
                                            <br/>
                                            <i> {{$program->aname}}</i>
                                        </td>
                                        <td><label class="label label-danger" onclick="unsetprogram({{$program->id}}, this)"><i class="fa fa-trash"> </i> Remove</label></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- /.box-body --> 
                <!-- endprograms offered -->
                <!-- endprograms offered -->
            </div>
        </div>
    </div>
</section>
<!-- /.content --> 
@stop

@section('scripts')
<link href="{{ url('public/site/libs/autocomplete/jquery.autocomplete.css') }}" rel="stylesheet" type="text/css">
<script type="text/javascript" src="{{ url('public/site/libs/autocomplete/jquery.autocomplete.js') }}"></script>
<script type='text/javascript'>
                                            var __baseurl = "{{url('/')}}";
                                            var userid = "{{$user->id}}";
                                            $(document.body).on('click', '.collegeprogram', function(){
                                            var college_id = $(this).attr('data-college');
                                            var program_id = $(this).attr('data-program');
                                            if ($(this).is(':checked')){
                                            setcollegeprogram(college_id, program_id);
                                            } else{
                                            unsetcollegeprogram(college_id, program_id);
                                            }
                                            });
                                            
                                            $('#program_input').autocomplete({
                                            valueKey: 'title',
                                                    source: [{
                                                    url: "{{url('search/autosuggestprogram')}}?query=%QUERY%",
                                                            type: 'remote',
                                                            getValue: function (item) {
                                                            return item;
                                                            },
                                                            ajax: {
                                                            dataType: 'json'
                                                            },
                                                            hintStyle: {color: '#333'},
                                                            minLength: 3,
                                                            limit: 15
                                                    }]
                                            });
                                            $('#college_input').autocomplete({
                                            valueKey: 'title',
                                                    source: [{
                                                    url: "{{url('search/autosuggestcollege')}}?query=%QUERY%",
                                                            type: 'remote',
                                                            getValue: function (item) {
                                                            return item;
                                                            },
                                                            ajax: {
                                                            dataType: 'json'
                                                            },
                                                            hintStyle: {color: '#333'},
                                                            minLength: 3,
                                                            limit: 15
                                                    }]
                                            });
                                            $('#searchcollege').click(function () {
                                            $.ajax({
                                            url: "{{url('admin/searchcollege')}}",
                                                    data: {cname: $('#college_input').val()},
                                                    success: function (data) {
                                                    var obj = $.parseJSON(data);
                                                    displayCollegeList(obj);
                                                    }
                                            });
                                            });
                                            $('#searchprogram').click(function () {
                                            $.ajax({
                                            url: "{{url('admin/searchprogram')}}",
                                                    data: {pname: $('#program_input').val()},
                                                    success: function (data) {
                                                    var obj = $.parseJSON(data);
                                                    displayProgramList(obj);
                                                    }
                                            });
                                            });
                                            function displayCollegeList(obj) {
                                            var html = "";
                                            for (var i in obj) {
                                            html += "<div class='col-md-12' style='border:1px solid #ccc;padding:5px;margin-bottom:10px'>";
                                            html += '<img src="' + __baseurl + '/' + obj[i].full_url + '" /><br/>';
                                            html += obj[i].cname + '<br/>';
                                            html += obj[i].address + '<br/>';
                                            html += '<button class="selectcollege" onclick="setcollege(' + obj[i].id + ',this)">select</button>';
                                            html += '</div>';
                                            }
                                            $('#search_result').html(html);
                                            }

                                            function displayProgramList(obj) {
                                            var html = "";
                                            for (var i in obj) {
                                            html += "<div class='col-md-12' style='border:1px solid #ccc;padding:5px;margin-bottom:10px'>";
                                            html += obj[i].pname + '<br/>';
                                            html += obj[i].aname + '<br/>';
                                            html += '<button class="selectprogram" onclick="setprogram(' + obj[i].id + ',this)">select</button>';
                                            html += '</div>';
                                            }
                                            $('#search_result').html(html);
                                            }
                                            function setcollege(id, btn) {
                                            $.ajax({
                                            url: "{{url('admin/selectcollege')}}",
                                                    data: {college_id: id, user_id: userid},
                                                    success: function (data) {
                                                    $(btn).addClass('btn btn-success');
                                                    $(btn).parent('div').hide('slow');
                                                    }
                                            });
                                            }

                                            function setprogram(id, btn) {
                                            $.ajax({
                                            url: "{{url('admin/selectprogram')}}",
                                                    data: {program_id: id, user_id: userid},
                                                    success: function (data) {
                                                    $(btn).addClass('btn btn-success');
                                                    $(btn).parent('div').hide('slow');
                                                    }
                                            });
                                            }

                                            function unsetcollege(id, btn) {
                                            $.ajax({
                                            url: "{{url('admin/unselectcollege')}}",
                                                    data: {college_id: id, user_id: userid},
                                                    success: function (data) {
                                                    $(btn).hide();
                                                    }
                                            });
                                            }

                                            function unsetprogram(id, btn) {
                                            $.ajax({
                                            url: "{{url('admin/unselectprogram')}}",
                                                    data: {program_id: id, user_id: userid},
                                                    success: function (data) {
                                                    $(btn).hide();
                                                    }
                                            });
                                            }

                                            //selectCollegeProgram

                                            function setcollegeprogram(college_id, program_id) {
                                            $.ajax({
                                            url: "{{url('admin/selectcollegeprogram')}}",
                                                    data: {college_id: college_id, program_id:program_id, user_id: userid},
                                                    success: function (data) {
                                                        alert('Access granted');
                                                    }
                                            });
                                            }

                                            function unsetcollegeprogram(college_id, program_id) {
                                            $.ajax({
                                            url: "{{url('admin/unselectcollegeprogram')}}",
                                                    data: {college_id: college_id, program_id:program_id, user_id: userid},
                                                    success: function (data) {
                                                        alert('Access Revoked');
                                                    }
                                            });
                                            }
</script>
@stop

