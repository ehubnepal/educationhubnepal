<aside class="main-sidebar"> 
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar"> 
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image"> <img src="{{ url('public/admin/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image"> </div>
            <div class="info">
                <p>Education Hub</p>
                <small> <i class="fa fa-circle text-success"></i> Administrator</small> </div>
            <div class="clearfix"></div>
        </div>
        <!-- search form --> 

        <!-- /.search form --> 
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li> <a href="{{ url('admin/home')}}"> <i class="fa fa-dashboard"></i> <span>Dashboard</span> </a> </li>
            <li> <a href="{{ url('/')}}" target="_blank"> <i class="fa fa-laptop"></i> <span>Visit Website</span> </a> </li>
            <li>
                <a href="#">
                    <i class="fa fa-globe"></i>
                    <span>EHN</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li> <a href="{{ url('admin/affiliation')}}"><i class="fa fa-university"></i> <span>University</span> </a> </li>
                    <li> <a href="{{ url('admin/college')}}"><i class="fa fa-home"></i> <span>College</span> </a> </li>
                    <li> <a href="{{ url('admin/program')}}"><i class="fa fa-graduation-cap"></i> <span>Program</span> </a> </li>
                    <li> <a href="{{ url('admin/programcategory')}}"><i class="fa fa-cubes"></i> <span>Category</span> </a>
                    <li> <a href="{{ url('admin/faculty')}}"><i class="fa fa-cube"></i> <span>Faculties</span> </a> </li>
                </ul>
            </li>

            <li>
                <a href="#">
                    <i class="fa fa-rss"></i>
                    <span>Blog</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li> <a href="{{ url('admin/blog/posts')}}"><i class="fa fa-rss"></i> <span>Blog Posts</span> </a> </li>
                    <li> <a href="{{ url('admin/blog/categories')}}"><i class="fa fa-rss-square"></i> <span>Blog Categories</span> </a> </li>
                    <li> <a href="{{ url('admin/blog/tags')}}"><i class="fa fa-tag"></i> <span>Blog Tags</span> </a> </li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-comment"></i>
                    <span>Reviews</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li> <a href="{{ url('admin/collegereviews')}}"><i class="fa fa-comment-o"></i> <span>College Reviews</span> </a> </li>
                    <li> <a href="{{ url('admin/programreviews')}}"><i class="fa fa-commenting"></i> <span>Program Reviews</span> </a> </li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Users</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li> <a href="{{ url('admin/user')}}"><i class="fa fa-user"></i> <span>Member</span> </a> </li>
                    <li> <a href="{{ url('admin/maintainer')}}"><i class="fa fa-wrench"></i> <span>Maintainer</span> </a> </li>
                </ul>
            </li>
            <li> <a href="{{ url('admin/editnotification')}}"> <i class="fa fa-bell"></i> <span>Notifications</span> </a> </li>
            <li> <a href="{{ url('admin/searchqueries')}}"> <i class="fa fa-search"></i> <span>Search Queries</span> </a> </li>
        </ul>
    </section>
    <!-- /.sidebar --> 
</aside>