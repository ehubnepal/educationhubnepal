<!-- ./wrapper -->
<footer class="main-footer">
    <div class="pull-right"> <strong>Copyright &copy; 2016 <a href="http://www.educationhubnepal.com/">Education Hub Nepal</a>.</strong> All rights reserved. </div>
    <div class="clearfix"></div>
</footer>