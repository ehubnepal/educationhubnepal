
<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <meta name="google-signin-client_id" content="744115937389-626gleln2jtaf7kroqfd90h3hi8trb27.apps.googleusercontent.com">
    <title>Member login screen</title>
        <link rel="stylesheet" href="{{url('public/admin/auth/css/style.css')}}">
  </head>

  <body>

    <div class="wrapper">
	<div class="container">
		<h1>Welcome</h1>
		
		<form class="form" method = 'POST' action="{{url('member/login')}}">
			<input type="text" name="username" placeholder="Username">
			<input type="password" name = "password" placeholder="Password">
                        <input type="submit"  value="Login">
                        <hr/>
                        <div class="g-signin2" data-onsuccess="onSignIn"></div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        
		</form>
	</div>
	
	<ul class="bg-bubbles">
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
	</ul>
</div>
    <script src='{{url('public/admin/auth/js/jQuery-2.1.4.min.js')}}'></script>
    <script src='{{url('public/admin/auth/js/index.js')}}'></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
</body>
</html>
