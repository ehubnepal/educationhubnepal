@extends('site.sitemaster')
@section('maincontent')
<div id="page-content">
    <!-- Breadcrumb -->
    @include('member.fragments.breadcrumb')
    <!-- end Breadcrumb -->

    <div class="container">
        <div class="row">
            <!-- sidebar -->
            <div class="col-md-3 col-sm-2">
                @include('member.fragments.sidebar')
            </div><!-- /.col-md-3 -->
            <!-- end Sidebar -->
            <!-- My Properties -->
            <div class="col-md-9 col-sm-10">
                <section id="course-info">
                    <div class="row">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#mycolleges" aria-controls="mycolleges" role="tab" data-toggle="tab">My Colleges ({{count($myeducation->colleges)}})</a></li>
                            <li role="presentation"><a href="#myprograms" aria-controls="myprograms" role="tab" data-toggle="tab">My Programs ({{count($myeducation->programs)}})</a></li>
                        </ul>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane in active" id="mycolleges">
                                <section id='properties'>
                                    <div class='grid'>
                                        @foreach($myeducation->colleges as $college)
                                        <div class="individual-course col-md-12 no-padding">
                                            <h3><a href="{{url('colleges/'.$college->url)}}">{{$college->cname}}</a></h3>
                                            <div class="program-info">
                                                <span class="program-duration"> 
                                                    <small> 
                                                        @if($college->status == 'current')
                                                        Currently studying
                                                        @else
                                                        Studied in this college
                                                        @endif
                                                    </small>
                                                </span>
                                            </div>
                                        </div>
                                        @endforeach
                                        <div class='clearfix'></div>
                                    </div>
                                </section>
                            </div>
                            <div role="tabpanel" class="tab-pane in active" id="myprograms">
                                <section id='properties'>
                                    <div class='grid'>
                                        @foreach($myeducation->programs as $program)
                                        <div class="individual-course col-md-12 no-padding">
                                            <h3><a href="{{url('programs/'.$program->url)}}">{{$program->pname}}</a></h3>
                                            <div class="program-info">
                                                <span class="program-duration"> 
                                                    <small> 
                                                        @if($program->status == 'current')
                                                        Currently studying
                                                        @else
                                                        Studied this program
                                                        @endif
                                                    </small>
                                                </span>
                                            </div>
                                        </div>
                                        @endforeach
                                        <div class='clearfix'></div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
@stop
