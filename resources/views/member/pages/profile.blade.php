@extends('site.sitemaster')
@section('maincontent')
<?php
$user = Auth::user();
$member = $USER;
?>
<div id="page-content">
    <!-- Breadcrumb -->
    @include('member.fragments.breadcrumb')
    <!-- end Breadcrumb -->

    <div class="container">
        @if($user->status != 'confirmed')
        @include('member.fragments.email_verification_warning')
        @endif
        <div class="row">
            <!-- sidebar -->
            <div class="col-md-3 col-sm-2">
                @include('member.fragments.sidebar')
            </div><!-- /.col-md-3 -->
            <!-- end Sidebar -->
            <!-- My Properties -->
            <div class="col-md-9 col-sm-10">
                <section id="profile">
                    <header><h1>Profile</h1></header>
                    <div class="account-profile">
                        <div class="row">
                            <div class="col-md-3 col-sm-3">
                                <img alt="" class="image" style="max-width: 80px;width:100%" src="{{$member->social_image}}">
                            </div>
                            <div class="col-md-9 col-sm-9">
                                {!! Form::open(['url'=>'member/profile','method'=>'post','id'=>'form-create-account','role'=>'form']) !!}
                                <section id="contact">
                                    @if(empty(Auth::user()->oauth))
                                    <div class="text-center alert alert-info">
                                        <a href="{{url('member/google_redirect_mergeaccount')}}" class="text-danger"><i class="fa fa-google-plus"></i> Link with google</a> <br/>
                                        ---- OR ---- <br/>
                                        <a href="{{url('member/facebook_redirect_mergeaccount')}}"><i class="fa fa-facebook-official"></i> Link with facebook</a>
                                    </div>
                                    @endif
                                    <dl class="contact-fields">
                                        <dt><label for="form-account-name">Name:</label></dt>
                                        <dd><div class="form-group">
                                                <input type="text" class="form-control" id="form-account-name" name="fullname" required value="{{$member->fullname}}">
                                            </div><!-- /.form-group --></dd>
                                        <dt><label for="form-account-phone">Phone:</label></dt>
                                        <dd><div class="form-group">
                                                <input type="text" class="form-control" id="form-account-phone" name="phone" value="{{$member->phone}}">
                                            </div><!-- /.form-group --></dd>
                                        <!--                                        <dt><label for="form-account-email">Email:</label></dt>
                                                                                <dd><div class="form-group">
                                                                                        <input type="text" class="form-control" id="form-account-email" name="email" value="{{$user->email}}">
                                                                                    </div> /.form-group </dd>-->
                                        <dt><label for="form-account-email">Address:</label></dt>
                                        <dd><div class="form-group">
                                                <input type="text" class="form-control" id="form-account-email" name="address" value="{{$member->address}}">
                                            </div><!-- /.form-group --></dd>
                                        <dt><label for="form-account-skype">Skype:</label></dt>
                                        <dd><div class="form-group">
                                                <input type="text" class="form-control" id="form-account-skype" name="skype" value="{{$member->skype}}">
                                            </div><!-- /.form-group --></dd>
                                    </dl>
                                </section>
                                <section id="social">
                                    <h3>My Social Network</h3>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-twitter"></i></span>
                                            <input type="text" class="form-control" id="account-social-twitter" name="twitter" value='{{$member->twitter}}' placeholder="eg. www.twitter.com/yourTwitterUsername">
                                        </div>
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                                            <input type="text" class="form-control" id="account-social-facebook" name="facebook" value='{{$member->facebook}}' placeholder="eg. www.twitter.com/yourFacebookUsername">
                                        </div>
                                    </div><!-- /.form-group -->
                                    <div class="form-group clearfix">
                                        <button type="submit" class="btn pull-right btn-default" id="account-submit">Save Changes</button>
                                    </div><!-- /.form-group -->
                                </section>
                                {!! Form::close() !!}<!-- /#form-contact -->
                                <section id="change-password">
                                    <header><h2>Change Your Username/Email</h2></header>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            {!! Form::open(['url'=>'member/updateemail','method'=>'post','id'=>'form-update-email','role'=>'form']) !!}
                                            <input type="text" id='newemail' name="newemail" class="form-control" value="{{$user->email}}" />
                                            <button>Update</button>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </section>
                                <section id="change-password">
                                    <header><h2>Change Your Password</h2></header>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <form role="form" id="form-account-password" method="get" >
                                                <div class="form-group">
                                                    <label for="form-account-password-current">Current Password</label>
                                                    <input type="password" class="form-control" id="form-account-password-current" name="form-account-password-current">
                                                </div><!-- /.form-group -->
                                                <div class="form-group">
                                                    <label for="form-account-password-new">New Password</label>
                                                    <input type="password" class="form-control" id="form-account-password-new" name="form-account-password-new">
                                                </div><!-- /.form-group -->
                                                <div class="form-group">
                                                    <label for="form-account-password-confirm-new">Confirm New Password</label>
                                                    <input type="password" class="form-control" id="form-account-password-confirm-new" name="form-account-password-confirm-new">
                                                </div><!-- /.form-group -->
                                                <div class="form-group clearfix">
                                                    <button type="submit" class="btn btn-default" id="form-account-password-submit">Change Password</button>
                                                </div><!-- /.form-group -->
                                            </form><!-- /#form-account-password -->
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <strong>Hint:</strong>
                                            <p>You password must be at least 6 characters long. We suggest you to keep a easy-to-remember password.
                                            </p>
                                        </div>
                                    </div>
                                </section>
                            </div><!-- /.col-md-9 -->
                        </div><!-- /.row -->
                    </div><!-- /.account-profile -->
                </section><!-- /#profile -->
            </div><!-- /.col-md-9 -->
            <!-- end My Properties -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div>
@stop