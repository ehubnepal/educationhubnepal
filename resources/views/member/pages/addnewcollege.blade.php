@extends('site.sitemaster')
@section('maincontent')
<div id="page-content">
    <!-- Breadcrumb -->
    @include('member.fragments.breadcrumb')
    <!-- end Breadcrumb -->

    <div class="container">
        <div class="row">
            <!-- sidebar -->
            <div class="col-md-3 col-sm-2">
                @include('member.fragments.sidebar')
            </div><!-- /.col-md-3 -->
            <!-- end Sidebar -->
            <!-- My Properties -->
            <div class="col-md-9 col-sm-10">
                @if(isset($mycolleges) && count($mycolleges)>0)
                <section id="course-info">
                    <div class="row">
                        List of colleges you have added !
                        <ul>
                            @foreach($mycolleges as $mycollege)
                            <li><a href='{{url('member/college/'.$mycollege->id.'/edit')}}'>{{$mycollege->cname}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </section>
                <hr/>
                @endif
                @include('member.forms.form_college')
            </div>
        </div>
    </div>
</div>
@stop
