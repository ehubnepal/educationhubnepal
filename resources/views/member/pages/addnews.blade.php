@extends('site.sitemaster')
@section('maincontent')
<div id="page-content">
    <!-- Breadcrumb -->
    @include('member.fragments.breadcrumb')
    <!-- end Breadcrumb -->

    <div class="container">
        <div class="row">
            <!-- sidebar -->
            <div class="col-md-3 col-sm-2">
                @include('member.fragments.sidebar')
            </div><!-- /.col-md-3 -->
            <!-- end Sidebar -->
            <!-- My Properties -->
            <div class="col-md-9 col-sm-10">
                @include('site.fragments.fragment_server_message')
                @if(isset($mynews) && count($mynews)>0)
                <section id="course-info">
                    <div class="row">
                        List of news you have added !
                        <ul>
                            @foreach($mynews as $n)
                            <li><a href='{{url('member/news/'.$n->id.'/edit')}}'>{{$n->title}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </section>
                <hr/>
                @endif
                @include('member.forms.form_news')
            </div>
        </div>
    </div>
</div>
@stop
