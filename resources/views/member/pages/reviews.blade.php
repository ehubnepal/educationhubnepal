@extends('site.sitemaster')
@section('maincontent')
<div id="page-content">
    <!-- Breadcrumb -->
    @include('member.fragments.breadcrumb')
    <!-- end Breadcrumb -->

    <div class="container">
        <div class="row">
            <!-- sidebar -->
            <div class="col-md-3 col-sm-2">
                @include('member.fragments.sidebar')
            </div><!-- /.col-md-3 -->
            <!-- end Sidebar -->
            <!-- My Properties -->
            <div class="col-md-9 col-sm-10">
                <section id="course-info">
                    <div class="row">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#collegereview" aria-controls="collegereview" role="tab" data-toggle="tab">College reviews ({{count($reviews->college)}})</a></li>
                                <li role="presentation"><a href="#programreview" aria-controls="programreview" role="tab" data-toggle="tab">Program reviews ({{count($reviews->program)}})</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane in active" id="collegereview">
                                    <section id='properties'>
                                        <div class='grid'>
                                            @foreach($reviews->college as $collegereview)
                                            <div class="individual-course col-md-12 no-padding">
                                                <h3><a href="{{url('colleges/'.$collegereview->url.'#reviews')}}">{{$collegereview->cname}}</a></h3>
                                                <div class="program-info">
                                                    <span class="program-duration"> 
                                                        @for($i = $collegereview->star;$i>0;$i--)
                                                        <i class="fa fa-star"></i> &nbsp;
                                                        @endfor <br/>
                                                        <small> reviwed on {{$collegereview->created_at}} </small>
                                                    </span>
                                                </div>
                                                <div class="program-more-info">
                                                    {{$collegereview->review}}
                                                </div>
                                            </div>
                                            @endforeach
                                            <div class='clearfix'></div>
                                        </div>
                                    </section>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="programreview">
                                    <section id='properties'>
                                        <div class='grid'>
                                            @foreach($reviews->program as $programreview)
                                            <div class="individual-course col-md-12 no-padding">
                                                <h3><a href="{{url('programs/'.$programreview->url.'#reviews')}}">{{$programreview->pname}}</a></h3>
                                                <div class="program-info">
                                                    <span class="program-duration"> 
                                                        @for($i = $programreview->star;$i>0;$i--)
                                                        <i class="fa fa-star"></i> &nbsp;
                                                        @endfor <br/>
                                                        <small> reviwed on {{$programreview->created_at}} </small>
                                                    </span>
                                                </div>
                                                <div class="program-more-info">
                                                    {{$programreview->review}}
                                                </div>
                                            </div>
                                            @endforeach
                                            <div class='clearfix'></div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                    </div><!-- end row-->
                </section>
            </div>
        </div>
    </div>
</div>
@stop
