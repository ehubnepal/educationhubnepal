<section id="sidebar">
    <header><h3>Account</h3></header>
    <aside>
        <ul class="sidebar-navigation">
            <li <?php isset($activemenu) && $activemenu=="profile"?print 'class="active"':'';?>><a href="{{url('member/home')}}"><i class="fa fa-user"></i><span>Profile</span></a></li>
            <li <?php isset($activemenu) && $activemenu=="review"?print 'class="active"':'';?>><a href="{{url('member/reviews')}}"><i class="fa fa-eye"></i><span>Reviews</span></a></li>
            <li <?php isset($activemenu) && $activemenu=="myeducation"?print 'class="active"':'';?>><a href="{{url('member/myeducation')}}"><i class="fa fa-graduation-cap"></i><span>My Education</span></a></li>
            <li <?php isset($activemenu) && $activemenu=="newcollge"?print 'class="active"':'';?>><a href="{{url('member/addnewcollege')}}"><i class="fa fa-university"></i><span>Add a new College</span></a></li>
            <li <?php isset($activemenu) && $activemenu=="news"?print 'class="active"':'';?>><a href="{{url('member/news/create')}}"><i class="fa fa-globe"></i><span>Add News</span></a></li>
            <li><a href="{{url('logout')}}"><i class="fa fa-lock"></i><span>Logout</span></a></li>
        @if(Auth::user()->role == 'maintainer')
            <li >
                <hr/>
                <span class="text-info"> You have been assigned a role of superuser. With this role, you can manage the information
                    of college and programs assigned to you.</span>
                <br/>
                <a href="{{url('superuser/home')}}" class='btn btn-info'><span style="color:#FFF !important"><i class="fa fa-bolt"></i> Go to superuser account</span></a></li>
            @endif
        </ul>
    </aside>
</section><!-- /#sidebar -->