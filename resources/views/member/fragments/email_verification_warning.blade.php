<div class="row">
    <div class="col-sm-12 padding alert alert-warning" style='border:1px solid #ccc;padding:5px;margin-bottom:10px;'>
        <div class="col-md-8 col-sm-12">
            You have not verified your email address <strong>({{$user->email}})</strong> yet. 
            Please check your email and click on the
            activation link we have sent to you. If you didn't get the 
            activation email , <a href="#">click here to resend</a>.
        </div>

        <div class="col-md-4 col-sm-12">
            Not your email address? Update here:<br/>
            {!! Form::open(['url'=>'member/updateemail','method'=>'post','id'=>'form-update-email','role'=>'form']) !!}
            <input type="text" name="newemail" class="form-control" value="{{$user->email}}" />
            <button>Update</button>
            {!! Form::close() !!}
        </div>

    </div>
</div>