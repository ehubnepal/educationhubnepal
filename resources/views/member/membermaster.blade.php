
<!DOCTYPE html>
<html lang="en-US">
@include('site.includes.header')
<body class="page-homepage map-google navigation-fixed-top horizontal-search" id="page-top" data-spy="scroll" data-target=".navigation" data-offset="90">
<!-- Wrapper -->
<div class="wrapper">
    <div class="navigation">
         @include('site.includes.mainmenu')
    </div><!-- /.navigation -->

    @if(isset($SERVER_MESSAGE))
    <br/>
    {!! $SERVER_MESSAGE !!}
    @endif
    @yield('maincontent')
   	@include('site.includes.footer')
</div> <!-- wrapper closing -->
 @include('site.includes.script')
 @yield('scripts')
</body>
</html>