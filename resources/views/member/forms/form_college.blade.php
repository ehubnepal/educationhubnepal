<section class="content">
    @if(isset($college))
    {!! Form::open(['url'=>'member/college/update','files'=>'true','method'=>'post']) !!}
    {!! Form::hidden('id',$college->id) !!}
    @else
    {!! Form::open(['url'=>'member/college','files'=>'true','method'=>'post']) !!}
    @endif
    <div class="row">
        <div class="col-md-12"> 
            <!-- /.box-body -->
            <div class="box-footer text-right">
                <button type="submit" class="btn btn-success">Save</button>
                <button type="reset" class="btn btn-primary">Reset</button>
                <a href="{{ url('member/addnewcollege') }}" class="btn btn-danger">Cancel</a> </div>
            <!-- /.box --> 
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"> <?php isset($college) ? print 'Edit college (' . $college->cname . ')' : print 'Add new college'; ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    <div class="form-group">
                        <?php $cname = isset($college->cname) ? $college->cname : ''; ?>
                        {!! Form::label('cname','College * ') !!}
                        {!! Form::text('cname', $cname,['class'=>'form-control','id'=>'model','placeholder'=>'Model name','required'=>'required']) !!}
                    </div>
                    <div class="form-group">

                        <?php $address = isset($college->address) ? $college->address : ''; ?>
                        {!! Form::label('address','Address * ') !!}
                        {!! Form::text('address', $address,['class'=>'form-control','id'=>'address','placeholder'=>'Enter address']) !!}
                    </div>
                    <div class="form-group">
                        <?php $district = isset($college->district) ? $college->district : ''; ?>
                        {!! Form::label('district','District ') !!}
                        {!! Form::text('district', $district,['class'=>'form-control','id'=>'district','placeholder'=>'Enter district']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('collegelogo','Logo ') !!}
                        {!! Form::file('collegelogo',['class'=>'form-control','id'=>'collegelogo']) !!}
                        <?php
                        if (!empty($college->full_url)) {
                            if (@getimagesize(url($college->full_url))) {
                                echo '<br/><img src="' . url($college->full_url) . '" />';
                                echo '<a href="' . url('member/college/' . $college->id . '/removelogo') . '" class="text-danger"><i class="fa fa-trash"></i> Remove logo</a>';
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"> Contact option </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <?php $phone = isset($college->phone) ? $college->phone : ''; ?>
                        {!! Form::label('phone','Phone ') !!}
                        {!! Form::text('phone', $phone,['class'=>'form-control','id'=>'phone','placeholder'=>'Enter Phone']) !!}
                    </div>
                    <div class="form-group">
                        <?php $fax = isset($college->fax) ? $college->fax : ''; ?>
                        {!! Form::label('fax','Fax ') !!}
                        {!! Form::text('fax', $fax,['class'=>'form-control','id'=>'fax','placeholder'=>'Enter fax']) !!}
                    </div>
                    <div class="form-group">
                        <?php $website = isset($college->website) ? $college->website : ''; ?>
                        {!! Form::label('website','Website ') !!}
                        {!! Form::text('website', $website,['class'=>'form-control','id'=>'website','placeholder'=>'Enter website']) !!}
                    </div>
                    <div class="form-group">
                        <?php $pobox = isset($college->pobox) ? $college->pobox : ''; ?>
                        {!! Form::label('pobox','POBox ') !!}
                        {!! Form::text('pobox', $pobox,['class'=>'form-control','id'=>'pobox','placeholder'=>'Enter pobox']) !!}
                    </div>
                    <div class="form-group">
                        <?php $email = isset($college->email) ? $college->email : ''; ?>
                        {!! Form::label('email','Email ') !!}
                        {!! Form::text('email', $email,['class'=>'form-control','id'=>'email','placeholder'=>'Enter email']) !!}
                    </div>

                    <hr/>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class='col-md-12'>
            <div class="form-group">
                <?php $description = isset($college->description) ? $college->description : ''; ?>
                {!! Form::label('description','Description ') !!}
                {!! Form::textarea('description', $description, ['class'=>'form-control','id'=>'description','placeholder'=>'Description']) !!}
            </div>
            <div class="form-group">
                <?php $facilities = isset($college->facilities) ? $college->facilities : ''; ?>
                {!! Form::label('facilities','Facilities ') !!}
                {!! Form::textarea('facilities', $facilities, ['class'=>'form-control','id'=>'facilities','placeholder'=>'Facilities']) !!}
            </div>
        </div>
    </div>
</form>
</section>


@section('scripts')
<script src="{{url('public/admin/js/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
var __base_url = "{{url('/')}}";
$(function () {
    CKEDITOR.replace('description');
    CKEDITOR.replace('facilities');
});
$('.image_is_banner').click(function () {
    var url = $(this).is(":checked") ? __base_url + '/member/college/setimagebanner/' + jQuery(this).attr('data') : __base_url + '/admin/college/unsetimagebanner/' + jQuery(this).attr('data');
    sendRequest(url);
});

$('.image_is_published').click(function () {
    var url = $(this).is(":checked") ? __base_url + '/member/college/setimagepublish/' + $(this).attr('data') : __base_url + '/admin/college/setimageunpublish/' + $(this).attr('data');
    sendRequest(url);
});

$('.image_ordering').change(function () {
    var url = __base_url + '/member/college/setimageordering/' + $(this).attr('data') + '/' + $(this).val();
    sendRequest(url);
});

function sendRequest(url) {
    jQuery('.loading').show();
    jQuery.ajax({
        url: url, type: "get", complete: function (data) {
            jQuery('.loading').hide();
        }
    });
}
</script>
@stop

