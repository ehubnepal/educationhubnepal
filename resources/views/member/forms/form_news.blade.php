<section class="content">
    @if(isset($news))
    {!! Form::open(['url'=>'member/news/update','files'=>'true','method'=>'post']) !!}
    {!! Form::hidden('id',$news->id) !!}
    @else
    {!! Form::open(['url'=>'member/news/store','files'=>'true','method'=>'post']) !!}
    @endif
    <div class="row">
        <div class="col-md-12"> 
            <!-- /.box-body -->
            <div class="box-footer text-right">
                <button type="submit" class="btn btn-success">Save</button>
            <!-- /.box --> 
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"> <?php isset($news) ? print 'Edit News' : print 'Create News'; ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    <div class="form-group">
                        <!-- college here -->
                    </div>
                    <div class="form-group">
                        <?php $title = isset($news->title) ? $news->title : ''; ?>
                        {!! Form::label('title','Title * ') !!}
                        {!! Form::text('title', $title,['class'=>'form-control','id'=>'title','placeholder'=>'Enter title']) !!}
                    </div>
                    <div class="form-group">
                        <?php $news1 = isset($news->news) ? $news->news : ''; ?>
                        {!! Form::label('news','News ') !!}
                        {!! Form::textarea('news', $news1,['class'=>'form-control','id'=>'news','placeholder'=>'Add news']) !!}
                    </div>
                    <div class="form-group">
                        Published : 
                        <input type="checkbox" name="published"   <?php if(isset($news->published)){$news->published == 1 ? print 'checked' : ''; }else{echo 'checked';} ?>/>
                    </div>
                </div>
            </div>
        </div>

    </div>


</form>
</section>

@section('scripts')
<script src="{{url('public/admin/js/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
$(function () {
    CKEDITOR.replace('news');
    //CKEDITOR.replace('facilities');
});

</script>
@stop

