@extends('wiki.wikimaster')
@section('maincontent')
<div class="container">
    <div class="row">
        <div class='col-md-12' style='margin-top:5px;border: 1px solid #ccc;padding:10px'>
            <a href='{{url('wiki/college/'.$college->url.'/edit/detail')}}' class='btn btn-danger'> <i class="fa fa-arrow-left"></i> Go back</a>
        </div>
        <div class="box-body col-md-12">
            <div class="form-group">
                <br/>
                <h2>Edit history of {{$college->cname}}</h2>
                <div>
                    <ul class='edit_history'>
                    @foreach($history as $h)
                    <li>
                        <h3>Reviewed by {{$h->user->fullname}} on {{$h->updated_at}}</h3>
                        <div style='background-color:#FCFCFC;padding:10px;'>{!! $h->description!!}</div>
                    </li>
                    @endforeach
                    </ul>
                </div>
            </div>
        </div>

    </div><!-- /.row -->
</div><!-- /.container -->
@stop

@section('scripts')

@stop