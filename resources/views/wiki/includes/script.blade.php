<script type="text/javascript" src="{{ url('public/site/js/jquery-2.1.0.min.js') }}"></script>

<!--[if gt IE 8]>
<script type="text/javascript" src="{{ url('public/site/js/ie.js') }}"></script>
<![endif]-->

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/574d3ec51be2250b3bd2fae9/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script--> 
