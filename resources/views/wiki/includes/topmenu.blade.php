<div class="secondary-navigation">
    <div class="container">
        <div class="contact">
            <figure><strong>We are volunteers. Your contribution means a lot.</strong></figure>
            <figure><strong>Email:</strong>info@educationhubnepal.com</figure>
        </div>
        <div class="user-area">
            <div class="actions">
                <!--<a href="create-agency.html" class="promoted"><strong>Advertiser</strong></a>-->
                @if(Auth::user())
                <a href="{{url('member/home')}}" class="promoted"><i class="fa fa-user"></i> {{$USER->fullname}}</a> &nbsp;| 
                <a href="{{url('logout')}}" ><i class="fa fa-lock"></i> Logout</a>
                @else
                <a href="{{url('signup')}}" class="promoted">Register</a>
                <a href="{{url('login')}}">Sign In</a>
                @endif
            </div>
        </div>
    </div>
</div>