<div class="container">
    <header class="navbar" id="top" role="banner">
        <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="navbar-brand nav" id="brand">
                <a href="{{url('/')}}">

                    <img src="{{ url('public/site/img/ehublogo.png') }}">
                    EducationHub Wiki<sub>Your contribution matters</sub></a>
            </div>
        </div>
        
        
    </header><!-- /.navbar -->
</div><!-- /.container -->