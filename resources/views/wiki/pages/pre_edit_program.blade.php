@extends('wiki.wikimaster')
@section('maincontent')
<div class="container">
    <?php
   // isset($server_message)?print $server_message:'';
    if(\Session::has('server_message')){
    echo \Session::get('server_message');
    \Session::flush();
    }
    ?>
    <div class="row">
        @include('wiki.forms.form_login')
        @include('wiki.fragments.fragment_program_pre_edit')
    </div><!-- /.row -->
</div><!-- /.container -->
@stop