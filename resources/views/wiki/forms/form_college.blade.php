@extends('wiki.wikimaster')
@section('maincontent')
<div class="container">
    <div class="row">
        @if(isset($college->id))
        {!! Form::open(['url'=>'wiki/college/update','files'=>'true','method'=>'post']) !!}
        {!! Form::hidden('id',$college->id) !!}
        @else
        {!! Form::open(['url'=>'wiki/college','files'=>'true','method'=>'post']) !!}
        @endif
        {!! Form::hidden('college_id',$college->college_id) !!}
        <div class='col-md-12' style='margin-top:5px;border: 1px solid #ccc;padding:10px'>
            <a href='{{url('/colleges/'.$college->url)}}' class='btn btn-danger'> <i class="fa fa-arrow-left"></i> Cancel Edit</a>
            @if(isset($college->id))
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Update</button>
            @else
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
            @endif

            <nav class="navbar-right" role="navigation" style='float:right !important'>
                <ul class="nav navbar-nav">
                    <li ><a href="{{url('/wiki/college/'.$college->url.'/edit/history')}}"><i class="fa fa-history"></i> Edit History ({{$edit_count}})</a> </li>
                </ul>
            </nav><!-- /.navbar collapse-->
        </div>
        <div class="box-body col-md-12">
            <h2>Edit General Information of {{$college->cname}} [ {{$college->address}}]</h2>
            <div class="col-md-6">
                <div class="form-group">
                    <?php $address = isset($college->address) ? $college->address : ''; ?>
                    {!! Form::label('address','Address') !!}
                    {!! Form::text('address', $address, ['class'=>'form-control','id'=>'address','placeholder'=>'Enter address']) !!}
                </div>
                <div class="form-group">
                    <?php $phone = isset($college->phone) ? $college->phone : ''; ?>
                    {!! Form::label('phone','Phone') !!}
                    {!! Form::text('phone', $phone, ['class'=>'form-control','id'=>'phone','placeholder'=>'Enter phone']) !!}
                </div> 
                <div class="form-group">
                    <?php $fax = isset($college->fax) ? $college->fax : ''; ?>
                    {!! Form::label('fax','Fax') !!}
                    {!! Form::text('fax', $fax, ['class'=>'form-control','id'=>'fax','placeholder'=>'Enter fax']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?php $email = isset($college->email) ? $college->email : ''; ?>
                    {!! Form::label('email','Email') !!}
                    {!! Form::text('email', $email, ['class'=>'form-control','id'=>'email','placeholder'=>'Enter email']) !!}
                </div>
                <div class="form-group">
                    <?php $website = isset($college->website) ? $college->website : ''; ?>
                    {!! Form::label('website','Website') !!}
                    {!! Form::text('website', $website, ['class'=>'form-control','id'=>'website','placeholder'=>'Enter website']) !!}
                </div>
            </div>
            <div class="col-md-12"
            <div class="form-group">
                <br/>
                <?php $description = isset($college->description) ? $college->description : ''; ?>
                {!! Form::label('description','Description') !!}
                {!! Form::textarea('description', $description, ['class'=>'form-control','id'=>'description','placeholder'=>'Description','cols'=>20]) !!}
            </div>
        </div>
        </div>
        {!!Form::close()!!}
    </div><!-- /.row -->
</div><!-- /.container -->
@stop

@section('scripts')
<script src="{{url('public/admin/js/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
CKEDITOR.replace('description', {height: '400px'});
</script>
@stop