<br/>
<div class='col-md-12'>
    <div class='alert alert-info'>To maintain the accuracy of data and prevent unwanted editing, 
        it is recommended that you login before editing data. If you don't have an account yet you can quickly
    get registered  by signing in with google or facebook.</div>
</div>
<div class="col-md-6 col-sm-6 col-md-6 col-xs-12 login-form">
    <h3>Register or Login with social media.</h3>
    <div class="social-login">
        <?php
        $redirect_url = url('wiki/college/'.$college->url.'/edit/detail');
        ?>
        <a href="{!!URL::to('login/facebook?redirect_url='.$redirect_url)!!}"><img src="{{url('/public/site/img/fb-signin.png')}}"/></a>
        <a href="{!!URL::to('login/google?redirect_url='.$redirect_url)!!}"><img src="{{url('/public/site/img/google-signin.png')}}"/></a>
        <span class="signin-option"><span>OR</span><hr/></span>
    </div>
    {!! Form::open(['url'=>'login','method'=>'post','id'=>'form-create-account','role'=>'form']) !!}
    {!! Form::hidden('redirect_url',$redirect_url) !!}
    <div class="form-group">
        <label for="form-create-account-email">Email:</label>
        <input type="email" name="username" class="form-control" id="form-create-account-email" required>
    </div><!-- /.form-group -->
    <div class="form-group">
        <label for="form-create-account-password">Password:</label>
        <input type="password" name="password" class="form-control" id="form-create-account-password" required>
    </div><!-- /.form-group -->
    <div class="form-group clearfix">
        <button type="submit" class="btn pull-right btn-default" id="account-submit">Sign to My Account</button>
    </div><!-- /.form-group -->
    {!! Form::close() !!}
    <hr>
    <!--<div class="forgot-link"><a href="forgotpassword">I don't remember my password</a></div>-->
</div>