<!DOCTYPE html>
<html lang="en-US">
@include('wiki.includes.header')
<body class="page-homepage map-google navigation-fixed-top horizontal-search" id="page-top" data-spy="scroll" data-target=".navigation" data-offset="90">
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-P4XWTC"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P4XWTC');</script>
<!-- End Google Tag Manager -->


<!-- Wrapper -->
<div class="wrapper">
    <div class="navigation">
        @include('wiki.includes.topmenu')
         @include('wiki.includes.mainmenu')
    </div><!-- /.navigation -->

    @if(isset($SERVER_MESSAGE))
    <br/>
    {!! $SERVER_MESSAGE !!}
    @endif
    @yield('maincontent')
   	@include('wiki.includes.footer')
</div> <!-- wrapper closing -->
 @include('wiki.includes.script')
 @yield('scripts')
</body>
</html>