<?php
header("location:http://www.educationhubnepal.com/comingsoon");
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Education Hub Nepal</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }
            input{
                width:80%;
                height:40px;
                outline: none;
                border:1px solid #ccc;
                font-size:22px;
                color:#333;
                padding-left:12px;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
                width:80%;
            }

            .title {
                font-size: 80px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">
                    Coming Soon <br/>Education Hub Nepal.
                </div>
            </div>
        </div>
    </body>
</html>
