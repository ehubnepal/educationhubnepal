@extends('site.sitemaster')
@section('maincontent')
<!-- Page Content -->


<div id="page-content">
    <div class="style-left col-md-4">
        <!-- Breadcrumb -->
        <div class="">
            <ol class="breadcrumb">
                <li><a href="{{url('')}}">Home</a></li>
                <li class="active"><a href="#">Level Listings</a></li>
            </ol>
        </div>
        <!-- end Breadcrumb -->
        <div class="page-intro col-md-10 col-md-offset-1">
            <span class="icon"><i class="fa fa-cubes"></i></span>
            <h2>All levels of studies in Nepal</h2>
            <p>This page represents all levels of studies that are currently being taught in Nepal, expects the secondary schools. All the levels has been presented in hierarchy, and when you click view detail, you will be redirected to the detail page consisting all the available programs. </p>
            <p>If you’re searching for a specific program, we suggest you to use our search, otherwise you can explore the courses with your desired level.</p>
        </div>
    </div>
    <div class="col-md-8">
        <div class="">
                <div class="row">
                    <!-- Results -->
                    <div class="col-md-12 col-sm-12">
                        <section id="results">
                            <section id="level-listing">
                                <div class="row">
                                    @foreach($levels as $level)
                                    <div class="col-md-4 col-sm-6">
                                        <div class="triangle-top"></div>
                                        <div class="individual-level">
                                            <a class="level_name" href="{{url('levels/'.$level->url)}}">{{$level->lname}}</a>
                                            <span class="heading">No. Of Programs ({{$level->count}})</span>
                                            <a class="bottom-nav-tab link-arrow" href="{{url('levels/'.$level->url)}}">View detail</a>
                                        </div>
                                    </div>
                                   @endforeach 
                                </div>



                                <!-- Pagination -->
                                <div class="center">
                                    
                                  
                                    
                                </div><!-- /.center-->

                            </section><!-- /#properties-->
                        </section><!-- /#results -->
                    </div><!-- /.col-md-9 -->
                    <!-- end Results -->

                    <!-- sidebar -->

                </div><!-- /.row -->
            </div><!-- /.container -->
    </div>
</div>
<!-- end Page Content -->
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "item": {
      "@id": "http://www.educationhubnepal.com/levels",
      "name": "Levels",
      "image":""
    }
  }]
}
</script>
@stop