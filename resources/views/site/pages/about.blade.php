@extends('site.sitemaster')
@section('maincontent')

<!-- Page Content -->
<div id="page-content">
    <!-- Breadcrumb -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{url('')}}">Home</a></li>
            <li class="active">About</li>
        </ol>
    </div>
    <div class="container">
    	<header><h1>About us</h1></header>
    </div>
    <div class="about-banner">
	    <h3>1600 Colleges More Than 800 Programs, 21 Categories</h3>
		<h3>Delivering Information About Courses, Colleges, Universities & Study Choices </h3>
	</div>
	<div class="container">
			<h3 class="about-heading">What Is Education Hub Nepal</h3>
		<p class="about-desc">Education Hub Nepal is a comprehensive search engine for finding courses, colleges, universities and exam in nepal, we have information of as many colleges and as many courses that are currently being taught in Nepal making your college and study search a breeze. We help you choose your next study target.</p>

		<h3 class="about-heading">What’s Our Goal</h3>
		<p class="about-desc">We’re working hard everyday to empower students and parents with the information about education and it’s choices. Thus we have a mission to be a single system with all the information about colleges, courses, fees, exams and updates so that student/parents won’t have to go anywhere for their desired information. </p>

		<div class="for-everyone container">
		<div class="row">
			<div class="col-md-4 for-students">
				<h2>For Students</h2>
				
				<h3 class="about-heading">After all it’s all about your dream and choosing future</h3>
				<p class="about-desc">Student can use EdcuationhubNepal as a search engine for their studies, colleges, affiliations and program choices, we have worked hard on our search algorithm to give you the correct information about your queries, thus we suggest you to hit our search button if you stuck anywhere. Or else we’ve made seamless exploration of studies, universities and level wise programs to simplify your education search. </p>
			</div>
			<div class="col-md-4 for-institution">
				<h2>For Institution</h2>
				<h3 class="about-heading">We are representing you</h3>
				<p class="about-desc">Currently we have more than 1600 colleges and 28 national as well as international universities information, their offering programs, and other details. If you’re a stakeholder of college, university or any educational institution, we suggest you to update your detail, so that students/parents could get right and uptodate information about your institution. Students use EducationhubNepal for comprehensive information about your colleges, exams and news, so we suggest you to update your institution right away. </p>
			</div>
			<div class="col-md-4 for-parents">
				<h2>For Parents</h2>

				<h3 class="about-heading">We Thrive To help You</h3>
				<p class="about-desc">If you’re searching for the study choices for your children's, or for siblings, we have much more to offer you. More than 1600 colleges, 21 different categories of programs and all level of studies, information about both national, international as well as other universities affiliation that you couldn’t find anywhere else in one place. </p>
			</div>
		</div>
		</div>

		<h3 class="about-heading">So Who We’re Are</h3>
		<p class="about-desc">We’re believers of hard work, and creative minds that are morphing with time but for innovation, and simplification</p>

		<h2>We’re Working For A Cause</h2>
		<h3>More than 500 Volunteers Volunteering Join Us</h3>
		<p>It has been very hard for us to collect and work with the data and make it decent, we want our information to be valid, true and uptodate, so we are always seeking help from students, colleges and stakeholders. If you are student of any colleges, or someone studying a program, please help us improve our data, and if you’re a college please claim your college and let us know accordingly. </p>
		<p>We’re volunteering for a cause, to simplify the education information search is our goal, be a part of this cause, and help every students have right information. </p>

		<h3>Advertise With Us</h3>
		<p>We’re growing among students and parents, and we gets millions of views every month. If you wanna showcase your institution or feature them in our listing pages, or else want to your own advertising banner, please contact us, with clear subject of “Advertising”.</p>

	</div>
</div>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "item": {
      "@id": "http://www.educationhubnepal.com/about",
      "name": "About Us",
      "image":""
    }
  }]
}
</script>
@stop