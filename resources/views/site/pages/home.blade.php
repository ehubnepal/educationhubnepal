@extends('site.sitemaster')
@section('maincontent')
<!-- Page Content -->
@include('site.fragments.fragment_grid_image')
@include('site.fragments.searchbox')
<div id="page-content">
    <section id="banner">
        <div class="hotlinks block has-dark-background background-color-default-darker center text-banner">
            <div class="container">
                @foreach($levels as $level)
                <div class="col-md-3"><a href="{{url('levels/'.$level->url)}}"><i class="fa fa-angle-right"></i> {{$level->lname}}</a></div>
                @endforeach
               
            </div>
        </div>
    </section><!-- /#banner -->
    <section id="explore-anything" class="block">
        <div class="container">
            <header class="listing-title">
                <h2>Explore Your Education Choices</h2>
                <p>Search & discover Programs, Colleges & Courses You Need</p>
            </header>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-4 individual-wrapper">
                    <a href="{{url('colleges')}}">
                        <i class="fa fa-5x fa-university"></i><br/>
                        <h3>Find best college</h3>
                        <p>Learn about the best colleges in Nepal</p>
                    </a>
                </div>
                <div class="col-md-4 individual-wrapper">
                    <a href="{{url('programs')}}">
                        <i class="fa fa-5x fa-book"></i><br/>
                        <h3>Top courses</h3>
                        <p>Find the popular courses</p>
                    </a>
                </div>
                <div class="col-md-4 individual-wrapper">
                    <a href="{{url('blog')}}">
                        <i class="fa fa-feed fa-5x"></i><br/>
                        <h3>Get updates</h3>
                        <p>Stay informed via our blogs</p>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section id="course-listing" class="block">
        <div class="container">
            <header class="course-listing-title">
                <h2>Choose your faculty</h2>
                <p>Search programs by faculties below</p>
            </header>
        </div>
        <div class="container">
            <div class="custom-row row">
                @foreach($categories as $category)

                <div class="col-md-2 col-xs-6 no-padding">
                    <a href="{{url('categories/'.$category->url)}}">
                        <div class="individual-course-wrap">
                            @if(!empty($category->logo))
                                {!! $category->logo!!}
                            @else
                            <img src="{{ url('public/site/img/ehublogo.png')}}"/>
                            @endif
                            <h3>{{$category->category}}</h3>
                            <p>{{$category->numprograms}} Programs</p>
                        </div>
                    </a>
                </div>
                @endforeach
               
            </div>
            <div class="row">
                <div class="col-md-12 explore-all">
                    <a href="{{url('/categories')}}">Explore more <i class="fa fa-angle-right"></i></a>
                </div>
            </div>
        </div>
            
    </section>
    <section id="top-featured-list" class="block">
        <div class="container">
            <header class="listing-title">
                <h2>Most Searched Colleges</h2>
                
            </header>
        </div>
        <!--without container-->
            <div class="popular-wrapper">
                @foreach($featured_colleges as $college)
                <div class="col-md-3 col-sm-6">
                    <div class="property">
                        <a href="{{url('colleges/'.$college->url)}}">
                            <div class="property-image"> 
                            {!!get_banner($college)!!}
                            </div>
                            <div class="overlay">
                                <div class="info">
                                    <div class="college-logo">
                                        @if($college->full_url !=='')
                                        <img src="{{url($college->full_url)}}" alt="{{$college->cname}}"/>
                                    @else
                                    <img src="{{ url('public/site/img/ehublogo.png')}}"/>
                                    @endif
                                    </div>
                                    <h3>{{$college->cname}}</h3>
                                    <figure><i class="fa fa-map-marker"></i> {{$college->address}}</figure>
                                </div>
                            </div>
                        </a>
                    </div><!-- /.property -->
                </div><!-- /.col-md-3 -->
                @endforeach
            </div>
    </section>
    <section id="top-courses">
        <div class="container">
            <header class="listing-title">
                <h2>Top Courses</h2>
            </header>
            <ul>
                @foreach($top_courses as $course)
                <li>
                    <a href="{{url('programs/'.$course->url)}}" title="{{$course->pname}}">{{$course->pname}}</a>
                </li>
                @endforeach
        </div>
    </section>
    <section id="blog-social">
        <div class="container">
         <header class="listing-title">
                <h2>Blog / Updates</h2>
         </header>
         <div class="col-md-8">
            @foreach($blogs as $post)
              <div class="single-blog">
                <a href="{{url('blog/'.$post->slug)}}" class="title">{{$post->title}}</a>
                <p class="intro"><?php echo strip_tags(substr($post->content,0,300), '<br>'); ?></p>
                <a href="{{url('blog/'.$post->slug)}}">Read More</a>
              </div>
            @endforeach
         
          <a class="visit-blog" href="http://www.educationhubnepal.com/blog">View all blogs</a>
        </div>
         <div class="col-md-4">
            <div id="fb-root"></div>
            <script>(function(d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) return;
              js = d.createElement(s); js.id = id;
              js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6&appId=1374507662867919";
              fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
            <div class="fb-page" data-href="https://www.facebook.com/educationhubnepal/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/educationhubnepal/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/educationhubnepal/">Education Hub Nepal</a></blockquote></div>
            </div>
        </div>
    </section>
</div><!-- end Page Content -->

<!-- jsonld markeup @vaghawan-->
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "WebSite",
  "url": "http://www.educationhubnepal.com/",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "http://www.educationhubnepal.com/search?searchby=program&query={query}",
    "query-input": "required name=query"
  },
  "@type" : "Organization",
  "name" : "Education Hub Nepal",
  "url" : "http://www.educationhubnepal.com",
  "logo":"http://www.educationhubnepal.com/public/site/img/ehublogo.png",
  "sameAs" : [
    "https://www.facebook.com/educationhubnepal",
    "https://twitter.com/educationhubnp",
    "https://plus.google.com/+Educationhubnepal/about"
  ]
}
</script>
@stop
