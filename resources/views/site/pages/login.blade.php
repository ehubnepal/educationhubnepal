@extends('site.sitemaster')
@section('maincontent')
<div class="container">
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li class="active">Sign In</li>
    </ol>
</div>
<!-- end Breadcrumb -->

<div class="container">
    <header><h1>Sign In</h1></header>
    <?php
   // isset($server_message)?print $server_message:'';
    if(\Session::has('server_message')){
    echo \Session::get('server_message');
    \Session::flush();
    }
    ?>
    <div class="row">
        @include('site.fragments.fragment_server_message')
        @include('site.forms.form_login')
        <div class="off-focus">@include('site.forms.form_signup')</div>
    </div><!-- /.row -->
</div><!-- /.container -->
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "item": {
      "@id": "http://www.educationhubnepal.com/login",
      "name": "Login",
      "image":""
    }
  }]
}
</script>
@stop