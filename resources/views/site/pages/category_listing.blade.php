  @extends('site.sitemaster')
@section('maincontent')
<div id="page-content" class="course-listing-page">
        <!-- Breadcrumb -->
        @include('site.fragments.searchbox_mini')
        <div class="container">
<!--            <ol class="breadcrumb">
                <li><a href="{{url('')}}">Home</a></li>
                <li><a href="{{url('colleges/')}}">Faculties</a></li>
                <li class="active">Faculties Listing</li>
            </ol>-->
            <div class="container">
                <header class='header-with-subtitle'>
                    <h2>Find Programs By Faculties</span></h2>
                </header>
            </div>
        </div>
        <section class="block">
                <div class="container">
                <div class="custom-row row">
                    @foreach($categories as $category)

                    <div class="col-md-2 col-xs-6 no-padding">
                        <a href="{{url('categories/'.$category->url)}}">
                            <div class="individual-course-wrap">
                                @if($category->logo !=='')
                                    {!!$category->logo!!}
                                @else
                                <img src="{{ url('public/site/img/ehublogo.png')}}"/>
                                @endif
                                <h3>{{$category->category}}</h3>
                                <p>{{$category->numprograms}} Programs</p>
                            </div>
                        </a>
                    </div>
                    @endforeach
                   
                </div>
            </div>
                
        </section>
    </div>
    <script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "item": {
      "@id": "http://www.educationhubnepal.com/categories",
      "name": "Faculties",
      "image":""
    }
  }]
}
</script>
    @stop