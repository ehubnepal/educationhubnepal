@extends('site.sitemaster')
@section('maincontent')
<!-- Page Content -->
@include('site.fragments.searchbox_mini')
<div id="course-search-content">
    <section id="course-search-list" class="block">
        <div class="container">
            <header class="listing-title">
                <h2>Searching college :<span class='text-info'> {{$param['query']}}</span></h2>
            </header><!--
            <div class="leftbar col-md-3">
                <h3>Filter results</h3>
                <div class="filter-results">
                    <h3>Affiliations</h3>
                    <ul>
                        <li>Pokhara University</li>
                        <li>Kathmandu University</li>
                        <li>Margarate University</li>
                    </ul>
                </div>
            </div>-->
            <!--without container-->
            @if(count($colleges)<1)
            <div style="margin-bottom:10px; width:100%;border:1px dashed #ccc;padding:15px;font-size:20px;text-align: center;background:#f0f0f0">Sorry the college you are looking for was not found.</div>
            <div class='alert alert-warning'>Our community is working hard to list all colleges in Nepal. 
                Education Hub Nepal is run by more than 200 people just like you. 
                You too can be a part of this community and help us in listing more institutions.
                If you've got time, click on this <a href="{{url('/signup')}}"><span class="label label-info" style="padding:4px"> signup </span></a> &nbsp; button and register in 30 seconds with google or facebook. 
                Then you can add this college by providing basic information such as college name 
                and location. We will search for the details and add this college in no time. Your cooperation is 
                very much valued by Education Hub Nepal.</div>
            @endif
            <div class="popular-wrapper col-md-12">
                @foreach($colleges as $college)
                <div class="col-md-4 col-sm-6">
                    <div class="grid_listing_college_snippet">
                        <div class="snippet-top">
                            <div class="div-container">
                                <?php $colors = array('225378','eb7f00','1695a3'); ?>
                                    <img src="http://dummyimage.com/600x400/<?php echo $colors[array_rand($colors)];?>/ffffff&text={{$college->cname}}">
                                <div class="overlay"></div>
                                <div class="snippet-extras">
                                    <div class="data-left"></div>
                                    <div class="data-right college_rating pull-right">
                                        <span class="college_affiliation"><i class="fa fa-university"></i>
                                           
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="snippet-middle">
                            <div class="thumbnail">
                                @if($college->logo !=='')
                                     <img src="{{ url($college->full_url)}}"/>
                                @else
                                    <img src="{{ url('public/site/img/ehublogo.png')}}"/>
                                 @endif
                    
                            </div>
                            <div class="college_details">
                                <a class="college_name" href="{{url('colleges/'.$college->url)}}">
                                    <span>{{$college->cname}}</span>
                                </a>
                            </div>
                            <span class="college_location">
                                <i class="fa fa-map-marker"></i> {{$college->address}}
                            </span>
                        </div>
                        <div class="snippet-bottom">
                            <div class="courses">
                                <span class="heading">courses offered ({{count($college->programs)}})</span>
                                <div class="course_detail_tab">
                                    <ul class="coursecarousel">
                                        @foreach($college->programs as $program)
                                        <li class="item"><i class="fa fa-graduation-cap"></i>{{$program->pname}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="bottom_navigation">
                                <a class="bottom-nav-tab" href="{{url('colleges/'.$college->url)}}">HOME</a>
                                <a class="bottom-nav-tab" href="{{url('colleges/courses'.$college->url)}}#courses">COURSES</a>
                                <a class="bottom-nav-tab" href="#">GALLERY</a>
                                <a class="bottom-nav-tab" href="{{url('colleges/'.$college->url)}}">View detail</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="col-md-4 col-sm-6">
                    <div class="course-card">
                        <div class="info">
                            <p class="course-name">{{$college->cname}}</p>
                        </div>	
                    </div>
                </div><!-- /.col-md-3 -->
                @endforeach
            </div>
            <div>{!!$colleges->links()!!}</div>
        </div>
    </section>
</div>
<!-- end Page Content -->
@stop
