@extends('site.sitemaster')
@section('maincontent')
<style type='text/css'>
    ul.list-links{list-style-type:none}
</style>
<!-- Page Content -->
    <div id="page-content">
        <!-- Breadcrumb -->
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{url('')}}">Home</a></li>
                <li><a href="#">Blog Listing</a></li>
                
            </ol>
        </div>
    
 <div class="container blog-listing">
            <div class="row">
                <!-- Content -->
                <div class="col-md-9 col-sm-9">
                    <section id="content">
                        <header><h1>Blog Listing</h1></header>
                        @foreach($posts as $post)
                        <article class="blog-post">
                            <a href="{{url('blog/'.$post->slug)}}">@if(!empty($post->image))<img src="{{url('public/admin/img/blog/'.$post->image)}}">@endif</a>
                            <header><a href="{{url('blog/'.$post->slug)}}"><h2>{{$post->title}}</h2></a></header>
                            <figure class="meta">
                                <a href="#" class="link-icon"><i class="fa fa-user"></i>Education Hub Nepal</a>
                                <a href="#" class="link-icon"><i class="fa fa-calendar"></i>{{date("F jS, Y",strtotime($post->created_at))}}</a>
                                @if(!empty($post->categories))
                                <div class="tags">
                                    @foreach($post->categories as $category)
                                    <a href="{{url('blog/categories/'.$category->slug)}}" class="tag article">{{$category->name}}</a>
                                    @endforeach
                                </div>
                                @endif
                            </figure>
                            <p><?php echo strip_tags(substr($post->content,0,300), '<br>'); ?></p>
                            <a href="{{url('blog/'.$post->slug)}}" class="link-arrow">Read More</a>
                        </article><!-- /.blog-post -->
                        @endforeach

                        <!-- Pagination -->
                        <div class="center">
                            {!!$posts->render() !!}
                            <!-- /.pagination-->
                        </div><!-- /.center-->
                    </section><!-- /#content -->
                </div><!-- /.col-md-9 -->
                <!-- end Content -->

                <!-- sidebar -->
                <div class="col-md-3 col-sm-3">
                    <section id="sidebar">
                       <!--
                        <aside id="search">
                            <header><h3>Search</h3></header>
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Enter Keyword">
                                <span class="input-group-btn"><button class="btn btn-default search" type="button"><i class="fa fa-search"></i></button></span>
                            </div>--><!-- /input-group 
                        </aside>--> 
                        <aside id="post-archive">
                            <header><h3>Tags</h3></header>
                            <ul class="list-links">
                                @foreach($tags as $tag)
                                <li><a href="{{url('blog/tags/'.$tag->tag_id)}}"><i class='fa fa-tag'></i> {{$tag->name}}</a></li>
                                @endforeach
                            </ul>
                        </aside><!-- /#post-archive -->
                        <aside id="categories">
                            <header><h3>Categories</h3></header>
                            <ul class="list-links">
                                @foreach($categories as $category)
                                <li><a href="{{url('blog/categories/'.$category->slug)}}"><i class='fa fa-cubes'></i>  {{$category->name}}</a></li>
                                @endforeach
                            </ul>
                        </aside><!-- /#categories -->
                    </section><!-- /#sidebar -->
                </div><!-- /.col-md-3 -->
                <!-- end Sidebar -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "item": {
      "@id": "http://www.educationhubnepal.com/blog",
      "name": "Blogs",
      "image":""
    }
  }]
}
</script>
@stop