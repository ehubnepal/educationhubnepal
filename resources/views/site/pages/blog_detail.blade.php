
@extends('site.sitemaster')
@section('maincontent')
<style type='text/css'>
    ul.list-links{list-style-type:none}
</style>
<div id="page-content">
        <!-- Breadcrumb -->
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li><a href="{{url('blog')}}">Blog</a></li>
                <li class="active">{{$post->title}}</li>
            </ol>
        </div>
        <!-- end Breadcrumb -->

        <div class="container">
            <div class="row">
                <!-- Content -->
                <div class="col-md-9 col-sm-9">
                    <section id="content">
                        
                        <article class="blog-post">
                            <a href="{{url('blog/'.$post->slug)}}">@if(!empty($post->image))<img src="{{url('public/admin/img/blog/'.$post->image)}}">@endif</a>
                            <header><a href="{{url('blog/'.$post->slug)}}"><h2>{{$post->title}}</h2></a></header>
                            <figure class="meta">
                                <a href="#" class="link-icon"><i class="fa fa-user"></i>Education Hub Nepal</a>
                                <a href="#" class="link-icon"><i class="fa fa-calendar"></i>{{date("F jS, Y",strtotime($post->created_at))}}</a>
                                @if(!empty($post->categories))
                                <div class="tags">
                                    @foreach($post->categories as $category)
                                    <a href="{{url('blog/categories')}}" class="tag article">{{$category->name}}</a>
                                    @endforeach
                                </div>
                                @endif
                            </figure>
                           {!!$post->content !!}
                            <hr/>
                            <div id="disqus_thread"></div>
                        </article><!-- /.blog-post-listing -->
                        <!--<section id="about-author">
                            <header><h3>About the Author</h3></header>
                            <div class="post-author">
                                <img src="assets/img/member-04.jpg">
                                <div class="wrapper">
                                    <header>Maria Scott</header>
                                    <p>Phasellus metus ipsum, sollicitudin lacinia turpis in, pellentesque pulvinar diam.
                                        Cras ultricies augue sapien, aliquam hendrerit mi suscipit at. Suspendisse vulputate felis eget
                                    </p>
                                </div>
                            </div>
                        </section>
                    </section>--><!-- /#content -->
                   
                </div><!-- /.col-md-9 -->
                <!-- end Content -->

                <!-- sidebar -->
                 <div class="col-md-3 col-sm-3">
                    <section id="sidebar">
                       <!--
                        <aside id="search">
                            <header><h3>Search</h3></header>
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Enter Keyword">
                                <span class="input-group-btn"><button class="btn btn-default search" type="button"><i class="fa fa-search"></i></button></span>
                            </div>--><!-- /input-group --> 
                        </aside>
                        <aside id="post-archive">
                            <header><h3>Tags</h3></header>
                            <ul class="list-links">
                                @foreach($tags as $tag)
                                <li><a href="{{url('blog/tags/'.$tag->tag_id)}}"><i class='fa fa-tag'></i>  {{$tag->name}}</a></li>
                                @endforeach
                            </ul>
                        </aside><!-- /#post-archive -->
                        <aside id="categories">
                            <header><h3>Categories</h3></header>
                            <ul class="list-links">
                                @foreach($categories as $category)
                                <li><a href="{{url('blog/categories/'.$category->slug)}}"><i class='fa fa-cubes'></i>  {{$category->name}}</a></li>
                                @endforeach
                            </ul>
                        </aside><!-- /#categories -->
                    </section><!-- /#sidebar -->
                </div><!-- /.col-md-3 -->
                <!-- end Sidebar -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div>
    <!-- end Page Content -->

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "item": {
      "@id": "http://www.educationhubnepal.com/blog",
      "name": "Blogs",
      "image":""
    }
  },{
    "@type": "ListItem",
    "position": 2,
    "item": {
      "@id": "{{url('blog/'.$post->slug)}}",
      "name": "{{$post->title}}",
      "image":""
      }
    
  }]
}
</script>


<script>
    
    var disqus_config = function () {
        this.page.url = "{{Request::url()}}";  // Replace PAGE_URL with your page's canonical URL variable
        this.page.identifier = "Request::path()"; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
    };
    
    (function() {  // DON'T EDIT BELOW THIS LINE
        var d = document, s = d.createElement('script');
        
        s.src = '//educationhubnepal.disqus.com/embed.js';
        
        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
    @stop