@extends('site.sitemaster')
@section('maincontent')
<!-- Page Content -->
@include('site.fragments.searchbox_mini')
<div id="course-search-content">
    

   
    <section id="course-search-list" class="block">
        <div class="container">
            <header class="listing-title">
               <h2>Searching course :<span class='text-info'> {{$param['query']}}</span></h2>
            </header><!--
            <div class="leftbar col-md-3">
                <h3>Filter results</h3>
                <div class="filter-results">
                <h3>Affiliations</h3>
                    <ul>
                        <li>Pokhara University</li>
                        <li>Kathmandu University</li>
                        <li>Margarate University</li>
                    </ul>
                </div>
            </div>-->
        <!--without container-->
         @if(count($programs)<1)
            <div style="margin-bottom:10px; width:100%;border:1px dashed #ccc;padding:15px;font-size:20px;text-align: center;background:#f0f0f0">
                Sorry the course you are looking for was not found.
            </div>
            @endif
            <div class="popular-wrapper col-md-12">
                @foreach($programs as $program)
                <div class="col-md-3 col-sm-6">
                    <div class="course-card">
                                <div class="info">
                                    <p class="course-name"><a href="{{url('programs/'.$program->url)}}">{{$program->pname}}</a></p>
                                    <p class="offering-count"><a href="{{url('programs/'.$program->url.'#offeringcolleges')}}"><span>{{$program->collegecount}}</span>Offering colleges</a></p>
                                    <p class="affiliation"><a href="{{url('universities/'.$program->affiliation->url)}}"><i class="fa fa-university"></i> {{$program->affiliation->aname}}</p>
                                </div>	
                    </div><!-- /.property -->
                </div><!-- /.col-md-3 -->
                @endforeach
            </div>
            <div>{!!$programs->links()!!}</div>
            </div>
    </section>
    
</div><!-- end Page Content -->
@stop
