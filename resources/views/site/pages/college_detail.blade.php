@extends('site.sitemaster')
@section('maincontent')
<!-- Page Content -->
<style type="text/css">
    /*College detail page specific css*/
    .navigation { position: absolute; top: 0px; left: 0px; right: 0px; background: rgba(255,255,255,0); }
    .college-banner { width: 100%; height: 260px;}
    #page-content { margin-top: -100px; }
    .navigation .secondary-navigation .contact figure, 
    .navigation .secondary-navigation a, .navigation .secondary-navigation a.promoted, 
    .navigation .navbar .navbar-nav > li a, .breadcrumb li a,.breadcrumb li,.breadcrumb > .active,#brand a{color:#fff;}
</style>

<div class="college-banner">
    <div class="bg-overlay"></div>
    <?php $colors = array('225378','eb7f00','1695a3'); $randcol=$colors[array_rand($colors)]; ?>
        <img class="img-resposnsive bg-college" src="http://dummyimage.com/1200x400/<?php echo $randcol.'/'.$randcol;?>/&text=-">
    <!--<img class="img-resposnsive bg-college" alt="" src="http://static.collegedunia.com/public/college_data/images/appImage/25621_NLSIU.jpg/1024/1024">-->
</div>

<!-- Page Content -->
<div id="page-content">
    <!-- Breadcrumb -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{url('')}}">Home</a></li>
            <li class="active">{{$college->cname}}</li>
        </ol>
    </div>
    <!-- end Breadcrumb -->

    <div class="container">
        <div class="row">
            <!-- Property Detail Content -->
            <div class="col-md-12 col-sm-12">
                <section id="property-detail college-detail">
                    <header class="property-title college-title">
                        <div class="college_logo_wrap">
                            <img class="bg-logo" src="{{ url($college->full_url)}}"/>
                        </div>
                        <h1>{{{$college->cname}}}</h1>
                        <figure>
                            <span><i class="fa fa-map-marker"></i> {{$college->address}}, {{$college->district}}</span>
                            @foreach($college->affiliations as $aff)
                            <span><i class="fa fa-university"></i> {{$aff->aname}}</span>
                            @endforeach
                            <!--<span><i class="fa fa-thumb-tack"></i> ESTD 2010</span>-->
                        </figure>
                        <div class="social-share">
                            <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A//educationhubnepal.com/{{'colleges/'.$college->url}}"><i class="fa fa-facebook"></i></a>
                            <a target="_blank" href="https://twitter.com/home?status=http%3A//educationhubnepal.com/{{'colleges/'.$college->url}}"><i class="fa fa-twitter"></i></a>
                        </div>
                    </header>
                    <div class="tabbable-panel">
                        <div class="tabbable-line">
                            <ul class="nav nav-tabs ">
                                <li class="active">
                                    <a data-toggle="tab" href="#tab_default_1">Home</a>
                                </li>
                                <li>
                                    <a data-toggle="tab" id="courses" href="#tab_default_2">Courses</a>
                                </li>
                                <li>
                                    <a data-toggle="tab" id="gallery" href="#tab_default_3">Gallery</a>
                                </li>
                                <li>
                                    <a data-toggle="tab" id="reviews" href="#tab_default_4">Reviews</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                @include('site.fragments.fragment_server_message')
                                <div id="tab_default_1" class="tab-pane active">
                                    <!--/ College description -->
                                    <div class="row">
                                        <div class="col-md-4 col-sm-12">
                                            <section id="quick-summary" class="clearfix">
                                                <header><h2>Quick Summary <a href="{{url('wiki/college/'.$college->url.'/pre_edit/detail')}}"><small>[edit/improve]</small></a></h2> </header>
                                                <div class="summary-info">
                                                    <table width="100%">
                                                        <tr>
                                                            <td>Location</td>
                                                            <td>{{$college->address}}, {{$college->district}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>No. Of Programs</td>
                                                            <td><span class="tag price">{{count($college->programs)}}</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Phone:</td>
                                                            <td>{{$college->phone}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Fax:</td>
                                                            <td>{{$college->fax}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Email:</td>
                                                            <td>{{$college->email}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Website:</td>
                                                            <td>{{$college->website}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Country:</td>
                                                            <td>{{$college->country}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Total views:</td>
                                                            <td>{{$college->viewcount}}</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </section><!-- /#quick-summary -->
                                        </div><!-- /.col-md-4 -->
                                        <div class="col-md-8 col-sm-12">
                                            <section id="description">
                                             <?php /*   @if(Auth::user()) */?>
                                                @include('site.fragments.set_my_college')
                                              <?php /*  @endif */ ?>
                                                <header><h2>About {{$college->cname}} <a href="{{url('wiki/college/'.$college->url.'/pre_edit/detail')}}"><small>[edit/improve]</small></a></h2> </header>
                                                {!!$college->description !!}
                                            </section>
                                        </div>
                                    </div> <!--/ end of college description -->
                                </div>
                                <div id="tab_default_2" class="tab-pane">
                                   
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <header><h3>Offering Courses ({{count($college->programs)}})</h3></header>
                                            <section id="properties">
                                                <div class="grid">
                                                    @foreach($college->programs as $program)
                                                    <div class="individual-course col-md-9 no-padding">
                                                        <h3><a href="{{url('programs/'.$program->url)}}">{{$program->pname}}</a></h3>
                                                        <div class="program-info">
                                                            <span class="program-duration"><i class="fa fa-clock-o"></i> {{$program->duration}}</span>
                                                            <span class="program-affiliation"><i class="fa fa-university"></i> <a title="{{$program->affiliation->aname}}" href="{{url('universities/'.$program->affiliation->url)}}">{{stringFirst($program->affiliation->aname)}}</a></span>
                                                            <div class="program-categories"> <i class="fa fa-bookmark"></i>
                                                                @if(!empty($program->category))
                                                                @foreach($program->category as $cat)
                                                                <a href="{{url('categories/'.$cat->url)}}">{{$cat->category}}</a>
                                                                @endforeach
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="program-more-info">
                                                            Academic Year - <span>{{$program->academic_year}}</span>,
                                                            Cost Range - <span>
                                                                @if(!empty($program->cost_range))
                                                                {{$program->cost_range}}
                                                                @else
                                                                N/A
                                                                @endif
                                                            </span>
                                                        </div>
                                                        <div class="buttonsets">
                                                            <button data-toggle="collapse" data-target="#eli{{$program->id}}" class="btn btn-default">Check Eligibility</button>
                                                            <button class="btn btn-default" onclick="window.location.href ='{{url('programs/'.$program->url)}}';return false;">View Details</button>
                                                        </div>
                                                        <div class="eligibility collapse" id="eli{{$program->id}}">
                                                            @if(!empty($program->eligibility))
                                                            {!! $program->eligibility !!}
                                                            @else
                                                            N/A
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <!--
                                                    <div class="property masonry">
                                                         <div class="inner">
                                                             <a href="{{url('programs/'.$program->url)}}">
                                                                 <div class="property-image">
                                                                     @foreach($program->category as $cat)
                                                                     <figure class="tag status">{{$cat->category}}</figure>
                                                                     @endforeach
                                                                     <div class="overlay">
                                                                         <div class="info">
                                                                             <div class="tag price"></div>
                                                                         </div>
                                                                     </div>
                                                                 </div>
                                                             </a>
                                                             <aside>
                                                                 <header>
                                                               
                                                                     <a href="{{url('programs/'.$program->url)}}"><h3>{{$program->pname}}</h3></a>
                                                                     
                                                                 </header>
                                                                 
                                                                 <dl>
                                                                     @if(!empty($program->category))
                                                                     <dt>Categories:</dt>
                                                                     @foreach($program->category as $cat)
                                                                     <dd><a href="{{url('categories/'.$cat->url)}}">{{$cat->category}}</a></dd>
                                                                     @endforeach
                                                                     @endif
                                                                     <dt>Duration:</dt>
                                                                     <dd>{{$program->duration}}</dd>
                                                                     <dt>Academic Year:</dt>
                                                                     <dd>{{$program->academic_year}}</dd>
                                                                     <dt>Cost Range:</dt>
                                                                     @if(!empty($program->cost_range))
                                                                     <dd>{{$program->cost_range}}</dd>
                                                                     @else
                                                                     <dd>N/A</dd>
                                                                     @endif
                                                                     <dt>Affiliation:</dt>
                                                                     <dd><a title="{{$program->affiliation->aname}}" href="{{url('universities/'.$program->affiliation->url)}}">{{stringFirst($program->affiliation->aname)}}</dd>

                                                                 </dl>
                                                                 <a href="{{url('programs/'.$program->url)}}" class="link-arrow"> More</a>
                                                             </aside>
                                                         </div>
                                                     </div>-->
                                                    @endforeach
                                            </section>
                                        </div>
                                    </div>
                                </div>
                                <div id="tab_default_3" class="tab-pane">
                                    <section id="property-gallery collge-gallery">
                                        <header><h4  class="empty"> Sorry! No Images Are Available Right Now.</h4></header>
                                    </section>
                                </div>
                                <div id="tab_default_4" class="tab-pane">
                                    @include('site.fragments.college_review')
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div></div>
<!-- end Page Content -->

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "item": {
      "@id": "http://www.educationhubnepal.com/colleges",
      "name": "Colleges",
      "image":""
    }
  },{
    "@type": "ListItem",
    "position": 2,
    "item": {
      "@id": "{{url('colleges/'.$college->url)}}",
      "name": "{{$college->cname}}",
      "image":""
      }
    
  }]
}
</script>
@stop