@extends('site.sitemaster')
@section('maincontent')
    
    <div class="main-container">

        <section class="page-head bg-blue" style="height: 280px;">
            
            <div class="container vertical-center-rel">
                <div class="row">
                    
                    <div class="col-sm-12 text-left p-t-md">
                        <h1 class="h2 text-white ">User Guide</h1>
                        <p class="text-white">Education Hub Npeal</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="p-y-md">
            <div class="container">

                <div class="row">
                    <div class="col-md-3 scrollspy">
                        <ul id="side-nav" class="nav hidden-xs hidden-sm affix-top" data-spy="affix">
                            <li>
                                <a href="#start" class="smooth-scroll">Getting Started</a>
                                
                            </li>
                            <li>
                                <a href="#structure" class="smooth-scroll">How To Register</a>
                               
                            </li>
                             <li>
                                <a href="#login" class="smooth-scroll">How To Login</a>
                               
                            </li>
                            <li>
                                <a href="#pagebuilder" class="smooth-scroll">Why To Be A Member</a>
                               
                            </li>
                            <li>
                                <a href="#elements" class="smooth-scroll">How To Review A College</a>
                
                            </li>
                            <li>
                                <a href="#forms" class="smooth-scroll">How To Review A Program/Course</a>
                               
                            </li>
                            <li>
                                <a href="#credits" class="smooth-scroll">College/Initution Missing</a>
                            </li> 
                            <li>
                             <li>
                                <a href="#addcollege" class="smooth-scroll">How To Add A College</a>
                            </li> 
                            <li>
                                <a href="#outdated" class="smooth-scroll">Some Information Are Outdated Why?</a>
                            </li> 
                            <li>
                                <a href="#markcollege" class="smooth-scroll">Marking college as my past college or currently studying college</a>
                            </li>
                            <li>
                                <a href="#markprogram" class="smooth-scroll">Marking program as my past program or currently studying program.</a>
                            </li> 
                            <li>
                                <a href="#memberarea" class="smooth-scroll">Easy To Use Member Area</a>
                            </li>
                             <li>
                                <a href="#maintainer" class="smooth-scroll">How To Be A Maintainer</a>
                            </li>
                            <li>
                                <a href="#contribute" class="smooth-scroll">How does a maintainer contribute?</a>
                            </li>
                            <li>
                                <a href="#events" class="smooth-scroll">Adding A Events Of My College</a>
                            </li>
                            <li>
                                <a href="#blog" class="smooth-scroll">Why Should You Write A Blog In Education Hub Nepal</a>
                            </li>
                            <li>
                                <a href="#share" class="smooth-scroll">Why Should You Suggest This Site To Others</a>
                            </li>
                            <li>
                                <a href="#community" class="smooth-scroll">Education Hub Nepal as A Community</a>
                            </li> 
                            <li>
                                <a href="#support" class="smooth-scroll">Support</a>
                            </li>  

                        </ul>
                    </div>
                    <div class="col-md-9 doc">
                        
                        <div id="start" class="p-t">
                            <h2>Getting Started</h2>
                            <p>First of all, Thank you very much for being intrested in Education Hub Nepal, it is a community build by the people like you, thus we welcome you have a look through this user guide,
                            so that to make sure you won't stuck anywhere.
                            <p><strong>Thanks so much!</strong></p>
                            <hr>
                        </div><!-- End Getting Started -->



                        <div id="structure" class="p-t-md">
                            <h2>User Registration</h2>
                            <p>You have three different options to register with Education Hub Nepal, either sign up with email, or sign up with google or with facebook.
                                Signing up with social media(facebook/google) is easy process. You just need to choose one and allow the authentication to complete the registration process. It will take less than 20 seconds.
                                With email, you need to input your name, email & password.
                                Then you will receive a confirmation email to verify your account.</p>

                    
                        </div><!-- End Structure -->
                        <div id="login" class="p-t-md">
                            <h2> How To Login?</h2>
                            <p>To login, you must have an account already or you can just login through facebook/google. In case of having account, you can just click on the “Login” which you can see on the top menu.
                            </p>
                            </hr>
                        </div> <!--end of login -->


                        <div id="pagebuilder" class="p-t-md">
                            <h2>Why To Be A Member?</h2>
                            <p>First motto “We're all working together and that’s the secret for our success.”  You need to register to be the part of community, to edit information whatever is seen on the website. Also after signing up you will be able to  write reviews of colleges, and courses which will also  help other people choose the best.</p>

                        </div><!-- /End Page Builder -->


                        <div id="elements" class="p-t-md">
                            <h2>How To Review A College?</h2>
                            <ul>
                                <li><p>Your review can help others to know about the college. To write a review , you must login first. You can search for the college and go the detail page. There you will see a review section with a form. Write a review, give ratings and click on leave a review. Great you just helped thousands of students like you :)</p></li>
                                <li><p>After you register and verify your account, you can login in Education Hub Nepal, first you will be redirected to your dashboard, where you can update information about you, link your social accounts and many more. Then you can click on College link in the menu, and search the colleges, which you want to review. Now Go to the detail page of the college. You can also click, “I’ve studied this college” and add the colleges to your dashboard, so that it would be helpful to determine the authenticity and power of your review. </p></li>
                                <li><p>You will see Review Tab in the college detail page, click on Leave A Review, a form will be displayed, add your reviews and rating. Great you just helped thousands of students like you :)
                                </p></li>
                                <li><p>In the college detail page you will also see two buttons at the top “I am currently studying here” and “I have studied in this college” You can click on any one button that applies to you.</p></li>
                            </ul>
                            
                            <hr>

                        </div><!-- /End Elements -->

                        <div id="forms" class="p-t-md">
                            <h2>How To Review A Program/Course</h2>
                            <ul>
                                <li><p>Reviewing program is just easy like reviewing college, first make sure you’re logged in. Then Go to the detail page of your desired program. In the detail tabs, you will see, a Review tab, click on it, you will see a input form. Go ahead and add your rating and reviews. </p></li>
                                <li><p>Moreover, you can add particular course/program as your ‘Studied Program’, to empower your review. If you add the review about the program, it will help other students to decide.
                                </p></li>

                            </ul>
                            

                            <hr>


                        </div> <!-- /End Forms -->

                        <div id="credits" class="p-t-md">
                            <h2>My college is not listed. What to do?</h2>
                            <p>To add a college, you must first login to your account, in your account dashboard, you can see “Add a New College” link, just click on that link, you will be redirect to http://www.educationhubnepal.com/member/addnewcollege page. Add the required information, and hit ‘Save’ Button. The college will be reviewed by one of our admin and promptly available in the website within 30 minutes.
                            </p>
                            <hr>
                        </div><!-- /End Credits-->

                        <div id="addcollege" class="p-t-md">
                            <h2>How To Add A College</h2>
                            <p>If you’re already logged in, then you can either click on this <a href="http://www.educationhubnepal.com/member/addnewcollege">http://www.educationhubnepal.com/member/addnewcollege</a> link, or go to your user dashboard and click on “Add A New College”, fill the required form, and hit save. You just helped Education Hub Nepal community to expand. Good job!  </p>
                        </div>
                        <div id="outdated" class="p-t-md">
                            <h2>Some information are outdated. Why? </h2>
                            <p>
                                If you see any outdated information, you can first login to Education Hub Nepal, and navigate to that page, their at the top of every content, you will see, ‘Edit/Improve’, click on that . A New Wiki Edit page will appear, do your changes and hit save. Your edits will be reviewed by our other members and will be published promptly. See, making the world a better place is not that hard!.<i> By doing that, you are doing great job, because you’re You just helped thousands of students seeking right information.</i>
                            </p>
                        </div>
                        <div id="markcollege" class="p-t-md">
                            <h2>Marking college as my past college or currently studying college</h2>
                            <p>If you’re logged in, then you will see “I’m Currently studying this college”  and “I’ve Studied in this College” button inside the college detail page. If it’s your past college, click on “I’ve studied in this College”, or if you’re studying on that college, click on “I’m studying at this College”. The blue button will turn into green. You can see your colleges in your dashboard.</p>
                        </div>
                        <div id="markprogram" class="p-t-md">
                            <h2>Marking program as my past program or currently studying program</h2>
                            <p>
                                If you’re logged in, then you will see “I’m Currently studying this Program”  and “I’ve Studied this Program” button inside every program detail page. If it’s your past program , click on “I’ve studied this Program”, or if you’re studying on that program, click on “I’m studying this Program”. The blue button will turn into green. Great, you just added program in your dashboard. 
                            </p>
                        </div>
                        <div id="memberarea" class="p-t-md">
                            <h2>Easy To Use Member Area</h2>
                            <p>Education Hub Nepal provides very powerful and easy to use user dashboard, in which users can see everything related to their account and activity. As a first step, after registration, you can update the information about you and  link  your social profile to Education Hub Nepal account. We sync your activities and show them into your dashboard. Do know that, your information is safe with us and will be kept private. </p>
                            <ul>
                                <li>The Review section will display your reviews, the college reviews tab will display your reviewed college names and reviews, whereas programs review will show your reviewed program name and reviews.</li>
                                <li>My Education section will list your past, and currently studying colleges as well as programs. </li>
                                <li>Add A New College section will let you add any new college or missing college in Education Hub Nepal.</li>
                                <p>After registration, you will be able to use our Wiki feature, which will let you edit/improve any information inside Education Hub Nepal. You will be able to add your past/present college/programs as well as post a blog post. Moreover if you want to volunteer, you can submit a request to be a maintainer.</p>
                                
                            </ul>
                        </div>
                        <div id="maintainer" class="p-t-md">
                            <h2>What Is A Maintainer?</h2>
                            <p>Maintainer is a superuser who is responsible to maintain a particular college/programs. As a maintainer, he/she will have the ability to update the information, and publish news about the assigned college/programs and also the ability to upload images. Moreover institution themselves can be a maintainer and maintain their subsequent institutions and programs. 
                            </p>
                        </div>
                        <div id="contribute" class="p-t-md">
                            <p>A Maintainer will  be given a super user dashboard, in which he/she will have full privilege of adding/removing/editing information of assigned college/programs . And Maintainer and contribute Education Hub Nepal Community by working closely with admin. A Maintainer can create/publish news. 
                            </p>
                        </div>
                        <div id="events" class="p-t-md">
                            <p>A Member/Maintainer can add events of the college in Education Hub Nepal, thus to help everyone by notifying.</p>
                        </div>
                        <div id="blog" class="p-t-md">
                            <h2>Why Should You Write Blogs In Education Hub Nepal </h2>
                            <p>Education Hub Nepal, is a community of students and institutions working together to acquire and deliver right information. Thus if you have any interesting news/experience/story/tips, that might help the community, please add/request a new blog post.</p>

                        </div>
                        <div id="share" class="p-t-md">
                            <h2>Suggest this website to you friend. Why should you do that?
                                </h2>
                            <p>Education Hub Nepal is powered by community, and it has information of as many institutions and programs that exists in Nepal. Not only that our community gives continuous updates through blog. Students can find out information about college, admission, exams, preparation and many more just by visiting our site. Thus you should suggest this website to help your friends.
                                </p>
                        </div>
                        <div id="community" class="p-t-md">
                            <h2>Concept of community and how powerful a community driven service/ application can be.</h2>
                            <p>Education Hub Nepal is totally powered by community, because without the contribution of community, we can’t alone keep the authenticity of information that are being delivered. Currently over 200 members has already joined our community, and the number is growing day by day.</p>
                            <br/>
                            <p>Site like wikipedia, which powers the largest information in the web, is a community, thus to make information authentic, informative and accurate, and we believe in same thing. </p>
                        </div>

                        <div id="support" class="p-t-md">
                            <h2>Support</h2>
                            <p>We truly appreciate that you've been one of the member of Education Hub Nepal Community. Helping students/parents choosing the programs/colleges/perparation/initutions is our goal. Our Community provide friendly and helpful support so don't hesitate to ask us anything!</p>
                            <ul class="list-unstyled">
                                <li><strong>Contact Us:</strong> <a href="http://www.educationhubnepal.com/contact">Contact Education Hub Nepal</a></li>
                                <li><strong>Send us an Email:</strong> <a href="mailto:info@educationhubnepal.com">info@educationhubnepal.com</a></li>
                            </ul>
                        </div><!-- /End Support -->
                        <hr>
                        <h2>Thank You Very Much For Being A Member Of Our Community!</h2>
                        <div class="p-y-lg"></div>
                    

                    </div><!-- /End Col Doc -->
                </div>
            </div>
        </section>

        


             
    </div>
    </div>
    </div>    
        


    </div><!-- /End Main Container-->


    <!-- =========================
         SCRIPTS 
    ============================== -->
   
    <script src="public/site/js/jquery.easing.1.3.min.js"></script>
    <script src="public/site/js/jquery.formchimp.min.js"></script>
    <!-- Custom Script -->
    <script src="public/site/js/custom-help.js"></script>
      <!-- CUSTOM STYLESHEET -->
    <link rel="stylesheet" href="public/site/css/help/style.css">

    <!-- RESPONSIVE FIXES -->
    <link rel="stylesheet" href="public/site/css/help/responsive.css">
    
    <script>
        $('#side-nav').affix({
            offset: {
                top: $('#side-nav').offset().top
            }
        });


    </script>
@stop