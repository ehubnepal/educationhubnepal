@extends('site.sitemaster')
@section('maincontent')
<div class="container">
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Member</a></li>
        <li class="active">Create an Account</li>
    </ol>
</div>
<!-- end Breadcrumb -->

<div class="container">
    <header><h1>Create an Account</h1></header>
    <div class="row">
        @include('site.fragments.fragment_server_message')
        @include('site.forms.form_signup')
        <div class="off-focus">@include('site.forms.form_login')</div>
    </div><!-- /.row -->
</div><!-- /.container -->
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "item": {
      "@id": "http://www.educationhubnepal.com/signup",
      "name": "Sign Up",
      "image":""
    }
  }]
}
</script>
@stop