@extends('site.sitemaster')
@section('maincontent')
<!-- Page Content -->
<div id="page-content">
    <!-- Breadcrumb -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{url('')}}">Home</a></li>
            <li class="active">FAQ</li>
        </ol>
    </div>
    <!-- end Breadcrumb -->

    <div class="container">
        <div class="row">
            <!-- Property Detail Content -->
            <div class="col-md-12 col-sm-12">
                <section id="property-detail college-detail">
                    <header class="">
                        <h1>Frequently Asked Questions & Answers</h1>
                    </header>
                    <br/>
                    <!-- starts accordion -->
                            <div class="panel-group faq-listing" id="accordion" role="tablist" aria-multiselectable="true">
                                          <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                              <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                  i) What is EducationHubNepal?
                                                </a>
                                              </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                              <div class="panel-body">
                                                EducationHubNepal is a search portal for  courses, colleges, programs and universities which are available in Nepal, our goal is to help students and parents discover right information about available education choices. 
                                              </div>
                                            </div>
                                          </div>
                                          <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingTwo">
                                              <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                  ii) How Much Information Does EducationHubNepal Currently Have?
                                                </a>
                                              </h4>
                                            </div>
                                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                              <div class="panel-body">
                                                We have information about more than 1600 colleges and 800 courses, and we’re updating our data every single day, keep checking our site to know about the updates. 
                                              </div>
                                            </div>
                                          </div>
                                          <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingThree">
                                              <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                  iii) Does EducationHubNepal has information about Test/Exams? 
                                                </a>
                                              </h4>
                                            </div>
                                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                              <div class="panel-body">
                                                We’re working with the data regarding various universities test like CMAT, KUMAT, SOMAT, MBBS, IOE and many more, please keep checking our website for the updates. 
                                              </div>
                                            </div>
                                          </div>

                                          <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingFour">
                                              <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                                  iv) How EducationHubNepal helps Students and parents?  
                                                </a>
                                              </h4>
                                            </div>
                                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                              <div class="panel-body">
                                                We provide information about every study choices, programs, universities, colleges, fees and many more, and we help student to choose their dream courses, and help parents to have right information at right time.  
                                              </div>
                                            </div>
                                          </div>

                                          <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingFive">
                                              <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                                  v) I’m a student, How can I contribute to EducationHubNepal?  
                                                </a>
                                              </h4>
                                            </div>
                                            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                              <div class="panel-body">
                                                If you’re a student, studying at a college, or engaged with a program, you can help us improve the information about college and program. You can update the the updates of colleges in college news section, and add reviews about college and programs. If you’re a past student, then you can also help us, enhance the information regarding colleges, programs and affiliation by adding reviews and rating and suggesting the edit. By doing this, you will help everyone have right information.
                                              </div>
                                            </div>
                                          </div>

                                          <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingSix">
                                              <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                                  vi) Why Should I be a Member in EducationHubNepal?  
                                                </a>
                                              </h4>
                                            </div>
                                            <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                              <div class="panel-body">
                                                After being a member, you will get chance to add your reviews, update information, suggest updates and add your education and programs to your profile. That way you will help everyone to have uptodate and correct information about education. 
                                              </div>
                                            </div>
                                          </div>

                                          <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="heading7">
                                              <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false" aria-controls="collapse7">
                                                  vii) How Can I join EducationHubNepal?
  
                                                </a>
                                              </h4>
                                            </div>
                                            <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
                                              <div class="panel-body">
                                                You can register from your email address, or can login from your google or facebook account, registering with EHN is a matter of few seconds, however features that you will get after that are awesome.
                                              </div>
                                            </div>
                                          </div>


                                          <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="heading8">
                                              <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse8" aria-expanded="false" aria-controls="collapse8">
                                                 viii) I’m a representative/stakeholder of an intuition, what can I do in EducationHubNepal?  
                                                </a>
                                              </h4>
                                            </div>
                                            <div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8">
                                              <div class="panel-body">
                                                If you’re a representative or stakeholder of an intuition, you can improve your presence inside EHN, by giving continuous update about your intuition and updating the existing information.
                                              </div>
                                            </div>
                                          </div>


                                          <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="heading9">
                                              <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse9" aria-expanded="false" aria-controls="collapse9">
                                                  ix) I want to volunteer EducationHub Nepal, What should I do? 
                                                </a>
                                              </h4>
                                            </div>
                                            <div id="collapse9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading9">
                                              <div class="panel-body">
                                                If you just want to volunteer for improving the information on our website, then you can register an account, and keep improving it, all the improvements made by you, will be credited  to you. Or If you want to be a special volunteer, working closely with EHN team, you can contact us from our contact us page. 

                                              </div>
                                            </div>
                                          </div>


                                          <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="heading10">
                                              <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse10" aria-expanded="false" aria-controls="collapse10">
                                                  x) Why Should I join and volunteer EHN?   
                                                </a>
                                              </h4>
                                            </div>
                                            <div id="collapse10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading10">
                                              <div class="panel-body">
                                                We’re working for a national cause, everybody has right of right information, and in the present context, it’s very hard for students and parents to have right information about colleges, courses and affiliations. Thus when you join, and improve the information inside EHN it will help everyone to have correct information. 
                                              </div>
                                            </div>
                                          </div>

                                          <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="heading11">
                                              <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse11" aria-expanded="false" aria-controls="collapse11">
                                                  xi) Why Should I review course/college in EHN?  
                                                </a>
                                              </h4>
                                            </div>
                                            <div id="collapse11" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11">
                                              <div class="panel-body">
                                                Your reviews will help students like you, parents like yours to decide what’s a good choice, and what’s bad among available options of colleges and programs. Thus your review means a lot, keep reviewing. 
                                              </div>
                                            </div>
                                          </div>
                                          <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="heading12">
                                              <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse12" aria-expanded="false" aria-controls="collapse12">
                                                  xii) Can I copy and reuse data from EducationHubNepal? 
                                                </a>
                                              </h4>
                                            </div>
                                            <div id="collapse12" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading12">
                                              <div class="panel-body">
                                                Although all the information inside EducationHubNepal website is public, no one can copy and reuse/republish the information from EducationHubNepal. Our data are strictly protected by law, read more in Privacy Policy and Terms And Condition for more information. 
                                              </div>
                                            </div>
                                          </div>
                                           <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="heading13">
                                              <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse13" aria-expanded="false" aria-controls="collapse13">
                                                 xiii) How many active volunteers are currently in EducationHubNepal? 
                                                </a>
                                              </h4>
                                            </div>
                                            <div id="collapse13" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading13">
                                              <div class="panel-body">
                                                Currently we have more than 200 volunteers who are using and updating EducationHubNepal, if you wanna be one of them then join us today and help everyone have correct education information.  
                                              </div>
                                            </div>
                                          </div>


                                        </div>

            </section>
        </div>
    </div>
</div>
</div>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "item": {
      "@id": "http://www.educationhubnepal.com/faq",
      "name": "FQQ",
      "image":""
    }
  }]
}
</script>
@stop