@extends('site.sitemaster')
@section('maincontent')
<style>
    .programwiki{display:none}
</style>
<!-- Page Content -->
<div id="page-content">
    <!-- Breadcrumb -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="{{url('programs/')}}">Programs</a></li>
            <li class="active">{{$program->pname}}</li>
        </ol>
    </div>
    <!-- end Breadcrumb -->

    <div class="container">
        <div class="row">
            <!-- Agent Detail -->
            <div class="col-md-9 col-sm-9">
                <section id="agent-detail">
                    <header><h1>{{$program->pname}}</h1></header>
                    @include('site.fragments.set_my_program')
                    <section id="agent-info">
                        <div class="row">
                            <!--
                            <div class="col-md-3 col-sm-3">
                                <figure class="agent-image"><img src="http://placehold.it/350x350?text=Program"></figure>
                            </div><!-- /.col-md-3 -->
                            @include('site.fragments.fragment_server_message')
                            <div class="col-md-12 col-sm-12 program-info-top">
                                <h3>Program Info <small class="programwiki"><a href='' style='color:white'>[ edit/improve ]</a></small></h3>
                                <div class="top-info-wrapper">
                                    <div class="col-md-7">
                                        <dl>
                                            <dt>Program Name:</dt>
                                            <dd>{{$program->pname}}</dd>
                                            <dt>Level:</dt>
                                            <dd>
                                                @if(isset($program->level->lname))
                                                    {{$program->level->lname}}
                                                    @else
                                                    N/A
                                                @endif
                                            </dd>
                                            <dt>Affiliated To:</dt>
                                            <dd><a href="{{$program->affiliation->url}}">{{$program->affiliation->aname}}</a></dd>
                                            @if(!empty($program->facutly))
                                            <dt>Facutly:</dt>
                                            <dd><a href="{{$program->faculty->url}}">{{$program->facutly->faculty}}</a></dd>
                                            @endif
                                            @if(!empty($program->category))
                                            <dt>Categories:</dt>
                                            @foreach($program->category as $cat)
                                            <dd><a href="{{url('categories/'.$cat->url)}}">{{$cat->category}}</a></dd>
                                            @endforeach
                                            @endif
                                        </dl>
                                    </div>
                                    <div class="col-md-5">
                                        <dl>
                                            <dt>Program Duration:</dt>
                                            <dd>{{$program->duration}}</dd>
                                            <dt>Academic Year:</dt>
                                            <dd>{{$program->academic_year}}</dd>
                                            <dt>Evaluation Method:</dt>
                                            <dd>{{strtoupper($program->evaluation_method)}}</dd>
                                            <dt>Cost Range:</dt>
                                            @if(!empty($program->cost_range))
                                            <dd>{{$program->cost_range}}</dd>
                                            @else
                                            <dd>Unavailable</dd>
                                            @endif
                                        </dl>
                                    </div>
                                </div>
                            </div><!-- /.col-md-4 -->
                        </div><!-- /.row -->

                    </section><!-- /#agent-info -->
                    <!-- course info more tab -->
                    <hr class="thick">
                    <section id="course-info">
                        <header><h3>Detail Inforamation</h3></header>
                        <div class="row">
                            <div>

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Course Description</a></li>
                                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Eligibility</a></li>
                                    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Curriculam</a></li>
                                    <li role="presentation"><a href="#reviews" aria-controls="reviews" role="tab" data-toggle="tab" >Reviews</a> </li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane in active" id="home">
                                        <a href="#" class="programwiki">[<i class="fa fa-pencil"></i> edit]</a>
                                        {!! $program->course_description !!}
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="profile">
                                        <a href="#" class="programwiki">[<i class="fa fa-pencil"></i> edit]</a>
                                        {!! $program->eligibility !!}
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="messages">
                                        <a href="#" class="programwiki">[<i class="fa fa-pencil"></i> edit]</a>
                                        {!! $program->curriculum !!}
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="reviews">@include('site.fragments.program_review')</div>
                                </div>

                            </div>

                        </div><!-- end row-->
                    </section>



                    <!--end of course info tab -->
                    <hr class="thick">
                    <!--
                    <section id="agent-properties">
                        <header><h3>Offering Colleges ({{$program->college_count}})</h3></header>
                        <div class="layout-expandable">
                            
                            <div class="row">
                                    @foreach($program->college as $college)
                                <div class="col-md-4 col-sm-4">
                                    <div class="property">
                                        <a href="{{$college->url}}">
                                            <div class="property-image">
                                                <img alt="" src="{{$college->full_url}}">
                                            </div>
                                            <div class="overlay">
                                                <div class="info">
                                                    
                                                    <h3>{{$college->cname}}</h3>
                                                    <figure>{{$college->address}}</figure>
                                                </div>
                                                <ul class="additional-info">
                                                    <li>
                                                        <header>Phone:</header>
                                                        <figure>{{$college->phone}}</figure>
                                                    </li>
                                                    <li>
                                                        <header>Email:</header>
                                                        <figure>{{$college->email}}</figure>
                                                    </li>
                                                    <li>
                                                        <header>Detail:</header>
                                                        <figure><a href="{{url('colleges/'.$college->url)}}">Go</a></figure>
                                                    </li>
                                                  
                                                </ul>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            @endforeach 
                            </div>
                        </div>
                        <div class="center">
                            <span class="show-all">Show All Properties</span>
                        </div>
                    </section><!-- /#agent-properties -->

                </section><!-- /#agent-detail -->
                <header id="offeringcolleges"><h3>Offering Colleges ({{$program->college_count}})</h3></header>
                <section class="offering-colleges row">
                    <div class="popular-wrapper row">
                        @foreach($program->college as $college)
                        <div class="col-md-4 col-sm-6">
                            <div class="property">
                                <a href="{{url('colleges/'.$college->url)}}">
                                    <div class="property-image">
                                    <?php $colors = array('225378','eb7f00','1695a3'); ?>
                                    <img src="http://dummyimage.com/600x400/<?php echo $colors[array_rand($colors)];?>/ffffff&text={{$college->cname}}">

                                    </div>
                                    <div class="overlay">
                                        <div class="info">
                                            <div class="college-logo">
                                                @if($college->full_url !=='')
                                                <img src="{{url($college->full_url)}}" alt="{{$college->cname}}"/>
                                                @else
                                                <img src="{{ url('public/site/img/ehublogo.png')}}"/>
                                                @endif
                                            </div>
                                            <h3>{{$college->cname}}</h3>
                                            <figure><i class="fa fa-map-marker"></i> {{$college->address}}</figure>
                                        </div>
                                    </div>
                                </a>
                            </div><!-- /.property -->
                        </div><!-- /.col-md-3 -->
                        @endforeach
                    </div>
                </section>
            </div><!-- /.col-md-9 -->
            <!-- end Agent Detail -->

            <!-- sidebar -->
            <div class="col-md-3 col-sm-3">
                <section id="sidebar">
                    <aside id="edit-search">
                        <header><h3>Search Other Courses</h3></header>
                        <form role="form" id="form-sidebar" class="form-search" action="{{url('search')}}">
                            <div class="form-group">
                                <select name="searchby">

                                    <option value="program">Course</option>
                                    <option value="college">College</option>
                                </select>
                            </div><!-- /.form-group -->
                            <div class="form-group">
                                <input type="text" placeholder="Keyword" name="query" value="<?php isset($param['query']) ? print $param['query'] : ''; ?>"/>
                            </div><!-- /.form-group -->
                            <div class="form-group">
                                <button type="submit" class="btn btn-default">Search Now</button>
                            </div><!-- /.form-group -->
                        </form><!-- /#form-map -->
                    </aside><!-- /#edit-search -->
                    <aside id="featured-properties">
                        <header><h3>Related Programs</h3></header>
                        @if(!empty($program->related))
                        @foreach($program->related as $related)
                        <div class="property small">
                            <a href="{{url('programs/'.$related->url)}}">
                                <div class="property-image">
                                    <img alt="" src="http://placehold.it/80x80?text={{$related->pname}}">
                                </div>
                            </a>
                            <div class="info">
                                <a href="{{url('programs/'.$related->url)}}"><h4>{{$related->pname}}</h4></a>
                                <figure>{{$related->affiliation->aname}} </figure>

                            </div>
                        </div><!-- /.property -->
                        @endforeach
                        @endif
                    </aside><!-- /#featured-properties -->
                </section><!-- /#sidebar -->
            </div><!-- /.col-md-3 -->
            <!-- end Sidebar -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div>
<!-- end Page Content -->
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "item": {
      "@id": "http://www.educationhubnepal.com/programs",
      "name": "Colleges",
      "image":""
    }
  },{
    "@type": "ListItem",
    "position": 2,
    "item": {
      "@id": "{{url('programs/'.$program->url)}}",
      "name": "{{$program->pname}}",
      "image":""
      }
    
  }]
}
</script>
@stop