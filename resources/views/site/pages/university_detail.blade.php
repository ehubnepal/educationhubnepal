@extends('site.sitemaster')
@section('maincontent')
<!-- Page Content -->
<style type="text/css">
    /*College dedtail page specific css*/
    .navigation { position: absolute; top: 0px; left: 0px; right: 0px; background: rgba(255,255,255,0); }
    .college-banner { width: 100%; height: 260px;}
    #page-content { margin-top: -100px; }
    .navigation .secondary-navigation .contact figure, 
    .navigation .secondary-navigation a, .navigation .secondary-navigation a.promoted, 
    .navigation .navbar .navbar-nav > li a, .breadcrumb li a,.breadcrumb li,.breadcrumb > .active,#brand a{color:#fff;}
</style>


<div class="college-banner">
    <div class="bg-overlay"></div>
    @if($university->cover_image)
    	<img class="img-resposnsive bg-college" src="{{url('public/site/img/university/cover/'.$university->cover_image)}}">
    @else
    	<?php $colors = array('225378','eb7f00','1695a3'); $randcol=$colors[array_rand($colors)]; ?>
        <img class="img-resposnsive bg-college" src="http://dummyimage.com/1200x400/<?php echo $randcol.'/'.$randcol;?>/&text=-">
    @endif
    <!--<img class="img-resposnsive bg-college" alt="" src="http://static.collegedunia.com/public/college_data/images/appImage/25621_NLSIU.jpg/1024/1024">-->
</div>

<!-- Page Content -->
    <div id="page-content">
        <!-- Breadcrumb -->
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{url('')}}">Home</a></li>
                <li><a href="{{url('universities')}}">Universities</a></li>
                <li class="active">{{$university->aname}}</li>
            </ol>
        </div>
        <!-- end Breadcrumb -->

        <div class="container">
            <div class="row">
                <!-- Property Detail Content -->
                <div class="col-md-12 col-sm-12">
                    <section id="property-detail college-detail">
                        <header class="property-title college-title">
                            <div class="college_logo_wrap">
                            	@if($university->thumb_url !=='')
                                     <img class="bg-logo" src="{{ url('public/site/img/university/logo/'.$university->thumb_url)}}"/>
                                @else
                                    <img class="bg-logo" src="{{ url('public/site/img/ehublogo.png')}}"/>
                                @endif
                            </div>
                            <h1>{{$university->aname}}</h1>
                            <figure>
                              <span><i class="fa fa-university"></i></i> {{substr($university->established,0,4)}}</span>
                              
                              <span><i class="fa fa-font-awesome"></i> {{count($university->program)}} Programs</span>
                                <span><i class="fa fa-home"></i> {{$university->college}} Colleges</span>
                              
                              <!--<span><i class="fa fa-thumb-tack"></i> ESTD 2010</span>-->
                            </figure>
                            <div class="social-share">
                            	<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A//educationhubnepal.com/{{'universities/'.$university->url}}"><i class="fa fa-facebook"></i></a>
                            	<a target="_blank" href="https://twitter.com/home?status=http%3A//educationhubnepal.com/{{'university/'.$university->url}}"><i class="fa fa-twitter"></i></a>
                            </div>
                        </header>
                        <div class="tabbable-panel">
							<div class="tabbable-line">
								<ul class="nav nav-tabs ">
									<li class="active" id="fir">
										<a data-toggle="tab" href="#tab_default_1">Home</a>
									</li>
									
									<li id="cor">
										<a id="course" data-toggle="tab" href="#courses">Courses</a>
									</li>
								
									<li>
										<a id="gallery" data-toggle="tab" href="#tab_default_3">Gallery</a>
									</li>
								</ul>
								<div class="tab-content">
									<div id="tab_default_1" class="tab-pane active">
										<!--/ College description -->
										<div class="row">
											<div class="col-md-4 col-sm-12">
				                                <section id="quick-summary" class="clearfix">
				                                    <header><h2>Quick Summary</h2></header>
				                                    <div class="summary-info">
					                                    <table width="100%">
					                                    	<tr>
					                                    		<td>University Type</td>
					                                    		<td>{{$university->education_body_type}}</td>
					                                    	</tr>
					                                    	<tr>
					                                    		<td>Affiliated Collges</td>
					                                    		<td><span class="tag price">{{$university->college}}</span></td>
					                                    	</tr>
					                                    	<tr>
					                                    		<td>Established:</td>
					                                    		<td>{{substr($university->established,0,4)}}</td>
					                                    	</tr>
					                                    	<tr>
					                                    		<td>Language Of Instruction:</td>
					                                    		<td>{{$university->language_of_education}}</td>
					                                    	</tr>
					                                    	<tr>
					                                    		<td>Offering Programs</td>
					                                    		<td><span class="tag price">{{count($university->program)}}</span></td>
					                                    	</tr>
					                                    </table>
					                                </div>
				                                </section><!-- /#quick-summary -->
				                            </div><!-- /.col-md-4 -->
											<div class="col-md-8 col-sm-12">
				                                <section id="description">
				                                    <header><h2>About {{$university->aname}}</h2></header>
				                                    {!!$university->description !!}
				                                </section>
				                            </div>
			                        	</div> <!--/ end of college description -->
									</div>
									<div id="courses" class="tab-pane">
										<div class="row">
										<div class="col-md-12 col-sm-12">
										<header><h3>Offering Courses ({{count($university->program)}})</h3></header>
										<section id="properties">
											 <div class="grid">
											 	@foreach($university->program as $program)
							                       	<div class="individual-course col-md-9 no-padding">
							                       		<h3><a href="{{url('programs/'.$program->url)}}">{{$program->pname}}</a></h3>
							                       		<div class="program-info">
							                       			<span class="program-duration"><i class="fa fa-clock-o"></i> {{$program->duration!=''?$program->duration:'N/A'}}</span>
							                       			<div class="program-categories"> <i class="fa fa-bookmark"></i>
							                       				@if(!empty($program->category))
							                                        @foreach($program->category as $cat)
							                                        	<a href="{{url('categories/'.$cat->url)}}">{{$cat->category}}</a>
							                                        @endforeach
							                                    @else
							                                    	N/A
						                                        @endif
							                       			</div>
							                       		</div>
							                       		<div class="program-more-info">
							                       			Academic Year - <span>{{$program->academic_year!=''?$program->academic_year:'N/A'}}</span>,
							                       			Cost Range - <span>
							                                        @if(!empty($program->cost_range))
							                                        {{$program->cost_range}}
							                                        @else
							                                        N/A
							                                        @endif
							                                    	</span>
							                       		</div>
							                       		<div class="buttonsets">
							                       			<button data-toggle="collapse" data-target="#eli{{$program->id}}" class="btn btn-default">Check Eligibility</button>
							                       			<button class="btn btn-default" onclick="window.location.href='{{url('programs/'.$program->url)}}';return false;">View Details</button>
							                       		</div>
							                       		<div class="eligibility collapse" id="eli{{$program->id}}">
							                       			@if(!empty($program->eligibility))
							                       			{!! $program->eligibility !!}
							                       			@else
							                       				N/A
							                       			@endif
							                       		</div>
							                       </div>
						                       <!--
						                       <div class="property masonry">
						                            <div class="inner">
						                                <a href="{{url('programs/'.$program->url)}}">
						                                    <div class="property-image">
						                                    	@foreach($program->category as $cat)
						                                        <figure class="tag status">{{$cat->category}}</figure>
						                                        @endforeach
						                                        <div class="overlay">
						                                            <div class="info">
						                                                <div class="tag price"></div>
						                                            </div>
						                                        </div>
						                                    </div>
						                                </a>
						                                <aside>
						                                    <header>
						                                  
						                                        <a href="{{url('programs/'.$program->url)}}"><h3>{{$program->pname}}</h3></a>
						                                        
						                                    </header>
						                                    
						                                    <dl>
						                                    	@if(!empty($program->category))
						                                        <dt>Categories:</dt>
						                                        @foreach($program->category as $cat)
						                                        <dd><a href="{{url('categories/'.$cat->url)}}">{{$cat->category}}</a></dd>
						                                        @endforeach
						                                        @endif
						                                        <dt>Duration:</dt>
						                                        @if(!empty($program->duration))
						                                        <dd>{{$program->duration}}</dd>
						                                        @else
						                                        <dd>N/A</dd>
						                                        @endif
						                                        <dt>Academic Year:</dt>
						                                        @if(!empty($program->academic_year))
						                                        <dd>{{$program->academic_year}}</dd>
						                                        @else
						                                        <dd>N/A</dd>
						                                        @endif
						                                        <dt>Cost Range:</dt>
						                                        @if(!empty($program->cost_range))
						                                        <dd>{{$program->cost_range}}</dd>
						                                        @else
						                                        <dd>N/A</dd>
						                                        @endif
						                                     

						                                    </dl>
						                                    <a href="{{url('programs/'.$program->url)}}" class="link-arrow"> More</a>
						                                </aside>
						                            </div>
						                        </div>-->
						                        @endforeach
										</section>
									</div>
								</div>
							</div>
									<div id="tab_default_3" class="tab-pane">
										<section id="college-gallery">
				                           <header><h4> Sorry! No Images Are Available Right Now.</h4></header>
				                        </section>
									</div>
								</div>
							</div>
						</div>
					</section>
    			</div>
		</div>
	</div></div>
    <!-- end Page Content -->
    <script type ="text/javascript">
if(window.location.hash !==''){
	if(window.location.hash ==='#courses'){
		var cor = document.getElementById('cor');
		var fir = document.getElementById('fir')
		fir.className ='dummy';
		cor.className +=' active';
	}
}


</script>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "item": {
      "@id": "http://www.educationhubnepal.com/universities",
      "name": "Universities",
      "image":""
    }
  },{
    "@type": "ListItem",
    "position": 2,
    "item": {
      "@id": "{{url('universities/'.$university->url)}}",
      "name": "{{$university->aname}}",
      "image":""
      }
    
  }]
}
</script>

@stop

