@extends('site.sitemaster')
@section('maincontent')
<!-- Page Content -->

<!-- Page Content -->
<div id="page-content" class="course-listing">
    <!-- Breadcrumb -->
<!--    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{url('')}}">Home</a></li>
            <li class="active">All Courses</li>
        </ol>
    </div>-->
    <!-- end Breadcrumb -->
 @include('site.fragments.searchbox_mini')
                    <br/>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <section id="property-detail" class="course-listing-section">
                   
                    <header class="property-title">
                        <h1>All Courses Available In Nepal</h1>

                        <div class="social-share">
                            <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A//educationhubnepal.com/{{'colleges/'}}"><i class="fa fa-facebook"></i></a>
                            <a target="_blank" href="https://twitter.com/home?status=http%3A//educationhubnepal.com/{{'colleges/'}}"><i class="fa fa-twitter"></i></a>
                        </div>
                    </header>

                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="row">
                        <div class="col-md-8">
                        <?php $counterOuter = 0; ?>
                        <?php $counterInner = 200; ?>
                        <?php foreach ($categories as $category): 
                            if($category->program_count<1): continue; endif;?>
                            <div class="no-padding-mobile">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne<?php echo $counterOuter; ?>">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?php echo $counterOuter; ?>"  aria-expanded="false" aria-controls="collapseOne<?php echo $counterOuter; ?>">
                                            {{$category->category}} <small>({{$category->program_count}} program<?php if($category->program_count>1): echo 's'; endif; ?> )</small> <span><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne<?php echo $counterOuter; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne<?php echo $counterOuter;?>">
                                    <div class="panel-body">
                                        <!-- inner tab -->
                                        <div class="panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
                                            
                                            <?php foreach ($category->programs as $level => $programs): ?>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab" id="headingOne<?php echo $counterInner;?>">
                                                        <h4 class="panel-title">
                                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne<?php echo $counterInner;?>" aria-expanded="false" aria-controls="collapseOne<?php echo $counterInner;?>">
                                                                {{$level}} <small>({{count($programs)}} program<?php if(count($programs)>1): echo 's'; endif; ?> ) </small> <span><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne<?php echo $counterInner;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne<?php echo $counterInner;?>">
                                                        <div class="panel-body">
                                                            @if(!empty($programs))
                                                            <ul>
                                                            @foreach($programs as $program)
                                                            <li><a href="{{url('programs/'.$program->url)}}">{{$program->pname}}</a></li>
                                                            @endforeach
                                                            </ul>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php $counterInner++; ?>
                                            <?php endforeach; ?>

                                        </div>
                                        <!-- end inner tab -->
                                    </div>
                                </div>
                            </div>
                        </div>
                            <?php $counterOuter++; ?>
                        <?php endforeach; ?>
                        </div>
                        <div class="col-md-4 courses-by-level">
                            <h3>Or, browse by level</h3>
                                @foreach($levels as $level)
                                        <div class="col-md-12">
                                            <div class="triangle-top"></div>
                                            <div class="individual-level">
                                                <a class="level_name" href="{{url('levels/'.$level->url)}}">{{$level->lname}}</a>
                                                <span class="heading">No. Of Programs ({{$level->count}})</span>
                                                <a class="bottom-nav-tab link-arrow" href="{{url('levels/'.$level->url)}}">View detail</a>
                                            </div>
                                        </div>
                                   @endforeach
                        </div>
                        </div><!--/ row -->
                    </div>
                </section>
                <div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "item": {
      "@id": "http://www.educationhubnepal.com/programs",
      "name": "Programs",
      "image":""
    }
  }]
}
</script>
@stop