@extends('site.sitemaster')
@section('maincontent')
<!-- Page Content -->
<div id="page-content">
            <!-- Breadcrumb -->
            <div class="container">
                <ol class="breadcrumb">
                    <li><a href="{{url('')}}">Home</a></li>
                    <li class="active">University Listing</li>
                </ol>
            </div>
            <!-- end Breadcrumb -->
            <div class="container">
                <div class="row">
                    <!-- Results -->

                    <div class="col-md-12 col-sm-12 universities-listing">
                        <a name ="national"></a>
                        <div id="#national">
                            <header><h1>National Universities in Nepal</h1></header>
                            <section id="College-listing">
                                <div class="row">
                                	@foreach($nationaluniversities as $university)
                                    <div class="col-md-4 col-sm-6">
                                        <div class="grid_listing_college_snippet">
                                            <div class="snippet-top">
                                                <div class="div-container">
                                                    @if($university->cover_image)
                                                    <img src="{{url('public/site/img/university/cover/'.$university->cover_image)}}">
                                                    @else
                                                    <img src="{{url('public/site/img/university/cover/default.jpg')}}">
                                                    @endif
                                                    <div class="overlay"></div>
                                                    <div class="snippet-extras">
                                                        <div class="data-left"></div>
                                                        <div class="data-right college_rating pull-right">
                                                            <span class="college_affiliation"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="snippet-middle">
                                                <div class="thumbnail">
                                                    <a href="{{url('universities/'.$university->url)}}">
                                                    @if($university->thumb_url !=='')
                                                         <img src="{{ url('public/site/img/university/logo/'.$university->thumb_url)}}"/>
                                                    @else
                                                        <img src="{{ url('public/site/img/ehublogo.png')}}"/>
                                                     @endif
                                                     </a>
                                                </div>
                                                <div class="college_details">
                                                    <a class="college_name" href="{{url('universities/'.$university->url)}}">
                                                        <span>{{$university->aname}}</span>
                                                    </a>
                                                </div>
                                                <span class="college_location">
                                                    <i class="fa fa-map-marker"></i> {{substr($university->established,0,4)}}
                                                </span>
                                            </div>
                                            <div class="snippet-bottom">
                                                <div class="courses">
                                                    <div>
                                                        <a href="{{url('universities/'.$university->url.'#course')}}">
                                                            <span class="heading">Program offered</span><span><i class="fa fa-graduation-cap"></i> ({{count($university->programs)}})</span>
                                                        </a>
                                                    </div>
                                                    <div>
                                                        <a href="{{url('universities/'.$university->url)}}">
                                                            <span class="heading">Affiliated colleges</span><span><i class="fa fa-flag"></i> ({{$university->college}})</span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="bottom_navigation">
                                                    <a class="bottom-nav-tab" href="{{url('universities/'.$university->url)}}">Home</a>
                                                    <a class="bottom-nav-tab" href="{{url('universities/'.$university->url.'#courses')}}">COURSES</a>
                                                    <!--<a class="bottom-nav-tab" href="#">GALLERY</a>-->
                                                    <a class="bottom-nav-tab" href="{{url('universities/'.$university->url)}}">View detail</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </section>
                        </div><!-- end national-->
                            <a name="other"></a>
                            <div id="#other">
                            <header><h1>National(Other Affiliations) in Nepal</h1></header>
                            <section id="College-listing2">
                                <div class="row">
                                	@foreach($nationalothers as $university)
                                    <div class="col-md-4 col-sm-6">
                                        <div class="grid_listing_college_snippet">
                                            <div class="snippet-top">
                                                <div class="div-container">
                                                    <?php $colors = array('225378','eb7f00','1695a3'); ?>
                                                    <img src="http://dummyimage.com/600x400/<?php echo $colors[array_rand($colors)];?>/ffffff&text={{$university->aname}}">
                                                    <div class="overlay"></div>
                                                    <div class="snippet-extras">
                                                        <div class="data-left"></div>
                                                        <div class="data-right college_rating pull-right">
                                                            <span class="college_affiliation"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="snippet-middle">
                                                <div class="thumbnail">
                                                    <a href="{{url('universities/'.$university->url)}}">
                                                    @if($university->thumb_url !=='')
                                                         <img src="{{ url('public/site/img/university/logo/'.$university->thumb_url)}}"/>
                                                    @else
                                                        <img src="{{ url('public/site/img/ehublogo.png')}}"/>
                                                     @endif
                                                     </a>
                                                </div>
                                                <div class="college_details">
                                                    <a class="college_name" href="{{url('universities/'.$university->url)}}">
                                                        <span>{{$university->aname}}</span>
                                                    </a>
                                                </div>
                                                <span class="college_location">
                                                    <i class="fa fa-map-marker"></i> {{substr($university->established,0,4)}}
                                                </span>
                                            </div>
                                            <div class="snippet-bottom">
                                                <div class="courses">
                                                    <div>
                                                        <a href="{{url('universities/'.$university->url.'#course')}}">
                                                            <span class="heading">Program offered</span><span><i class="fa fa-graduation-cap"></i> ({{count($university->programs)}})</span>
                                                        </a>
                                                    </div>
                                                    <div>
                                                        <a href="{{url('universities/'.$university->url)}}">
                                                            <span class="heading">Affiliated colleges</span><span><i class="fa fa-flag"></i> ({{$university->college}})</span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="bottom_navigation">
                                                    <a class="bottom-nav-tab" href="{{url('universities/'.$university->url)}}">Home</a>
                                                     <a class="bottom-nav-tab" href="{{url('universities/'.$university->url.'#courses')}}">COURSES</a>
                                                    <!--<a class="bottom-nav-tab" href="#">GALLERY</a>-->
                                                    <a class="bottom-nav-tab" href="{{url('universities/'.$university->url)}}">View detail</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </section>
                        </div><!-- end other-->
                        <a name="international"></a>
                        <div id="#international">
                            <header><h1>International Affiliations in Nepal</h1></header>
                            <section id="College-listing3">
                                <div class="row">
                                	@foreach($internationaluniversities as $university)
                                    <div class="col-md-4 col-sm-6">
                                        <div class="grid_listing_college_snippet">
                                            <div class="snippet-top">
                                                <div class="div-container">
                                                    <?php $colors = array('225378','eb7f00','1695a3'); ?>
                                                    <img src="http://dummyimage.com/600x400/<?php echo $colors[array_rand($colors)];?>/ffffff&text={{$university->aname}}">
                                                    <div class="overlay"></div>
                                                    <div class="snippet-extras">
                                                        <div class="data-left"></div>
                                                        <div class="data-right college_rating pull-right">
                                                            <span class="college_affiliation"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="snippet-middle">
                                                <div class="thumbnail">
                                                    <a href="{{url('universities/'.$university->url)}}">
                                                    @if($university->thumb_url !=='')
                                                         <img src="{{ url('public/site/img/university/logo/'.$university->thumb_url)}}"/>
                                                    @else
                                                        <img src="{{ url('public/site/img/ehublogo.png')}}"/>
                                                     @endif
                                                     </a>
                                                </div>
                                                <div class="college_details">
                                                    <a class="college_name" href="{{url('universities/'.$university->url)}}">
                                                        <span>{{$university->aname}}</span>
                                                    </a>
                                                </div>
                                                <span class="college_location">
                                                    <i class="fa fa-map-marker"></i> {{substr($university->established,0,4)}}
                                                </span>
                                            </div>
                                            <div class="snippet-bottom">
                                                <div class="courses">
                                                    <div>
                                                        <a href="{{url('universities/'.$university->url.'#course')}}">
                                                            <span class="heading">Program offered</span><span><i class="fa fa-graduation-cap"></i> ({{count($university->programs)}})</span>
                                                        </a>
                                                    </div>
                                                    <div>
                                                        <a href="{{url('universities/'.$university->url)}}">
                                                            <span class="heading">Affiliated colleges</span><span><i class="fa fa-flag"></i> ({{$university->college}})</span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="bottom_navigation">
                                                    <a class="bottom-nav-tab" href="{{url('universities/'.$university->url)}}">Home</a>
                                                     <a class="bottom-nav-tab" href="{{url('universities/'.$university->url.'#courses')}}">COURSES</a>
                                                    <!--<a class="bottom-nav-tab" href="#">GALLERY</a>-->
                                                    <a class="bottom-nav-tab" href="{{url('universities/'.$university->url)}}">View detail</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </section>
                        </div><!-- end international-->
                    </div>
                </div>
            </div>
</div>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "item": {
      "@id": "http://www.educationhubnepal.com/universities",
      "name": "Universities",
      "image":""
    }
  }]
}
</script>
@stop