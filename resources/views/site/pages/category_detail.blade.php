@extends('site.sitemaster')
@section('maincontent')
<!-- Page Content -->
<style type="text/css">
    /*College dedtail page specific css*/
    .navigation { position: absolute; top: 0px; left: 0px; right: 0px; background: rgba(255,255,255,0); }
    .college-banner { width: 100%; height: 260px;}
    #page-content { margin-top: -100px; }
    .navigation .secondary-navigation .contact figure, 
    .navigation .secondary-navigation a, .navigation .secondary-navigation a.promoted, 
    .navigation .navbar .navbar-nav > li a, .breadcrumb li a,.breadcrumb li,.breadcrumb > .active,#brand a{color:#fff;}
</style>


<div class="college-banner">
    <div class="bg-overlay"></div>
    <img class="img-resposnsive bg-college" src="{{url('public/site/img/courses.jpg')}}">
    <!--<img class="img-resposnsive bg-college" alt="" src="http://static.collegedunia.com/public/college_data/images/appImage/25621_NLSIU.jpg/1024/1024">-->
</div>

<!-- Page Content -->
    <div id="page-content">
        <!-- Breadcrumb -->
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{url('')}}">Home</a></li>
                <li><a href="{{url('categories')}}">Categories</a></li>
                <li class="active">{{$category->category}}</li>
            </ol>
        </div>
        <!-- end Breadcrumb -->

        <div class="container course-detail">
            <div class="row">
                <!-- Property Detail Content -->
                <div class="col-md-12 col-sm-12">
                    <section id="property-detail college-detail">
                        <header class="property-title college-title">
                            <div class="college_logo_wrap">
                            	@if(!empty($category->logo))
                                {!! $category->logo !!}
                                @else
                                <img class="bg-logo" src="{{url('public/site/img/ehublogo.png')}}"/>
                                @endif
                            </div>
                            <h1>{{$category->category}}</h1>
                            <figure>
                              <span><i class="fa fa-book"></i> {{count($category->programs)}} Programs</span>
                          
                              <!--<span><i class="fa fa-thumb-tack"></i> ESTD 2010</span>-->
                            </figure>
                            <div class="social-share">
                            	<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A//educationhubnepal.com/{{'categories/'.$category->url}}"><i class="fa fa-facebook"></i></a>
                            	<a target="_blank" href="https://twitter.com/home?status=http%3A//educationhubnepal.com/{{'categories/'.$category->url}}"><i class="fa fa-twitter"></i></a>
                            </div>
                        </header>
                        <div class="tabbable-panel">
							<div class="tabbable-line">
								<ul class="nav nav-tabs ">
									<li class="active">
										<a data-toggle="tab" href="#tab_default_1">Home</a>
									</li>
									@if(!empty($plus_two))
									<li>
										<a data-toggle="tab" href="#tab_default_2">+2 Level ({{count($plus_two)}})</a>
									</li>
									@else
									<li>
										<a data-toggle="tab" href="#tab_default_2">+2 Level (0)</a>
									</li>
									@endif
									@if(!empty($bachelor))
									<li>
										<a data-toggle="tab" href="#tab_default_3">Bachelor ({{count($bachelor)}})</a>
									</li>
									@else
									<li>
										<a data-toggle="tab" href="#tab_default_3">Bachelor (0)</a>
									</li>
									@endif
									@if(!empty($post_graduation))
									<li>
										<a data-toggle="tab" href="#tab_default_4">Post Graduation ({{count($post_graduation)}})</a>
									</li>
									@else
									<li>
										<a data-toggle="tab" href="#tab_default_4">Post Graduation (0)</a>
									</li>
									@endif
									@if(!empty($master))
									<li>
										<a data-toggle="tab" href="#tab_default_5">Master ({{count($master)}})</a>
									</li>
									@else
									<li>
										<a data-toggle="tab" href="#tab_default_5">Master (0)</a>
									</li>
									@endif
									@if(!empty($mphil))
									<li>
										<a data-toggle="tab" href="#tab_default_6">M.Phil ({{count($mphil)}})</a>
									</li>
									@else
									<li>
										<a data-toggle="tab" href="#tab_default_6">M.Phil (0)</a>
									</li>
									@endif
									@if(!empty($phd))
									<li>
										<a data-toggle="tab" href="#tab_default_7">Phd ({{count($phd)}})</a>
									</li>
									@else
									<li>
										<a data-toggle="tab" href="#tab_default_7">Phd (0)</a>
									</li>
									@endif
								</ul>
								<div class="tab-content">
									<div id="tab_default_1" class="tab-pane active">
										<!--/ College description -->
										<div class="row">
											<div class="col-md-4 col-sm-12">
				                                <section id="quick-summary" class="clearfix">
				                                    <header><h2>Quick Summary</h2></header>
				                                    <dl>
				                                        
				                                        <dt>No. Of Programs</dt>
				                                            <dd><span class="tag price">{{count($category->programs)}}</span></dd>
				                                       
				                                    
				                                        <!--<dt>Rating:</dt>
				                                            <dd><div class="rating rating-overall" data-score="4"></div></dd>-->
				                                    </dl>
				                                </section><!-- /#quick-summary -->
				                            </div><!-- /.col-md-4 -->
											<div class="col-md-8 col-sm-12">
				                                <section id="description">
				                                    <header><h2>About {{$category->category}}</h2></header>
				                                    
				                                </section>
				                            </div>
			                        	</div> <!--/ end of college description -->
									</div>
									<div id="tab_default_2" class="tab-pane">
										<div class="row">
										<div class="col-md-12 col-sm-12">
										<header><h3>+2 Programs Under {{$category->category}}</h3></header>
										<section id="properties">
											 <div class="grid">
											 	@if(!empty($plus_two))
											 	@foreach($plus_two as $program)
											 	<div class="individual-course col-md-9 no-padding">
						                       		<h3><a href="{{url('programs/'.$program->url)}}">{{$program->pname}}</a></h3>
						                       		<div class="program-info">
						                       			<span class="program-duration"><i class="fa fa-clock-o"></i> {{$program->duration}}</span>
						                       			<span class="program-affiliation"><i class="fa fa-university"></i> <a title="{{$program->affiliation->aname}}" href="{{url('universities/'.$program->affiliation->url)}}">{{stringFirst($program->affiliation->aname)}}</a></span>
						                       		</div>
						                       		<div class="program-more-info">
						                       			Academic Year - <span>{{$program->academic_year}}</span>,
						                       			Cost Range - <span>
						                                        @if(!empty($program->cost_range))
						                                        {{$program->cost_range}}
						                                        @else
						                                        N/A
						                                        @endif
						                                    	</span>
						                       		</div>
						                       		<div class="buttonsets">
						                       			<button data-toggle="collapse" data-target="#eli{{$program->id}}" class="btn btn-default">Check Eligibility</button>
						                       			<button class="btn btn-default" onclick="window.location.href='{{url('programs/'.$program->url)}}';return false;">View Details</button>
						                       		</div>
						                       		<div class="eligibility collapse" id="eli{{$program->id}}">
						                       			@if(!empty($program->eligibility))
						                       			{!! $program->eligibility !!}
						                       			@else
						                       				N/A
						                       			@endif
						                       		</div>
						                       </div>
						                        
						                        @endforeach
						                        @else
						                        <h3 class="empty"> Sorry There is no program in +2 Level associated with {{$category->category}}.</h3>
						                        @endif
										</section>
									</div>
								</div>
							</div>
									<div id="tab_default_3" class="tab-pane">
										<div class="row">
										<div class="col-md-12 col-sm-12">
										<header><h3>Bachelor Programs Under {{$category->category}}</h3></header>
										<section id="properties">
											 <div class="grid">
											 	@if(!empty($bachelor))
											 	@foreach($bachelor as $program)
						                        <div class="individual-course col-md-9 no-padding">
						                       		<h3><a href="{{url('programs/'.$program->url)}}">{{$program->pname}}</a></h3>
						                       		<div class="program-info">
						                       			<span class="program-duration"><i class="fa fa-clock-o"></i> {{$program->duration}}</span>
						                       			<span class="program-affiliation"><i class="fa fa-university"></i> <a title="{{$program->affiliation->aname}}" href="{{url('universities/'.$program->affiliation->url)}}">{{stringFirst($program->affiliation->aname)}}</a></span>
						                       		</div>
						                       		<div class="program-more-info">
						                       			Academic Year - <span>{{$program->academic_year}}</span>,
						                       			Cost Range - <span>
						                                        @if(!empty($program->cost_range))
						                                        {{$program->cost_range}}
						                                        @else
						                                        N/A
						                                        @endif
						                                    	</span>
						                       		</div>
						                       		<div class="buttonsets">
						                       			<button data-toggle="collapse" data-target="#eli{{$program->id}}" class="btn btn-default">Check Eligibility</button>
						                       			<button class="btn btn-default" onclick="window.location.href='{{url('programs/'.$program->url)}}';return false;">View Details</button>
						                       		</div>
						                       		<div class="eligibility collapse" id="eli{{$program->id}}">
						                       			@if(!empty($program->eligibility))
						                       			{!! $program->eligibility !!}
						                       			@else
						                       				N/A
						                       			@endif
						                       		</div>
						                       </div>
						                        
						                        @endforeach
						                        @else
						                        <h3 class="empty"> Sorry There is no program in +2 Level associated with {{$category->category}}.</h3>
						                        @endif
										</section>
									</div>
								</div>
							</div>
								<!-- tab4 Post Graduation -->
								<div id="tab_default_4" class="tab-pane">
										<div class="row">
										<div class="col-md-12 col-sm-12">
										<header><h3>Post Graduate Programs Under {{$category->category}}</h3></header>
										<section id="properties">
											 <div class="grid">
											 	@if(!empty($post_graduation))
											 	@foreach($post_graduation as $program)
											 	<div class="individual-course col-md-9 no-padding">
						                       		<h3><a href="{{url('programs/'.$program->url)}}">{{$program->pname}}</a></h3>
						                       		<div class="program-info">
						                       			<span class="program-duration"><i class="fa fa-clock-o"></i> {{$program->duration}}</span>
						                       			<span class="program-affiliation"><i class="fa fa-university"></i> <a title="{{$program->affiliation->aname}}" href="{{url('universities/'.$program->affiliation->url)}}">{{stringFirst($program->affiliation->aname)}}</a></span>
						                       		</div>
						                       		<div class="program-more-info">
						                       			Academic Year - <span>{{$program->academic_year}}</span>,
						                       			Cost Range - <span>
						                                        @if(!empty($program->cost_range))
						                                        {{$program->cost_range}}
						                                        @else
						                                        N/A
						                                        @endif
						                                    	</span>
						                       		</div>
						                       		<div class="buttonsets">
						                       			<button data-toggle="collapse" data-target="#eli{{$program->id}}" class="btn btn-default">Check Eligibility</button>
						                       			<button class="btn btn-default" onclick="window.location.href='{{url('programs/'.$program->url)}}';return false;">View Details</button>
						                       		</div>
						                       		<div class="eligibility collapse" id="eli{{$program->id}}">
						                       			@if(!empty($program->eligibility))
						                       			{!! $program->eligibility !!}
						                       			@else
						                       				N/A
						                       			@endif
						                       		</div>
						                       </div>
						                        
						                        @endforeach
						                        @else
						                        <h3 class="empty"> Sorry There is no program in Post Graduation Level associated with {{$category->category}}.</h3>
						                        @endif
										</section>
									</div>
								</div>
							</div><!-- tab 4 ends -->

								<!-- tab 5 master programs -->
								<div id="tab_default_5" class="tab-pane">
										<div class="row">
										<div class="col-md-12 col-sm-12">
										<header><h3>Master Programs Under {{$category->category}}</h3></header>
										<section id="properties">
											 <div class="grid">
											 	@if(!empty($master))
											 	@foreach($master as $program)
											 	
						                       	<div class="individual-course col-md-9 no-padding">
						                       		<h3><a href="{{url('programs/'.$program->url)}}">{{$program->pname}}</a></h3>
						                       		<div class="program-info">
						                       			<span class="program-duration"><i class="fa fa-clock-o"></i> {{$program->duration}}</span>
						                       			<span class="program-affiliation"><i class="fa fa-university"></i> <a title="{{$program->affiliation->aname}}" href="{{url('universities/'.$program->affiliation->url)}}">{{stringFirst($program->affiliation->aname)}}</a></span>
						                       		</div>
						                       		<div class="program-more-info">
						                       			Academic Year - <span>{{$program->academic_year}}</span>,
						                       			Cost Range - <span>
						                                        @if(!empty($program->cost_range))
						                                        {{$program->cost_range}}
						                                        @else
						                                        N/A
						                                        @endif
						                                    	</span>
						                       		</div>
						                       		<div class="buttonsets">
						                       			<button data-toggle="collapse" data-target="#eli{{$program->id}}" class="btn btn-default">Check Eligibility</button>
						                       			<button class="btn btn-default" onclick="window.location.href='{{url('programs/'.$program->url)}}';return false;">View Details</button>
						                       		</div>
						                       		<div class="eligibility collapse" id="eli{{$program->id}}">
						                       			@if(!empty($program->eligibility))
						                       			{!! $program->eligibility !!}
						                       			@else
						                       				N/A
						                       			@endif
						                       		</div>
						                       </div>
						                        
						                        @endforeach
						                        @else
						                        <h3 class="empty"> Sorry There is no program in Master Level associated with {{$category->category}}.</h3>
						                        @endif
										</section>
									</div>
								</div>
							</div>
								<!-- end of the master level -->
							<div id="tab_default_6" class="tab-pane">
										<div class="row">
										<div class="col-md-12 col-sm-12">
										<header><h3>M.Phil Programs Under {{$category->category}}</h3></header>
										<section id="properties">
											 <div class="grid">
											 	@if(!empty($mphil))
											 	@foreach($mphil as $program)
											 	<div class="individual-course col-md-9 no-padding">
						                       		<h3><a href="{{url('programs/'.$program->url)}}">{{$program->pname}}</a></h3>
						                       		<div class="program-info">
						                       			<span class="program-duration"><i class="fa fa-clock-o"></i> {{$program->duration}}</span>
						                       			<span class="program-affiliation"><i class="fa fa-university"></i> <a title="{{$program->affiliation->aname}}" href="{{url('universities/'.$program->affiliation->url)}}">{{stringFirst($program->affiliation->aname)}}</a></span>
						                       		</div>
						                       		<div class="program-more-info">
						                       			Academic Year - <span>{{$program->academic_year}}</span>,
						                       			Cost Range - <span>
						                                        @if(!empty($program->cost_range))
						                                        {{$program->cost_range}}
						                                        @else
						                                        N/A
						                                        @endif
						                                    	</span>
						                       		</div>
						                       		<div class="buttonsets">
						                       			<button data-toggle="collapse" data-target="#eli{{$program->id}}" class="btn btn-default">Check Eligibility</button>
						                       			<button class="btn btn-default" onclick="window.location.href='{{url('programs/'.$program->url)}}';return false;">View Details</button>
						                       		</div>
						                       		<div class="eligibility collapse" id="eli{{$program->id}}">
						                       			@if(!empty($program->eligibility))
						                       			{!! $program->eligibility !!}
						                       			@else
						                       				N/A
						                       			@endif
						                       		</div>
						                       </div>
						                        
						                        @endforeach
						                        @else
						                        <h3 class="empty"> Sorry There is no program in M.phil Level associated with {{$category->category}}.</h3>
						                        @endif
										</section>
									</div>
								</div>
							</div>
								<!-- end of the M.phil level -->
								<div id="tab_default_7" class="tab-pane">
										<div class="row">
										<div class="col-md-12 col-sm-12">
										<header><h3>Phd Programs Under {{$category->category}}</h3></header>
										<section id="properties">
											 <div class="grid">
											 	@if(!empty($phd))
											 	@foreach($phd as $program)
											 	<div class="individual-course col-md-9 no-padding">
						                       		<h3><a href="{{url('programs/'.$program->url)}}">{{$program->pname}}</a></h3>
						                       		<div class="program-info">
						                       			<span class="program-duration"><i class="fa fa-clock-o"></i> {{$program->duration}}</span>
						                       			<span class="program-affiliation"><i class="fa fa-university"></i> <a title="{{$program->affiliation->aname}}" href="{{url('universities/'.$program->affiliation->url)}}">{{stringFirst($program->affiliation->aname)}}</a></span>
						                       		</div>
						                       		<div class="program-more-info">
						                       			Academic Year - <span>{{$program->academic_year}}</span>,
						                       			Cost Range - <span>
						                                        @if(!empty($program->cost_range))
						                                        {{$program->cost_range}}
						                                        @else
						                                        N/A
						                                        @endif
						                                    	</span>
						                       		</div>
						                       		<div class="buttonsets">
						                       			<button data-toggle="collapse" data-target="#eli{{$program->id}}" class="btn btn-default">Check Eligibility</button>
						                       			<button class="btn btn-default" onclick="window.location.href='{{url('programs/'.$program->url)}}';return false;">View Details</button>
						                       		</div>
						                       		<div class="eligibility collapse" id="eli{{$program->id}}">
						                       			@if(!empty($program->eligibility))
						                       			{!! $program->eligibility !!}
						                       			@else
						                       				N/A
						                       			@endif
						                       		</div>
						                       </div>
						                        
						                        @endforeach
						                        @else
						                        <h3 class="empty"> Sorry There is no program in phd Level associated with {{$category->category}}.</h3>
						                        @endif
										</section>
									</div>
								</div>
							</div>
								<!-- end of phd level -->



								</div>
							</div>
						</div>
					</section>
    			</div>
		</div>
	</div></div>
    <!-- end Page Content -->
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "item": {
      "@id": "http://www.educationhubnepal.com/categories",
      "name": "Levels",
      "image":""
    }
  },{
    "@type": "ListItem",
    "position": 2,
    "item": {
      "@id": "{{url('categories/'.$category->url)}}",
      "name": "{{$category->category}}",
      "image":""
      }
    
  }]
}
</script>
@stop