<?php 
	/*foreach($res as $r):
		foreach($r as $a):
			print_r($a->affiliation);
			die();
		endforeach;
	endforeach;
	*/
?>

@extends('site.sitemaster')
@section('maincontent')
<!-- Page Content -->
    <div id="page-content">
        <!-- Breadcrumb -->
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{url('')}}">Home</a></li>
                <li><a href="{{url('levels')}}">Levels</a></li>
                <li class="active">{{$level->lname}}</li>
            </ol>
        </div>
        <!-- end Breadcrumb -->

            <div class="container levels-detail">
                <div class="row">
                    <!-- Results -->
                    <div class="col-md-4 col-sm-12 left-sidebar" data-spy="affix" data-offset-top="60" data-offset-bottom="500">
                        <header class="property-title level-title">
                            <h1>{{$level->lname}}</h1>
                            <figure>
                           
                          
                              <!--<span><i class="fa fa-thumb-tack"></i> ESTD 2010</span>-->
                            </figure>
                            @include('site.forms.level_search')
                        </header>
                        </div>
                        <div class="col-md-8" style="float:right;">
                        <section id="results">
                            <header class="property-title level-title">
                                <h2> Search Result For {{$title}}</h2>
                            </header>
                            <section id="College-listing">
                                <div class="row">
                                    @foreach($res as $r)
                                    @foreach($r as $program)
                                        <div class="individual-course col-md-12 no-padding">
                                                    <h3><a href="{{url('programs/'.$program->url)}}">{{$program->pname}}</a></h3>
                                                    <div class="program-info">
                                                        <span class="program-duration"><i class="fa fa-clock-o"></i> {{$program->duration}}</span>
                                                        <!-<span class="program-affiliation"><i class="fa fa-university"></i> <a title="{{$program->affiliation->aname}}" href="{{url('universities/'.$program->affiliation->url)}}">{{stringFirst($program->affiliation->aname)}}</a></span>
                                                        <div class="program-categories"> <i class="fa fa-bookmark"></i>
                                                            @if(!empty($program->categories))
                                                            @foreach($program->categories as $cat)
                                                            <a href="{{url('categories/'.$cat->url)}}">{{$cat->category}}</a>
                                                            @endforeach
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="program-more-info">
                                                        Academic Year - <span>{{$program->academic_year}}</span>,
                                                        Cost Range - <span>
                                                                @if(!empty($program->cost_range))
                                                                {{$program->cost_range}}
                                                                @else
                                                                N/A
                                                                @endif
                                                                </span>
                                                    </div>
                                                    <div class="buttonsets">
                                                        <button data-toggle="collapse" data-target="#eli{{$program->id}}" class="btn btn-default">Check Eligibility</button>
                                                        <button class="btn btn-default" onclick="window.location.href='{{url('programs/'.$program->url)}}';return false;">View Details</button>
                                                    </div>
                                                    <div class="eligibility collapse" id="eli{{$program->id}}">
                                                        @if(!empty($program->eligibility))
                                                        {!! $program->eligibility !!}
                                                        @else
                                                            N/A
                                                        @endif
                                                    </div>
                                               </div>
                                               <!--
                                        <section id="properties">
                                             <div class="grid">
                                               <div class="property masonry">
                                                    <div class="inner">
                                                        <a href="{{url('programs/'.$program->url)}}">
                                                            <div class="property-image">
                                                                
                                                                <div class="overlay">
                                                                    <div class="info">
                                                                        <div class="tag price"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                        <aside>
                                                            <header>
                                                          
                                                                <a href="{{url('programs/'.$program->url)}}"><h3>{{$program->pname}}</h3></a>
                                                                
                                                            </header>
                                                            
                                                            <dl>
                                                           
                                                                <dt>Duration:</dt>
                                                                <dd>{{$program->duration}}</dd>
                                                                <dt>Academic Year:</dt>
                                                                <dd>{{$program->academic_year}}</dd>
                                                                <dt>Cost Range:</dt>
                                                                @if(!empty($program->cost_range))
                                                                <dd>{{$program->cost_range}}</dd>
                                                                @else
                                                                <dd>N/A</dd>
                                                                @endif
                                                                @if(!empty($program->categories))
                                                                <dt>Academic Year:</dt>
                                                                @foreach($program->categories as $cat)
                                                                <dd><a href="{{url('categories/'.$cat->url)}}">{{$cat->category}}</dd>
                                                                @endforeach
                                                                @endif
                                                                <dt>Affiliation:</dt>
                                                                <dd><a title="{{$program->affiliation->aname}}" href="{{url('universities/'.$program->affiliation->url)}}">{{stringFirst($program->affiliation->aname)}}</dd>

                                                            </dl>
                                                            <a href="{{url('programs/'.$program->url)}}" class="link-arrow"> More</a>
                                                        </aside>
                                                    </div>
                                                </div>    
                                        </section>-->
                                       
                                    
                                   @endforeach 

                                </div>
                                 <div class="center">
                                    
                                        
                                    
                                </div><!-- /.center-->

                                @endforeach

                                <!-- Pagination -->
                                
                                

                            </section><!-- /#properties-->
                        </section><!-- /#results -->
                    </div><!-- /.col-md-9 -->
                    <!-- end Results -->

                    <!-- sidebar -->

                </div><!-- /.row -->
            </div><!-- /.container -->
        </div>
        <!-- end Page Content -->
@stop