@extends('site.sitemaster')
@section('maincontent')


 <div id="page-content">
        <!-- Breadcrumb -->
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">Contact</li>
            </ol>
        </div>
        <!-- end Breadcrumb -->

        <div class="container">
            <div class="row">
                <!-- Contact -->
                <div class="col-md-9 col-sm-9">
                    <section id="agent-detail">
                        <header><h1>Contact Us</h1></header>
                        <section id="contact-information">
                            <div class="row">
                                <div class="col-md-4 col-sm-5">
                                    <section id="address">
                                        <header><h3>Address</h3></header>
                                        <address>
                                            <strong>Education Hub Nepal</strong><br>
                                            DurbarMarga Kathmandu<br>
                                            Bagmati, Nepal
                                        </address>
                                        
                                        <a href="mailto:info@educationhubnepal.com">info@educationhubnepal.com</a><br>
                                        
                                    </section><!-- /#address -->
                                    <section id="social">
                                        <header><h3>Social Profiles</h3></header>
                                        <div class="agent-social">
                                        <a href="http://facebook.com/educationhubnepal/" target="_blank" class="fa fa-facebook btn btn-grey-dark"></a>
                                            <a href="https://twitter.com/educationhubnp" target="_blank" class="fa fa-twitter btn btn-grey-dark"></a>
                                            <a href="https://plus.google.com/+Educationhubnepal/" target="_blank" class="fa fa-google btn btn-grey-dark"></a>
                                            
                                        </div>
                                    </section><!-- /#social -->
                                </div><!-- /.col-md-4 -->
                                <div class="col-md-8 col-sm-7">
                                    <header><h3>Write us about anything</h3></header>
                                    <div id="contact-map"><p>We're working for a cause, we believe to help everyone with right information,
                                    	if you've quries about the data in our site, please mention "Regarding data in the subject"</p>
                                    	<p> We need resources to work, and continiously update our system, if you're an institution seeking to 
                                    		advertise in our website, you're welcome, just choose "About Advertising" in the subject option. </p>

                                    		<p>Even if you just want to say "Hello" to us. You're heartly welcome :)</p>
                                    </div>
                                </div><!-- /.col-md-8 -->
                            </div><!-- /.row -->
                        </section><!-- /#agent-info -->
                        <hr class="thick">
                        <section id="form">
                            <header><h3>Send Us a Message</h3></header>
                            {!! Form::open(['url'=>'contact/submit','method'=>'post','id'=>'form-create-account','role'=>'form','class'=>'clearfix']) !!}
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="form-contact-name">Your Name<em>*</em></label>
                                            <input type="text" class="form-control" id="form-contact-name" name="contact-name" required>
                                        </div><!-- /.form-group -->
                                    </div><!-- /.col-md-6 -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="form-contact-email">Your Email<em>*</em></label>
                                            <input type="email" class="form-control" id="form-contact-email" name="contact-email" required>
                                        </div><!-- /.form-group -->
                                    </div><!-- /.col-md-6 -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="form-contact-email">Subject<em>*</em></label>
                                            <select name="options" required>
                                            	<option value="nice reason">Nice Reason</option>
                                            	<option value="About Data">About Data</option>
                                            	<option value="About Ad">About Advertisement</option>
                                            	<option value="error">Bug In Our System</option>
                                            	<option value="report">Report A Review</option>
                                            	<option value="Other">Other</option>
                                            </select>
                                        </div><!-- /.form-group -->
                                    </div><!-- /.col-md-6 -->
                                </div><!-- /.row -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="form-contact-message">Your Message<em>*</em></label>
                                            <textarea class="form-control" id="form-contact-message" rows="8" name="contact-message" required></textarea>
                                        </div><!-- /.form-group -->
                                    </div><!-- /.col-md-12 -->
                                </div><!-- /.row -->
                                <div class="form-group clearfix">
                                    <button type="submit">Send a Message</button>
                                </div><!-- /.form-group -->
                                <div id="form-status"></div>
                            {!! Form::close() !!}<!-- /#form-contact -->
                        </section>
                    </section><!-- /#agent-detail -->
                </div><!-- /.col-md-9 -->
                <!-- end Contact -->

                <!-- sidebar -->
                <div class="col-md-3 col-sm-3">
                    <section id="sidebar">
                       
                        <aside id="featured-properties">
                            <header><h3>Recent Blog Updates</h3></header>
                            @if(!empty($posts))
                            @foreach($posts as $post)
                            <div class="property small">
                            	@if(!empty($post->image))
                                <a href="{{url('blog/'.$post->slug)}}">
                                    <div class="property-image">
                                        <img alt="" src="{{url('public/admin/img/blog/'.$post->image)}}">
                                    </div>
                                </a>
                                @else
                                	<a href="{{url('blog/'.$post->slug)}}">
                                    <div class="property-image">
                                        <img src="http://placehold.it/350x150?text=Blog Post"/>
                                    </div>
                                </a>
                                @endif
                                <div class="info">
                                    <a href="{{url('blog/'.$post->slug)}}"><h4>{{$post->title}}</h4></a>
                    
                                </div>
                            </div><!-- /.property -->
                            @endforeach
                            @endif
                            
                        </aside><!-- /#featured-properties -->
                        
                    </section><!-- /#sidebar -->
                </div><!-- /.col-md-3 -->
                <!-- end Sidebar -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div>
    <!-- end Page Content -->

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "item": {
      "@id": "http://www.educationhubnepal.com/contact",
      "name": "Contact Us",
      "image":""
    }
  }]
}
</script>








@stop