@extends('site.sitemaster')
@section('maincontent')
<!-- Page Content -->


<div id="page-content">
    <!-- Breadcrumb -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{url('')}}">Home</a></li>
            <li><a href="{{url('colleges/')}}">Colleges</a></li>
            <li class="active">College Listing</li>
        </ol>
    </div>
    <!-- end Breadcrumb -->
    @include('site.forms.college_search')
    <div class="container college-listing-results list-colleges">
        <div class="row">
            <!-- Results -->
            <div class="col-md-12 col-sm-12">
                <section id="results">
                    
                    <header class="listing-title">
                        @if(isset($param['query']))
                        <h2>Searching :<span class='text-info'> {{$param['query']}}</span></h2>
                        @else
                        <h2>List of all colleges</h2>
                        @endif
                    </header>
                    @if(count($colleges)<1)
                    <div style="margin-bottom:10px; width:100%;border:1px dashed #ccc;padding:15px;font-size:20px;text-align: center;background:#f0f0f0">Sorry the college you are looking for was not found.</div>
                    <div class='alert alert-warning'>Our community is working hard to list all colleges in Nepal. 
                        Education Hub Nepal is run by more than 200 people just like you. 
                        You too can be a part of this community and help us in listing more institutions.
                        If you've got time, click on this <a href="{{url('/signup')}}"><span class="label label-info" style="padding:4px"> signup </span></a> &nbsp; button and register in 30 seconds with google or facebook. 
                        Then you can add this college by providing basic information such as college name 
                        and location. We will search for the details and add this college in no time. Your cooperation is 
                        very much valued by Education Hub Nepal.</div>
                    @endif
                    <section id="College-listing" class="college-listing-results">
                        <div class="row">
                            @foreach($colleges as $college)
                            <div class="col-md-4 col-sm-6">
                                <div class="grid_listing_college_snippet">
                                    <div class="snippet-top">
                                        <div class="div-container">
                                            <?php $colors = array('225378','eb7f00','1695a3'); ?>
                                            <img src="http://dummyimage.com/600x400/<?php echo $colors[array_rand($colors)];?>/ffffff&text={{$college->cname}}">
                                            <div class="overlay"></div>
                                            <div class="snippet-extras">
                                                <div class="data-left"></div>
                                                <div class="data-right college_rating pull-right">
                                                    <span class="college_affiliation"><i class="fa fa-university"></i>
                                                        @foreach($college->affiliations as $aff)
                                                        {{stringFirst($aff->aname)}}
                                                        @endforeach
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="snippet-middle">
                                        <div class="thumbnail">
                                            @if($college->logo !=='')
                                            <a href="{{url('colleges/'.$college->url)}}">
                                                <img src="{{ url($college->full_url)}}"/>
                                            </a>
                                            @else
                                            <a href="{{url('colleges/'.$college->url)}}">
                                                <img src="{{ url('public/site/img/ehublogo.png')}}"/>
                                            </a>
                                            @endif

                                        </div>
                                        <div class="college_details">
                                            <a class="college_name" href="{{url('colleges/'.$college->url)}}">
                                                <span>{{$college->cname}}</span>
                                            </a>
                                        </div>
                                        <span class="college_location">
                                            <i class="fa fa-map-marker"></i> {{$college->address}}
                                        </span>
                                    </div>
                                    <div class="snippet-bottom">
                                        <div class="courses">
                                            <span class="heading"><a href="{{url('colleges/'.$college->url.'#courses')}}">courses offered ({{count($college->programs)}})</a></span>
                                            <div class="course_detail_tab">
                                                <ul class="coursecarousel">
                                                    @foreach($college->programs as $program)
                                                    <li class="item"><a href="{{url('colleges/'.$college->url.'#courses')}}"><i class="fa fa-graduation-cap"></i>{{$program->pname}}</a></li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="bottom_navigation">
                                            <a class="bottom-nav-tab" href="{{url('colleges/'.$college->url.'#courses')}}">COURSES</a>
                                            <a class="bottom-nav-tab" href="{{url('colleges/'.$college->url.'#gallery')}}">GALLERY</a>
                                            <a class="bottom-nav-tab" href="{{url('colleges/'.$college->url)}}">View detail</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach 
                        </div>



                        <!-- Pagination -->
                        <div class="center">

                            {!!$colleges->links()!!}

                        </div><!-- /.center-->

                    </section><!-- /#properties-->
                </section><!-- /#results -->
            </div><!-- /.col-md-9 -->
            <!-- end Results -->

            <!-- sidebar -->

        </div><!-- /.row -->
    </div><!-- /.container -->
</div>
<!-- end Page Content -->
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "item": {
      "@id": "http://www.educationhubnepal.com/colleges",
      "name": "Colleges",
      "image":""
    }
  }]
}
</script>
@stop