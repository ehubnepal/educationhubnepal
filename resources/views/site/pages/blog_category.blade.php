@extends('site.sitemaster')
@section('maincontent')
<style type='text/css'>
    ul.list-links{list-style-type:none}
</style>
<!-- Page Content -->
    <div id="page-content">
        <!-- Breadcrumb -->
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{url('')}}">Home</a></li>
                <li><a href="{{url('blog')}}">Blog </a></li>
                <li><a href="#">{{$category->name}} Category</a></li>
                
            </ol>
        </div>
    
 <div class="container blog-by-category">
            <div class="row">
                <!-- Content -->
                <div class="col-md-9 col-sm-9">
                    <section id="content">
                        <header><h1>{{$category->name}} Blog Listing</h1></header>
                        @if(!empty($category->posts))
                        @foreach($category->posts as $post)
                        <article class="blog-post">
                            <a href="{{url('blog/'.$post->slug)}}">@if(!empty($post->image))<img src="{{url('public/admin/img/blog/'.$post->image)}}">@endif</a>
                            <header><a href="{{url('blog/'.$post->slug)}}"><h2>{{$post->title}}</h2></a></header>
                            <figure class="meta">
                                <a href="#" class="link-icon"><i class="fa fa-user"></i>Education Hub Nepal</a>
                                <a href="#" class="link-icon"><i class="fa fa-calendar"></i>{{date("F jS, Y",strtotime($post->created_at))}}</a>
                            </figure>
                            <p>{!! substr($post->content,0,300)!!}
                            </p>
                            <a href="{{url('blog/'.$post->slug)}}" class="link-arrow">Read More</a>
                        </article><!-- /.blog-post -->
                        @endforeach
                        @else
                        <header><h2> Sorry No post has been associated with this category</h2></header>
                        @endif

                        <!-- Pagination -->
                        <div class="center">
                            {!!$category->posts->render() !!}
                            <!-- /.pagination-->
                        </div><!-- /.center-->
                    </section><!-- /#content -->
                </div><!-- /.col-md-9 -->
                <!-- end Content -->

                <!-- sidebar -->
                <div class="col-md-3 col-sm-3">
                    <section id="sidebar">
                       <!--
                        <aside id="search">
                            <header><h3>Search</h3></header>
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Enter Keyword">
                                <span class="input-group-btn"><button class="btn btn-default search" type="button"><i class="fa fa-search"></i></button></span>
                            </div>--><!-- /input-group --> 
                        </aside>
                        <aside id="post-archive">
                            <header><h3>Tags</h3></header>
                            <ul class="list-links">
                                @foreach($tags as $tag)
                                <li><a href="{{url('blog/tags/'.$tag->tag_id)}}"><i class='fa fa-tag'></i>  {{$tag->name}}</a></li>
                                @endforeach
                            </ul>
                        </aside><!-- /#post-archive -->
                        <aside id="categories">
                            <header><h3>Categories</h3></header>
                            <ul class="list-links">
                                @foreach($categories as $category)
                                <li><a href="{{url('blog/categories/'.$category->slug)}}"><i class='fa fa-cubes'></i>  {{$category->name}}</a></li>
                                @endforeach
                            </ul>
                        </aside><!-- /#categories -->
                    </section><!-- /#sidebar -->
                </div><!-- /.col-md-3 -->
                <!-- end Sidebar -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div>

    <!--<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "item": {
      "@id": "http://www.educationhubnepal.com/blog",
      "name": "Blog",
      "image":""
    }
  },{
    "@type": "ListItem",
    "position": 2,
    "item": {
      "@id": "http://www.educationhubnepal.com/blog/categories/{{$category->slug}}",
      "name": "{{$category->name}}",
      "image":""
      }
    
  }]
}
</script>-->
@stop