<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Educationhunnepal">
    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,700' rel='stylesheet' type='text/css'>
    <link href="{{ url('public/site/fonts/font-awesome.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ url('public/site/bootstrap/css/bootstrap.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ url('public/site/css/bootstrap-select.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ url('public/site/css/jquery.slider.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ url('public/site/css/owl.carousel.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ url('public/site/css/style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ url('public/site/css/magnific-popup.css') }}" type="text/css">
    <link rel="shortcut icon" type="image/x-icon" href="{{url('public/site/img/favicon.ico')}}">
    <script type="text/javascript" src="{{ url('public/site/js/jquery-2.1.0.min.js') }}"></script>
    @if(isset($pagetitle))
        <title>{{$pagetitle}}</title>
    @else
        <title>EducationHub Nepal | Search & Find Your Next Education</title>
    @endif
</head>