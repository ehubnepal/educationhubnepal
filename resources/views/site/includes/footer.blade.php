<!-- Page Footer -->
    <footer id="page-footer">
        <div class="inner">
            <aside id="footer-main">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <article>
                                <h3>About Us</h3>
                                <p>Education Hub Nepal is a comprehensive search portal for finding studies, courses, colleges, exams and universities inside nepal, we have information of as many colleges
                                    and as many courses that're currently being taught in Nepal making your college and study search a breeze. Choose your next study target.
                                </p>
                                <hr>
                                <a href="{{url('about')}}" class="link-arrow">Read More</a>
                            </article>
                        </div><!-- /.col-sm-3 -->
                        <div class="col-md-6 col-sm-6">
                            <article>
                                <h3>We're Volunteers</h3>
                                <p>We work hard everyday and night to improve the information inside EducationHubNepal, currently over 200 volunteers are helping us to enhance the information about colleges, courses and universities. Add reviews, suggest edit, and update the information, your small contribution means alot to everyone seeking right information. Come join us to solve the problem together. </p>
                            </article>
                            <section class="find-us social-area">
                            <div class="">
                            FIND US &nbsp;
                            <a class="fa fa-facebook btn btn-grey-dark" target="_blank" href="http://facebook.com/educationhubnepal/"></a>
                                <a class="fa fa-twitter btn btn-grey-dark" target="_blank" href="https://twitter.com/educationhubnp"></a>
                                <a class="fa fa-google btn btn-grey-dark" target="_blank" href="https://plus.google.com/+Educationhubnepal/"></a>
                                
                            </div>
                        </section>
                        </div><!-- /.col-sm-3 -->
                    </div>
                    <!--
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <article>
                                <h3>Contact</h3>
                                <address>
                                    <strong>Education Hub Nepal</strong><br>
                                    SVG Tech<br>
                                    DurbarMarga, Kathmandu, Nepal
                                </address>
                               
                                <a href="mailto:info@educationhubnepal.com">info@educationhubnepal.com</a>
                            </article>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <article>
                                <h3>Useful Links</h3>
                                <ul class="list-unstyled list-links">
                                    <li><a href="{{url('blog')}}">Blog</a></li>
                                    <li><a href="{{url('#about')}}">About</a></li>
                                    <li><a href="{{url('privacy-policy')}}">Privacy Policy</a></li>
                                    <li><a href="{{url('signup')}}">Login and Register Account</a></li>
                                    <li><a href="{{url('faq')}}">FAQ</a></li>
                                    <li><a href="{{url('terms-and-condition')}}">Terms and Conditions</a></li>
                                </ul>
                            </article>
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </aside><!-- /#footer-main -->
            <aside id="footer-thumbnails" class="footer-thumbnails">
                <div class="container">
                    <img src="{{url('/public/site/img/ehn_logo.png')}}"/>
                    <ul class="list-unstyled">
                        <li><a href="{{url('blog')}}">Blog</a></li>
                        <li><a href="{{url('about')}}">About</a></li>
                        <li><a href="{{url('signup')}}">Login/Register</a></li>
                        <li><a href="{{url('faq')}}">FAQ</a></li>
                        
                    </ul>
                </div>
            </aside><!-- /#footer-thumbnails -->
            <aside id="footer-copyright">
                <div class="container">
                    <span>Copyright © 2016. All Rights Reserved.</span>
                    <ul class="pull-right">
                        <li><a href="{{url('privacy-policy')}}">Privacy Policy</a></li>
                        <li><a href="{{url('terms-and-condition')}}">Terms and Conditions</a></li>
                        <li><a href="#page-top" class="roll">Go to top</a></li>
                    </ul>
                </div>
            </aside>
        </div><!-- /.inner -->
    </footer>
    <!-- end Page Footer -->