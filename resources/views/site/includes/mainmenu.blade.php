<div class="container">
    <header class="navbar" id="top" role="banner">
        <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="navbar-brand nav" id="brand">
                <a href="{{url('/')}}">

                    <img src="{{ url('public/site/img/ehublogo.png') }}">
                    EducationHubNepal<sub>Find your next education</sub></a>
            </div>
        </div>
        <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
            <ul class="nav navbar-nav">
                <li class="<?php echo $_SERVER["REQUEST_URI"]=='/'?'active':'' ?>"><a href="{{url('/')}}"><i class="fa fa-home"></i> Home</a> </li>
                <li class="<?php echo $_SERVER["REQUEST_URI"]=='/colleges'?'active':''?>"><a href="{{url('/colleges')}}"><i class="fa fa-mortar-board"></i> Colleges</a> </li>
                <li class="<?php echo $_SERVER["REQUEST_URI"]=='/programs'?'active':''?>"><a href="{{url('/programs')}}"><i class="fa fa-book"></i> Courses</a> </li>
                <li class="<?php echo $_SERVER["REQUEST_URI"]=='/universities'?'active':''?>"><a href="{{url('/universities')}}"><i class="fa fa-university"></i> Universities</a> </li>
                <li class="<?php echo $_SERVER["REQUEST_URI"]=='/categories'?'active':''?>"><a href="{{url('/categories')}}"><i class="fa fa-bookmark"></i> Faculties</a> </li>
                <li class="<?php echo $_SERVER["REQUEST_URI"]=='/blog'?'active':''?>"><a href="{{url('/blog')}}"><i class="fa fa-feed"></i> Blog</a> </li>
                <li class="<?php echo $_SERVER["REQUEST_URI"]=='/contact'?'active':''?>"><a href="{{url('/contact')}}"><i class="fa fa-envelope"></i> Contact</a></li>
            </ul>
        </nav><!-- /.navbar collapse-->
        
    </header><!-- /.navbar -->
</div><!-- /.container -->