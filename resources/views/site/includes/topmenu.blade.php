<div class="secondary-navigation">
    <div class="container">
        <div class="contact">
            <figure><strong>We are volunteers. Your contribution means a lot.</strong></figure>
            <figure><strong>Email:</strong>info@educationhubnepal.com</figure>
        </div>
        <div class="user-area">
            <div class="social">
                <div id="fb-root"></div>
                <script>(function(d, s, id) {
                  var js, fjs = d.getElementsByTagName(s)[0];
                  if (d.getElementById(id)) return;
                  js = d.createElement(s); js.id = id;
                  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6&appId=1374507662867919";
                  fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
               <div class="fb-like" data-href="https://facebook.com/educationhubnepal" data-width="250" data-layout="standard" data-action="like" data-show-faces="false" data-share="true"></div>   
            </div>
            <div class="actions">
                <a href="http://facebook.com/educationhubnepal/" target="_blank"><i class="fa fa-facebook"></i></a>
                <a href="https://twitter.com/educationhubnp" target="_blank"><i class="fa fa-twitter"></i></a>
                <a href="https://plus.google.com/+Educationhubnepal/" target="_blank"><i class="fa fa-google"></i></a>
                <!--<a href="create-agency.html" class="promoted"><strong>Advertiser</strong></a>-->
                @if(Auth::user())
                <span>
                <a href="{{url('member/home')}}" class="promoted"><i class="fa fa-user"></i> {{$USER->fullname}}</a> &nbsp;| 
                <a href="{{url('logout')}}" ><i class="fa fa-lock"></i> Logout</a></span>
                @else
                <span><a href="{{url('signup')}}" class="promoted">Register</a>
                <a href="{{url('login')}}">Sign In</a></span>
                @endif
                &nbsp;
                <span class="ehnsearchlens"><i class="fa fa-search"></i> Search</span>
            </div>
        </div>
    </div>
</div>
