<link type="text/css" rel="stylesheet" href="{{ url('public/site/css/style.custom.css') }}" />

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="{{ url('public/site/js/jquery-migrate-1.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ url('public/site/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ url('public/site/js/smoothscroll.js') }}"></script>
<script type="text/javascript" src="{{ url('public/site/js/markerwithlabel_packed.js') }}"></script>
<script type="text/javascript" src="{{ url('public/site/js/infobox.js') }}"></script>
<script type="text/javascript" src="{{ url('public/site/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ url('public/site/js/bootstrap-select.min.js') }}"></script>
<script type="text/javascript" src="{{ url('public/site/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ url('public/site/js/jquery.placeholder.js') }}"></script>
<script type="text/javascript" src="{{ url('public/site/js/icheck.min.js') }}"></script>
<script type="text/javascript" src="{{ url('public/site/js/jquery.vanillabox-0.1.5.min.js') }}"></script>
<script type="text/javascript" src="{{ url('public/site/js/retina-1.1.0.min.js') }}"></script>
<script type="text/javascript" src="{{ url('public/site/js/jshashtable-2.1_src.js') }}"></script>
<script type="text/javascript" src="{{ url('public/site/js/jquery.numberformatter-1.2.3.js') }}"></script>
<script type="text/javascript" src="{{ url('public/site/js/tmpl.js') }}"></script>
<script type="text/javascript" src="{{ url('public/site/js/jquery.dependClass-0.1.js') }}"></script>
<script type="text/javascript" src="{{ url('public/site/js/draggable-0.1.js') }}"></script>
<script type="text/javascript" src="{{ url('public/site/js/jquery.slider.js') }}"></script>
<script type="text/javascript" src="{{ url('public/site/js/markerclusterer_packed.js') }}"></script>
<script type="text/javascript" src="{{ url('public/site/js/custom-map.js') }}"></script>
<script type="text/javascript" src="{{ url('public/site/js/custom.js') }}"></script>
<script type="text/javascript" src="{{ url('public/site/js/jquery.magnific-popup.min.js') }}"></script>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-575bf367d805b15f"></script>

<!--[if gt IE 8]>
<script type="text/javascript" src="{{ url('public/site/js/ie.js') }}"></script>
<![endif]-->
<script>
_latitude = 48.87;
_longitude = 2.29;
createHomepageGoogleMap(_latitude, _longitude);
$(window).load(function () {
    initializeOwl(false);
});
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ehnsearchlens').click(function () {
            $('.ehnsearch').show();
            $('#ehnsearchinput').focus();
        });
    });
</script>
<?php /*
  <!--Start of Tawk.to Script-->
  <script type="text/javascript">
  var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
  (function(){
  var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
  s1.async=true;
  s1.src='https://embed.tawk.to/574d3ec51be2250b3bd2fae9/default';
  s1.charset='UTF-8';
  s1.setAttribute('crossorigin','*');
  s0.parentNode.insertBefore(s1,s0);
  })();
  </script>
  <!--End of Tawk.to Script-->
 */ ?>