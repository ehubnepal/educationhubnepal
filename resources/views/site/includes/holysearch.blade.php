<style type='text/css'>
    .ehnsearch{
        height:115px;
        background:#fff;
        position:fixed;
        top:0;
        width:100%;
        z-index: 9999;
        border-bottom: 1px solid #ccc;
        /*display:none;*/
    }
    .ehnsearch #ehnsearchinput{
        font-size:3em;
        font-family:open sans;
        font-weight:300;
        padding-left:40px;
    }
    .searchoptionwrapper{
        padding-top: 6px;
        padding-left: 40px;
    }
    .searchoptionwrapper label{
        padding:5px 10px;
        background:#CCC;
        color:#333;;
    }
    .searchoptionwrapper label.active{
        background:#1695a3;
        color:white;
    }
    .searchoptionwrapper .closebutton{
        margin-right:20px;
        cursor: default;
    }
    .searchoptionwrapper .closebutton:hover{
        color:#1695a3;
    }
    
</style>
<div class='ehnsearch' style="display:none">
    <div class='searchwrapper'>
        <form action="{{url('search')}}" method="get" role="form" id="form-map-sale" class="form-map form-search clearfix has-dark-background">
            <input type='search' name="query" class='form-control' id="ehnsearchinput" autofocus placeholder='Search college'/>
            <input type="hidden" name="searchby" id="searchby" value="college" />
            <input type="submit" style="display:none" />
        </form>
    </div>
    <div class='searchoptionwrapper'>
        Search : 
        <label class='label active ehncollege'>College</label>
        &nbsp;
        <label class='label ehnprogram'>Program</label>
        <span class='pull-right closebutton'><i class='fa fa-close'></i> Close</span>
    </div>
</div>

<link href="{{ url('public/site/libs/autocomplete/jquery.autocomplete.css') }}" rel="stylesheet" type="text/css">
<script type="text/javascript" src="{{ url('public/site/libs/autocomplete/jquery.autocomplete.js') }}"></script>

<script type='text/javascript'>
$(document).ready(function () {
    $('#page-content,.closebutton,.homepage-search,.gridimage').click(function () {
        $('.ehnsearch').hide();
    });
});

$('#ehnsearchinput').autocomplete({
    valueKey: 'title',
    source: [{
            url: "{{url('search/autosuggestcollege')}}?query=%QUERY%",
            type: 'remote',
            getValue: function (item) {
                return item;
            },
            ajax: {
                dataType: 'json'
            },
            hintStyle: {color: '#333'},
            minLength: 3,
            limit: 15
        }]
});
jQuery('#ehnsearchinput').autocomplete('update');
$(document.body).on('click', '.ehncollege', function () {
    $('.searchoptionwrapper label').removeClass('active');
    $('.ehncollege').addClass('active');
    $('.searchby').val('college');
    $('#ehnsearchinput').attr('placeholder', 'Enter college name');
    var first = $('#ehnsearchinput')
            .autocomplete('getSource', 0);
    first.url = "{{url('search/autosuggestcollege')}}?query=%QUERY%";
});

$(document.body).on('click', '.ehnprogram', function () {
    $('.searchoptionwrapper label').removeClass('active');
    $('.ehnprogram').addClass('active');
    $('.searchby').val('program');
    $('#ehnsearchinput').attr('placeholder', 'Enter course eg. BBA');
    var first = $('#ehnsearchinput')
            .autocomplete('getSource', 0);
    first.url = "{{url('search/autosuggestprogram')}}?query=%QUERY%";
});

</script>