<div class="col-md-6 col-sm-6 col-md-6 col-xs-12">
    <h3>Signing up is easy</h3>
    <div class="social-login">
        <a href="{!!URL::to('login/facebook')!!}"><img src="{{url('/public/site/img/signup-facebook.png')}}"/></a>
        <a href="{!!URL::to('login/google')!!}"><img src="{{url('/public/site/img/signup-google.png')}}"/></a>
        <span class="signin-option"><span>OR</span><hr/></span>
    </div>
    {!! Form::open(['url'=>'signup','method'=>'post','id'=>'form-create-account','role'=>'form']) !!}
    <div class="form-group">
        <label for="form-create-account-full-name">Full Name:</label>
        <input type="text" name="fullname" class="form-control" id="form-create-account-full-name" required>
    </div><!-- /.form-group -->
    <div class="form-group">
        <label for="form-create-account-email">Email:</label>
        <input type="email" name="email" class="form-control" id="form-create-account-email" required>
    </div><!-- /.form-group -->
    <div class="form-group">
        <label for="form-create-account-password">Password:</label>
        <input type="password" name="password" class="form-control" id="form-create-account-password" required>
    </div><!-- /.form-group -->
    <div class="form-group clearfix">
        <button type="submit" class="btn pull-right btn-default" id="account-submit">Create an Account</button>
    </div><!-- /.form-group -->
    {!! Form::close() !!}
    <hr>
    <div class="center">
        <figure class="note">By clicking the “Create an Account” button you agree with our <a href="terms-conditions.html">Terms and conditions</a></figure>
    </div>
</div>