<section id="search-filter">
                                <figure class="sorting-figure">
                                    <div class="sorting">
                                        <form role="form" id="form-sidebar" class="form-search" action="{{url('levels/search')}}">
                                            <input type="hidden" name="level_id" value="{{$level->id}}"/>
                                        <div class="form-group">
                                            <span>Categories</span>
                                            <select name="category">
                                                <option value="">All type</option>
                                                @foreach($categories as $category)
                                                <option value="{{$category->category}}">{{$category->category}}</option>
                                                @endforeach
                                            </select>
                                        </div><br/>
                                        <div class="form-group">
                                            <span>Universities</span>
                                            <select name="affiliation">
                                                <option value="">All </option>
                                                @foreach($universities as $university)
                                                <option value="{{$university->aname}}">{{$university->aname}}</option>
                                                @endforeach
                                            </select>
                                        </div><br/>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-default search-button">Search</button>
                                        </div>
                                    </form>
                                    </div>
                                </figure>
                            </section>