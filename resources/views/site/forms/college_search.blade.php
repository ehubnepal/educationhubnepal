<div class="search-box-wrapper">
    <div class="search-box-inner no-padding">
        <div class="container">
            <div class="col-md-8 col-md-offset-2 search-box map">
                <div class="tab-pane fade in active" id="search-form-college">

                    <form action="{{url('searchcollege')}}" method="get" role="form" id="form-map-sale" class="form-map form-search clearfix has-dark-background">
                        <input type="hidden" name="searchby" value="college" />
                        <section id="search-filter" class="row">
                            <figure class="sorting-figure"> <!--/ data-offset-top="197" data-spy="affix"-->
                                <div class="sorting row">
                                    <div class="form-group col-md-4">
                                        <span>Institution type</span>
                                        <select name="institutiontype">
                                            <option value="all">All</option>
                                            <option value="national_affiliation" <?php isset($param['institutiontype']) && $param['institutiontype']=="national_affiliation"?print "selected='selected'":'';?> >National affiliation</option>
                                            <option value="intl_affiliation" <?php isset($param['institutiontype']) && $param['institutiontype']=="intl_affiliation"?print "selected='selected'":'';?>>Intl. Affiliation</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <span>Location</span>
                                        <select name="location">
                                            <option value="all">All Nepal</option>
                                            <option value="inside_kathmandu" <?php isset($param['location']) && $param['location']=="inside_kathmandu"?print "selected='selected'":'';?>>Inside kathmandu valley</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <span>Course level</span>
                                        <select name="level">
                                            <option value="0">All Level</option>
                                            <?php if (isset($levels)):
                                                foreach ($levels as $level):
                                                $selected =  isset($param['level']) && $param['level']==$level->id? "selected='selected'":'';?>
                                                    ?>
                                                    <option value="{{$level->id}}" {{$selected}}>{{$level->lname}}</option>
                                                <?php endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </div><!-- /.form-group -->
                                </div>
                            </figure>
                        </section>
                        <div class="row">
                            <div class="col-md-10 col-sm-4 no-padding">
                                <div class="form-group">
                                    <input type="search" placeholder="Keyword" id="searchkeyword" name="query" value="<?php isset($param['query']) ? print $param['query'] : ''; ?>" />
                                </div><!-- /.form-group -->
                            </div>
                            <div class="col-md-2 col-sm-4 no-padding">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-default find-colleges">Find</button>
                                </div><!-- /.form-group -->
                            </div>
                        </div>
                    </form><!-- /#form-map-sale -->
                </div><!-- /#browse-by-course -->


            </div><!-- /.search-box -->
        </div><!-- /.container -->
    </div><!-- /.search-box-inner -->
    <div class="background-image"><img class="opacity-20" src="{{ url('public/site/img/searchbox-bg.jpg')}}"></div>
</div>
<!-- end Search Box -->
@section('scripts')
<link href="{{ url('public/site/libs/autocomplete/jquery.autocomplete.css') }}" rel="stylesheet" type="text/css">
<script type="text/javascript" src="{{ url('public/site/libs/autocomplete/jquery.autocomplete.js') }}"></script>
<script type='text/javascript'>

$('#searchkeyword').autocomplete({
    valueKey: 'title',
    source: [{
            url: "{{url('search/autosuggestcollege')}}?query=%QUERY%",
            type: 'remote',
            getValue: function (item) {
                return item;
            },
            ajax: {
                dataType: 'json'
            },
            hintStyle: {color: '#333'},
            minLength: 3,
            limit: 15
        }]
});
jQuery('#searchkeyword').autocomplete('update');
$('#searchby').change(function () {
    if ($(this).val() == 'program') {
        var first = $('#searchkeyword')
                .autocomplete('getSource', 0);
        first.url = "{{url('search/autosuggestprogram')}}?query=%QUERY%";
    } else {
        var first = $('#searchkeyword')
                .autocomplete('getSource', 0);
        first.url = "{{url('search/autosuggestcollege')}}?query=%QUERY%";
    }
});
</script>
@stop

