<div class="col-md-6 col-sm-6 col-md-6 col-xs-12 login-form">
    <h3>Already have an account?</h3>
    <div class="social-login">
        <a href="{!!URL::to('login/facebook')!!}"><img src="{{url('/public/site/img/fb-signin.png')}}"/></a>
        <a href="{!!URL::to('login/google')!!}"><img src="{{url('/public/site/img/google-signin.png')}}"/></a>
        <span class="signin-option"><span>OR</span><hr/></span>
    </div>
    {!! Form::open(['url'=>'login','method'=>'post','id'=>'form-create-account','role'=>'form']) !!}
    <div class="form-group">
        <label for="form-create-account-email">Email:</label>
        <input type="email" name="username" class="form-control" id="form-create-account-email" required>
    </div><!-- /.form-group -->
    <div class="form-group">
        <label for="form-create-account-password">Password:</label>
        <input type="password" name="password" class="form-control" id="form-create-account-password" required>
    </div><!-- /.form-group -->
    <div class="form-group clearfix">
        <button type="submit" class="btn pull-right btn-default" id="account-submit">Sign to My Account</button>
    </div><!-- /.form-group -->
    {!! Form::close() !!}
    <hr>
    <!--<div class="forgot-link"><a href="forgotpassword">I don't remember my password</a></div>-->
</div>