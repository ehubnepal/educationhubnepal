@extends('site.sitemaster')
@section('maincontent')
<div class="container">
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}">Home</a></li>
        <li class="active">Error</li>
    </ol>
</div>
<!-- end Breadcrumb -->

<div class="container">
    <header><h1>Token Expired</h1></header>
    <div class='alert alert-danger'>The token has expired. Please go back. Refresh the page and try to submit the form again.</div>
</div><!-- /.container -->
@stop