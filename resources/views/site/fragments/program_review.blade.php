<section id="college-review" class="program-review">
    @if(Auth::user())
    @if(isset($program->myreview) && !empty($program->myreview))
    <div class="self-review">
        <div>
            <span class="profile-picture"><img src="{{url('public/site/img/ehublogo.png')}}" /></span>
            <div class="review-rating">
                <span class="my-rating user-rating rating-{{$program->myreview->star}}"></span>
                <span class="reviewer">{{$program->myreview->fullname}}</span>
                <span class="review-date">{{$program->myreview->created_at}}}</span>
            </div>
        </div>
        <span class="my-review user-review">
            {{$program->myreview->review}}
        </span>
    </div>
    @else
        <div class="review-form">

        <div class="well well-sm">
<!--            <div class="text-right">
                <a class="btn btn-success btn-green" href="#reviews-anchor" id="open-review-box">Leave a Review</a>
            </div>-->
        
            <div class="row" id="post-review-box">
                <div class="col-md-12">
                    {!! Form::open(['url'=>'review/reviewprogram','files'=>'true','method'=>'post']) !!}
                        <input type='hidden' name='cid' value='{{$program->id}}' />
                        <input id="ratings-hidden" name="rating" type="hidden">
                        <!--<input type="text" name="title" required="required" placeholder="Review title" />--> 
                        {!! Form::textarea('review', null, ['class'=>'form-control','rows'=>3,'id'=>'review','placeholder'=>'Write a review']) !!}
        
                        <div class="text-right review-action">
                            <div class="stars starrr" data-rating="0"></div>
                            <!--<a class="btn btn-danger" href="#" id="close-review-box" style="display:none; margin-right: 10px;"><i class="fa fa-times"></i> Cancel</a>-->
                            <button class="btn btn-success" type="submit"><i class="fa fa-paper-plane-o"></i> Save</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
            <hr/>
        </div>
    @endif
    @if(count($program->reviews)>0)
    <h2>All Reviews ({{count($program->reviews)}})</h2>
    <ul class='college_review'>
        @foreach($program->reviews as $review)
        <li>
            <div>
                <span class="profile-picture"><img src="{{url('public/site/img/ehublogo.png')}}" /></span>
                <div class="review-rating">
                    <span class="user-rating rating-{{$review->star}}"></span>
                    <span class="reviewer">{{$review->user->fullname}}</span>
                    <span class="review-date">{{$review->created_at}}</span>
                </div>
            </div>
            <span class="user-review">
                <!--<h3>Review title something awesome</h3>-->
                {{$review->review}}
            </span>
        </li>
        @endforeach
    </ul>
    @endif
    @else
    <header>
        <h4  class="empty"> No one has reviewed this Program yet. Login with social media to add a review.</h4>
    <div class="text-center">
            <?php $redirect_url = Request::url(); ?>
            <a href="{{url('login/facebook?redirect_url='.$redirect_url.'#reviews')}}" style="margin-bottom:10px;display:inline-block"><img src="{{url('public/site/img/fb-signin.png')}}"></a>
            <a href="{{url('login/google?redirect_url='.$redirect_url.'#reviews')}}"><img src="{{url('public/site/img/google-signin.png')}}"></a>
        </div>
    </header>
    @endif
</section>