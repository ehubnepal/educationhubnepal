<section id="property-gallery collge-gallery">
    @if(Auth::user())
    @if(isset($program->myreview) && !empty($program->myreview))
    <img src="{{url('public/site/img/ehublogo_white.png')}}" />
    {{$program->myreview->review}}
    <br/>
    Rating: {{$program->myreview->star}} stars
    @else
    {!! Form::open(['url'=>'review/reviewprogram','files'=>'true','method'=>'post']) !!}
    {!! Form::textarea('review', null, ['class'=>'form-control','rows'=>3,'id'=>'review','placeholder'=>'Write a review']) !!}
    <input type='hidden' name='pid' value='{{$program->id}}' />
    Rating : 
    <input type="radio" name="rating" value="1">1  &nbsp;
    <input type="radio" name="rating" value="2">2  &nbsp;
    <input type="radio" name="rating" value="3">3  &nbsp;
    <input type="radio" name="rating" value="4">4  &nbsp;
    <input type="radio" name="rating" value="5">5
    <br/>
    <button type="submit" class="btn btn-success">Save</button>
    {!! Form::close() !!}
    <hr/>
    @endif
    @if(count($program->reviews)>0)
    <h2>All Reviews ({{count($program->reviews)}})</h2>
    <ul class='college_review'>
        @foreach($program->reviews as $review)
        <li>
            <img src="{{url('public/site/img/ehublogo_white.png')}}" />
            {{$review->review}}
            <br/>
            Rating: {{$review->star}} stars
        </li>
        @endforeach
    </ul>
    @endif
    @else
    <header><h4  class="empty"> No one has reviewed this program yet. Please <a href="{{url('login')}}">login</a> to add a review.</h4></header>
    @endif
</section>