<style>
    .know_what_you_can_do{ background: #225378;
    border: 1px solid #ccc;
    padding: 14px 30px;
    border-radius: 16px;
    cursor:default;
    color: white;}
</style>
<div class="search-box-wrapper homepage-search">
    <div class="search-box-inner">
        <div class="container">
            <div class="col-md-8 col-md-offset-2 search-box map">
                <div class="row banner-text">
                    <h1>Find over <span>1600 colleges</span> / <span>800 courses</span> in Nepal</h1>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="search-form-college">
                        <form action="{{url('search')}}" method="get" role="form" id="form-map-sale" class="form-map form-search clearfix has-dark-background">
                            <div class="row">
                                <div class="col-md-3 col-sm-4 select-search-type no-padding">
                                    <div class="form-group">
                                        <select name="searchby" id="searchby">
                                            <option value="college">College</option>
                                            <option value="program">Course</option>
                                        </select>
                                    </div><!-- /.form-group -->
                                </div>
                                <div class="col-md-7 col-sm-4 no-padding keyword-field">
                                    <div class="form-group">
                                        <input type="search" placeholder="Enter college name" id="searchkeyword" name="query" required=""/>
                                    </div><!-- /.form-group -->
                                </div>
                                <div class="col-md-2 col-sm-4 no-padding search-submit">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-default find-colleges">Search</button>
                                    </div><!-- /.form-group -->
                                </div>
                                
                                <span class="know_what_you_can_do" onclick="$('.gridimage').slideDown();$('html, body').animate({scrollTop:0}, 'slow');"> Know what you can do</span>
                            </div>
                        </form><!-- /#form-map-sale -->
                    </div><!-- /#browse-by-course -->
                </div><!-- /.tab-content -->
            </div><!-- /.search-box -->
        </div><!-- /.container -->
    </div><!-- /.search-box-inner -->
    <div class="background-image"><img class="opacity-20" src="{{ url('public/site/img/searchbox-bg.jpg')}}"></div>
</div>
<!-- end Search Box -->
@section('scripts')
<link href="{{ url('public/site/libs/autocomplete/jquery.autocomplete.css') }}" rel="stylesheet" type="text/css">
<script type="text/javascript" src="{{ url('public/site/libs/autocomplete/jquery.autocomplete.js') }}"></script>
<script type='text/javascript'>

$('#searchkeyword').autocomplete({
    valueKey: 'title',
    source: [{
            url: "{{url('search/autosuggestcollege')}}?query=%QUERY%",
            type: 'remote',
            getValue: function (item) {
                return item;
            },
            ajax: {
                dataType: 'json'
            },
            hintStyle:{color:'#333'},
            minLength:3,
            limit:15
        }]
});
jQuery('#searchkeyword').autocomplete('update');
$('#searchby').change(function () {
    if ($(this).val() == 'program') {
        $('#searchkeyword').attr('placeholder','Enter course eg. BBA');
        var first = $('#searchkeyword')
                .autocomplete('getSource', 0);
        first.url = "{{url('search/autosuggestprogram')}}?query=%QUERY%";
    } else {
        $('#searchkeyword').attr('placeholder','Enter college name');
        var first = $('#searchkeyword')
                .autocomplete('getSource', 0);
        first.url = "{{url('search/autosuggestcollege')}}?query=%QUERY%";
    }
});
</script>
@stop