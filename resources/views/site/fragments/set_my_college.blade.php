<table width='100%' style='margin-bottom:6px;border: 1px dashed #ccc'>
    <tr>
        <td style='padding:6px'>
            <h3 style='display:inline-block'> Please Select: </h3> &nbsp;

            <?php
            if (isset($college->isCurrentCollege) && $college->isCurrentCollege):
                $class1 = "btn-success";
                $icon1 = '<i class ="fa fa-check"></i>';
                $href1 = url('member/currently_studying_college/remove/' . $college->id);
            else:
                $class1 = 'btn-info';
                $icon1 = '';
                $href1 = url('member/currently_studying_college/add/' . $college->id);
                ;
            endif;

            if (isset($college->isPastCollege) && $college->isPastCollege):
                $class2 = "btn-success";
                $icon2 = '<i class="fa fa-check"></i>';
                $href2 = url('member/previously_studied_college/remove/' . $college->id);
            else:
                $class2 = 'btn-info';
                $icon2 = '';
                $href2 = url('member/previously_studied_college/add/' . $college->id);
            endif;
            ?>
            @if(Auth::user())
            <a class='btn {{$class1}}' href='{{$href1}}' data="current">
                {!! $icon1 !!}
                I am currently studying here
            </a> &nbsp; &nbsp; OR &nbsp; &nbsp;
            <a class='btn {{$class2}}' href='{{$href2}}' data="past">
                {!! $icon2 !!}
                I have studied in this college
            </a>
            @else:
            <a class='btn {{$class1}} quick_login' href='#social_login' data="current">
                {!! $icon1 !!}
                I am currently studying here
            </a> &nbsp; &nbsp; OR &nbsp; &nbsp;
            <a class='btn {{$class2}} quick_login' href='#social_login' data="past">
                {!! $icon2 !!}
                I have studied in this college
            </a>
            @endif
        </td>
    </tr>
</table>
<div style="display:none">
    <div id="social_login">
        <div class="text-center">
            <?php $redirect_url = Request::url(); ?>
            <a href="{{url('login/facebook')}}" class="loginlink" data="facebook" style="margin-bottom:10px;display:inline-block"><img src="{{url('public/site/img/fb-signin.png')}}"></a>
            <a href="{{url('login/google')}}" class="loginlink" data="google"><img src="{{url('public/site/img/google-signin.png')}}"></a>
        </div>
    </div>
</div>

<script type="text/javascript" src="{{url('public/site/libs/colorbox/jquery.colorbox-min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{url('public/site/libs/colorbox/colorbox.css')}}" />
<script>
var currently_studyng = "{{url('member/currently_studying_college/add/' . $college->id)}}";
var past_study = "{{url('member/previously_studied_college/add/' . $college->id)}}";
var base = "{{url('/')}}";
$(document.body).on('click', '.quick_login', function () {
    if ($(this).attr('data') == "current") {
        $('.loginlink').each(function () {
            $(this).attr('href', base + '/login/' + $(this).attr('data') + "?redirect_url=" + currently_studyng);
        });
        // $('.loginlink').attr('href',  $('.loginlink').attr('href') + "?redirect_url=" + currently_studyng);
    } else {
        $('.loginlink').each(function () {
            $(this).attr('href', base + '/login/' + $(this).attr('data') + "?redirect_url=" + past_study);
        });
        //$('.loginlink').attr('href', $('.loginlink').attr('href')  + "?redirect_url=" + past_study);
    }
});
$('.quick_login').colorbox({inline: true});
</script>