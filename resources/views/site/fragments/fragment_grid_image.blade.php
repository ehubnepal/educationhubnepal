<style type="text/css">
    .gridicons{padding-top:50px;padding-bottom:20px;}
    .thinbanner{border-top:1px dashed #CCC;padding-top:20px}
    .close_thinbanner{cursor:default}
</style>
<div class="gridimage container gridicons" style="display:none">
    <div class="row text-center">
        <div class="col-md-3 col-xs-6">
            <img src="{{url('public/site/img/gridicon/search_ehn.png')}}" />
            <br/><br/>
            <p>Search for</p>
            <h3>Colleges and Programs</h3>
        </div>
        <div class="col-md-3 col-xs-6">
            <img src="{{url('public/site/img/gridicon/college_ehn.png')}}" />
            <br/><br/>
            <p>Get updates from your</p>
            <h3>Favorite Colleges</h3>
        </div>
        <div class="col-md-3 col-xs-6">
            <img src="{{url('public/site/img/gridicon/review_ehn.png')}}" />
            <br/><br/>
            <p>Write a blog or</p>
            <h3>Review Colleges and Programs</h3>
        </div>
        <div class="col-md-3 col-xs-6">
            <img src="{{url('public/site/img/gridicon/help_ehn.png')}}" />
            <br/><br/>
            <p>Become a contributer and</p>
            <h3>Help others</h3>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="row thinbanner text-center">
        <div class="col-md-10">
            <div class="social-login">
                <a href="{{url('login/facebook')}}"><img src="{{url('public/site/img/signup-facebook.png')}}"></a>
                <a href="{{url('login/google')}}"><img src="{{url('public/site/img/signup-google.png')}}"></a>
            </div>
        </div>
        <div class="col-md-2"><span class="close_thinbanner text-danger pull-right" onclick="$('.gridimage').slideUp()"><i class="fa fa-close"></i> close</span></div>
    </div>
</div>