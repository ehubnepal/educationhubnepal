@extends('superuser.superusermaster')
@section('maincontent')

<section class="content">
    {!! Form::open(['url'=>'superuser/collegeprograms/update','files'=>'true','method'=>'post']) !!}
    {!! Form::hidden('id',$collegeprogram->id) !!}
    {!! Form::hidden('pname',$collegeprogram->pname) !!}
    <input type="hidden" name="college_id" value="{{$collegeprogram->college_id}}" />
    <div class="row">
        <div class="col-md-12"> 
            <!-- /.box-body -->
            <div class="box-footer text-right">
                <button type="submit" class="btn btn-success">Save</button>
                <button type="reset" class="btn btn-primary">Reset</button>
                <a href="{{ url('superuser/home') }}" class="btn btn-danger">Cancel</a> </div>
            <!-- /.box --> 
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"> Edit detail of <span class='text-info'>{{$collegeprogram->pname}}</span></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class='row'>
                        <div class='col-md-6'>
                            <div class="form-group">
                                <?php $no_of_seat = isset($collegeprogram->no_of_seat) ? $collegeprogram->no_of_seat : ''; ?>
                                {!! Form::label('no_of_seat','Number of seats') !!}
                                {!! Form::text('no_of_seat', $no_of_seat ,['class'=>'form-control','id'=>'no_of_seat','placeholder'=>'eg 60']) !!}
                            </div>
                            <div class="form-group">
                                <?php $total_fee = isset($collegeprogram->total_fee) ? $collegeprogram->total_fee : ''; ?>
                                {!! Form::label('total_fee','Total fee ') !!}
                                {!! Form::text('total_fee', $total_fee,['class'=>'form-control','id'=>'total_fee','placeholder'=>'Eg. 2,00,000']) !!}
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class="form-group">
                                <?php $program_since = isset($collegeprogram->program_since) ? $collegeprogram->program_since : ''; ?>
                                {!! Form::label('program_since','Program since ') !!}
                                {!! Form::text('program_since', $program_since,['class'=>'form-control','id'=>'program_since','placeholder'=>'eg. 2006']) !!}
                            </div>
                            <div class="form-group">
                                <?php $weight = isset($collegeprogram->weight) ? $collegeprogram->weight : ''; ?>
                                {!! Form::label('weight','Weight (ordering) ') !!}
                                {!! Form::text('weight', $weight,['class'=>'form-control','id'=>'weight','placeholder'=>'Autopopulate latitudeEg. 10']) !!}
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-md-12'>
                            <div class="form-group">
                                <?php $fee_structure = isset($collegeprogram->fee_structure) ? $collegeprogram->fee_structure : ''; ?>
                                {!! Form::label('fee_structure','Fee Structure') !!}
                                {!! Form::textarea('fee_structure', $fee_structure, ['class'=>'form-control','id'=>'editor0','placeholder'=>'Fee Structure']) !!}
                            </div>
                            
                            <div class="form-group">
                                <?php $description = isset($collegeprogram->description) ? $collegeprogram->description : ''; ?>
                                {!! Form::label('description','Description ') !!}
                                {!! Form::textarea('description', $description, ['class'=>'form-control','id'=>'description','placeholder'=>'Description']) !!}
                            </div>
                            <div class="form-group">
                                <?php $eligibility_criteria = isset($collegeprogram->eligibility_criteria) ? $collegeprogram->eligibility_criteria : ''; ?>
                                {!! Form::label('eligibility_criteria','Eligibility Criteria ') !!}
                                {!! Form::textarea('eligibility_criteria', $eligibility_criteria, ['class'=>'form-control','id'=>'eligibility_criteria','placeholder'=>'Eligibility criteria']) !!}
                            </div>
                            <div class="form-group">
                                <?php $scholorship_criteria = isset($collegeprogram->description) ? $collegeprogram->description : ''; ?>
                                {!! Form::label('scholorship_criteria','Scholorship criteria ') !!}
                                {!! Form::textarea('scholorship_criteria', $scholorship_criteria, ['class'=>'form-control','id'=>'scholorship_criteria','placeholder'=>'Scholorship criteria']) !!}
                            </div>
                            <div class="form-group">
                                <?php $additional_info = isset($collegeprogram->additional_info) ? $collegeprogram->additional_info : ''; ?>
                                {!! Form::label('additional_info','Additional info ') !!}
                                {!! Form::textarea('additional_info', $additional_info, ['class'=>'form-control','id'=>'additional_info','placeholder'=>'Additional info']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
</section>


@section('scripts')
<script src="{{url('public/admin/js/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
$(function () {
    CKEDITOR.replace('editor0');
    CKEDITOR.replace('description');
    CKEDITOR.replace('eligibility_criteria');
    CKEDITOR.replace('scholorship_criteria');
    CKEDITOR.replace('additional_info');
});
</script>
@stop


@stop
