@extends('superuser.superusermaster')
@section('maincontent')
<!-- Main content -->
<section class="content">
    {!! Form::open(['url'=>'superuser/college/update','files'=>'true','method'=>'post']) !!}
    {!! Form::hidden('id',$college->id) !!}
    {!! Form::hidden('cname',$college->cname) !!}
    <div class="row">
        <div class="col-md-12"> 
            <!-- /.box-body -->
            <div class="box-footer text-right">
                <button type="submit" class="btn btn-success">Save</button>
                <button type="reset" class="btn btn-primary">Reset</button>
                <a href="{{ url('superuser/home') }}" class="btn btn-danger">Cancel</a> </div>
            <!-- /.box --> 
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"> <?php isset($college) ? print 'Edit college (' . $college->cname . ')' : print 'Add new college'; ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">


                    <div class="form-group">

                        <?php $address = isset($college->address) ? $college->address : ''; ?>
                        {!! Form::label('address','Address * ') !!}
                        {!! Form::text('address', $address,['class'=>'form-control','id'=>'address','placeholder'=>'Enter address']) !!}
                    </div>
                    <div class="form-group">
                        <?php $longitude = isset($college->longitude) ? $college->longitude : ''; ?>
                        {!! Form::label('longitude','Longitude ') !!}
                        {!! Form::text('longitude', $longitude,['class'=>'form-control','id'=>'longitude','placeholder'=>'Autopopulate longitude']) !!}
                    </div>
                    <div class="form-group">
                        <?php $latitude = isset($college->latitude) ? $college->latitude : ''; ?>
                        {!! Form::label('latitude','Latitude ') !!}
                        {!! Form::text('latitude', $latitude,['class'=>'form-control','id'=>'latitude','placeholder'=>'Autopopulate latitude']) !!}
                    </div>
                    <div class="form-group">
                        <?php $district = isset($college->district) ? $college->district : ''; ?>
                        {!! Form::label('district','District ') !!}
                        {!! Form::text('district', $district,['class'=>'form-control','id'=>'district','placeholder'=>'Enter district']) !!}
                    </div>

                    <div class="form-group">
                        <?php $country = "Nepal" ?>
                        {!! Form::label('country','Country ') !!}
                        {!! Form::text('country', $country,['class'=>'form-control','id'=>'country','placeholder'=>'Country']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"> Contact option </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <?php $phone = isset($college->phone) ? $college->phone : ''; ?>
                        {!! Form::label('phone','Phone ') !!}
                        {!! Form::text('phone', $phone,['class'=>'form-control','id'=>'phone','placeholder'=>'Enter Phone']) !!}
                    </div>
                    <div class="form-group">
                        <?php $fax = isset($college->fax) ? $college->fax : ''; ?>
                        {!! Form::label('fax','Fax ') !!}
                        {!! Form::text('fax', $fax,['class'=>'form-control','id'=>'fax','placeholder'=>'Enter fax']) !!}
                    </div>
                    <div class="form-group">
                        <?php $website = isset($college->website) ? $college->website : ''; ?>
                        {!! Form::label('website','Website ') !!}
                        {!! Form::text('website', $website,['class'=>'form-control','id'=>'website','placeholder'=>'Enter website']) !!}
                    </div>
                    <div class="form-group">
                        <?php $pobox = isset($college->pobox) ? $college->pobox : ''; ?>
                        {!! Form::label('pobox','POBox ') !!}
                        {!! Form::text('pobox', $pobox,['class'=>'form-control','id'=>'pobox','placeholder'=>'Enter pobox']) !!}
                    </div>
                    <div class="form-group">
                        <?php $email = isset($college->email) ? $college->email : ''; ?>
                        {!! Form::label('email','Email ') !!}
                        {!! Form::text('email', $email,['class'=>'form-control','id'=>'email','placeholder'=>'Enter email']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"> <?php isset($college) ? print 'Edit college (' . $college->cname . ')' : print 'Add new college'; ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <?php $description = isset($college->description) ? $college->description : ''; ?>
                        {!! Form::label('description','Description ') !!}
                        {!! Form::textarea('description', $description, ['class'=>'form-control','id'=>'description','placeholder'=>'Description']) !!}
                    </div>
                    <div class="form-group">
                        <?php $facilities = isset($college->facilities) ? $college->facilities : ''; ?>
                        {!! Form::label('facilities','Facilities ') !!}
                        {!! Form::textarea('facilities', $facilities, ['class'=>'form-control','id'=>'facilities','placeholder'=>'Facilities']) !!}
                    </div>
                    <div class="form-group col-md-6">
                        {!! Form::label('collegeimage','Upload Images of your collge') !!} &nbsp;<span class="loading" style="display:none"> | Loading ...</span>
                        <div class="row">
                            <div class="col-sm-8">
                                {!! Form::file('collegeimage',['class'=>'form-control','id'=>'collegeimage']) !!}
                            </div>
                        </div>
                        <br/>
                        @if (!empty($college->media))
                        @foreach ($college->media as $media)
                        @if (@getimagesize(url($media->full_url)))
                        <div style="margin-bottom:10px;border-bottom:1px dashed #ccc;" class="row">
                            <div class="col-sm-4">
                                <img src="{{url($media->thumb_url)}}" />
                                <br/>
                            </div>
                            <div class="col-sm-8">
                                <a href="{{url('superuser/college/' . $media->id . '/removeimage')}}" class="text-danger"><i class="fa fa-trash"></i> Remove image</a>
                            </div>
                        </div>
                        @endif
                        @endforeach
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </div>
</form>
</section>
@stop

@section('scripts')
<script src="{{url('public/admin/js/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
$(function () {
    CKEDITOR.replace('description');
    CKEDITOR.replace('facilities');
});

</script>
@stop

