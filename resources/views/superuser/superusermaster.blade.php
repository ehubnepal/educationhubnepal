<!doctype html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title><?php isset($pagetitle)?print $pagetitle:print 'Education Hub Nepal';?></title>
        <link rel="stylesheet" type="text/css" href="{{ url('public/admin/css/style.css') }}" media="screen" />
        <link rel="stylesheet" type="text/css" href="{{ url('public/admin/css/navi.css') }}" media="screen" />
        <link rel="stylesheet" type="text/css" href="{{ url('public/admin/css/jquery-ui.css') }}" media="screen" />
<!--        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">-->
        <link rel="stylesheet" type="text/css" href="{{ url('public/admin/css/bootstrap.css') }}">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="{{ url('public/admin/css/dist/css/AdminLTE.min.css') }}">
        <link rel="stylesheet" href="{{ url('public/admin/css/dist/css/skins/_all-skins.min.css') }}">
        <link rel="stylesheet" href="{{ url('public/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
        <script src="{{ url('public/admin/js/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
$.widget.bridge('uibutton', $.ui.button);
        </script>
        <script type="text/javascript" src="{{ url('public/admin/js/tinymce.min.js') }}"></script>
        <script type="text/javascript" src="{{ url('public/admin/js/plugins/imagetools/plugin.min.js') }}"></script> 
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            @include('superuser.includes.header')
            @include('superuser.includes.navigation')

            <div class="content-wrapper"> 
                @if(isset($SERVER_MESSAGE))
                <br/>
                {!! $SERVER_MESSAGE !!}
                @endif
                @yield('maincontent')
            </div>


            <div class="clearfix"></div>
            <div class="control-sidebar-bg"></div>
        </div>
        @include('superuser.includes.footer')
        @include('superuser.includes.script')
        @yield('scripts')
    </body>
</html>