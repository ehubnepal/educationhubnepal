<?php /*@extends('superuser.superusermaster')
@section('maincontent')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Application buttons -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title text-center">Your Colleges and Programs</h3>
                </div>
                <div class="box-body">
                    <ul class="treeview-menu">
                        @foreach($colleges as $college)
                        <li> <a href="{{ url('superuser/college/'.$college->id.'/edit')}}"><span>{{$college->cname}}</span> </a> </li>
                        @endforeach
                    </ul>
                </div><!-- /.box -->
            </div>
        </div>
    </div>
</section>
@stop

*/ ?>

@extends('superuser.superusermaster')
@section('maincontent')
<?php
//echo '<pre>';
//print_r($brands);
//echo '</pre>';
?>
<section class="content actionbuttonbox">
    <div class="row">
        <div class="col-xs-12">
            <div class="box text-right actionbutton clearfix padding">
                <div class="col-sm-4">
                    <input type="search" placeholder="search" class="form-control search" autofocus="true">
                </div>
                <div class="col-sm-8">
                    <a class="btn btn-success" href="{{ url('superuser/news/create')}}"> <i class="fa fa-plus"></i> Add new </a> 
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">All News</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                        <th><input class="checkall" type="checkbox" onclick="checkAll(this)"></th>
                        <th>ID</th>
                        <th>College</th>
                        <th>News Title</th>
                        <th>Status</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach($news as $n)
                            <tr>
                                <td><input class="checksingle" type="checkbox" data-id="{{$n->id}}"></td>
                                <td> <a href="#">{{$n->id}}</a></td>
                                <td> <a href="{{url('superuser/news/'.$n->id.'/edit')}}" class="text-info"><i class="fa fa-pencil"></i> {{$n->cname}}</a></td>
                                <td> <a href="{{ url('supeuser/news/'.$n->id)}}">
                                        {{$n->title}} 
                                    </a></td>
                                <?php
                                if ($n->published == 0):
                                    $publishLink = '<a href="' . url('superuser/news/publish/' . $n->id) . '" class="text-danger" ><i class="fa fa-circle"></i> Unublished</a>';
                                else:
                                    $publishLink = '<a href="' . url('superuser/news/unpublish/' . $n->id) . '" class="text-success" ><i class="fa fa-circle"></i> Published</a>';
                                endif;
                                ?>
                                <td>{!! $publishLink !!}</td>
                                <td><a href="{{url('superuser/news/'.$n->id.'/edit')}}" class="text-info"><i class="fa fa-pencil"></i> Edit</a> |
                                    <a href="{{url('superuser/news/'.$n->id.'/delete')}}" class="text-danger"><i class="fa fa-trash"></i> Delete</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {!! $news->links() !!}
                </div>
                <!-- /.box-body --> 
            </div>
            <!-- /.box --> 
        </div>
    </div>
</section>
<!-- /.content --> 
@stop

@section('scripts')
<script type='text/javascript'>
    $('.search').keyup(function () {
        var val = $(this).val();
        $('tbody>tr').hide();
        $('tbody>tr').each(function () {
            if ($(this).text().toLowerCase().indexOf(val.toLowerCase()) > -1) {
                $(this).show();
            }
        });
    });
</script>
@stop
