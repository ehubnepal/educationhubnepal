@extends('superuser.superusermaster')
@section('maincontent')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Application buttons -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title text-center">Your Colleges and Programs</h3>
                </div>
                <div class="box-body">
                   @if(isset($colleges) && count($colleges)>0)
                   You are monitoring following colleges: <br/>
                   <ul style='list-style-type: none'>
                   @foreach($colleges as $college)
                    <li><i class='fa fa-university'></i> <a href="{{ url('superuser/college/'.$college->id.'/edit')}}"><span>{{$college->cname}}</span> </a> </li>
                    @endforeach</ul>
                   @endif
                    
                   <hr/>
                   
                    @if(isset($collegeprograms) && count($collegeprograms)>0)
                    You are monitoring following programs: <br/>
                    <ul style='list-style-type: none'>
                        @foreach($collegeprograms as $programs)
                            @foreach($programs as $cp)
                            <li> <i class='fa fa-graduation-cap'></i> <a href="{{ url('superuser/collegeprogram/'.$cp->college_id.'/'.$cp->id.'/edit')}}"><span>{{$cp->pname}}</span> </a> </li>
                            @endforeach
                        @endforeach
                    </ul>
                   @endif
                    <div class="box box-solid">
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </div>
        </div>
    </div>
</section>
@stop
