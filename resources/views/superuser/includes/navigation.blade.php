<aside class="main-sidebar"> 
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar"> 
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image"> <img src="{{ $USER->social_image }}" class="img-circle" alt="User Image"> </div>
            <div class="info">
                <p>Education Hub</p>
                <small> <i class="fa fa-circle text-success"></i> Superuser</small> </div>
            <div class="clearfix"></div>
        </div>
        <!-- search form --> 
        <!-- /.search form --> 
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header"><a href="{{url('member/home')}}" style="color:#FFF !important"><strong><i class="fa fa-gear"></i> MY ACCOUNT</strong></a></li>
           @if(!empty($colleges))
            <li>
                <a href="#">
                    <i class="fa fa-university"></i>
                    <span>Colleges</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    @foreach($colleges as $college)
                    <li> <a href="{{ url('superuser/college/'.$college->id.'/edit')}}"><span>{{$college->cname}}</span> </a> </li>
                    @endforeach
                </ul>
            </li>
            @endif
            
             @if(!empty($collegeprograms))
            <li>
                <a href="#">
                    <i class="fa fa-graduation-cap"></i>
                    <span>College Program</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    @foreach($collegeprograms as $programs)
                    @foreach($programs as $cp)
                    <li> <a href="{{ url('superuser/collegeprogram/'.$cp->college_id.'/'.$cp->id.'/edit')}}"><span>{{$cp->pname}}</span> </a> </li>
                    @endforeach
                    @endforeach
                </ul>
            </li>
            @endif
            
            @if(!empty($programs))
            <li>
                <a href="#">
                    <i class="fa fa-pencil"></i>
                    <span>Programs</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    @foreach($programs as $program)
                    <li> <a href="{{ url('superuser/program/'.$program->id.'/edit')}}"><span>{{$program->pname}}</span> </a> </li>
                    @endforeach
                </ul>
            </li>
            @endif
            <li> <a href="{{url('superuser/news')}}"> <i class="fa fa-globe"></i> <span>News</span> </a> </li>
        </ul>
        
        
    </section>
    <!-- /.sidebar --> 
</aside>