<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Third Party Services
      |--------------------------------------------------------------------------
      |
      | This file is for storing the credentials for third party services such
      | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
      | default location for this type of information, allowing packages
      | to have a conventional place to find your various credentials.
      |
     */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],
    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],
    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],
    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'google' => [
        'client_id' => '744115937389-626gleln2jtaf7kroqfd90h3hi8trb27.apps.googleusercontent.com',
        'client_secret' => 'z4UD5sdYdlGQnKx0lTX6wUyZ',
        'redirect' => 'http://www.educationhubnepal.com/account/google',
    ],
    
    'facebook' => [
        'client_id' => '1762487207297773',
        'client_secret' => '26b25971c437dfa79459c7144299cfd5',
        'redirect' => 'http://www.educationhubnepal.com/account/facebook',
    ],
    
    'twitter' => [
        'client_id' => '2ze5gbif3ZbUMLQzKXlygPuMo',
        'client_secret' => '2fQF9p4HHMvtJYC6j28AS99yNxcchZm3eI9uyiuZ4pdibC3Bm8',
        'redirect' => 'http://localhost:8888/educationhubnepal/account/twitter',
    ],
    
    'github' => [
        'client_id' => 'f285091e4d346f1ac05459',
        'client_secret' => '866e847c2343fb77dc21a68f5cbc234fea41d7549e8c62',
        'redirect' => 'http://localhost:8888/educationhubnepal/gaurab',
    ],
];
