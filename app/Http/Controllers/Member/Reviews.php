<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class Reviews extends Controller {

    private $collegeReviewObj;
    private $programReviewObj;

    function __construct() {
        $this->collegeReviewObj = new \App\Models\Site\CollegeReview();
        $this->programReviewObj = new \App\Models\Site\ProgramReview();
    }

    function getReviews() {
        $userId = Auth::user()->id;
        $reviews = new \stdClass();
        $reviews->college = $this->collegeReviewObj->fetchAllByUserId($userId);
        $reviews->program = $this->programReviewObj->fetchAllByUserId($userId);
        $data['reviews'] = $reviews;
        $data['activemenu'] = 'review';
        return $this->loadpage('member.pages.reviews', 'All reviews', $data);
    }

    function myCurrentCollege($action, $collegeId) {
        $college = \App\Models\Site\College::find($collegeId);
        switch ($action):
            case 'add':
                $this->setCurrentCollege($collegeId);
                $commonobj = new \App\Models\CommonModel();
                $commonobj->sendmail('ehubnepal@gmail.com', 'curent college set', "somene just set " . $college->cname . " ashis/her current college");
                break;
            case 'remove':
                $this->unsetCurrentCollege($collegeId);
                break;
        endswitch;
        $url = 'colleges/' . $college->url;
        return redirect($url);
    }

    function myPastCollege($action, $collegeId) {
        $college = \App\Models\Site\College::find($collegeId);
        switch ($action):
            case 'add':
                $this->setPastCollege($collegeId);
                $commonobj = new \App\Models\CommonModel();
                $commonobj->sendmail('ehubnepal@gmail.com', 'past college set', "somene just set " . $college->cname . " ashis/her [ast college");
                break;
            case 'remove':
                $this->unsetPastCollege($collegeId);
                break;
        endswitch;
        //return redirect()->back();
        $url = 'colleges/' . $college->url;
        return redirect($url);
       // return redirect('colleges/' . $college->url);
    }

    function setCurrentCollege($collegeId) {
        \App\Models\Site\MemberCollege::where(['user_id' => Auth::user()->id, 'college_id' => $collegeId])
                ->delete();
        \App\Models\Site\MemberCollege::create(['user_id' => Auth::user()->id, 'college_id' => $collegeId, 'status' => 'current']);
    }

    function unsetCurrentCollege($collegeId) {
        \App\Models\Site\MemberCollege::where(['user_id' => Auth::user()->id, 'college_id' => $collegeId])
                ->delete();
    }

    function setPastCollege($collegeId) {
        \App\Models\Site\MemberCollege::where(['user_id' => Auth::user()->id, 'college_id' => $collegeId])
                ->delete();
        \App\Models\Site\MemberCollege::create(['user_id' => Auth::user()->id, 'college_id' => $collegeId, 'status' => 'past']);
    }

    function unsetPastCollege($collegeId) {
        \App\Models\Site\MemberCollege::where(['user_id' => Auth::user()->id, 'college_id' => $collegeId])
                ->delete();
    }

    /* programs */

    function myCurrentProgram($action, $programId) {
        $program = \App\Models\Site\Program::find($programId);
        switch ($action):
            case 'add':
                $this->setCurrentProgram($programId);
                break;
            case 'remove':
                $this->unsetCurrentProgram($programId);
                break;
        endswitch;
       // return redirect()->back();
        $url = 'programs/' . $program->url;
        return redirect($url);
       // return redirect('colleges/' . $college->url);
    }

    function myPastProgram($action, $programId) {
        $program = \App\Models\Site\Program::find($programId);
        switch ($action):
            case 'add':
                $this->setPastProgram($programId);
                break;
            case 'remove':
                $this->unsetPastProgram($programId);
                break;
        endswitch;
        $url = 'programs/' . $program->url;
        return redirect($url);
        //return redirect()->back();
    }

    function setCurrentProgram($programId) {
        \App\Models\Site\MemberProgram::where(['user_id' => Auth::user()->id, 'program_id' => $programId])
                ->delete();
        \App\Models\Site\MemberProgram::create(['user_id' => Auth::user()->id, 'program_id' => $programId, 'status' => 'current']);
    }

    function unsetCurrentProgram($programId) {
        \App\Models\Site\MemberProgram::where(['user_id' => Auth::user()->id, 'program_id' => $programId])
                ->delete();
    }

    function setPastProgram($programId) {
        \App\Models\Site\MemberProgram::where(['user_id' => Auth::user()->id, 'program_id' => $programId])
                ->delete();
        \App\Models\Site\MemberProgram::create(['user_id' => Auth::user()->id, 'program_id' => $programId, 'status' => 'past']);
    }

    function unsetPastProgram($programId) {
        \App\Models\Site\MemberProgram::where(['user_id' => Auth::user()->id, 'program_id' => $programId])
                ->delete();
    }

}
