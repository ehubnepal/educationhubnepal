<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class Home extends Controller {

    public function index() {
        return $this->dashboard();
    }

    public function dashboard() {
        $data['activemenu'] = 'profile';
        return $this->loadpage('member.pages.profile', 'My profile', $data);
    }

    function signup(Request $request) {
        $param = $request->all();
        $userData['username'] = $param['email'];
        $userData['email'] = $param['email'];
        $userData['password'] = \Illuminate\Support\Facades\Hash::make($param['password']);
        $userData['role'] = "member";
        $userData['status'] = "pending";

        $userObj = new \App\Models\Member\User();
        if ($userObj->user_exist($userData['username'])) {
            $this->set_message('This email is already taken by someone one. '
                    . 'If that person is you you can directly login or if you forgot your password, you can request a new one.')->error();
            return redirect('signup');
        }

        $user = \App\Models\Member\User::create($userData);
        $memberData['fullname'] = $param['fullname'];
        $memberData['id'] = $user->id;
        \App\Models\Member\Member::create($memberData);
        $message = $this->getActivationEmail($user->id, $userData['email'], $memberData['fullname']);
        $subject = "Verify your account";
        $to_email = $param['email'];

        $notification = new Notification();
        $notification->sendmail($to_email, $subject, $message);

        if ($user->id > 0):
            $this->set_message("You have been registered successfully. Please login to continue.")->success();
            return redirect('login');
        else:
            $this->set_message("Registration failed. Please try again")->error();
            return redirect('signup');
        endif;
    }

    function getActivationLink($email, $salt, $token) {
        return url('member/activate/' . $email . '/' . $salt . '/' . $token);
    }

    function saveActivationParam($userId, $salt, $email, $token) {
        $userActivationData['user_id'] = $userId;
        $userActivationData['salt'] = $salt;
        $userActivationData['email'] = $email;
        $userActivationData['token'] = $token;
        \App\Models\Member\Useractivation::where(['user_id' => $userId])->delete();
        \App\Models\Member\Useractivation::create($userActivationData);
    }

    function getActivationEmail($userId, $email, $fullname, $newaccount = true) {
        $salt = str_random(15);
        $token = str_random(60);
        $this->saveActivationParam($userId, $salt, $email, $token);

        $activationlink = $this->getActivationLink($email, $salt, $token);
        $introtext = $newaccount ? "Your account has been created in education hub nepal." : "You have updated your email recently.";

        /* return "hello " . $fullname . ', ' . $introtext
          . 'Click on the link below to activate your account.<br/>'
          . '<a href="' . $activationlink . '">' . $activationlink . '</href>';
         */

        return '<div style="margin:0 auto;padding:20px 0 0 0;font-family:Arial">
    <div style="width:700px;padding:30px;margin:0px auto;padding-top:0;font-family:Arial">
        <div style="width:100%;padding:10px 20px;border-radius:20px 20px 0 0;margin-top:20px;text-align:center;min-height:50px">
            <a href="#" target="_blank" rel="noreferrer">
                <img style="max-height:50px;max-width:250px" src="http://www.educationhubnepal.com/public/site/img/ehn_logo.png" title="Education Hub Nepal - Find your next education" alt="EducationHubNepal LOGO">
            </a>
        </div>
        <div style="clear:both"></div>
        <div style="width:700px;padding:10px 20px;font-size:12px;color:#999;line-height:18px">
            <h2 style="color:#205b81;font-size:18px;font-weight:normal">Congratulations ' . $fullname . '</h2>
            <p>' . $introtext . '</p>
            <p>Please click on the link below to activate your account</p>
            <br/>
            <a href="' . $activationlink . '" style="margin-top:20px;padding:10px 30px;background:#2185c5;margin-bottom:80px;color:#fff;text-decoration:none;text-transform:uppercase" target="_blank" rel="noreferrer">Activate Now</a>
            <br/>
            <p>Thank You </p>
            <hr style="border:none;border-top:1px solid #e1e1e1"><a href="#" target="_blank" rel="noreferrer">
            </a><p style="text-align:center"><a href="#" target="_blank" rel="noreferrer">
                    © 2016 </a><a style="color:#2185c5;text-decoration:none" href="#" target="_blank" rel="noreferrer">Education Hub Nepal</a>.
                Powered by community.
            </p>
            <p></p></div>
    </div>
</div>';
    }

    function activate(Request $request, $email, $salt, $token) {
        $userActivationObj = new \App\Models\Member\Useractivation();
        $status = $userActivationObj->activate($email, $salt, $token);
        if ($status):
            $this->set_message("User verified successfully. Please login to continue.")->success();
        else:
            $this->set_message("<div class='alert alert-danger'>User verification failed. This might have happened due "
                    . "to the expired activation link. "
                    . "Please login and request a new activation link.</div>")->error();
        endif;
        return redirect('login');
    }

    function updateProfile(Request $request) {
        $userId = Auth::user()->id;
        $member = \App\Models\Member\Member::find($userId);
        $member->fullname = $request->get('fullname');
        $member->phone = $request->get('phone');
        $member->address = $request->get('address');
        $member->facebook = $request->get('facebook');
        $member->twitter = $request->get('twitter');
        $member->skype = $request->get('skype');
        if ($member->save()):
            $this->set_message('Profile updated successfully')->success();
        endif;
        return redirect('member/home');
    }

    function resendActivationLink() {
        $user = Auth::user();
        $member = \App\Models\Member\Member::find($user->id);
        $message = $this->getActivationEmail($user->id, $user->email, $member->fullname, false);
        $subject = "Verify your new email";
        $to_email = $user->email;

        $notification = new Notification();
        $notification->sendmail($to_email, $subject, $message);
        $this->set_message("We send you a verification link to " . $user->email . ". Please click on the verify button to verify your account.")->info();
        return redirect('member/home');
    }

    function updateEmail(Request $request) {
        $user = Auth::user();
        $email = $request->get('newemail');

        $userObj = new \App\Models\Member\User();
        if ($userObj->user_exist($email)) {
            $this->set_message('This email is already taken by someone else. '
                    . 'If that person is you you can directly login or if you forgot your password, you can request a new one.')->info();
            return redirect('member/home');
        } else {
            $temp_user = \App\Models\Member\User::find($user->id);
            $temp_user->email = $email;
            $temp_user->username = $email;
            $temp_user->status = "pending";
            $temp_user->save();

            $member = \App\Models\Member\Member::find($user->id);
            $message = $this->getActivationEmail($user->id, $email, $member->fullname, false);
            $subject = "Verify your new email";
            $to_email = $email;

            $notification = new Notification();
            $notification->sendmail($to_email, $subject, $message);
            $this->set_message("We send you a verification link to " . $email . ". Please click on the verify button to verify your account.")->info();
            return redirect('member/home');
        }
    }

    function myEducation() {
        $memberCollegeObj = new \App\Models\Site\MemberCollege();
        $memberProgramObj = new \App\Models\Site\MemberProgram();
        $myeducation = new \stdClass();
        $myeducation->colleges = $memberCollegeObj->fetchAll(Auth::user()->id);
        $myeducation->programs = $memberProgramObj->fetchAll(Auth::user()->id);
        $data['myeducation'] = $myeducation;
        $data['activemenu'] = 'myeducation';
        return $this->loadpage('member.pages.myeducation', 'My education', $data);
    }

    function addNewCollege() {
        $data['activemenu'] = 'newcolelge';
        $collegeObj = new \App\Models\Member\College();
        $data['mycolleges'] = $collegeObj->fetchAll();
        return $this->loadpage('member.pages.addnewcollege', 'Add a new college', $data);
    }

}
