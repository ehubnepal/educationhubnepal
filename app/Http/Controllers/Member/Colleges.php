<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Member\College;
use Illuminate\Support\Facades\Input;
use Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Colleges extends Controller {

    private $thumbwidth = 150;
    private $thumbheight = 150;
    private $collegeObj;

    function __construct() {
        $this->collegeObj = new College();
    }

   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $collegedata = $request->all();
        foreach($collegedata as $key => &$cdata):
           $cdata =  strip_tags($cdata, '<h1><h2><h3><div><p><span><ol><ul><li>');
        endforeach;
        $collegedata['user_id'] = Auth::user()->id;
        $college = College::create($collegedata);
        $temp_user_id = $collegedata['user_id'];
        unset($collegedata['user_id']);
        /* Store a copy for maintainer */
        $collegedata['id'] = $college->id;
        \App\Models\Superuser\College::create($collegedata);
        /* end storing for maintainer */

        if (!empty($college) && $college->id > 0):
            $logo = Input::file('collegelogo');
            if ($logo):
                /* upload logo */
                $this->uploadLogo($college, $logo);
            endif;

            $this->set_message('New college created successfully!')->success();
            $modelObj = new \App\Models\CommonModel();
            $modelObj->sendmail("ehubnepal@gmail.com","New college added by userid:".$temp_user_id,json_encode($collegedata));
        else:
            $this->set_message('New college could not be created!')->error();
        endif;
        return redirect('member/addnewcollege');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
          if (!$this->hasPrevilege($id)):
            $this->set_message('Record doesnt exist')->error();
            return redirect()->back();
        endif;
        $data['college'] = College::find($id);
        return $this->loadpage('member.pages.addnewcollege', 'Edit college', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $collegeData_raw = $request->all();
        
        $collegeData = clean_post_data($collegeData_raw);
        
        $college = College::find($collegeData['id']);
        $college->cname = $collegeData['cname'];
        $college->address = $collegeData['address'];
        $college->district = $collegeData['district'];
        $college->phone = $collegeData['phone'];
        $college->fax = $collegeData['fax'];
        $college->website = $collegeData['website'];
        $college->pobox = $collegeData['pobox'];
        $college->email = $collegeData['email'];
        $college->description = $collegeData['description'];
        $college->facilities = $collegeData['facilities'];
        if ($college->save()):
            $this->set_message('College detail updated successfully!')->success();
        else:
            $this->set_message('College detail could not be updated!')->error();
        endif;

        /* upload logo */
        $logo = Input::file('collegelogo');
        if ($logo):
            /* remove old image */
            if (file_exists(base_path($college->full_url))):
                @unlink(base_path($college->full_url));
                @unlink(base_path($college->thumb_url));
            endif;
            /* upload logo */
            $this->uploadlogo($college, $logo);
        endif;

        /* upload images */
        $image = Input::file('collegeimage');
        if ($image):
            /* upload image */
            $this->uploadImage($college, $image);
        endif;

        return redirect('member/college/'.$collegeData['id'].'/edit');
    }

    function uploadLogo(&$college, $image) {
        $filename = 'logo_' . $college->id . '_' . time() . '.' . $image->getClientOriginalExtension();
        $path = base_path('images/collegelogo/' . $filename);
        $thumbpath = base_path('images/collegelogo/thumbs/' . $filename);
        Image::make($image->getRealPath())->save($path);
        /* resize image */
        Image::make($image->getRealPath())
                ->resize($this->thumbwidth, $this->thumbheight, function($constraint) {
                    $constraint->aspectRatio();
                })->save($thumbpath);
        $college->full_url = 'images/collegelogo/' . $filename;
        $college->thumb_url = 'images/collegelogo/thumbs/' . $filename;
        $college->save();
    }

    function removeLogo($id) {
        $college = College::find($id);
        if ($college->user_id == Auth::user()->id  || Auth::user()->role=='admin') {
            if (file_exists(base_path($college->thumb_url))):
                @unlink(base_path($college->thumb_url));
                @unlink(base_path($college->full_url));
            endif;
            $college->thumb_url = '';
            $college->full_url = '';
            $college->save();
            $this->set_message("Logo removed successfully")->info();
        }
        return redirect()->back();
    }
    
    function hasPrevilege($collegeId) {
        if (Auth::user()->role == 'admin'):
            return true;
        else:
            $data = DB::table("m_colleges")
                    ->where(['id' => $collegeId, 'user_id' => Auth::user()->id])
                    ->select('id')
                    ->get();
            return !empty($data);
        endif;
    }

}
