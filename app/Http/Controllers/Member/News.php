<?php

namespace App\Http\Controllers\member;

use App\Http\Controllers\Controller;
use Auth;
use App\Models\Member\MaintainerCollegeProgramRef;
use Illuminate\Http\Request;
class News extends Controller {

    private $newsObj;

    function __construct() {
        $this->newsObj = new \App\Models\member\News;
    }

    function create(){
        $activemenu='news';
        $mynews = \App\Models\Member\News::where('user_id',Auth::user()->id)->get();
        return $this->loadpage('member.pages.addnews', 'Create news news',compact('mynews','activemenu'));
    }
    
    function edit($id){
        $data['activemenu']='news';
        $userid = Auth::user()->id;
        //$data = $this->getMenuData();
       // $data['colleges'] = $this->maintainerCollegeObj->fetchCollegeByUserId($userid);
        $news = \App\Models\Member\News::find($id);
        $data['news'] =$news;
        if($news->user_id != $userid){
            $this->set_message('Record doest exist')->error();
            return redirect()->back();
        }
        return $this->loadpage('member.pages.addnews', 'Edit news',$data);
    }
    
    function store(Request $request){
        $userid = Auth::user()->id;
        $data=[];
        $data['college_id'] = $request->get('college_id')==null?0:$request->get('college_id');
        $data['title'] = $request->get('title');
        $data['news'] = $request->get('news');
        $data['editable'] = 1;
        $data['user_id'] = $userid;
        $data['published'] = $request->get('published')==null?0:1;
        $news = \App\Models\Member\News::create($data);
        if($news->id>0){
            $this->set_message('News created successfully')->success();
        }else{
            $this->set_message('News creation failed')->error();
        }
        return redirect('member/news/create');
    }
    
    function update(Request $request){
        $userid = Auth::user()->id;
        $r = $request->all();
        $data=[];
        $data['title'] = $r['title'];
        //unset($data['_token'])
        $data['college_id'] = $request->get('college_id')==null?0:$request->get('college_id');
        $data['news'] = $r['news'];
        $data['user_id'] = $userid;
        $data['published'] = $request->get('published')==null?0:1;
        $news = \App\Models\member\News::find($request->get('id'));
        if($news->user_id != $userid){
             $this->set_message('Access denied.')->error();
            return redirect()->back();
        }
        \App\Models\Member\News::where('id',$request->get('id'))->update($data);
        $this->set_message('News updated successfully')->success();
        return redirect('member/news/create');
    }
    
    public function unpublish($id) {
        $news = \App\Models\Member\News::find($id);
        $news->published = 0;
        $news->save();
        $this->set_message('News unpublished successfully')->success();
        return redirect()->back();
    }

    public function publish($id) {
        $news = \App\Models\Member\News::find($id);
        $news->published = 1;
        $news->save();
        $this->set_message('News published successfully')->success();
        return redirect()->back();
    }
    
    public function destroy($id) {
        \App\Models\Member\News::destroy($id);
        $this->set_message('News deleted successfully')->success();
        return redirect()->back();
    }

    function getMenuData() {
        $maintainerProgramObj = new \App\Models\Member\MaintainerProgramRef();
        $maintainerCollegeObj = new \App\Models\Member\MaintainerCollegeRef();
        $maintainerCollegeProgramObj = new \App\Models\Member\MaintainerCollegeProgramRef();
        $userid = Auth::user()->id;
        $data['colleges'] = $maintainerCollegeObj->fetchCollegeByUserId($userid);
        $data['programs'] = $maintainerProgramObj->fetchProgramByUserId($userid);
        foreach ($data['colleges'] as $college):
            $data['collegeprograms'][] = $maintainerCollegeProgramObj->getCollegePrograms($userid, $college->id);
        endforeach;
        return $data;
    }
}
