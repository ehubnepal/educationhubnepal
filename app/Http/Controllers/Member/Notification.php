<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use PHPMailer;

/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class Notification extends Controller {

    private $commonModel;

    function __construct() {
        $this->commonModel = new \App\Models\CommonModel();
    }
    
    function sendmail($mailto, $subject, $message, $attachments = FALSE) {
        return $this->commonModel->sendmail($mailto, $subject, $message, $attachments = FALSE);
    }
}
