<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use Auth;
use Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use DB;

class AuthController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Registration & Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users, as well as the
      | authentication of existing users. By default, this controller uses
      | a simple trait to add these behaviors. Why don't you explore it?
      |
     */

use AuthenticatesAndRegistersUsers,
    ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectAfterLogin = 'admin/home';
    protected $redirectAfterLogout = 'admin/login';
    protected $redirectMaintainerAfterLogin = 'superuser/home';
    protected $redirectMaintainerAfterLogout = 'login';
    protected $redirectMemberAfterLogin = 'member/home';
    protected $redirectMemberAfterLogout = 'login';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware($this->guestMiddleware(), ['except' => 'logout']);
        // $this->middleware($this->guestMiddleware(), ['except' => 'superuser/logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
        return Validator::make($data, [
                    'name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:users',
                    'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data) {
        return User::create([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
        ]);
    }

    /* admin */

    public function login() {
        //$this->creatingDummyPassword();
        if (\Session::has('admin_loggedin')) {
            return redirect('admin/home');
        }
        return view('admin.login');
    }

    public function verifyLogin() {
        $admin = [
            'username' => Request::get('username'),
            'password' => Request::get('password'),
            'role' => 'admin'
        ];

        if (Auth::attempt($admin)) {
            \Session::put('admin_loggedin', 'yes');
            return redirect($this->redirectAfterLogin);
        } else {
            return redirect($this->redirectAfterLogout);
        }
    }

    public function Logout() {
        \Session::forget('admin_loggedin');
        Auth::logout();
        \Session::flush();
        return redirect($this->redirectAfterLogout);
    }

    /* maintainer */

    public function maintainerLogin(Request $request) {
        if (\Session::has('maintainer_loggedin')) {
            return redirect('superuser/home');
        }
        return view('superuser.login');
    }

    public function verifyMaintainerLogin() {
        $maintainer = [
            'username' => Request::get('username'),
            'password' => Request::get('password'),
            'role' => 'maintainer'
        ];
        if (Auth::attempt($maintainer)) {
            \Session::put('maintainer_loggedin', 'yes');
            \Session::put('member_loggedin', 'yes');
            if(Request::get('redirect_url')){
                return redirect(Request::get('redirect_url'));
            }
            return redirect($this->redirectMaintainerAfterLogin);
        } else {
            return redirect($this->redirectMaintainerAfterLogout);
        }
    }

    public function maintainerLogout() {
        \Session::forget('maintainer_loggedin');
        Auth::logout();
        \Session::flush();
        return redirect($this->redirectMaintainerAfterLogout);
    }

    /* member */

    public function memberLogin(Request $request) {
        if (\Session::has('member_loggedin')) {
            return redirect('member/home');
        }
        return view('member.login');
    }

    public function verifyMemberLogin() {
        $member = [
            'username' => Request::get('username'),
            'password' => Request::get('password'),
            'role' => 'member'
        ];

        if (Auth::attempt($member)) {
            if ($this->userIsBlocked(Auth::user())):
                return redirect('login');
            endif;
            \Session::put('member_loggedin', 'yes');
            if(Request::get('redirect_url')){
                return redirect(Request::get('redirect_url'));
            }
            return redirect($this->redirectMemberAfterLogin);
        } else {
            return $this->verifyMaintainerLogin();
        }
    }

    function loginForSocialMedia($member, $image) {
        $fan = \App\Models\Member\User::where(['username' => $member['username'], 'social_id' => $member['social_id']])->first();
        if (Auth::loginUsingId($fan->getAuthIdentifier())) {
            if ($this->userIsBlocked(Auth::user())):
                return redirect('login');
            endif;
            //update image in each login
          /*  if (!empty($image['avatar'])):
                $mem = \App\Models\Member\Member::find(Auth::user()->id);
                $mem->social_image = $image['avatar'];
                $mem->social_image_large = $image['avatar_lg'];
                $mem->save();
            endif; */
            \Session::put('member_loggedin', 'yes');
            if(\Session::get('redirect_url')):
                $redirect_url = \Session::get('redirect_url');
                \Session::forget('redirect_url');
                return redirect($redirect_url);
            endif;
            if ($fan->role == 'member'):
                return redirect($this->redirectMemberAfterLogin);
            else:
                \Session::put('maintainer_loggedin', 'yes');
                \Session::put('member_loggedin', 'yes');
                return redirect($this->redirectMaintainerAfterLogin);
            endif;
        } else {
            return redirect('login');
        }
    }

    /*
      public function verifyMaintainerLoginSocial($maintainer) {
      $maintainer['role'] = 'maintainer';
      if (Auth::attempt($maintainer, false)) {
      \Session::put('maintainer_loggedin', 'yes');
      \Session::put('member_loggedin', 'yes');
      return redirect($this->redirectMaintainerAfterLogin);
      } else {
      return redirect($this->redirectMaintainerAfterLogout);
      }
      } */

    function userIsBlocked($user) {
        if ($user->status == 'blocked'):
            \Session::flush();
            \Session::put('server_message', '<div class="alert alert-danger">Your account has been blocked. <a href="#">Learn why.</a></div>');
            return true;
        else:
            return false;
        endif;
    }

    public function memberLogout() {
        \Session::forget('member_loggedin');
        Auth::logout();
        \Session::flush();
        return redirect($this->redirectMemberAfterLogout);
    }

      function creatingDummyPassword(){
          die(\Illuminate\Support\Facades\Hash::make("gaurab123"));
      DB::table("users")
      ->where('username', '=',"admin")
      ->update(["username"=>"gaurab","password"=> \Illuminate\Support\Facades\Hash::make("gaurab123")]);
      }
     
}
