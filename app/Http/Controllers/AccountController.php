<?php

namespace App\Http\Controllers;

use Redirect;
use Socialize;
use Auth;
use DB;
use App\Http\Controllers\Auth\AuthController;

class AccountController extends Controller {

    public function google_redirect() {
        if(\Illuminate\Support\Facades\Request::get('redirect_url')):
            \Session::put('redirect_url',\Illuminate\Support\Facades\Request::get('redirect_url'));
        endif;
        return Socialize::with('google')->redirect();
        
    }

    public function google() {
        $user = Socialize::with('google')->user();
        if (!empty($user)):
            if (\Session::has('merge_google')):
                return $this->merge_google($user);
            endif;
            $userObj = new \App\Models\Member\User();
            $authUser = $userObj->user_exists_oauth('google', $user->id);
            if ($authUser !== false):
                $userAuth = array('username' => $authUser->username,
                    "social_id" => $user->id,
                    "role" => "member");
                $authControlerObj = new \App\Http\Controllers\Auth\AuthController();
                return $authControlerObj->loginForSocialMedia($userAuth,array('avatar'=>$user->avatar,'avatar_lg'=>$user->avatar));
                return redirect('member/home');
            else:
                return $this->registerNewSocialUser('google', $user->id, $user->email, $user->token, $user->name, $user->user['image']['url'], $user->avatar, json_encode($user));

            endif;
        else:
            echo 'Failed to authenticate';
            die();
        endif;
    }

    public function facebook_redirect() {
        if(\Illuminate\Support\Facades\Request::get('redirect_url')):
            \Session::put('redirect_url',\Illuminate\Support\Facades\Request::get('redirect_url'));
        endif;
        return Socialize::with('facebook')->redirect();
    }

    public function facebook() {
        $user = Socialize::with('facebook')->user();
        if (!empty($user)):
            if (\Session::has('merge_facebook')):
                return $this->merge_facebook($user);
            endif;
            $userObj = new \App\Models\Member\User();
            $authUser = $userObj->user_exists_oauth('facebook', $user->id);
            if ($authUser !== false):
                $userAuth = array('username' => $authUser->username,
                    "social_id" => $user->id,
                    "role" => "member");
                $authControlerObj = new \App\Http\Controllers\Auth\AuthController();
                return $authControlerObj->loginForSocialMedia($userAuth,array('avatar'=>$user->avatar,'avatar_lg'=>$user->avatar_original));
                return redirect('member/home');
            else:
                return $this->registerNewSocialUser('facebook', $user->id, $user->email, $user->token, $user->name, $user->avatar, $user->avatar_original, json_encode($user));
            endif;
        else:
            echo 'Failed to authenticate';
            die();
        endif;
    }

    function registerNewSocialUser($socialAccount, $socialid, $email, $token, $name, $image, $image_lg, $json) {
        $userdata['username'] = $email == null ? $socialid : $email;
        $userdata['email'] = $email == null ? '' : $email;
        $userdata['status'] = "confirmed";
        $userdata['password'] = \Illuminate\Support\Facades\Hash::make($socialid);
        $userdata['oauth'] = $socialAccount;
        $userdata['social_id'] = $socialid;
        $userdata['role'] = "member";
        $userdata['social_token'] = $token;
        $userdata['oauth_json'] = $json;

        $userObj = new \App\Models\Member\User();
        $newUser = $userObj->signUp($userdata);
        if (isset($newUser->id)):
            $memberData['id'] = $newUser->id;
            $memberData['fullname'] = $name;
            $memberData['social_image'] = $image;
            $memberData['social_image_large'] = $image_lg;
            $member = \App\Models\Member\Member::create($memberData);
            if (isset($member->id)):
                $userAuth = array('username' => $email == null ? $socialid : $email,
                    "social_id" => $socialid,
                    "role" => "member");
                $authControlerObj = new \App\Http\Controllers\Auth\AuthController();
                return $authControlerObj->loginForSocialMedia($userAuth,array('avatar'=>$image,'avatar_lg'=>$image_lg));
            //return redirect('member/home');
            else:
                \App\Models\Member\User::destroy($newUser->id);
                $this->set_message('Oops something went wrong while signing up with ' . $socialAccount . '. Pleae try again.')->error();
                return redirect('login');
            endif;
        else:
            echo 'Problem while authenticating. This might be due to bad connection';
            die();
        endif;
    }

    public function google_redirect_mergeaccount() {
        \Session::put('merge_google', 'yes');
        return Socialize::with('google')->redirect();
    }

    public function facebook_redirect_mergeaccount() {
        \Session::put('merge_facebook', 'yes');
        return Socialize::with('facebook')->redirect();
    }

    public function merge_facebook($user) {
         \Session::forget('merge_facebook');
        if (Auth::user()) {
            \App\Models\Member\Member::destroy(Auth::user()->id);
            $memberData['id'] = Auth::user()->id;
            $memberData['fullname'] = $user->name;
            $memberData['social_image'] = $user->avatar;
            $memberData['social_image_large'] = $user->avatar_original;
            \App\Models\Member\Member::create($memberData);

            $user = \App\Models\Member\User::find(Auth::user()->id);
            $user->oauth = 'facebook';
            $user->social_id = $user->id;
            $user->social_token = $user->token;
            $user->oauth_json = json_encode($user);
            $user->save();
            $this->set_message('Your account has been successfully merged with facebook')->success();
            return redirect('member/home');
        } else {
            $this->set_message('Session expired. Please login and try again.')->error();
            return redirect('login');
        }
    }

    public function merge_google($user) {
        \Session::forget('merge_google');
        if (Auth::user()) {
            \App\Models\Member\Member::destroy(Auth::user()->id);
            $memberData['id'] = Auth::user()->id;
            $memberData['fullname'] = $user->name;
            $memberData['social_image'] = $user->avatar;
            $memberData['social_image_large'] = $user->user['image']['url'];
            \App\Models\Member\Member::create($memberData);

            $user = \App\Models\Member\User::find(Auth::user()->id);
            $user->oauth = 'google';
            $user->social_id = $user->id;
            $user->social_token = $user->token;
            $user->oauth_json = json_encode($user);
            $user->save();
            $this->set_message('Your account has been successfully merged with google')->success();
            return redirect('member/home');
        } else {
            $this->set_message('Session expired. Please login and try again.')->error();
            return redirect('login');
        }
    }

}
