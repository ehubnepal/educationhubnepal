<?php
namespace App\Http\Requests;
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Admin\Article;
use App\Models\Admin\ArticleTag;
use App\Models\Admin\ArticleTagRef;
use App\Models\Admin\ArticleCategoryRef;
use Illuminate\Support\Facades\Input;
use DB;
use Image;
use Log;

class ArticleController extends Controller {
	function __construct(){
		$this->articleObj = new Article();
	}

function index(){
	$data['posts'] = $this->articleObj->fetch_all();
	return $this->loadpage('admin.lists.list_articles','List of all blog articles',$data);


}
function create(){
	 $tags = DB::table('tags')->lists('name','tag_id');
     $categories=DB::table('article_categories')->lists('name','category_id');
        
        
     return view('admin.forms.form_article_create',compact('tags','categories'));
}

public function store(Requests\CreateArticleRequest $request){
       // $input = Request::all(); // this will fetch all the input as an array.
        // after having the argument and Request as argument we no longer need to use Facade.
        //dd($request->input('tags'));

       // $input = Request::all();
        //dd($request->input('tags'));
        $article = new Article;
        $article->title = $request->input('title');
        $article->slug = $request->input('slug');
        $article->desc = $request->input('desc');
        $article->meta = $request->input('meta');
        $article->canonical = $request->input('canonical');
        if($request->input('created_at')):
            $article->created_at = $request->input('created_at');
        else:
            $article->created_at = date('Y-m-d');
        endif;
        $article->content=$request->input('content');
        if($request->has('active')){
            $article->active=1;
        }
        else{
            $article->active=0;
        }
         if($request->has('home_page')){
            $article->home_page = 1;
        }
        else{
            $article->home_page = 0;
        }
        //dd($request->file('image'));
        if($request->file('image')){

             $imageName = date('Y-m-d-H-m').'-'.$request->file('image')->getClientOriginalName();
       // $path = '/public/img/banner';
        $request->file('image')->move(
        base_path() . '/public/admin/img/blog/', $imageName
        );
        $article->image= $imageName;
        }
        
        
        
        //dd($article);
         if($article->save()):
         if($request->has('categories')){
            foreach($request->input('categories') as $category_id):
                ArticleCategoryRef::create(['post_id'=>$article->post_id,'category_id'=>$category_id]);
            endforeach;
            }
         
         

              if ( ! $request->has('tag_list'))
            {
            	$this->set_message('Article updated successfully!')->success();
                return redirect('admin/blog/posts');
            }
            else{

            $alltags = array();

            foreach ($request->input('tag_list') as $tag)

            {
             if (substr($tag, 0, 4) == 'new:')
                {
                   
                    $newTag = ArticleTag::create(['name' => substr($tag, 4)]);
                    //Log::info($newTag);
                    array_push($alltags,$newTag->tag_id);
                    //dd($newTag);
                    continue;
                }
                else{
                        array_push($alltags, $tag);
                }
            
            }
            ArticleTagRef::where('post_id',$article->post_id)->delete();
             foreach($alltags as $tag):
                ArticleTagRef::create(['post_id'=>$article->post_id,'tag_id'=>$tag]);
            endforeach;
          $this->set_message('Article updated successfully!')->success();     
          return redirect('admin/blog/posts');
        }
         else: 
             $this->set_message('Program could not be updated!')->error();
              return redirect('admin/blog/posts');
         endif;
    }

     public function edit($id){
        // fetch the articles.
        //$article = DB::table('articles')->where('slug',$slug)->first();
       
        $post = Article::where('post_id',$id)->first();
        $articleRef = new \App\Models\Admin\ArticleCategoryRef();
        $post->cats = $articleRef->getCategories($post->post_id);
        $post_categories = [];
        foreach($post->cats as $cat):
            $post_categories[]=$cat->category_id;
        endforeach;
        $articleTag = new \App\Models\Admin\ArticleTagRef();
        $post->tagging = $articleTag->getTags($post->post_id);
        $post_tags = [];
        foreach($post->tagging as $tag):
            $post_tags[] = $tag->tag_id;
        endforeach;
        
        
        $tags = DB::table('tags')->lists('name','tag_id');
        $categories=DB::table('article_categories')->lists('name','category_id');

        return view('admin.forms.form_article_edit', compact('post','tags','categories','post_categories','post_tags'));
    }

    public function update($id, Request $request){
    	$article = Article::where('post_id',$id)->first();
        //dd($article);
        //$input = Request::all();
        //dd($request->input('title'));
        $article->title=$request->input('title');
        $article->slug=$request->input('slug');
        $article->desc = $request->input('desc');
        $article->meta = $request->input('meta');
        $article->content=$request->input('content');
         if($request->input('created_at')):
            $article->created_at = $request->input('created_at');
        else:
            $article->created_at = date('Y-m-d');
        endif;
        if($request->has('active')){
        $article->active = 1;
        }
        else{
            $article->active=0;
        }
        if($request->has('home_page')){
            $article->home_page = 1;
        }
        else{
            $article->home_page = 0;
        }
         if($request->file('image')){

             $imageName = date('Y-m-d-H-m').'-'.$request->file('image')->getClientOriginalName();
       // $path = '/public/img/banner';
        $request->file('image')->move(
        base_path() . '/public/admin/img/blog/', $imageName
        );
        $article->image= $imageName;
        }
        //dd($article);
        if($article->update()):
            if(!$request->has('categories')){
                ArticleCategoryRef::where('post_id', $article->post_id)->delete();    
        
                }
            else{
                ArticleCategoryRef::where('post_id', $article->post_id)->delete();
                foreach($request->input('categories') as $category_id):
                    ArticleCategoryRef::create(['post_id'=>$article->post_id,'category_id'=>$category_id]);
                endforeach;
             
                }
        
       
        // Logic to sync the tags and proceed to make new tags in the list.
                 if(!$request->has('tag_list'))
                     {
                        ArticleTagRef::where('post_id', $article->post_id)->delete();
                        $this->set_message('Article updated successfully!')->success();
                        return redirect('admin/blog/posts');
                    }
                    else{
                        ArticleTagRef::where('post_id',$article->post_id)->delete();
                        foreach ($request->input('tag_list') as $tag)
                            //Log::info($tag);
                        {
                        if (substr($tag, 0, 4) == 'new:')
                        {
                   
                            $newTag = ArticleTag::create(['name' => substr($tag, 4)]);
                            //Log::info($newTag);
                            ArticleTagRef::create(['post_id'=>$article->post_id,'tag_id'=>$newTag->tag_id]);
                            }
                        else{
                             ArticleTagRef::create(['post_id'=>$article->post_id,'tag_id'=>$tag]);
                        }
            
                    } // end foreach
                    
                    $this->set_message('Article updated successfully!')->success();
                    return redirect('admin/blog/posts');
                }
            else:
                $this->set_message('Article Couldn not be updated!')->error();
                return redirect('admin/blog/posts');  
                endif;
    }

     public function destroy($id){
        Article::find($id)->delete();
        $this->set_message('Article Deleted successfully!')->success();
        return redirect('admin/blog/posts');
    }

     public function unpublish($id) {
        $article = Article::find($id);
        $article->active = 0;
        $article->save();
        $this->set_message('Article unpublished successfully')->success();
        return redirect()->back();
    }

    public function publish($id) {
        $article = Article::find($id);
        $article->active = 1;
        $article->save();
        $this->set_message('Article published successfully')->success();
        return redirect()->back();
    }


























}