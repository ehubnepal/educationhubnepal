<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Program;
use App\Models\Admin\ProgramCategory;
use Illuminate\Http\Request;
use DB;
use Image;

class Program_category extends Controller {

    private $collegeObj;

    function __construct() {
        $this->programcategoryObj = new ProgramCategory();
    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['programcategory']= $this->programcategoryObj->fetchAll();
        return $this->loadpage('admin.lists.list_programcategory', "List of program categories", $data);
    }

	/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return $this->loadpage('admin.forms.form_programcategory', 'Add new category');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['programcategory'] = ProgramCategory::find($id);
        return $this->loadpage('admin.forms.form_programcategory', 'Edit category', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $categoryData = $request->all();
        $category = ProgramCategory::find($categoryData['id']);
        $category->category = $categoryData['category'];
        $category->logo = $categoryData['logo'];
        $category->url = $categoryData['url'];
        $category->published = isset($categoryData['published']) ? 1 : 0;
        if ($category->save()):
            $this->set_message('Program category updated successfully!')->success();
        else:
            $this->set_message('Program category could not be updated!')->error();
        endif;

        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $programcategory = ProgramCategory::create($request->all());
        if (!empty($programcategory) && $programcategory->id > 0):
            $this->set_message('New category created successfully!')->success();
        else:
            $this->set_message('New category could not be created!')->error();
        endif;
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        ProgramCategory::destroy($id);
        $this->set_message('Category deleted successfully')->success();
        return redirect()->back();
    }

    public function unpublish($id) {
        $programcategory = ProgramCategory::find($id);
        $programcategory->published = 0;
        $programcategory->save();
        $this->set_message('Category unpublished successfully')->success();
        return redirect()->back();
    }

    public function publish($id) {
        $programcategory = ProgramCategory::find($id);
        $programcategory->published = 1;
        $programcategory->save();
        $this->set_message('Category published successfully')->success();
        return redirect()->back();
    }

}
