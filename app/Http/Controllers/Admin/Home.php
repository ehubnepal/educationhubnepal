<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Home extends Controller {

    
    public function index() {
        return $this->dashboard();
    }

    public function dashboard() {
        $statisticsObj = new \App\Models\Admin\StatisticsModel();
        $thirty_days_before_date = date('Y-m-d', strtotime('-30 days'));
        $data['users'] = $statisticsObj->get_users($thirty_days_before_date, date('Y-m-d'));
        return $this->loadpage('admin.pages.dashboard','Admin Dashboard',$data);
    }

}
