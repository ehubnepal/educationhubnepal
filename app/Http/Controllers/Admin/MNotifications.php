<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\MNotification;
use App\Http\Controllers\Controller;

class MNotifications extends Controller
{
    private $notificationObj;
    
    function __construct() {
        $this->notificationObj = new MNotification();
    }
    public function index(){
       $data['notifications'] = $this->notificationObj->fetchAll(40);
       return $this->loadpage('admin.lists.list_editnotifications','Changes made by maintainers',$data);
    }
}
