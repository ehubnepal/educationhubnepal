<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\User;
use App\Models\Admin\Member;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Models\Admin\CollegeReview;
use App\Models\Admin\ProgramReview;

class Reviews extends Controller {

    private $programReviewObj;
    private $collegeReviewObj;

    function __construct() {
        $this->programReviewObj = new \App\Models\Admin\ProgramReview();
        $this->collegeReviewObj = new \App\Models\Admin\CollegeReview();
    }

    function collegeReviews(){
        $data['collegeReviews'] = $this->collegeReviewObj->fetchAll();
        return $this->loadpage('admin.lists.list_collegereviews','College Reviews',$data);
    }
    
     function publishCollegeReviews($reviewId){
        $r = CollegeReview::findorfail($reviewId);
        $r->published=1;
        $r->save();
        $this->set_message('Review published successfully')->success();
        return redirect()->back();
    }
    
    function unpublishCollegeReviews($reviewId){
        $r = CollegeReview::findorfail($reviewId);
        $r->published=0;
        $r->save();
        $this->set_message('Review unpublished successfully')->success();
        return redirect()->back();
    }
    
    function programReviews(){
        $data['programReviews'] = $this->programReviewObj->fetchAll();
        return $this->loadpage('admin.lists.list_programreviews','Program Reviews',$data);
    }
    
    function publishProgramReviews($reviewId){
        $r = ProgramReview::findorfail($reviewId);
        $r->published=1;
        $r->save();
        $this->set_message('Review published successfully')->success();
        return redirect()->back();
    }
    
    function unpublishProgramReviews($reviewId){
        $r = ProgramReview::findorfail($reviewId);
        $r->published=0;
        $r->save();
        $this->set_message('Review unpublished successfully')->success();
        return redirect()->back();
    }

}
