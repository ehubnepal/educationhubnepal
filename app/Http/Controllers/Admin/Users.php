<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\User;
use App\Models\Admin\Member;
use Illuminate\Support\Facades\Input
;
use Illuminate\Support\Facades\DB;

class Users extends Controller {

    private $userObj;

    function __construct() {
        $this->userObj = new User();
    }

    function index() {
        $data['users'] = $this->userObj->fetchAll();
        return $this->loadpage('admin.lists.list_users', 'List of all users', $data);
    }

    function show($userId) {
        $user = $this->userObj->fetch($userId);
        if ($user):
            $data['user'] = $user;
            return $this->loadpage('admin.pages.user', 'Detail of user', $data);
        else:
            return redirect('admin/user');
        endif;
    }

    function edit($userId) {
        $user = $this->userObj->fetch($userId);
        if ($user):
            $maintainerCollegeRef = new \App\Models\Admin\MaintainerCollegeRef();
            $maintainerProgramRef = new \App\Models\Admin\MaintainerProgramRef();
            $user->colleges = $maintainerCollegeRef->fetchCollegeByUserId($user->id);
            $user->programs = $maintainerProgramRef->fetchProgramByUserId($user->id);
            $data['user'] = $user;
            return $this->loadpage('admin.pages.setmaintainer', 'Set maintainer', $data);
        else:
            return redirect('admin/user');
        endif;
    }

    function setMaintainer($userId) {
        $user = User::find($userId);
        $user->role = "maintainer";
        $user->save();
        $this->set_message('User has been assigned the role of a maintainer.')->success();
        return redirect('admin/user/' . $userId . '/edit');
    }

    function unsetMaintainer($userId) {
        $user = User::find($userId);
        $user->role = "member";
        $user->save();
        $this->set_message('User\'s role as a mainterner has been revoked')->info();
        return redirect()->back();
    }

    function alertDelete($userId) {
        $data['userId'] = $userId;
        $data['collegeReviewCount'] = count(\App\Models\Site\CollegeReview::where(['user_id' => $userId])->get());
        $data['programReviewCount'] = count(\App\Models\Site\ProgramReview::where(['user_id' => $userId])->get());
        $data['maintainerProgramCount'] = count(\App\Models\Admin\MaintainerProgramRef::where(['user_id' => $userId])->get());
        $data['maintainerCollegeCount'] = count(\App\Models\Admin\MaintainerCollegeRef::where(['user_id' => $userId])->get());
        $data['maintainerCollegeProgramCount'] = count(\App\Models\Admin\MaintainerCollegeProgramRef::where(['user_id' => $userId])->get());
        $reviewcount = $data['collegeReviewCount'] + $data['programReviewCount'] + $data['maintainerProgramCount'] + $data['maintainerCollegeCount'] + $data['maintainerCollegeProgramCount'];
        if ($reviewcount < 1):
            return $this->delete($userId);
        endif;
        return $this->loadpage('admin.pages.alertdelete', 'Heads up, you sure?', $data);
    }

    function delete($userId) {
        \App\Models\Admin\MaintainerCollegeProgramRef::where(['user_id' => $userId])->delete();
        \App\Models\Admin\MaintainerCollegeRef::where(['user_id' => $userId])->delete();
        \App\Models\Admin\MaintainerProgramRef::where(['user_id' => $userId])->delete();
        \App\Models\Member\Member::where(['id' => $userId])->delete();
        \App\Models\Site\MemberCollege::where(['user_id' => $userId])->delete();
        \App\Models\Site\MemberProgram::where(['user_id' => $userId])->delete();
        \App\Models\Site\CollegeReview::where(['user_id' => $userId])->delete();
        \App\Models\Site\ProgramReview::where(['user_id' => $userId])->delete();
        \App\Models\Admin\User::destroy($userId);
        $this->set_message("User deleted successfully")->success();
        return redirect('admin/user');
    }

}
