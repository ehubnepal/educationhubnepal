<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Program;
use App\Models\Admin\ProgramCategoryRef;
use Illuminate\Support\Facades\Input
;
use Image;
use Illuminate\Support\Facades\DB;

class Programs extends Controller {

    private $programObj;

    function __construct() {
        $this->programObj = new Program();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['programs'] = $this->programObj->fetchAll();
        return $this->loadpage('admin.lists.list_programs', 'List of programs', $data);
    }

    function test() {
        $sql = 'select * from programs';
        $rows = DB::select(DB::raw($sql));
        $counter = 0;
        foreach ($rows as $row):
            $details = explode('~', $row->detail);
            $program = Program::find($row->id);
            if (isset($details[0])):
                $program->course_description = $details[0];
            endif;
            if (isset($details[1])):
                $program->eligibility = $details[1];
            endif;
            if (isset($details[2])):
                $program->curriculum = $details[2];
            endif;
            $program->save();
            echo ++$counter . ' | data for ' . $program->pname . ' saved <br/>';
        endforeach;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $affiliationObj = new \App\Models\Admin\Affiliation();
        $data['affiliations'] = $affiliationObj->fetchAll(false);
        $levelObj = new \App\Models\Admin\Level();
        $data['levels'] = $levelObj->fetchAll();
        if (!empty($data['affiliations'])):
            $first_affiliation_id = $data['affiliations'][0]->id;
            $facultyObj = new \App\Models\Admin\Faculty();
            $data['faculties'] = $facultyObj->getFaculties($first_affiliation_id);
        endif;
        return $this->loadpage('admin.forms.form_program', 'Add new program', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $program = Program::create($request->all());

        if (!empty($program) && $program->id > 0):
            ProgramCategoryRef::where('program_id', $program->id)->delete();
            foreach(Input::get('programcategory') as $category_id):
                \App\Models\Admin\ProgramCategoryRef::create(['program_id'=>$program->id,'category_id'=>$category_id]);
            endforeach;
            $this->set_message('New program created successfully!')->success();
        else:
            $this->set_message('New program could not be created!')->error();
        endif;
        return redirect('admin/program');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data['program'] = $this->programObj->fetch($id);
        return $this->loadpage('admin.pages.program', $data['program']->pname, $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['program'] = $this->programObj->fetch($id);
        $levelObj = new \App\Models\Admin\Level();
        $data['levels'] = $levelObj->fetchAll();
        $data['program_category'] = \App\Models\Admin\ProgramCategory::all(['id','category']);
        $affiliationObj = new \App\Models\Admin\Affiliation();
        $data['affiliations'] = $affiliationObj->fetchAll(false);
        $facultyObj = new \App\Models\Admin\Faculty();
        $data['faculties'] = $facultyObj->getFaculties($data['program']->affiliation_id);
        return $this->loadpage('admin.forms.form_program', 'Edit program', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $programData = $request->all();
        $program = Program::find($programData['id']);
        $program->affiliation_id = $programData['affiliation_id'];
        $program->pname = $programData['pname'];
        $program->program_since = $programData['program_since'];
        $program->weight = $programData['weight']; //edite by vaghawan
        $program->level_id = $programData['level_id'];
        $program->url = $programData['url'];
        $program->duration = $programData['duration'];
        $program->cost_range = $programData['cost_range'];
        $program->academic_year = $programData['academic_year'];
        $program->evaluation_method = $programData['evaluation_method'];
        $program->faculty_id = $programData['faculty_id'];
        $program->course_description = $programData['course_description'];
        $program->eligibility = $programData['eligibility'];
        $program->curriculum = $programData['curriculum'];
        $program->published = isset($programData['published']) ? 1 : 0;
        if ($program->save()):
            ProgramCategoryRef::where('program_id', $program->id)->delete();
            foreach(Input::get('programcategory') as $category_id):
                \App\Models\Admin\ProgramCategoryRef::create(['program_id'=>$program->id,'category_id'=>$category_id]);
            endforeach;
            $this->set_message('Program updated successfully!')->success();
        else:
            $this->set_message('Program could not be updated!')->error();
        endif;
        return redirect()->back();
    }

    function search(Request $request) {
        $query = $request->get('search');
        $data['query'] = $query;
        $data['programs'] = $this->programObj->search($query);
        return $this->loadpage('admin.lists.list_programs', 'List of programs', $data);
    }

    function getFacultyDropdown($affiliation_id) {
        $facultyObj = new \App\Models\Admin\Faculty();
        $faculties = $facultyObj->getFaculties($affiliation_id);
        $html = "";
        foreach ($faculties as $faculty):
            $html .= "<option value='" . $faculty->id . "'>" . $faculty->faculty . "</option>\n";
        endforeach;
        $html .= "<option value='-1'>n/a</option>";
        echo $html;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Program::destroy($id);
        $this->set_message('Program deleted successfully')->success();
        return redirect()->back();
    }

    public function unpublish($id) {
        $program = Program::find($id);
        $program->published = 0;
        $program->save();
        return redirect()->back();
    }

    public function publish($id) {
        $program = Program::find($id);
        $program->published = 1;
        $program->save();
        $this->set_message('Program published successfully')->success();
        return redirect()->back();
    }

}
