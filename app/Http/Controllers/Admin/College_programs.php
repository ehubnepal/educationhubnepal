<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Program;
use App\Models\Admin\CollegeProgramRef;
use Illuminate\Http\Request;
use DB;
use Image;

class College_programs extends Controller {

    private $collegeObj;

    function __construct() {
        $this->collegeprogramObj = new CollegeProgramRef();
    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function index($id) {
        $data['college_id'] = $id;
        $programObj = new Program();
        $data['programs'] = $programObj->fetchAll(false);
        $data['collegeprograms'] = $this->collegeprogramObj->getProgram($id);
        return $this->loadpage('admin.lists.list_collegeprograms', "List of programs", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($college_id,$program_id) {
        $data['college_id'] = $college_id;
        $programObj = new Program();
        $data['programs'] = $programObj->fetchAll(false);
        $data['collegeprograms'] = $this->collegeprogramObj->getProgram($college_id);
        $data['collegeprogram'] = $this->collegeprogramObj->fetch($college_id,$program_id);
        return $this->loadpage('admin.lists.list_collegeprograms', 'Edit program', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $collegeprogramData = $request->all();
        $collegeprogram = CollegeProgramRef::find($collegeprogramData['id']);
        $collegeprogram->program_id = $collegeprogramData['program_id'];
        $collegeprogram->no_of_seat = $collegeprogramData['no_of_seat'];
        $collegeprogram->monthly_fee = $collegeprogramData['monthly_fee'];
        $collegeprogram->total_fee = $collegeprogramData['total_fee'];
        $collegeprogram->eligibility_criteria = $collegeprogramData['eligibility_criteria'];
        $collegeprogram->scholorship_criteria = $collegeprogramData['scholorship_criteria'];
        $collegeprogram->additional_info = $collegeprogramData['additional_info'];
        $collegeprogram->program_since = $collegeprogramData['program_since'];
        $collegeprogram->weight = $collegeprogramData['weight'];
        $collegeprogram->published = isset($collegeprogramData['published']) ? 1 : 0;
        if ($collegeprogram->save()):
            $this->set_message('Program detail updated successfully!')->success();
        else:
            $this->set_message('Program detail could not be updated!')->error();
        endif;

       
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $collegeprogram = CollegeProgramRef::create($request->all());

        if (!empty($collegeprogram) && $collegeprogram->id > 0):
           
            $this->set_message('New program linked successfully!')->success();
        else:
            $this->set_message('New program could not be linked!')->error();
        endif;
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        CollegeProgramRef::destroy($id);
        $this->set_message('Relation deleted successfully')->success();
        return redirect()->back();
    }

    public function unpublish($id) {
        $collegeprogram = CollegeProgramRef::find($id);
        $collegeprogram->published = 0;
        $collegeprogram->save();
        $this->set_message('Program unpublished successfully')->success();
        return redirect()->back();
    }

    public function publish($id) {
        $collegeprogram = CollegeProgramRef::find($id);
        $collegeprogram->published = 1;
        $collegeprogram->save();
        $this->set_message('Program published successfully')->success();
        return redirect()->back();
    }

}
