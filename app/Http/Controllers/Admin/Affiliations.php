<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Affiliation;
use Illuminate\Support\Facades\Input;
use Image;

class Affiliations extends Controller {

    private $thumbwidth = 150;
    private $thumbheight = 150;
    private $affiliationObj;

    function __construct() {
        $this->affiliationObj = new Affiliation();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['affiliations'] = $this->affiliationObj->fetchAll();
        return $this->loadpage('admin.lists.list_affiliations', 'List of universities', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return $this->loadpage('admin.forms.form_affiliation', 'Add new university');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $affiliation = Affiliation::create($request->all());

        if (!empty($affiliation) && $affiliation->id > 0):
            $logo = Input::file('universitylogo');
            if ($logo):
                /* upload image */
                $this->uploadLogo($affiliation, $logo);
            endif;

            /* upload images */
            $image = Input::file('universityimage');
            if ($image):
                /* upload image */
                $this->uploadImage($affiliation, $image);
            endif;

            $this->set_message('New university created successfully!')->success();
        else:
            $this->set_message('New university could not be created!')->error();
        endif;
        return redirect('admin/affiliation');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data['affiliation'] = $this->affiliationObj->fetch($id);
        return $this->loadpage('admin.pages.affiliation', $data['affiliation']->aname, $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['affiliation'] = $this->affiliationObj->fetch($id);
        return $this->loadpage('admin.forms.form_affiliation', 'Edit affiliation', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $affiliationData = $request->all();
        $affiliation = Affiliation::find($affiliationData['id']);
        $affiliation->aname = $affiliationData['aname'];
        $affiliation->description = $affiliationData['description'];
        $affiliation->established = $affiliationData['established'];
        $affiliation->education_body_type = $affiliationData['education_body_type'];
        $affiliation->language_of_education = $affiliationData['language_of_education'];
        $affiliation->vice_chancellor = $affiliationData['vice_chancellor'];
        $affiliation->published = isset($affiliationData['published']) ? 1 : 0;
        if ($affiliation->save()):
            $this->set_message('Affiliation updated successfully!')->success();
        else:
            $this->set_message('Affiliation could not be updated!')->error();
        endif;

        $logo = Input::file('universitylogo');
        if ($logo):
            /* remove old image */
            if (file_exists(base_path($affiliation->thumb_url))):
                @unlink(base_path($affiliation->thumb_url));
                @unlink(base_path($affiliation->full_url));
            endif;
            /* upload logo */
            $this->uploadLogo($affiliation, $logo);
        endif;

        /* upload images */
        $image = Input::file('universityimage');
        if ($image):
            /* upload image */
            $this->uploadImage($affiliation, $image);
        endif;

        return redirect('admin/affiliation');
    }

    function uploadLogo(&$affiliation, $image) {
        $filename = 'logo_' . $affiliation->id . '_' . time() . '.' . $image->getClientOriginalExtension();
        $path = base_path('images/universitylogo/' . $filename);
        $thumbpath = base_path('images/universitylogo/thumbs/' . $filename);
        Image::make($image->getRealPath())->save($path);
        /* resize image */
        Image::make($image->getRealPath())
                ->resize($this->thumbwidth, $this->thumbheight, function($constraint) {
                    $constraint->aspectRatio();
                })->save($thumbpath);
        $affiliation->full_url = 'images/universitylogo/' . $filename;
        $affiliation->thumb_url = 'images/universitylogo/thumbs/' . $filename;
        $affiliation->save();
    }

    function uploadImage(&$university, $image) {
        $filename = 'image_' . $university->id . '_' . time() . '.' . $image->getClientOriginalExtension();
        $path = base_path('images/universityimage/' . $filename);
        $thumbpath = base_path('images/universityimage/thumbs/' . $filename);
        Image::make($image->getRealPath())->save($path);
        /* resize image */
        Image::make($image->getRealPath())
                ->resize($this->thumbwidth, $this->thumbheight, function($constraint) {
                    $constraint->aspectRatio();
                })->save($thumbpath);

        $media = \App\Models\Admin\Media::create(array(
                    "full_url" => 'images/universityimage/' . $filename,
                    "thumb_url" => 'images/universityimage/thumbs/' . $filename,
                    "type" => 'image'
        ));
        if ($media->id > 0):
            \App\Models\Admin\AffiliationMediaRef::create(array(
                'affiliation_id' => $university->id,
                "media_id" => $media->id,
                "is_banner" => Input::get('is_banner') == null ? 0 : 1,
                "published" => 1,
                "ordering" => '50'
            ));
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $affiliation = Affiliation::find($id);
        if (file_exists(base_path($affiliation->thumb_url))):
            @unlink(base_path($affiliation->thumb_url));
            @unlink(base_path($affiliation->full_url));
        endif;
        $this->removeAllImage($id);
        Affiliation::destroy($id);
        $this->set_message('University deleted successfully')->success();
        return redirect()->back();
    }

    public function unpublish($id) {
        $affiliation = Affiliation::find($id);
        $affiliation->published = 0;
        $affiliation->save();
        return redirect()->back();
    }

    public function publish($id) {
        $affiliation = Affiliation::find($id);
        $affiliation->published = 1;
        $affiliation->save();
        $this->set_message('University published successfully')->success();
        return redirect()->back();
    }

    function removeLogo($id) {
        $affiliation = Affiliation::find($id);
        if (file_exists(base_path($affiliation->thumb_url))):
            @unlink(base_path($affiliation->thumb_url));
            @unlink(base_path($affiliation->full_url));
        endif;
        $affiliation->thumb_url = '';
        $affiliation->full_url = '';
        $affiliation->save();
        $this->set_message("Logo removed successfully")->info();
        return redirect()->back();
    }

    function removeImage($mediaId) {
        $mediaRefObj = new \App\Models\Admin\AffiliationMediaRef();
        $media = $mediaRefObj->getMedia($mediaId);
        if (file_exists(base_path($media->thumb_url))):
            @unlink(base_path($media->thumb_url));
            @unlink(base_path($media->full_url));
        endif;
        \App\Models\Admin\Media::destroy($media->id);
        \App\Models\Admin\AffiliationMediaRef::destroy($media->refid);
        $this->set_message("Image removed successfully")->info();
        return redirect()->back();
    }

    function removeAllImage($affiliationId) {
        $mediaRefObj = new \App\Models\Admin\AffiliationMediaRef();
        $medias = $mediaRefObj->getAllMedia($affiliationId);
        foreach ($medias as $media):
            if (file_exists(base_path($media->thumb_url))):
                @unlink(base_path($media->thumb_url));
                @unlink(base_path($media->full_url));
            endif;
            \App\Models\Admin\Media::destroy($media->id);
            \App\Models\Admin\AffiliationMediaRef::destroy($media->refid);
        endforeach;
    }

    function setimagebanner($mediaRefId) {
        $ref = \App\Models\Admin\AffiliationMediaRef::find($mediaRefId);
        $ref->is_banner = 1;
        $ref->save();
    }

    function unsetimagebanner($mediaRefId) {
        $ref = \App\Models\Admin\AffiliationMediaRef::find($mediaRefId);
        $ref->is_banner = 0;
        $ref->save();
    }

    function setimagepublish($mediaRefId) {
        $ref = \App\Models\Admin\AffiliationMediaRef::find($mediaRefId);
        $ref->published = 1;
        $ref->save();
    }

    function setimageunpublish($mediaRefId) {
        $ref = \App\Models\Admin\AffiliationMediaRef::find($mediaRefId);
        $ref->published = 0;
        $ref->save();
    }

    function setimageordering($mediaRefId, $value) {
        $ref = \App\Models\Admin\AffiliationMediaRef::find($mediaRefId);
        $ref->ordering = $value;
        $ref->save();
    }

}
