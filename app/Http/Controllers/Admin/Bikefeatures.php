<?php
/*
 * Features cannot be deleted directly
 * Features are directly related with bike models and 
 * will nbe deleted only when bike models are deleted
 */
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Bikefeature;
use App\Models\Admin\Bikemodel;
use Illuminate\Http\Request;

class Bikefeatures extends Controller {

    private $featureObj;

    function __construct() {
        $this->featureObj = new Bikefeature();
    }

    function edit($modelId) {
        $data['bikefeature'] = Bikefeature::where('model_id',$modelId)->first();
        
        $bikemodel = new Bikemodel();
        $data['bikemodel'] = $bikemodel->fetch($modelId);
        return $this->loadpage('admin.forms.form_bikefeature', 'Edit features',$data);
    }
    
    function update(Request $request){
        $featureData = $request->all();
        $feature = Bikefeature::find($featureData['id']);
        $feature->displacement = $featureData['displacement'];
        
        $feature->gearbox = $featureData['gearbox'];
        $feature->seat_height = $featureData['seat_height'];
        $feature->ground_clearance = $featureData['ground_clearance'];
        $feature->tank_capacity = $featureData['tank_capacity'];
        $feature->top_speed = $featureData['top_speed'];
        $feature->mileage = $featureData['mileage'];
        $feature->brake = $featureData['brake'];
        $feature->tyre = $featureData['tyre'];
        
        $feature->engine = $featureData['engine'];
        if ($feature->save()):
            $this->set_message('Feature updated successfully!')->success();
        else:
            $this->set_message('Feature could not be updated!')->error();
        endif;
        return redirect('admin/bikemodel');
    }

}
