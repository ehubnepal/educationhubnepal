<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Program;
use App\Models\Admin\Faculty;
use Illuminate\Http\Request;
use DB;
use Image;

class Faculties extends Controller {

    private $facultyObj;

    function __construct() {
        $this->facultyObj = new Faculty();
    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['faculties']= $this->facultyObj->fetchAll();
        return $this->loadpage('admin.lists.list_faculties', "List of faculties", $data);
    }

	/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
    	$affiliationObj = new \App\Models\Admin\Affiliation();
        $data['affiliations'] = $affiliationObj->fetchAll(false);
        return $this->loadpage('admin.forms.form_faculty', 'Add new faculty',$data);
    }

/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $faculty = Faculty::create($request->all());
        if (!empty($faculty) && $faculty->id > 0):
            $this->set_message('New faculty created successfully!')->success();
        else:
            $this->set_message('New faculty could not be created!')->error();
        endif;
        return redirect('admin/faculty');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['faculty'] = Faculty::find($id);
        $affiliationObj = new \App\Models\Admin\Affiliation();
        $data['affiliations'] = $affiliationObj->fetchAll(false);
        return $this->loadpage('admin.forms.form_faculty', 'Edit Faculty', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $facultyData = $request->all();
        $faculty = Faculty::find($facultyData['id']);
        $faculty->faculty = $facultyData['faculty'];
        $faculty->affiliation_id = $facultyData['affiliation_id'];
        $faculty->logo = $facultyData['logo'];
        $faculty->url = $facultyData['url'];
        $faculty->published = isset($facultyData['published']) ? 1 : 0;
        if ($faculty->save()):
            $this->set_message('Faculty updated successfully!')->success();
        else:
            $this->set_message('Faculty could not be updated!')->error();
        endif;

        return redirect('admin/faculty');
    }

    function search(Request $request){
     $query = $request->get('search');   
     $data['query'] = $query;
     $data['faculties']=$this->facultyObj->search($query);
     return $this->loadpage('admin.lists.list_faculties', 'List of faculties', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Faculty::destroy($id);
        $this->set_message('Faculty deleted successfully')->success();
        return redirect()->back();
    }

    public function unpublish($id) {
        $faculty = Faculty::find($id);
        $faculty->published = 0;
        $faculty->save();
        $this->set_message('Faculty unpublished successfully')->success();
        return redirect()->back();
    }

    public function publish($id) {
        $faculty = Faculty::find($id);
        $faculty->published = 1;
        $faculty->save();
        $this->set_message('Faculty published successfully')->success();
        return redirect()->back();
    }

}
