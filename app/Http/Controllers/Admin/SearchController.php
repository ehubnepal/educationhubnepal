<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\User;
use App\Models\Admin\Member;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller {

    private $collegeObj;
    private $programObj;

    function __construct() {
        $this->collegeObj = new \App\Models\Admin\College();
        $this->programObj = new \App\Models\Admin\Program();
    }

    function searchcollege(Request $request) {
        echo json_encode($this->collegeObj->searchAjax($request->get('cname')));
    }

    function searchprogram(Request $request) {
        echo json_encode($this->programObj->searchAjax($request->get('pname')));
    }

    function selectcollege(Request $request) {
        $data['user_id'] = $request->get('user_id');
        $data['college_id'] = $request->get('college_id');
        \App\Models\Admin\MaintainerCollegeRef::where(['college_id' => $data['college_id'], 'user_id' => $data['user_id']])->delete();
        \App\Models\Admin\MaintainerCollegeRef::create($data);
    }

    function selectprogram(Request $request) {
        $data['user_id'] = $request->get('user_id');
        $data['program_id'] = $request->get('program_id');
        \App\Models\Admin\MaintainerProgramRef::where(['program_id' => $data['program_id'], 'user_id' => $data['user_id']])->delete();
        \App\Models\Admin\MaintainerProgramRef::create($data);
    }

    function unselectcollege(Request $request) {
        $data['user_id'] = $request->get('user_id');
        $data['college_id'] = $request->get('college_id');
        \App\Models\Admin\MaintainerCollegeRef::where(['college_id' => $data['college_id'], 'user_id' => $data['user_id']])->delete();
        \App\Models\Admin\MaintainerCollegeProgramRef::where(['college_id' => $data['college_id'], 'user_id' => $data['user_id']])->delete();
    }

    function unselectprogram(Request $request) {
        $data['user_id'] = $request->get('user_id');
        $data['program_id'] = $request->get('program_id');
        \App\Models\Admin\MaintainerProgramRef::where(['program_id' => $data['program_id'], 'user_id' => $data['user_id']])->delete();
    }
    
    function selectCollegeProgram(Request $request){
        $data['user_id'] = $request->get('user_id');
        $data['program_id'] = $request->get('program_id');
        $data['college_id'] = $request->get('college_id');
        \App\Models\Admin\MaintainerCollegeProgramRef::where(['program_id' => $data['program_id'],'college_id'=>$data['college_id'], 'user_id' => $data['user_id']])->delete();
        \App\Models\Admin\MaintainerCollegeProgramRef::create($data);
    }
    function unselectCollegeProgram(Request $request){
        $data['user_id'] = $request->get('user_id');
        $data['program_id'] = $request->get('program_id');
        $data['college_id'] = $request->get('college_id');
        \App\Models\Admin\MaintainerCollegeProgramRef::where(['program_id' => $data['program_id'],'college_id'=>$data['college_id'], 'user_id' => $data['user_id']])->delete();
    }

}
