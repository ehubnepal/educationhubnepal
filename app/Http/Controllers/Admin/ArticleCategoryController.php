<?php
namespace App\Http\Requests;
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Admin\ArticleCategory;



class ArticleCategoryController extends Controller{

public function index(){
	$categories = ArticleCategory::all();
	return view('admin.lists.list_article_categories',compact('categories'));
}

public function create(){
	return view('admin.forms.form_article_category_create');
}
public function store(Request $request){
	 	$category = new ArticleCategory;
        $category->name = $request->input('name');
        $category->slug = $request->input('slug');
        $category->title = $request->input('title');
        $category->desc = $request->input('desc');
       
        if($request->has('published')){
            $category->published =1;
        }
        else{
            $category->published = 0;
        }
        if($request->file('image')){

             $imageName = date('Y-m-d-H-m') . '-' . 
        $request->file('image')->getClientOriginalName();
      
        $request->file('image')->move(
        base_path() . '/public/admin/img/blog/', $imageName
        );
        $category->image= $imageName;
        }
        $category->save();
        $this->set_message('Category published successfully')->success();
        return redirect('admin/blog/categories');
}
public function edit($id){
		$category = ArticleCategory::where('category_id',$id)->first();

        return view('admin.forms.form_article_category_edit', compact('category'));
}
public function update($id,Request $request){
		$category = ArticleCategory::where('category_id',$id)->first();
        $category->name = $request->input('name');
        $category->slug = $request->input('slug');
        $category->title = $request->input('title');
        $category->desc = $request->input('desc');
        if($request->file('image')){

             $imageName = date('Y-m-d-H-m') . '-' . 
        $request->file('image')->getClientOriginalName();
      
        $request->file('image')->move(
        base_path() . '/public/admin/img/blog/', $imageName
        );
        $category->image= $imageName;
    	}
         if($request->has('published')){
            $category->published =1;
        }
        else{
            $category->published = 0;
        }

        if($category->update()):
	        $this->set_message('Category Updated successfully')->success();
	        return redirect('admin/blog/categories');
        else:
        	$this->set_message('Category Could not be updated')->error();
        	return redirect('admin/blog/categories');
        endif;
}
  public function destroy($id){
        ArticleCategory::find($id)->delete();
        $this->set_message('Category Deleted successfully!')->success();
        return redirect('admin/blog/categories');
    }

     public function unpublish($id) {
        $article = ArticleCategory::find($id);
        $article->published = 0;
        $article->save();
        $this->set_message('Category unpublished successfully')->success();
        return redirect()->back();
    }

    public function publish($id) {
        $article = ArticleCategory::find($id);
        $article->published = 1;
        $article->save();
        $this->set_message('Category published successfully')->success();
        return redirect()->back();
    }

}