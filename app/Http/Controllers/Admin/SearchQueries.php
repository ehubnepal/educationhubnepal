<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Affiliation;
use Illuminate\Support\Facades\Input;
use Image;

class SearchQueries extends Controller {

    private $searchQuery;

    function __construct() {
        $this->searchQueryObj = new \App\Models\Admin\SearchQuery();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['queries'] = $this->searchQueryObj->fetchAll();
        return $this->loadpage('admin.lists.list_searchqueries', 'List of searchqueries', $data);
    }
    
    function delete(Request $request){
        $query = $request->get('query');
        \App\Models\Admin\SearchQuery::where(['query' => $query])->delete();
        $this->set_message("query : <strong>".$query.'</strong> deleted successfully')->success();
        return redirect()->back();
    }

}
