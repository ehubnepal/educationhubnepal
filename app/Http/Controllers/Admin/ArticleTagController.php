<?php 
namespace App\Http\Requests;
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Admin\Article;
use App\Models\Admin\ArticleTag;
use DB;

class ArticleTagController extends Controller{

	function index(){
		$tags = ArticleTag::all();
		return view('admin.lists.list_article_tags',compact('tags'));
	}
	function create(){
		return view('admin.forms.form_article_tag_create');
	}
	function store(Request $request){
		$tag = new ArticleTag();
		$tag->name = $request->input('name');
		$tag->save();
		return redirect('admin/blog/tags');

	}
	function edit($id){
		$tag = ArticleTag::where('tag_id',$id)->first();
		return view('admin.forms.form_article_tag_edit',compact('tag'));
	}
	function update($id,Request $request ){
		$tag = ArticleTag::where('tag_id',$id)->first();
		$tag->name = $request->input('name');
		$tag->update();
		 $this->set_message('Tag Updated successfully!')->success();
        return redirect()->back();
	}
	 function destroy($id){
        ArticleTag::find($id)->delete();
        $this->set_message('Tag Deleted successfully!')->success();
        return redirect()->back();
    }
}