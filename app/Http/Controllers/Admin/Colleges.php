<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\College;
use Illuminate\Support\Facades\Input;
use Image;

class Colleges extends Controller {

    private $thumbwidth = 150;
    private $thumbheight = 150;
    private $collegeObj;

    function __construct() {
        $this->collegeObj = new College();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['colleges'] = $this->collegeObj->fetchAll();
        return $this->loadpage('admin.lists.list_colleges', 'List of colleges', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $affiliationObj = new \App\Models\Admin\Affiliation();
        $data['affiliations'] = $affiliationObj->fetchAll(false);
        return $this->loadpage('admin.forms.form_college', 'Add new college');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $collegedata = $request->all();
        $college = College::create($request->all());
        /* Store a copy for maintainer */
        $collegedata['id'] = $college->id;
        \App\Models\Superuser\College::create($collegedata);
        /* end storing for maintainer */
                
        if (!empty($college) && $college->id > 0):
            $logo = Input::file('collegelogo');
            if ($logo):
                /* upload logo */
                $this->uploadLogo($college, $logo);
            endif;

            /* upload images */
            $image = Input::file('collegeimage');
            if ($image):
                /* upload image */
                $this->uploadImage($college, $image);
            endif;
            $this->set_message('New college created successfully!')->success();
        else:
            $this->set_message('New college could not be created!')->error();
        endif;
        return redirect('admin/college');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data['college'] = $this->collegeObj->fetch($id);
        return $this->loadpage('admin.pages.college', $data['college']->cname, $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $affiliationObj = new \App\Models\Admin\Affiliation();
        $data['affiliations'] = $affiliationObj->fetchAll(false);
        $data['college'] = $this->collegeObj->fetch($id);
        return $this->loadpage('admin.forms.form_college', 'Edit college', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $collegeData = $request->all();
        $college = College::find($collegeData['id']);
        $college->cname = $collegeData['cname'];
        $college->url = $collegeData['url'];
        $college->address = $collegeData['address'];
        $college->longitude = $collegeData['longitude'];
        $college->latitude = $collegeData['latitude'];
        $college->district = $collegeData['district'];
        $college->affiliation_ids = json_encode(isset($collegeData['affiliation_ids']) ? $collegeData['affiliation_ids'] : array());
        $college->phone = $collegeData['phone'];
        $college->fax = $collegeData['fax'];
        $college->website = $collegeData['website'];
        $college->pobox = $collegeData['pobox'];
        $college->email = $collegeData['email'];
        $college->description = $collegeData['description'];
        $college->facilities = $collegeData['facilities'];
        $college->country = $collegeData['country'];
        $college->published = isset($collegeData['published']) ? 1 : 0;
        if ($college->save()):
            $this->set_message('College detail updated successfully!')->success();
        else:
            $this->set_message('College detail could not be updated!')->error();
        endif;

        /* upload logo */
        $logo = Input::file('collegelogo');
        if ($logo):
            /* remove old image */
            if (file_exists(base_path($college->full_url))):
                @unlink(base_path($college->full_url));
                @unlink(base_path($college->thumb_url));
            endif;
            /* upload logo */
            $this->uploadlogo($college, $logo);
        endif;

        /* upload images */
        $image = Input::file('collegeimage');
        if ($image):
            /* upload image */
            $this->uploadImage($college, $image);
        endif;

        return redirect('admin/college');
    }

    function search(Request $request) {
        $query = $request->get('search');
        $data['query'] = $query;
        $data['colleges'] = $this->collegeObj->search($query);
        return $this->loadpage('admin.lists.list_colleges', 'List of colleges', $data);
    }

    function uploadLogo(&$college, $image) {
        $filename = 'logo_' . $college->id . '_' . time() . '.' . $image->getClientOriginalExtension();
        $path = base_path('images/collegelogo/' . $filename);
        $thumbpath = base_path('images/collegelogo/thumbs/' . $filename);
        Image::make($image->getRealPath())->save($path);
        /* resize image */
        Image::make($image->getRealPath())
                ->resize($this->thumbwidth, $this->thumbheight, function($constraint) {
                    $constraint->aspectRatio();
                })->save($thumbpath);
        $college->full_url = 'images/collegelogo/' . $filename;
        $college->thumb_url = 'images/collegelogo/thumbs/' . $filename;
        $college->save();
    }

    function uploadImage(&$college, $image) {
        $filename = 'image_' . $college->id . '_' . time() . '.' . $image->getClientOriginalExtension();
        $path = base_path('images/collegeimage/' . $filename);
        $thumbpath = base_path('images/collegeimage/thumbs/' . $filename);
        Image::make($image->getRealPath())->save($path);
        /* resize image */
        if( Input::get('is_banner') != null){
            $this->thumbwidth=320;
            $this->thumbheight=300;
        }
        Image::make($image->getRealPath())
                ->resize($this->thumbwidth, $this->thumbheight, function($constraint) {
                    $constraint->aspectRatio();
                })->save($thumbpath);

        $media = \App\Models\Admin\Media::create(array(
                    "full_url" => 'images/collegeimage/' . $filename,
                    "thumb_url" => 'images/collegeimage/thumbs/' . $filename,
                    "type" => 'image'
        ));
        if ($media->id > 0):
            \App\Models\Admin\CollegeMediaRef::create(array(
                'college_id' => $college->id,
                "media_id" => $media->id,
                "is_banner" => Input::get('is_banner') == null ? 0 : 1,
                "published" => 1,
                "ordering" => '50'
            ));
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $college = College::find($id);
        if (file_exists(base_path($college->full_url))):
            @unlink(base_path($college->full_url));
            @unlink(base_path($college->thumb_url));
        endif;
        $this->removeAllImage($college->id);
        College::destroy($id);
        $this->set_message('College deleted successfully')->success();
        return redirect()->back();
    }

    public function unpublish($id) {
        $college = College::find($id);
        $college->published = 0;
        $college->save();
        return redirect()->back();
    }

    public function publish($id) {
        $college = College::find($id);
        $college->published = 1;
        $college->save();
        $this->set_message('College published successfully')->success();
        return redirect()->back();
    }

    function removeLogo($id) {
        $college = College::find($id);
        if (file_exists(base_path($college->thumb_url))):
            @unlink(base_path($college->thumb_url));
            @unlink(base_path($college->full_url));
        endif;
        $college->thumb_url = '';
        $college->full_url = '';
        $college->save();
        $this->set_message("Logo removed successfully")->info();
        return redirect()->back();
    }

    function removeAllImage($collegeId) {
        $mediaRefObj = new \App\Models\Admin\CollegeMediaRef();
        $medias = $mediaRefObj->getAllMedia($collegeId);
        foreach ($medias as $media):
            if (file_exists(base_path($media->thumb_url))):
                @unlink(base_path($media->thumb_url));
                @unlink(base_path($media->full_url));
            endif;
            \App\Models\Admin\Media::destroy($media->id);
            \App\Models\Admin\CollegeMediaRef::destroy($media->refid);
        endforeach;
    }

    function removeImage($mediaId) {
        $mediaRefObj = new \App\Models\Admin\CollegeMediaRef();
        $media = $mediaRefObj->getMedia($mediaId);
        if (file_exists(base_path($media->thumb_url))):
            @unlink(base_path($media->thumb_url));
            @unlink(base_path($media->full_url));
        endif;
        \App\Models\Admin\Media::destroy($media->id);
        \App\Models\Admin\CollegeMediaRef::destroy($media->refid);
        $this->set_message("Image removed successfully")->info();
        return redirect()->back();
    }

    function setimagebanner($mediaRefId) {
        $ref = \App\Models\Admin\CollegeMediaRef::find($mediaRefId);
        $ref->is_banner = 1;
        $ref->save();
    }

    function unsetimagebanner($mediaRefId) {
        $ref = \App\Models\Admin\CollegeMediaRef::find($mediaRefId);
        $ref->is_banner = 0;
        $ref->save();
    }

    function setimagepublish($mediaRefId) {
        $ref = \App\Models\Admin\CollegeMediaRef::find($mediaRefId);
        $ref->published = 1;
        $ref->save();
    }

    function setimageunpublish($mediaRefId) {
        $ref = \App\Models\Admin\CollegeMediaRef::find($mediaRefId);
        $ref->published = 0;
        $ref->save();
    }

    function setimageordering($mediaRefId, $value) {
        $ref = \App\Models\Admin\CollegeMediaRef::find($mediaRefId);
        $ref->ordering = $value;
        $ref->save();
    }

    
   
}
