<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Bikemodel;
use App\Models\Admin\Bikefeature;
use App\Models\Admin\Media;
use App\Models\Admin\MediaBikemodelRef;
use Input;
use Image;

class Bikemodels extends Controller {

    private $thumbwidth = 150;
    private $thumbheight = 150;
    private $modelObj;

    function __construct() {
        $this->modelObj = new Bikemodel();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['models'] = $this->modelObj->fetchAll();
        return $this->loadpage('admin.lists.list_bikemodel', 'List of bike models', $data);
    }

    /**
     * Display a trashed items
     *
     * @return \Illuminate\Http\Response
     */
    public function viewtrash() {
        $data['models'] = $this->modelObj->fetchTrashed();
        return $this->loadpage('admin.lists.list_bikemodel_trash', 'List of trashed bike models', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data['brands'] = \App\Models\Admin\Brand::all();
        return $this->loadpage('admin.forms.form_bikemodel', 'Add new brands', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $bikemodel = Bikemodel::create($request->all());

        if (!empty($bikemodel) && $bikemodel->id > 0):
            Bikefeature::create(['model_id' => $bikemodel->id]);
            $image = Input::file('bikeimage');
            if ($image):
                /* upload image */
                $this->uploadMedia($bikemodel->id, $image);
            endif;
            $this->set_message('New Bike model successfully!')->success();
        else:
            $this->set_message('New Bike model could not be created!')->error();
        endif;
        return redirect('admin/bikemodel');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        echo $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['bikemodel'] = $this->modelObj->fetch($id);
        $data['brands'] = \App\Models\Admin\Brand::all();
        return $this->loadpage('admin.forms.form_bikemodel', 'Edit model', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $modelData = $request->all();
        $model = Bikemodel::find($modelData['id']);
        $model->brand_id = $modelData['brand_id'];
        $model->description = $modelData['description'];
        $model->model = $modelData['model'];
        $model->published = isset($modelData['published']) ? 1 : 0;
        if ($model->save()):
            $this->set_message('Brand updated successfully!')->success();
        else:
            $this->set_message('Brand could not be updated!')->error();
        endif;

        $image = Input::file('bikeimage');
        if ($image):
            /* remove old image */
            $mediaRef = new MediaBikemodelRef();
            $mediaRef->removeOldImage($model->id);
            /* upload image */
            $this->uploadMedia($model->id);
        endif;
        return redirect('admin/bikemodel');
    }

    function uploadMedia($modelId, $image) {
        $filename = 'model_' . $modelId . '_' . time() . '.' . $image->getClientOriginalExtension();
        $path = base_path('images/models/' . $filename);
        $thumbpath = base_path('images/models/thumbs/' . $filename);
        $media = Media::create(['type' => 'image', 'full_url' => 'images/models/' . $filename, 'thumb_url' => 'images/models/thumbs/' . $filename]);

        if (!empty($media) && $media->id > 0):
            Image::make($image->getRealPath())->save($path);
            /* resize image */
            Image::make($image->getRealPath())
                    ->resize($this->thumbwidth, $this->thumbheight, function($constraint) {
                        $constraint->aspectRatio();
                    })->save($thumbpath);

            MediaBikemodelRef::create(['model_id' => $modelId, 'media_id' => $media->id]);
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $mediaRef = new MediaBikemodelRef();
        $mediaRef->removeOldImage($id);
        Bikefeature::where('model_id',$id)->delete();
        Bikemodel::destroy($id);
        $this->set_message('Bike model deleted successfully')->success();
        return redirect()->back();
    }

    /**
     * Trashes the specified resource that can be recycled in future.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function trash($id) {
        $model = Bikemodel::find($id);
        $model->trashed = 1;
        $model->save();
        $this->set_message('Bike model trashed successfully')->success();
        return redirect()->back();
    }

    /**
     * UnTrashes the bike model.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function untrash($id) {
        $model = Bikemodel::find($id);
        $model->trashed = 0;
        $model->save();
        $this->set_message('Bike model untrashed successfully')->success();
        return redirect()->back();
    }

    public function unpublish($id) {
        $model = Bikemodel::find($id);
        $model->published = 0;
        $model->save();
        $this->set_message('Bike model unpublished successfully')->success();
        return redirect()->back();
    }

    public function publish($id) {
        $model = Bikemodel::find($id);
        $model->published = 1;
        $model->save();
        $this->set_message('Bike model published successfully')->success();
        return redirect()->back();
    }

    function removeLogo($id) {
        $mediaRef = new MediaBikemodelRef();
        $mediaRef->removeOldImage($id);
        $this->set_message('Image removed successfully')->success();
        return redirect()->back();
    }

    function editfeature($id) {
        return $this->loadpage('admin.forms.form_bikefeature', 'Edit features');
    }

}
