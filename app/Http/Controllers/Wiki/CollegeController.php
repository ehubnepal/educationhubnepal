<?php

namespace App\Http\Controllers\Wiki;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Wiki\College;
use App\Models\Wiki\Program;
use Auth;

class CollegeController extends Controller {

    private $collegeObj;

    function __construct() {
        $this->collegeObj = new College();
    }

    /*
     * 
     */

    function preEditDetail($url) {
        if (Auth::user()) {
            return redirect('wiki/college/' . $url . '/edit/detail');
        }
        $data['college'] = $this->collegeObj->fetch($url);
        $data['college']->college_id = $data['college']->id;
        unset($data['college']->id);
        return $this->loadpage('wiki.pages.pre_edit_college', 'Login to edit', $data);
    }

    function editDetail($url) {
        $data['college'] = $this->collegeObj->fetch($url);
        if ($this->already_edited_by_this_user($data['college']->id)) {
           return redirect('wiki/college/'.$url.'/editdetailagain');
        }
        $data['college']->college_id = $data['college']->id;
        unset($data['college']->id);
        $counter = \App\Models\Wiki\WikiCollegeDescription::select('id')->where('college_id', $data['college']->college_id)->get();
        $data['edit_count'] = count($counter);
        return $this->loadpage('wiki.forms.form_college', 'Edit college detail', $data);
    }

    function already_edited_by_this_user($college_id){
        $record = \App\Models\Wiki\WikiCollegeDescription::select('id')->where(['college_id'=>$college_id,'user_id'=>Auth::user()->id])->get();
        return count($record)>0;
    }
    
    function store(Request $request) {
        $data['college_id'] = $request->get('college_id');
        $data['user_id'] = Auth::user()->id;
        $data['published'] = 1;
        $data['description'] = $request->get('description');
        $data['phone'] = $request->get('phone');
        $data['email'] = $request->get('email');
        $data['fax'] = $request->get('fax');
        $data['address'] = $request->get('address');
        $data['website'] = $request->get('website');
        $data['up_votes'] = '[]';
        $data['down_votes'] = '[]';
        $wiki = \App\Models\Wiki\WikiCollegeDescription::create($data);
        if ($wiki->id > 0):
            $college = College::findOrFail($wiki->college_id);
            $this->set_message('Congratulations! You have successfully posted the edits. It will be reviewed by other members and will be published soon.')->success();
            return redirect('wiki/college/' . $college->url . '/editdetailagain');
        else:
            $this->set_message('Something went wrong while saving detail. Please try again')->error();
            return redirect('wiki/college/' . $college->url . '/edit/detail');
        endif;
    }

    function editDetailAgain($url) {
        $college = College::where('url', $url)->first();
        $data['college'] = \App\Models\Wiki\WikiCollegeDescription::where(['college_id' => $college->id, "user_id" => Auth::user()->id])->first();
        $data['college']->cname = $college->cname;
        // $data['college']->address = $college->address;
        $data['college']->url = $college->url;
        $counter = \App\Models\Wiki\WikiCollegeDescription::select('id')->where('college_id', $data['college']->college_id)->get();
        $data['edit_count'] = count($counter);
        return $this->loadpage('wiki.forms.form_college', 'Edit college detail', $data);
    }

    function update(Request $request) {
        $wiki = \App\Models\Wiki\WikiCollegeDescription::findOrFail($request->get('id'));
        $college = College::findOrFail($wiki->college_id);
        $wiki->description = $request->get('description');
        $wiki->phone = $request->get('phone');
        $wiki->email = $request->get('email');
        $wiki->fax = $request->get('fax');
        $wiki->address = $request->get('address');
        $wiki->website = $request->get('website');
        $wiki->save();
        $this->set_message('Successfully updated')->success();
        return redirect('wiki/college/' . $college->url . '/editdetailagain');
    }

    function listEditHistory($url) {
        $college = $this->collegeObj->fetch($url);
        $history = \App\Models\Wiki\WikiCollegeDescription::select('*')->where('college_id', $college->id)->get();
        foreach ($history as &$row):
            $row->user = \App\Models\Member\Member::findOrFail($row->user_id);
        endforeach;
        return $this->loadpage('wiki.lists.list_edithistory_collegediscription', 'Edit history', compact('college', 'history'));
    }

}
