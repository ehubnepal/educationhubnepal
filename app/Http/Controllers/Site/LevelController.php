<?php
namespace App\Http\Requests;
namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Site\Level;
use App\Models\Site\Affiliation;
use App\Models\Site\Category;
//use Input;



class LevelController extends Controller {

	 private $facultyObj;

    function __construct() {
        $this->levelObj = new Level();
    }

	/* this function loads the signle detail page */
	public function single_detail($url){
		$data['level'] = $this->levelObj->fetch($url);
		if(empty($data['level'])):
			return redirect('levels');
		endif;
		$data['categories'] = Category::all()->where('published',1);
		$data['universities'] = Affiliation::all();
		$title = $data['level']->lname.' Level Programs';

		return $this->loadpage('site.pages.level_detail',$title,$data);
	}
	public function index(){
		$data['levels'] = $this->levelObj->fetch_all();
		return $this->loadpage('site.pages.level_listing','All Levels Of Study | Education Hub Nepal', $data);
	}

	public function find(){
		$category = Input::get('category');
		$university = Input::get('affiliation');
		$lev = Input::get('level_id');
		if(empty($lev)):
			return redirect()->back();
		endif;
		$data['res'] = $this->levelObj->search($category,$university,$lev);
		if(empty($data['res'])):
			$this->set_message('Something went wrong with your search')->error();
			return redirect()->back();
		endif;
		$level = Level::where('id',$lev)->first();
		$data['level'] = $level;
		$data['categories'] = Category::all()->where('published',1);
		$data['universities'] = Affiliation::all();
		$data['cat'] = $category;
		$data['uni'] = $university;
		
		if($category!=='' AND $university !==''):
			$title = 'Programs Of '.$university. ' Under '. $category.' Faculty In '.$level->lname;
		elseif($category=='' AND $university!==''):
			$title = 'Programs Of '.$university.' In '.$level->lname;
		elseif($category!=='' AND $university==''):
			$title = 'Progrms Of '.$level->lname.' Under '.$category.' Faculty';
		endif;
		$data['title'] =$title;
		return $this->loadpage('site.pages.level_search',$title,$data);

	}

}