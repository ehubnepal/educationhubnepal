<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Site\CollegeReview;
use App\Models\Site\ProgramReview;
use Auth;

class ReviewController extends Controller {

    private $collegeReviewObj;
    private $programReviewObj;

    function __construct() {
        $this->collegeReviewObj = new CollegeReview();
        $this->programReviewObj = new ProgramReview();
    }

    function storeCollegeReview(Request $request) {
        $reviewData['user_id'] = Auth::user()->id;
        $reviewData['college_id'] = $request->get('cid');
        $reviewData['star'] = $request->get('rating');
        $reviewData['review'] = $request->get('review');
        $reviewData['published'] = 0;

        $review = CollegeReview::create($reviewData);
        if ($review->id > 0):
            $this->set_message("Review created successfully. It will be visible in the website within 30 minutes.")->success();
            $commonobj = new \App\Models\CommonModel();
            $commonobj->sendmail('ehubnepal@gmail.com', 'need approval: new colelge review created', $reviewData['review']);
        else:
            $this->set_message("Review could not be created.")->error();
        endif;
        return redirect()->back();
    }

    function storeProgramReview(Request $request) {
        $reviewData['user_id'] = Auth::user()->id;
        $reviewData['program_id'] = $request->get('pid');
        $reviewData['star'] = $request->get('rating');
        $reviewData['review'] = $request->get('review');
        $reviewData['published'] = 0;

        $review = ProgramReview::create($reviewData);
        if ($review->id > 0):
            $this->set_message("Review created successfully. It will be visible in the website withing 30 minutes.")->success();
            $commonobj = new \App\Models\CommonModel();
            $commonobj->sendmail('ehubnepal@gmail.com', 'need approval: new program review created', $reviewData['review']);
        else:
            $this->set_message("Review could not be created.")->error();
        endif;
        return redirect()->back();
    }

}
