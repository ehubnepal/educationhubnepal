<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Site\College;
use App\Models\Site\Program;
use Input;
use Image;
use DB;

class SearchController extends Controller {

    private $collegeObj;
    private $programObj;

    function __construct() {
        $this->collegeObj = new College();
        $this->programObj = new Program();
    }

    public function find(Request $request) {
        $param = $request->all();
        $param['searchby'] = isset($param['searchby']) ? $param['searchby'] : 'program';
        $param['query'] = isset($param['query']) ? $param['query'] : '';
        if ($param['searchby'] == 'program'):
            \App\Models\Site\SearchQuery::create(array(
                "type"=>"program",
                "query"=>$param['query']
            ));
            $data['programs'] = $this->programObj->search($param['query'], $param);
            $view = 'site.pages.coursesearch';
        else:
            \App\Models\Site\SearchQuery::create(array(
                "type"=>"college",
                "query"=>$param['query']
            ));
            $data['colleges'] = $this->collegeObj->search($param);
            $view = 'site.pages.collegesearch';
        endif;
        $data['param'] = $param;

        return $this->loadpage($view, 'Search result for : ' . $param['query'], $data);
    }

    /* this function loads the signle detail page */

    public function single_detail($url) {
        $detail = $this->collegeObj->fetch($url);
        echo '<pre>';
        print_r($detail);
        die();
        //return view('site.pages.college_detail',compact('detail'));
    }

    public function college_all() {
        return view('site.pages.course_listing');
    }

    function autosuggestprogram(Request $request) {
        $param = $request->all();
        $courses = DB::table("programs")
                ->join('affiliations', 'affiliations.id', '=', 'programs.affiliation_id')
                ->where('pname', 'like', '%' . $param['query'] . '%')
                ->select('pname', 'aname')
                ->get();
        $data = array();
        foreach ($courses as $course):
            if (!in_array($course->pname, $data)) {
                $data[] = $course->pname; //.' ~ ['.Stringfirst($course->aname).']';
            }
        endforeach;
        echo json_encode($data);
    }

    function autosuggestcollege(Request $request) {
        $param = $request->all();
        $colleges = DB::table('colleges')
                ->where('cname', 'like', '%' . $param['query'] . '%')
                ->select('cname')
                ->get();
        $data = array();
        foreach ($colleges as $college):
            $data[] = $college->cname;
        endforeach;
        echo json_encode($data);
    }

    function searchCollege(Request $request) {
        $param = $request->all();
        $colleges = array();
        if (isset($param['query']) && !empty($param['query'])) {
            $colleges = DB::table("colleges")
                    ->where('cname', '=', $param['query'])
                    ->select('id')
                    ->get();
        }
        if (empty($colleges)) {
            $sql = "select distinct(c.id) FROM colleges c"
                    . " INNER JOIN college_program cp ON cp.college_id = c.id "
                    . "INNER JOIN programs p ON p.id = cp.program_id "
                    . "INNER JOIN affiliations a ON a.id = p.affiliation_id ";
            $condition = array();
            if (isset($param['location']) && $param['location'] == "inside_kathmandu") {
                $condition[] = " c.district IN ('kathmandu','lalitpur','bhaktapur') ";
            }

            if (isset($param['institutiontype'])) {
                if ($param['institutiontype'] == 'intl_affiliation') {
                    $condition[] = " a.education_body_type like 'International%'";
                } else if ($param['institutiontype'] == 'national_affiliation') {
                    $condition[] = " a.education_body_type like 'National%'";
                }
            }
            if (isset($param['query'])) {
                $condition[] = " c.cname like '%" . $param['query'] . "%'";
            }

            if (isset($param['level']) && $param['level'] > 0) {
                $condition[] = " p.level_id = " . $param['level'];
            }
            if (!empty($condition)) {
                $condition_string = implode(' AND', $condition);
                $sql .= " WHERE " . $condition_string;
            }
            $colleges = DB::select(DB::raw($sql));
        }
        $collegeObj = new College();
        $data['colleges'] = $collegeObj->getPaginatedResult($colleges, $param);
        $data['levels'] = \App\Models\Site\Level::all();
        $data['param'] = $param;
        return $this->loadpage('site.pages.college_listing', 'Search From All Colleges Of Nepal', $data);
    }

}
