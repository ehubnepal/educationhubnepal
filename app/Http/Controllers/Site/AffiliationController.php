<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Site\Affiliation;


class AffiliationController extends Controller {

	 private $affiliationObj;

    function __construct() {
        $this->affiliationObj = new Affiliation();
    }

    public function index(){
    	$data['universities'] = $this->affiliationObj->fetchAll();
    	$data['nationaluniversities'] = $this->affiliationObj->fetchByType('National(Universities)');
   		$data['nationalothers'] = $this->affiliationObj->fetchByType('National(Other Affiliations)');
   		$data['internationaluniversities'] = $this->affiliationObj->fetchByType('International Affiliations');
    	return $this->loadpage('site.pages.university_listing', 'Search From All Universities Of Nepal', $data);
    }

	/* this function loads the signle detail page */
	public function single_detail($url){
		$data['university'] = $this->affiliationObj->fetch($url);
        if(empty($data['university'])):
            return redirect('universities');
        endif;
		$title = $data['university']->aname;
		return $this->loadpage('site.pages.university_detail',$title,$data);
	}
   

}