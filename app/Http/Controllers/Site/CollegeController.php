<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Site\College;
use App\Models\Site\Program;



class CollegeController extends Controller {

	 private $collegeObj;

    function __construct() {
        $this->collegeObj = new College();
    }


    public function index(){
    	$data['colleges'] = $this->collegeObj->fetchAll();
        $data['levels'] = \App\Models\Site\Level::all();
//    	echo '<pre>';
//    	print_r($data['colleges'] );
//    	die();
    	
    	return $this->loadpage('site.pages.college_listing', 'Search From All Colleges Of Nepal', $data);
    }

	/* this function loads the signle detail page */
	public function single_detail($url){
		$data['college'] = $this->collegeObj->fetch($url);
        if(empty($data['college'])):
            return redirect('colleges');
        endif;
		$title = $data['college']->cname;
                if(isset($data['college']->id)):
                    $college = College::find($data['college']->id);
                    $college->viewcount = $college->viewcount+1;
                    $college->save();
                endif;
		return $this->loadpage('site.pages.college_detail',$title,$data);
	}

}