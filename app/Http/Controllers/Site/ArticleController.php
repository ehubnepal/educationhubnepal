<?php 
namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Site\Article;
use App\Models\Site\ArticleCategory;
use App\Models\Site\ArticleTag;
use DB;

class ArticleController extends Controller {
private $articleObj;

 function __construct() {
        $this->articleObj = new Article();
    }

 public function index(){
    	$data['posts'] = $this->articleObj->fetch_all();
    	$data['categories'] = DB::table('article_categories')->where('published',1)->get();
    	$data['tags'] = DB::table('tags')->get();

    	return $this->loadpage('site.pages.blog_listing','EducationHubNepal Blogs | Education News & Updates', $data);
    }
 public function show($slug){
 		$data['post'] = $this->articleObj->fetch($slug);
        if(empty($data['post'])):
            return redirect('blog');
        endif;
 		$title = $data['post']->title;
 		$data['categories'] = DB::table('article_categories')->where('published',1)->get();
    	$data['tags'] = DB::table('tags')->get();

    	return $this->loadpage('site.pages.blog_detail',$title,$data);
 }

 public function category($slug){
 	$cat = new ArticleCategory();
 	$data['category'] = $cat->fetch($slug);
    if(empty($data['category'])):
        return redirect('blog');
    endif;
 	$title = $data['category']->title;
 	$data['categories'] = DB::table('article_categories')->where('published',1)->get();
    $data['tags'] = DB::table('tags')->get();
 	return $this->loadpage('site.pages.blog_category',$title,$data);
 }
 public function tag($id){
 	$tags = new ArticleTag();
 	$data['tag'] = $tags->fetch($id);
    if(empty($data['tag'])):
        return redirect('blog');
    endif;
 	$title = $data['tag']->name;
 	$data['categories'] = DB::table('article_categories')->where('published',1)->get();
    $data['tags'] = DB::table('tags')->get();
    return $this->loadpage('site.pages.blog_tag',$title,$data);
 }

}