<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Site\Category;
use App\Models\Site\Level;

class CategoryController extends Controller {

    private $categoryObj;

    function __construct() {
        $this->categoryObj = new Category();
        $this->levelObj = new Level();
    }

    /* this function loads the signle detail page */

    public function single_detail($url) {
        $data['category'] = $this->categoryObj->fetch($url);
        if(empty($data['category'])):
            return redirect('categories');
        endif;
        $data['plus_two'] = [];
        $data['bachelor'] = array();
        $data['post_graduation'] = [];
        $data['master'] = [];
        $data['mphil'] = [];
        $data['phd'] = [];
        $title = $data['category']->category.' Faculty Study Programs Available In Nepal';
        foreach ($data['category']->programs as $program) {
            $level = $program->level_id;
            switch ($level) {
                case 1:
                    array_push($data['plus_two'], $program);
                    break;
                case 2:
                    array_push($data['bachelor'], $program);
                    break;
                case 3:
                    array_push($data['post_graduation'], $program);
                    break;
                case 4:
                    array_push($data['master'], $program);
                    break;
                case 5:
                    array_push($data['mphil'], $program);
                    break;
                case 6:
                    array_push($data['phd'], $program);
                    break;
            }
        }

        return $this->loadpage('site.pages.category_detail', $title, $data);


        //return view('site.pages.college_detail',compact('detail'));
    }

    function index() {
        $data['categories'] = $this->categoryObj->fetch_all();
        return $this->loadpage('site.pages.category_listing', 'All Courses Category Available In Nepal', $data);
    }

    public function courses() {
        $data['categories'] = $this->categoryObj->getCategorizedPrograms();
        $data['levels'] = $this->levelObj->fetch_all();
//        echo '<pre>';
//        print_r($data['categories'] );
//        echo '</pre>';die;
        return $this->loadpage('site.pages.courses_listing', 'All Courses Available In Nepal', $data);
    }

}
