<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Site\College;
use App\Models\Site\Category;
use App\Models\Site\Program;
use App\Models\Site\Level;
use App\Models\Site\Article;
use DB;
use Mail;

class HomeController extends Controller {

    private $facultyObj;

    function __construct() {
        $this->collegeObj = new College();
        $this->categoryObj = new Category();
        $this->programObj = new Program();
        $this->levelObj = new Level();
    }

    /* this function loads the homepage view */

    public function index() {
        $data['categories'] = $this->categoryObj->fetch_home();
        $data['top_courses'] = $this->programObj->homepage()->get();
        $featured_college = $this->collegeObj->published()->featured()->take(10)->get();
        $mediaRefObj = new \App\Models\Admin\CollegeMediaRef();
        foreach ($featured_college as &$college) {
            $college->media = $mediaRefObj->getAllMedia($college->id);
        }
        $data['featured_colleges']=$featured_college;
        $data['blogs'] = Article::published()->latest('created_at')->paginate(3);
        //$data['recent_college'] = $this->collegeObj->latest('created_at')->take(2);
        $data['levels'] = $this->levelObj->fetch_all();
        /* echo '<pre>';
          print_r($data['categories']);
          die();
         */


        return $this->loadpage('site.pages.home', 'EducationHub Nepal | Search & Find Your Next Education', $data);
    }

    function login() {
        return $this->loadpage('site.pages.login');
    }

    public function signup() {
        return $this->loadpage('site.pages.signup');
    }

    function faq() {
        return $this->loadpage('site.pages.faq', 'FAQ About Education Hub Nepal');
    }

    function about() {
        return $this->loadpage('site.pages.about', 'About EducationHubNepal');
    }

    function privacy() {
        return $this->loadpage('site.pages.privacy_policy', 'Privacy Policy Education Hub Nepal');
    }

    function terms() {
        return $this->loadpage('site.pages.terms', 'Terms & Condition | Education Hub Nepal');
    }

    function help() {
        return $this->loadpage('site.pages.help', 'Help Documentation | Education Hub Nepal');
    }

    function contact() {
        $data['posts'] = Article::latest('created_at')->where('active', 1)->take(5)->get();
        return $this->loadpage('site.pages.contact', 'Contact Us | Education Hub Nepal', $data);
    }

    function submit(Request $request) {
        $data['name'] = $request->input('contact-name');
        $data['email'] = $request->input('contact-email');
        $data['subject'] = $request->input('options');
        $data['text'] = $request->input('contact-message');
        $message = 'Message From ' . $data['name'] . ' ' . $data['email'] . ' /n' . ' ' . $data['text'];

        //  $commonModelObj = new \App\Models\CommonModel();
        //  $commonModelObj->sendmail($mailto, $subject, $message)
        $modelObj = new \App\Models\CommonModel();
        $emails = ['vaghawan781@gmail.com', 'gaurab.d@gmail.com', 'sumankc55@gmail.com', 'ehubnepal@gmail.com'];
        foreach ($emails as $email):
            $modelObj->sendmail($email, $data['subject'], $message);
        endforeach;
        $this->set_message('Your Message sent successfully! We will get back to you within 24 hours. Thank You!')->success();
        return redirect()->back();
    }

}
