<?php

namespace App\Http\Controllers\Site;
//namespace App\Http\Requests;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Site\Program;




class ProgramController extends Controller {

	 private $programObj;

    function __construct() {
        $this->programObj = new Program();
    }

	/* this function loads the signle detail page */
	public function single_detail($url){
		$data['program'] = $this->programObj->fetch($url);
        if(empty($data['program'])):
            return redirect('programs');
        endif;
		$title = $data['program']->pname;
		return $this->loadpage('site.pages.program_detail',$title , $data);

		//return view('site.pages.college_detail',compact('detail'));
	}
    // We need this function only once 
	public function updates(){
        $programs = Program::all();
        foreach($programs as &$program):
            //$program->url = str_slug($program->title,'-');
                $affiliation = $this->programObj->fetchAffiliation($program->affiliation_id);
                $university_name = $this->getUniversityAbbr($affiliation->aname);
                $program->url = str_slug($program->pname.'-'.$university_name,'-');
                
                $program->update();
            
        endforeach;
    }
    public function getUniversityAbbr($title){
    if(str_word_count($title)>3):
        return stringFirst($title);
    else:
    return $title;
    endif;
  

}
}
