<?php

namespace App\Http\Controllers\Superuser;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Superuser\College;
use Illuminate\Support\Facades\Input;
use Image;
use DB;
use Auth;

class Colleges extends Controller {

     private $thumbwidth = 150;
    private $thumbheight = 150;
    private $collegeObj;

    function __construct() {
        $this->collegeObj = new College();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = $this->getMenuData();
        $data['college'] = $this->collegeObj->fetch($id);
        return $this->loadpage('superuser.pages.college', $data['college']->cname, $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id) {
        if (!$this->hasPrevilege($id)):
            $this->set_message('Record doesnt exist')->error();
            return redirect()->back();
        endif;
        if ($request->get('_user') != null):
            if ($request->get('_id') != null && intval($request->get('_id')) > 0):
                $notification = \App\Models\Admin\MNotification::find($request->get('_id'));
                $notification->visited = 1;
                $notification->save();
            endif;
        endif;
        $data = $this->getMenuData();
        $affiliationObj = new \App\Models\Admin\Affiliation();
        //$data['affiliations'] = $affiliationObj->fetchAll(false);
        $data['college'] = $this->collegeObj->fetch($id);
        return $this->loadpage('superuser.forms.form_college', 'Edit college', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $collegeData = $request->all();
        $college = College::find($collegeData['id']);
        if (!isset($college->id) || !$this->hasPrevilege($college->id)):
            $this->set_message('Record doesnt exist')->error();
            return redirect()->back();
        endif;
        $college->address = $collegeData['address'];
        $college->longitude = $collegeData['longitude'];
        $college->latitude = $collegeData['latitude'];
        $college->district = $collegeData['district'];
        $college->affiliation_ids = json_encode(isset($collegeData['affiliation_ids']) ? $collegeData['affiliation_ids'] : array());
        $college->phone = $collegeData['phone'];
        $college->fax = $collegeData['fax'];
        $college->website = $collegeData['website'];
        $college->pobox = $collegeData['pobox'];
        $college->email = $collegeData['email'];
        $college->description = $collegeData['description'];
        $college->facilities = $collegeData['facilities'];
        $college->country = $collegeData['country'];
        if ($college->save()):
            $this->set_message('College detail updated successfully!')->success();
            if ($this->getMinuteDifference(date('Y-m-d H:i:s'), $college->updated_at) > 5):
                \App\Models\Admin\MNotification::create(array(
                    "message" => "Detail of college [ " . $collegeData['cname'] . " ] updated",
                    "reference_id" => $collegeData['id'],
                    "type" => "college",
                    "user_id" => Auth::user()->id,
                    "visited" => 0
                ));
            endif;
            /* upload images */
            $image = Input::file('collegeimage');
            if ($image):
                /* upload image */
                $this->uploadImage($college, $image);
            endif;
        else:
            $this->set_message('College detail could not be updated!')->error();
        endif;

        return redirect('superuser/college/' . $collegeData['id'] . '/edit');
    }

    function getMenuData() {
        $maintainerProgramObj = new \App\Models\Superuser\MaintainerProgramRef();
        $maintainerCollegeObj = new \App\Models\Superuser\MaintainerCollegeRef();
        $maintainerCollegeProgramObj = new \App\Models\Superuser\MaintainerCollegeProgramRef();
        $userid = Auth::user()->id;
        $data['colleges'] = $maintainerCollegeObj->fetchCollegeByUserId($userid);
        $data['programs'] = $maintainerProgramObj->fetchProgramByUserId($userid);
        foreach ($data['colleges'] as $college):
            $data['collegeprograms'][] = $maintainerCollegeProgramObj->getCollegePrograms($userid, $college->id);
        endforeach;
        return $data;
    }

    function getMinuteDifference($time1, $time2) {
        $to_time = strtotime($time1);
        $from_time = strtotime($time2);
        return intval(abs($to_time - $from_time) / 60, 2);
    }

    function hasPrevilege($collegeId) {
        if (Auth::user()->role == 'admin'):
            return true;
        else:
            $data = DB::table("maintainer_college")
                    ->where(['college_id' => $collegeId, 'user_id' => Auth::user()->id])
                    ->select('id')
                    ->get();
            return !empty($data);
        endif;
    }

    function uploadImage(&$college, $image) {
        if(!$this->isWithinUploadLimit($college->id)){
            $this->set_message("You have reached the max limit of 5 uploads")->warning();
            return;
        }
        $filename = 'image_' . $college->id . '_' . time() . '.' . $image->getClientOriginalExtension();
        $path = base_path('images/collegeimage/maintainer/' . $filename);
        $thumbpath = base_path('images/collegeimage/maintainer/thumbs/' . $filename);
        Image::make($image->getRealPath())->save($path);
        /* resize image */
        Image::make($image->getRealPath())
                ->resize($this->thumbwidth, $this->thumbheight, function($constraint) {
                    $constraint->aspectRatio();
                })->save($thumbpath);

        $media = \App\Models\Admin\Media::create(array(
                    "full_url" => 'images/collegeimage/maintainer/' . $filename,
                    "thumb_url" => 'images/collegeimage/maintainer/thumbs/' . $filename,
                    "type" => 'image'
        ));
        if ($media->id > 0):
            \App\Models\Superuser\MaintainerCollegeMediaRef::create(array(
                'college_id' => $college->id,
                "media_id" => $media->id,
                "is_banner" => Input::get('is_banner') == null ? 0 : 1,
                "published" => 1,
                "user_id"=>Auth::user()->id,
                "ordering" => '50'
            ));
        endif;
    }
    
    function removeImage($mediaId) {
        $mediaRefObj = new \App\Models\Superuser\MaintainerCollegeMediaRef();
        $media = $mediaRefObj->getMedia($mediaId);
        if (file_exists(base_path($media->thumb_url))):
            @unlink(base_path($media->thumb_url));
            @unlink(base_path($media->full_url));
        endif;
        \App\Models\Admin\Media::destroy($media->id);
        \App\Models\Superuser\MaintainerCollegeMediaRef::destroy($media->refid);
        $this->set_message("Image removed successfully")->info();
        return redirect()->back();
    }
    
    function isWithinUploadLimit($collegeId){
        $record = DB::table("maintainercollege_media")
                    ->where(['college_id' => $collegeId, 'user_id' => Auth::user()->id])
                    ->select('id')
                    ->get();
        return count($record)<5;
    }

}
