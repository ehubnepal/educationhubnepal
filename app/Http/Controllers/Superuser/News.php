<?php

namespace App\Http\Controllers\Superuser;

use App\Http\Controllers\Controller;
use Auth;
use App\Models\Superuser\MaintainerProgramRef;
use App\Models\Superuser\MaintainerCollegeRef;
use App\Models\Superuser\MaintainerCollegeProgramRef;
use Illuminate\Http\Request;
class News extends Controller {

    private $newsObj;
    private $maintainerCollegeObj;

    function __construct() {
        $this->newsObj = new \App\Models\Superuser\News;
        $this->maintainerCollegeObj = new MaintainerCollegeRef();
    }

    public function index() {
        return $this->dashboard();
    }

    public function dashboard() {
        
//        $data['colleges'] = $this->maintainerCollegeObj->fetchCollegeByUserId($userid);
//        $data['programs'] = $this->maintainerProgramObj->fetchProgramByUserId($userid);
//        foreach ($data['colleges'] as $college):
//            $data['collegeprograms'][] = $this->maintainerCollegeProgramObj->getCollegePrograms($userid, $college->id);
//        endforeach;
//        return $this->loadpage('superuser.pages.dashboard', 'Your dashboard', $data);
        $data = $this->getMenuData();
        $data['news'] = $this->newsObj->fetchAll();
        return $this->loadpage('superuser.pages.news_dashboard', 'News dashboard',$data);
    }
    
    function create(){
        $userid = Auth::user()->id;
        $data = $this->getMenuData();
       // $data['colleges'] = $this->maintainerCollegeObj->fetchCollegeByUserId($userid);
        return $this->loadpage('superuser.forms.form_news', 'Create news',$data);
    }
    
    function edit($id){
        $userid = Auth::user()->id;
        $data = $this->getMenuData();
       // $data['colleges'] = $this->maintainerCollegeObj->fetchCollegeByUserId($userid);
        $news = \App\Models\Superuser\News::find($id);
        $data['news'] =$news;
        if($news->user_id != $userid){
            $this->set_message('Record doest exist')->error();
            return redirect()->back();
        }
        return $this->loadpage('superuser.forms.form_news', 'Edit news',$data);
    }
    
    function store(Request $request){
        $userid = Auth::user()->id;
        $data=[];
        $data['college_id'] = $request->get('college_id');
        $data['title'] = $request->get('title');
        $data['news'] = $request->get('news');
        $data['user_id'] = $userid;
        $data['published'] = $request->get('published')==null?0:1;
        $news = \App\Models\Superuser\News::create($data);
        if($news->id>0){
            $this->set_message('News created successfully')->success();
        }else{
            $this->set_message('News creation failed')->error();
        }
        return redirect('superuser/news');
    }
    
    function update(Request $request){
        $userid = Auth::user()->id;
        $r = $request->all();
        $data=[];
        $data['title'] = $r['title'];
        //unset($data['_token'])
        $data['college_id'] = $r['college_id'];
        $data['news'] = $r['news'];
        $data['user_id'] = $userid;
        $data['published'] = $request->get('published')==null?0:1;
        $news = \App\Models\Superuser\News::find($request->get('id'));
        if($news->user_id != $userid){
             $this->set_message('Access denied.')->error();
            return redirect()->back();
        }
        \App\Models\Superuser\News::where('id',$request->get('id'))->update($data);
        return redirect('superuser/news');
    }
    
    public function unpublish($id) {
        $news = \App\Models\Superuser\News::find($id);
        $news->published = 0;
        $news->save();
        $this->set_message('News unpublished successfully')->success();
        return redirect()->back();
    }

    public function publish($id) {
        $news = \App\Models\Superuser\News::find($id);
        $news->published = 1;
        $news->save();
        $this->set_message('News published successfully')->success();
        return redirect()->back();
    }
    
    public function destroy($id) {
        \App\Models\Superuser\News::destroy($id);
        $this->set_message('News deleted successfully')->success();
        return redirect()->back();
    }

    function getMenuData() {
        $maintainerProgramObj = new \App\Models\Superuser\MaintainerProgramRef();
        $maintainerCollegeObj = new \App\Models\Superuser\MaintainerCollegeRef();
        $maintainerCollegeProgramObj = new \App\Models\Superuser\MaintainerCollegeProgramRef();
        $userid = Auth::user()->id;
        $data['colleges'] = $maintainerCollegeObj->fetchCollegeByUserId($userid);
        $data['programs'] = $maintainerProgramObj->fetchProgramByUserId($userid);
        foreach ($data['colleges'] as $college):
            $data['collegeprograms'][] = $maintainerCollegeProgramObj->getCollegePrograms($userid, $college->id);
        endforeach;
        return $data;
    }
}
