<?php

namespace App\Http\Controllers\Superuser;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Superuser\CollegeProgramRef;
use Illuminate\Support\Facades\Input;
use Image;
use Auth;
use DB;

class CollegeProgram extends Controller {

    private $collegeProgramObj;

    function __construct() {
        $this->collegeProgramObj = new CollegeProgramRef();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $collegeId, $programId) {
        if (!$this->hasPrevilege($collegeId, $programId)):
            $this->set_message('Record doesnt exist')->error();
            return redirect()->back();
        endif;
        $data = $this->getMenuData();
        if ($request->get('_user') != null):
            if ($request->get('_id') != null && intval($request->get('_id')) > 0):
                $notification = \App\Models\Admin\MNotification::find($request->get('_id'));
                $notification->visited = 1;
                $notification->save();
            endif;
        endif;

        $data['collegeId'] = $collegeId;
        $data['collegeprogram'] = $this->collegeProgramObj->fetch($collegeId, $programId);
        return $this->loadpage('superuser.forms.form_college_program', 'Edit program', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $collegeprogramData = $request->all();
        $collegeprogram = CollegeProgramRef::find($collegeprogramData['id']);
        if (!isset($collegeprogram->no_of_seat) || !$this->hasPrevilege($collegeprogram->college_id, $collegeprogram->program_id)):
            $this->set_message('Record doesnt exist')->error();
            return redirect()->back();
        endif;
        $collegeprogram->no_of_seat = $collegeprogramData['no_of_seat'];
        $collegeprogram->fee_structure = $collegeprogramData['fee_structure'];
        $collegeprogram->total_fee = $collegeprogramData['total_fee'];
        $collegeprogram->eligibility_criteria = $collegeprogramData['eligibility_criteria'];
        $collegeprogram->scholorship_criteria = $collegeprogramData['scholorship_criteria'];
        $collegeprogram->additional_info = $collegeprogramData['additional_info'];
        $collegeprogram->program_since = $collegeprogramData['program_since'];
        $collegeprogram->weight = $collegeprogramData['weight'];
        if ($collegeprogram->save()):
            $this->set_message('Program detail updated successfully!')->success();
            if ($this->getMinuteDifference(date('Y-m-d H:i:s'), $collegeprogram->updated_at) > 5):
                \App\Models\Admin\MNotification::create(array(
                    "message" => "Detail of Program [ " . $collegeprogramData['pname'] . " ] updated",
                    "reference_id" => $collegeprogram->college_id,
                    "reference_id2" => $collegeprogram->program_id,
                    "type" => "collegeprogram",
                    "user_id" => Auth::user()->id,
                    "visited" => 0
                ));
            endif;
        else:
            $this->set_message('Program detail could not be updated!')->error();
        endif;
        return redirect()->back();
    }

    function getMenuData() {
        $maintainerProgramObj = new \App\Models\Superuser\MaintainerProgramRef();
        $maintainerCollegeObj = new \App\Models\Superuser\MaintainerCollegeRef();
        $maintainerCollegeProgramObj = new \App\Models\Superuser\MaintainerCollegeProgramRef();
        $userid = Auth::user()->id;
        $data['colleges'] = $maintainerCollegeObj->fetchCollegeByUserId($userid);
        $data['programs'] = $maintainerProgramObj->fetchProgramByUserId($userid);
        foreach ($data['colleges'] as $college):
            $data['collegeprograms'][] = $maintainerCollegeProgramObj->getCollegePrograms($userid, $college->id);
        endforeach;
        return $data;
    }

    function getMinuteDifference($time1, $time2) {
        $to_time = strtotime($time1);
        $from_time = strtotime($time2);
        return intval(abs($to_time - $from_time) / 60, 2);
    }

    function hasPrevilege($collegeId, $programId) {
        if (Auth::user()->role == 'admin'):
            return true;
        else:
            $data = DB::table("maintainer_college_program")
                    ->where(['college_id' => $collegeId, 'program_id' => $programId, 'user_id' => Auth::user()->id])
                    ->select('id')
                    ->get();
            return !empty($data);
        endif;
    }

}
