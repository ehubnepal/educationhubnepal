<?php

namespace App\Http\Controllers\Superuser;

use App\Http\Controllers\Controller;
use Auth;
use App\Models\Superuser\MaintainerProgramRef;
use App\Models\Superuser\MaintainerCollegeRef;
use App\Models\Superuser\MaintainerCollegeProgramRef;

class Home extends Controller {

    private $maintainerProgramObj;
    private $maintainerCollegeObj;
    private $maintainerCollegeProgramObj;

    function __construct() {
        $this->maintainerProgramObj = new MaintainerProgramRef();
        $this->maintainerCollegeObj = new MaintainerCollegeRef();
        $this->maintainerCollegeProgramObj = new MaintainerCollegeProgramRef();
    }

    public function index() {
        return $this->dashboard();
    }

    public function dashboard() {
        $userid = Auth::user()->id;
        $data['colleges'] = $this->maintainerCollegeObj->fetchCollegeByUserId($userid);
        $data['programs'] = $this->maintainerProgramObj->fetchProgramByUserId($userid);
        foreach ($data['colleges'] as $college):
            $data['collegeprograms'][] = $this->maintainerCollegeProgramObj->getCollegePrograms($userid, $college->id);
        endforeach;
        return $this->loadpage('superuser.pages.dashboard', 'Your dashboard', $data);
    }

}
