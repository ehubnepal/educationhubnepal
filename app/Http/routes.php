<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::get('/', 'Site\HomeController@index');
Route::get('colleges', 'Site\CollegeController@index');

Route::get('login/google', 'AccountController@google_redirect');
Route::get('account/google', 'AccountController@google');

Route::get('login/facebook', 'AccountController@facebook_redirect');
Route::get('account/facebook', 'AccountController@facebook');


Route::get('/colleges/{url}', 'Site\CollegeController@single_detail');
Route::get('/programs/{url}', 'Site\ProgramController@single_detail');
Route::get('/universities/{url}', 'Site\AffiliationController@single_detail');

Route::get('universities', 'Site\AffiliationController@index');
//Route::get('college-detail','Site\CollegeController@detail');


Route::get('colleges', 'Site\CollegeController@index');
Route::get('programs', 'Site\CategoryController@courses');
Route::get('programs/{url}', 'Site\ProgramController@single_detail');
Route::get('categories/{url}', 'Site\CategoryController@single_detail');
Route::get('categories', 'Site\CategoryController@index');
Route::get('levels/search', 'Site\LevelController@find');
Route::get('levels/{url}', 'Site\LevelController@single_detail');
Route::get('levels', 'Site\LevelController@index');
Route::get('blog', 'Site\ArticleController@index');
Route::get('blog/{slug}', 'Site\ArticleController@show');
Route::get('blog/categories/{slug}', 'Site\ArticleController@category');
Route::get('blog/tags/{id}', 'Site\ArticleController@tag');
// extra pages by @vaghawan
Route::get('faq', 'Site\HomeController@faq');
Route::get('privacy-policy', 'Site\HomeController@privacy');
Route::get('terms-and-condition', 'Site\HomeController@terms');
Route::get('contact','Site\HomeController@contact');
Route::post('contact/submit','Site\HomeController@submit');
Route::get('help','Site\HomeController@help');
/*Suman*/
Route::get('about', 'Site\HomeController@about');

/* gaurab */
Route::get('search', 'Site\SearchController@find');
Route::get('searchcollege', 'Site\SearchController@searchCollege');
Route::get('search/autosuggestprogram', 'Site\SearchController@autosuggestprogram');
Route::get('search/autosuggestcollege', 'Site\SearchController@autosuggestcollege');
Route::get('/phpinfo', 'Site\HomeController@phpinfo_');
Route::get("login", "Site\HomeController@login");
Route::post("login", "Auth\AuthController@verifyMemberLogin");
Route::get("signup", "Site\HomeController@signup");




/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | This route group applies the "web" middleware group to every route
  | it contains. The "web" middleware group is defined in your HTTP
  | kernel and includes session state, CSRF protection, and more.
  |
 */


/* * **************************************************************
 *                        ROUTES FOR ADMIN                      *
 * ************************************************************** */
Route::get("admin/login", "Auth\AuthController@login");
Route::post("admin/login", "Auth\AuthController@verifyLogin");

Route::group(['prefix' => 'admin', 'middleware' => ['web', 'admin']], function () {

    Route::get('home', 'Admin\Home@index');
    Route::get("logout", "Auth\AuthController@logout");
    
    Route::get("collegereviews","Admin\Reviews@collegeReviews");
    Route::get("collegereviews/publish/{id}","Admin\Reviews@publishCollegeReviews");
    Route::get("collegereviews/unpublish/{id}","Admin\Reviews@unpublishCollegeReviews");
    Route::get("programreviews","Admin\Reviews@programReviews");
    Route::get("programreviews/publish/{id}","Admin\Reviews@publishProgramReviews");
    Route::get("programreviews/unpublish/{id}","Admin\Reviews@unpublishProgramReviews");
    Route::get("searchqueries","Admin\SearchQueries@index");
    Route::get("searchqueries/delete","Admin\SearchQueries@delete");
    /* college management */
    Route::get('college', 'Admin\Colleges@index');
    Route::get('college/create', 'Admin\Colleges@create');
    Route::get('college/{id}/edit', 'Admin\Colleges@edit');
    Route::get('college/publish/{id}', 'Admin\Colleges@publish');
    Route::get('college/unpublish/{id}', 'Admin\Colleges@unpublish');
    Route::get('college/search', 'Admin\Colleges@search');
    Route::post('college', 'Admin\Colleges@store');
    Route::post('college/update', 'Admin\Colleges@update');
    Route::get('college/{id}/removelogo', 'Admin\Colleges@removeLogo');
    Route::get('college/{id}/removeimage', 'Admin\Colleges@removeImage');
    Route::get('college/{id}/delete', 'Admin\Colleges@destroy');
    Route::get('college/{id}', 'Admin\Colleges@show');
    Route::get('college/setimagebanner/{mediaRefId}', 'Admin\Colleges@setimagebanner');
    Route::get('college/unsetimagebanner/{mediaRefId}', 'Admin\Colleges@unsetimagebanner');
    Route::get('college/setimagepublish/{mediaRefId}', 'Admin\Colleges@setimagepublish');
    Route::get('college/setimageunpublish/{mediaRefId}', 'Admin\Colleges@setimageunpublish');
    Route::get('college/setimageordering/{mediaRefId}/{value}', 'Admin\Colleges@setimageordering');
    /* affiliation management */
    Route::get('affiliation', 'Admin\Affiliations@index');
    Route::get('affiliation/create', 'Admin\Affiliations@create');
    Route::get('affiliation/{id}/edit', 'Admin\Affiliations@edit');
    Route::get('affiliation/publish/{id}', 'Admin\Affiliations@publish');
    Route::get('affiliation/unpublish/{id}', 'Admin\Affiliations@unpublish');
    Route::post('affiliation', 'Admin\Affiliations@store');
    Route::post('affiliation/update', 'Admin\Affiliations@update');
    Route::get('affiliation/{id}/removelogo', 'Admin\Affiliations@removeLogo');
    Route::get('affiliation/{id}/removeimage', 'Admin\Affiliations@removeImage');
    Route::get('affiliation/{id}/delete', 'Admin\Affiliations@destroy');
    Route::get('affiliation/{id}', 'Admin\Affiliations@show');
    Route::get('affiliation/setimagebanner/{mediaRefId}', 'Admin\Affiliations@setimagebanner');
    Route::get('affiliation/unsetimagebanner/{mediaRefId}', 'Admin\Affiliations@unsetimagebanner');
    Route::get('affiliation/setimagepublish/{mediaRefId}', 'Admin\Affiliations@setimagepublish');
    Route::get('affiliation/setimageunpublish/{mediaRefId}', 'Admin\Affiliations@setimageunpublish');
    Route::get('affiliation/setimageordering/{mediaRefId}/{value}', 'Admin\Affiliations@setimageordering');
    /* program management */
    Route::get('program', 'Admin\Programs@index');
    Route::get('program/create', 'Admin\Programs@create');
    Route::get('program/search', 'Admin\Programs@search');
    Route::get('program/{id}', 'Admin\Programs@show');
    Route::get('program/{id}/edit', 'Admin\Programs@edit');
    Route::get('program/publish/{id}', 'Admin\Programs@publish');
    Route::get('program/unpublish/{id}', 'Admin\Programs@unpublish');
    Route::post('program', 'Admin\Programs@store');
    Route::post('program/update', 'Admin\Programs@update');
    Route::get('program/{id}/removelogo', 'Admin\Programs@removeLogo');
    Route::get('program/{id}/delete', 'Admin\Programs@destroy');
    Route::get('program/{affiliation_id}/getFaculties', 'Admin\Programs@getFacultyDropdown');
    /* college program management */
    Route::get('collegeprograms/{id}', 'Admin\College_programs@index');
    Route::get('collegeprograms/{college_id}/{program_id}/edit', 'Admin\College_programs@edit');
    Route::post('collegeprograms/update', 'Admin\College_programs@update');
    Route::post('collegeprograms', 'Admin\College_programs@store');
    Route::get('collegeprograms/publish/{id}', 'Admin\College_programs@publish');
    Route::get('collegeprograms/unpublish/{id}', 'Admin\College_programs@unpublish');
    Route::get('collegeprograms/{id}/delete', 'Admin\College_programs@destroy');
    /*  program category management */
    Route::get('programcategory', 'Admin\Program_category@index');
    Route::get('programcategory/create', 'Admin\Program_category@create');
    Route::get('programcategory/{id}/edit', 'Admin\Program_category@edit');
    Route::get('programcategory/publish/{id}', 'Admin\Program_category@publish');
    Route::get('programcategory/unpublish/{id}', 'Admin\Program_category@unpublish');
    Route::post('programcategory', 'Admin\Program_category@store');
    Route::post('programcategory/update', 'Admin\Program_category@update');
    /*  faculties management */
    Route::get('faculty', 'Admin\Faculties@index');
    Route::get('faculty/create', 'Admin\Faculties@create');
    Route::get('faculty/{id}/edit', 'Admin\Faculties@edit');
    Route::get('faculty/publish/{id}', 'Admin\Faculties@publish');
    Route::get('faculty/unpublish/{id}', 'Admin\Faculties@unpublish');
    Route::post('faculty', 'Admin\Faculties@store');
    Route::post('faculty/update', 'Admin\Faculties@update');
    Route::get('faculty/search', 'Admin\Faculties@search');
    Route::get('faculty/{id}/delete', 'Admin\Faculties@destroy');
    /* User management */
    Route::get('user', 'Admin\Users@index');
    Route::get('user/{userId}', 'Admin\Users@show');
    Route::get('user/{userId}/edit', 'Admin\Users@edit');
    Route::get('user/{userId}/alertdelete', 'Admin\Users@alertDelete');
    Route::get('user/{userId}/delete', 'Admin\Users@delete');
    Route::get('user/{userId}/setmaintainer', 'Admin\Users@setMaintainer');
    Route::get('user/{userId}/unsetmaintainer', 'Admin\Users@unsetMaintainer');
    Route::get('searchcollege', 'Admin\SearchController@searchcollege');
    Route::get('searchprogram', 'Admin\SearchController@searchprogram');
    Route::get('selectcollege', 'Admin\SearchController@selectcollege');
    Route::get('selectprogram', 'Admin\SearchController@selectprogram');
    Route::get('unselectcollege', 'Admin\SearchController@unselectcollege');
    Route::get('unselectprogram', 'Admin\SearchController@unselectprogram');
    Route::get('selectcollegeprogram', 'Admin\SearchController@selectCollegeProgram');
    Route::get('unselectcollegeprogram', 'Admin\SearchController@unselectCollegeProgram');
    /* Blog managment added by vaghawan */
    Route::get('blog/posts', 'Admin\ArticleController@index');
    Route::get('blog/posts/create', 'Admin\ArticleController@create');
    Route::post('blog/posts', 'Admin\ArticleController@store');
    Route::get('blog/posts/edit/{id}', 'Admin\ArticleController@edit');
    Route::patch('blog/posts/update/{id}', 'Admin\ArticleController@update');
    Route::get('blog/posts/delete/{id}', 'Admin\ArticleController@destroy');
    Route::get('blog/posts/publish/{id}', 'Admin\ArticleController@publish');
    Route::get('blog/posts/unpublish/{id}', 'Admin\ArticleController@unpublish');
    Route::get('blog/categories', 'Admin\ArticleCategoryController@index');
    Route::get('blog/categories/create', 'Admin\ArticleCategoryController@create');
    Route::post('blog/categories', 'Admin\ArticleCategoryController@store');
    Route::get('blog/categories/edit/{id}', 'Admin\ArticleCategoryController@edit');
    Route::patch('blog/categories/update/{id}', 'Admin\ArticleCategoryController@update');
    Route::get('blog/categories/publish/{id}', 'Admin\ArticleCategoryController@publish');
    Route::get('blog/categories/unpublish/{id}', 'Admin\ArticleCategoryController@unpublish');
    Route::get('blog/categories/delete/{id}', 'Admin\ArticleCategoryController@destroy');
    Route::get('blog/tags', 'Admin\ArticleTagController@index');
    Route::get('blog/tags/create', 'Admin\ArticleTagController@create');
    Route::post('blog/tags', 'Admin\ArticleTagController@store');
    Route::get('blog/tags/edit/{id}', 'Admin\ArticleTagController@edit');
    Route::patch('blog/tags/update/{id}', 'Admin\ArticleTagController@update');
    Route::get('blog/tags/delete/{id}', 'Admin\ArticleTagController@destroy');
    /* Maintainer Notification */
    Route::get('editnotification', 'Admin\MNotifications@index');
});

/* * **************************************************************
 *                     ROUTES FOR MAINTAINER                    *
 * ************************************************************** */
Route::get("superuser/login", "Auth\AuthController@maintainerLogin");
Route::post("superuser/login", "Auth\AuthController@verifyMaintainerLogin");

Route::group(['prefix' => 'superuser', 'middleware' => ['web', 'maintainer']], function () {
    Route::get("logout", "Auth\AuthController@maintainerLogout");
    Route::get('home', 'Superuser\Home@index');
    Route::get('college/{id}/edit', 'Superuser\Colleges@edit');
    Route::get('collegeprogram/{collegeId}/{programId}/edit', 'Superuser\CollegeProgram@edit');
    Route::post('collegeprograms/update', 'Superuser\CollegeProgram@update');
    Route::post('college/update', 'Superuser\Colleges@update');
    Route::get('college/{id}/removeimage', 'Superuser\Colleges@removeImage');
    
    Route::get('news', 'Superuser\News@index');
    Route::get('news/create', 'Superuser\News@create');
    Route::post('news', 'Superuser\News@store');
    Route::post('news/update', 'Superuser\News@update');
    Route::get('news/{id}/delete', 'Superuser\News@destroy');
    Route::get('news/{id}/edit', 'Superuser\News@edit');
    Route::get('news/publish/{id}', 'Superuser\News@publish');
    Route::get('news/unpublish/{id}', 'Superuser\News@unpublish');
});

/* * **************************************************************
 *                      ROUTES FOR MEMBER                         *
 * ************************************************************** */
Route::get("member/login", "Auth\AuthController@memberLogin");
Route::post("member/login", "Auth\AuthController@verifyMemberLogin");
Route::post("signup", "Member\Home@signup");
Route::get("member/activate/{email}/{salt}/{token}", "Member\Home@activate");
Route::get('wiki/college/{url}/pre_edit/detail','Wiki\CollegeController@preEditDetail');
Route::get('wiki/program/{url}/pre_edit/basicinfo','Wiki\ProgramController@preEditBasicinfo');

Route::group(['middleware' => ['web', 'member']], function () {
    Route::get("member/logout", "Auth\AuthController@memberLogout");
    Route::get("logout", "Auth\AuthController@memberLogout");
    Route::get('member/home', 'Member\Home@index');
    Route::post('member/profile', 'Member\Home@updateProfile');
    Route::get('member/reviews', 'Member\Reviews@getReviews');
    Route::get('member/myeducation', 'Member\Home@myEducation');
    Route::get('member/addnewcollege', 'Member\Home@addNewCollege');
    Route::get('member/currently_studying_college/{action}/{collegeId}', 'Member\Reviews@myCurrentCollege');
    Route::get('member/previously_studied_college/{action}/{collegeId}', 'Member\Reviews@myPastCollege');
    Route::get('member/currently_studying_program/{action}/{programId}', 'Member\Reviews@myCurrentProgram');
    Route::get('member/previously_studied_program/{action}/{programId}', 'Member\Reviews@myPastProgram');
    Route::post('review/reviewcollege', 'Site\ReviewController@storeCollegeReview');
    Route::post('review/reviewprogram', 'Site\ReviewController@storeProgramReview');
    Route::post('member/updateemail', 'Member\Home@updateEmail');
    Route::get('member/college/{id}/edit', 'Member\Colleges@edit');
    Route::post('member/college', 'Member\Colleges@store');
    Route::post('member/college/update', 'Member\Colleges@update');
    Route::get('member/google_redirect_mergeaccount', 'AccountController@google_redirect_mergeaccount');
    Route::get('member/facebook_redirect_mergeaccount', 'AccountController@facebook_redirect_mergeaccount');

    /* News */
    Route::get("member/news/create", "Member\News@create");
    Route::post("member/news/store", "Member\News@store");
    Route::get("member/news/{id}/edit", "Member\News@edit");
    Route::post("member/news/update", "Member\News@update");
    /* Wiki */
    Route::get('wiki/college/{url}/edit/detail','Wiki\CollegeController@editDetail');
    Route::get('wiki/college/{url}/edit/history','Wiki\CollegeController@listEditHistory');
    Route::post('wiki/college','Wiki\CollegeController@store');
    Route::post('wiki/college/update','Wiki\CollegeController@update');
    Route::get('wiki/college/{id}/editdetailagain','Wiki\CollegeController@editDetailAgain');
});
