<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if ($request->user()->role == 'maintainer') {
                 return redirect('superuser/home');
            }else if($request->user()->role == 'member'){
                return redirect('member/home');
            }else if($request->user()->role == 'admin'){
                return redirect('admin/home');
            }else{
                return redirect('/');
            }
           
        }

        return $next($request);
    }
}
