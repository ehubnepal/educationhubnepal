<?php

function stringFirst($string) {
    $expr = '/(?<=\s|^)[a-z]/i';
    preg_match_all($expr, $string, $matches);

    $result = implode('', $matches[0]);

    $result = strtoupper($result);
    return $result;
}

function clean_post_data($array) {
    foreach ($array as $key => &$cdata):
        $cdata = strip_tags($cdata, '<h1><h2><h3><div><p><span><ol><ul><li>');
    endforeach;
    return $array;
}

function get_banner($college) {
    $src = '';
    foreach ($college->media as $image):
        if ($image->is_banner == 1) {
            $src = $image->thumb_url;
        }
    endforeach;
    if ($src != '') {
        return '<img src="'.url($src).'"/>';
    }

    $colors = array('225378', 'eb7f00', '1695a3');
    $src = '<img src="http://dummyimage.com/600x400/' . $colors[array_rand($colors)] . '/ffffff&text=' . $college->cname.'" />';
    return $src;
}
?>