<?php

namespace App\Models\Admin;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class Level extends Model {

    protected $table = 'levels';
    protected $primarykey = 'id';
    public $timestamps = true;
    protected $fillable = [
        "lname",
        "url"
    ];

    function fetchAll($allcolumn = true) {
        if ($allcolumn):
            return Level::paginate(30);
        else:
            return DB::table($this->table)
                        ->select('id','lname','url')->get();
        endif;
    }

    function fetch($id) {
        $level = Level::find($id);
        return $level;
    }

    
}
