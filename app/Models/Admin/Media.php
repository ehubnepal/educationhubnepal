<?php

namespace App\Models\Admin;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class Media extends Model{
    protected $table = 'medias';
    protected $primarykey = 'id';
    public $timestamps = false;
    protected $fillable = [
        "full_url",
        "thumb_url"
    ];
}
