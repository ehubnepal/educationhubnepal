<?php

namespace App\MOdels\Admin;

use Illuminate\Database\Eloquent\Model;

class ArticleCategory extends Model
{
	protected $primaryKey = 'category_id'; 
	protected $table = 'article_categories';


    public function articles(){
    	return $this->belongsToMany('App\Models\Admin\Article','article_category','category_id','post_id');
    	// belongs to many or many to belongs. 
    	// http://stackoverflow.com/questions/21566705/laravel-attach-method-not-working-to-hasmany-side

    	// Have to see belongsToMany in the article controller and hasMany in the tag controller. 
    }
}
