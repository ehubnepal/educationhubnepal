<?php

namespace App\Models\Admin;

use DB;
use Illuminate\Database\Eloquent\Model;



/**
 * Description of Brand
 *
 * @author vaghawanojha
 */
class ProgramReview extends Model {

    protected $table = 'program_review';
    protected $primarykey = 'id';
     public $timestamps = true;
     protected $fillable = [
        "program_id",
        "user_id",
         "star",
         "review",
         "published"
    ];


		 /* this scope function fetch all the faculties from the faculties table which has been set to be published. 
		 */

    public function fetchAll(){
       $reviews = DB::table($this->table)
                        ->join('programs', 'programs.id', '=', 'program_review.program_id')
                        ->join('members', 'members.id', '=', 'program_review.user_id')
                        ->select('program_review.*','members.fullname','programs.pname','programs.url')
                        ->paginate(30);
        return $reviews;
    }
    
    
    
    function fetchAllByUserId($userId){
        $reviews = DB::table($this->table)
                        ->join('programs', 'programs.id', '=', 'program_review.college_id')
                        ->where('program_review.user_id', '=', $userId)
                        ->select('program_review.*','programs.pname','programs.url')
                        ->paginate(30);
        return $reviews;
    }
    
    function fetchAllByCollegeId($collegeId){
        $reviews = DB::table($this->table)
                        ->join('programs', 'programs.id', '=', 'program_review.college_id')
                        ->where('program_review.college_id', '=', $collegeId)
                        ->select('program_review.*','programs.pname','programs.url')
                        ->paginate(30);
        return $reviews;
    }
}