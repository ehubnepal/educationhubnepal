<?php

namespace App\Models\Admin;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\Affiliation;

/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class Faculty extends Model {

    protected $table = 'faculties';
    protected $primarykey = 'id';
    public $timestamps = false;
    protected $fillable = [
    	"affiliation_id",
        "faculty",
        "logo",
        "url",
        "published"
    ];

    function fetchAll(){
    	$faculties =  Faculty::paginate(50);
    	foreach ($faculties as &$faculty):
            $faculty->affiliation = Affiliation::find($faculty->affiliation_id);
        endforeach;
        return $faculties;
    }

function search($query){
        return DB::table($this->table)
                        ->where('faculty', 'like', '%'.$query.'%')
                        ->select('*')
                        ->paginate(30);
    }

    function getFaculties($affiliation_id){
        return DB::table($this->table)
                        ->where('affiliation_id', '=', $affiliation_id)
                        ->select('*')
                        ->get();
    }

}
