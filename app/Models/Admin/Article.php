<?php

namespace App\Models\Admin;

use DB;
use Illuminate\Database\Eloquent\Model;


/**
 * Article Model For Backend
 *
 * @author vaghawan
 */
class Article extends Model {

    protected $table = 'articles';
    protected $primaryKey = 'post_id';

    function fetch_all(){
    	$posts = Article::latest('created_at')->paginate(10);
    	return $posts;
    }

    public function tags(){
        return $this->belongsToMany('App\Models\Admin\ArticleTag','article_tag','post_id','tag_id');
    }
    public function categories(){
        return $this->belongsTOMany('App\Models\Admin\ArticleCategory','article_category','post_id','category_id');
    }

     public function getTagListAttribute(){
        return $this->tags->lists('post_id');
    }
    public function getCategoryListAttribute(){
        return $this->categories->lists('post_id');
    }

  


























}