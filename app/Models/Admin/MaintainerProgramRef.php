<?php

namespace App\Models\Admin;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class MaintainerProgramRef extends Model {
    protected $table = 'maintainer_program';
    protected $primarykey = 'id';
    public $timestamps = false;
    protected $fillable = [
        "user_id",
        "program_id",
        "startdate",
        "enddate"
    ];
    
    function fetchProgramByUserId($userId){
        return DB::table($this->table)
                        ->join('programs', 'programs.id', '=', 'maintainer_program.program_id')
                        ->join('affiliations', 'affiliations.id', '=', 'programs.affiliation_id')
                        ->where('maintainer_program.user_id', '=', $userId)
                        ->select('programs.*','affiliations.aname')
                        ->get();
    }
}
