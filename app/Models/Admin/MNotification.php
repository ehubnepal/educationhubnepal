<?php

namespace App\Models\Admin;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class MNotification extends Model{
    protected $table = 'maintainer_notification';
    protected $primarykey = 'id';
    public $timestamps = true;
    protected $fillable = [
        "message",
        "reference_id",
        "reference_id2",
        "type",
        "user_id",
        "created_at",
        "updated_at",
        "visited"
    ];
    
    function fetchAll($perpage){
        return DB::table($this->table)
                    ->join('members', 'members.id', '=', 'maintainer_notification.user_id')
                        ->select('maintainer_notification.*','members.fullname','members.social_image')
                        ->paginate($perpage);
    }
    
}
