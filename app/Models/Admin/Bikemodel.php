<?php

namespace App\Models\Admin;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\MediaBrandRef;
use App\Models\Admin\Ad;
use App\Models\Admin\Brand;

/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class Bikemodel extends Model {

    protected $table = 'bikemodels';
    protected $primarykey = 'id';
    public $timestamps = true;
    protected $fillable = [
        "model",
        "brand_id",
        "description",
        "published"
    ];

    function fetchAll() {
        $models = Bikemodel::select('id','model','brand_id','published')->where('trashed', 0)->get();
        foreach ($models as $model):
            $brand = new Brand();
            $model->media = $this->fetchMedia($model->id);
            $model->brand = $brand->getBrandname($model->brand_id);
            $model->adCount = $this->countAd($model->id);
        endforeach;
        return $models;
    }
    
    function fetchTrashed(){
        $models = Bikemodel::where('trashed', 1)->get();
        foreach ($models as $model):
            $brand = new Brand();
            $model->media = $this->fetchMedia($model->id);
            $model->brand = $brand->getBrandname($model->brand_id);
            $model->adCount = $this->countAd($model->id);
        endforeach;
        return $models;
    }
    
    function fetch($id){
        $model = Bikemodel::find($id);
        if(!empty($model)):
            $brand = new Brand();
            $model->media = $this->fetchMedia($model->id);
            $model->brand = $brand->getBrandname($model->brand_id);
            $model->adCount = $this->countAd($model->id);
        endif;
        return $model;
    }

    function fetchMedia($modelId) {
        $mediaRef = new MediaBikemodelRef();
        return $mediaRef->getMedia($modelId);
    }

    function countAd($modelId) {
        return Ad::where('model_id', $modelId)->get()->count();
    }

    
}
