<?php

namespace App\Models\Admin;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class Ad extends Model{
    protected $table = 'ads';
    protected $primarykey = 'id';
    public $timestamps = true;
    protected $fillable = [
    ];
    
    
}
