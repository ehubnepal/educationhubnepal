<?php

namespace App\Models\Admin;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;


/**
 * Description of Brand
 *
 * @author vaghawanojha
 */
class SearchQuery extends Model {

    protected $table = 'search_query';
    protected $primarykey = 'id';
    public $timestamps = true;
    protected $fillable = [
        "type",
        "query",
        "created_at"
    ];
    
    function fetchAll(){
        $sql = "SELECT query,type, count(query) as count FROM search_query group by query order by count desc";
        $rows = DB::select(DB::raw($sql));
        $collection = new Collection($rows);
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $currentPageSearchResults = $collection->slice(($currentPage - 1) * 50, 50)->all();
        $temp = new LengthAwarePaginator($currentPageSearchResults, count($collection), 50);
        $temp->setPath('searchqueries');
        return $temp;
        }
}