<?php

namespace App\Models\Admin;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class MediaBikemodelRef extends Model {

    protected $table = 'media_models';
    protected $primarykey = 'id';
    public $timestamps = false;
    protected $fillable = [
        "model_id",
        "media_id"
    ];

   function getMedia($modelId) {
        return DB::table('media_models')
                        ->join('medias', 'medias.id', '=', 'media_models.media_id')
                        ->where('media_models.model_id', '=', $modelId)
                        ->select('medias.*')
                        ->get();
    }
  


    function removeOldImage($modelId) {
        $medias = $this->getMedia($modelId);
        foreach ($medias as $media):
            @unlink(base_path($media->full_url));
            @unlink(base_path($media->thumb_url));
            \App\Models\Admin\Media::destroy($media->id);
            \App\Models\Admin\MediaBikemodelRef::where('model_id', $modelId)->delete();
        endforeach;
    }
}
