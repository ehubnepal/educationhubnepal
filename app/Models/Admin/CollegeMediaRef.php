<?php

namespace App\Models\Admin;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class CollegeMediaRef extends Model {

    protected $table = 'college_media';
    protected $primarykey = 'id';
    public $timestamps = false;
    protected $fillable = [
        "college_id",
        "media_id",
        "published",
        "is_banner",
        "ordering"
    ];
    
    function getAllMedia($collegeId){
        $medias =  DB::table($this->table)
                        ->join('medias', 'medias.id', '=', 'college_media.media_id')
                        ->where('college_media.college_id', '=', $collegeId)
                        ->select('medias.*','college_media.id as refid','college_media.published','college_media.is_banner','college_media.ordering')
                        ->get();
        return $medias;
    }
    
    function getMedia($mediaId){
        $medias =  DB::table($this->table)
                        ->join('medias', 'medias.id', '=', 'college_media.media_id')
                        ->where('college_media.media_id', '=', $mediaId)
                        ->select('medias.*','college_media.id as refid','college_media.published','college_media.is_banner','college_media.ordering')
                        ->first();
        return $medias;
    }
    
   
}
