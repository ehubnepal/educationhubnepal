<?php

namespace App\Models\Admin;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class AffiliationMediaRef extends Model {

    protected $table = 'affiliation_media';
    protected $primarykey = 'id';
    public $timestamps = false;
    protected $fillable = [
        "affiliation_id",
        "media_id",
        "published",
        "is_banner",
        "ordering"
    ];
    
    function getAllMedia($affiliationId){
        $medias =  DB::table($this->table)
                        ->join('medias', 'medias.id', '=', 'affiliation_media.media_id')
                        ->where('affiliation_media.affiliation_id', '=', $affiliationId)
                        ->select('medias.*','affiliation_media.id as refid','affiliation_media.published','affiliation_media.is_banner','affiliation_media.ordering')
                        ->get();
        return $medias;
    }
    
    function getMedia($mediaId){
        $medias =  DB::table($this->table)
                        ->join('medias', 'medias.id', '=', 'affiliation_media.media_id')
                        ->where('affiliation_media.media_id', '=', $mediaId)
                        ->select('medias.*','affiliation_media.id as refid','affiliation_media.published','affiliation_media.is_banner','affiliation_media.ordering')
                        ->first();
        return $medias;
    }
    
   
}
