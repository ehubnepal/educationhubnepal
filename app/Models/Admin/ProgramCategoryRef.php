<?php

namespace App\Models\Admin;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class ProgramCategoryRef extends Model {

    protected $table = 'programcategory_program';
    protected $primarykey = 'id';
    public $timestamps = false;
    protected $fillable = [
        "program_id",
        "category_id"
    ];

    function getCategory($programId) {
        return DB::table($this->table)
                        ->join('categories', 'categories.id', '=', 'programcategory_program.category_id')
                        ->where('programcategory_program.program_id', '=', $programId)
                        ->select('categories.*')
                        ->get();
    }
    function getCategoryIds($programId) {
        $rows = $this->getCategory($programId);
        $temp = array();
        foreach($rows as $row):
            $temp[] = $row->id;
        endforeach;
       return $temp;
    }

}
