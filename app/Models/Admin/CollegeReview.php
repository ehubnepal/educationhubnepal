<?php

namespace App\Models\Admin;

use DB;
use Illuminate\Database\Eloquent\Model;



/**
 * Description of Brand
 *
 * @author vaghawanojha
 */
class CollegeReview extends Model {

    protected $table = 'college_review';
    protected $primarykey = 'id';
     public $timestamps = true;
     protected $fillable = [
        "college_id",
        "user_id",
         "star",
         "review",
         "published"
    ];


		 /* this scope function fetch all the faculties from the faculties table which has been set to be published. 
		 */

    public function fetchAll(){
       $reviews = DB::table($this->table)
                        ->join('colleges', 'colleges.id', '=', 'college_review.college_id')
                        ->join('members', 'members.id', '=', 'college_review.user_id')
                     //   ->where('college_review.published', '=', 1)
                        ->select('college_review.*','members.fullname','colleges.cname','colleges.url','colleges.full_url','colleges.thumb_url')
                        ->paginate(30);
        return $reviews;
    }
    
    
    
    function fetchAllByUserId($userId){
        $reviews = DB::table($this->table)
                        ->join('colleges', 'colleges.id', '=', 'college_review.college_id')
                        ->where('college_review.user_id', '=', $userId)
                        ->select('college_review.*','colleges.cname','colleges.url','colleges.full_url','colleges.thumb_url')
                        ->paginate(30);
        return $reviews;
    }
    
    function fetchAllByCollegeId($collegeId){
        $reviews = DB::table($this->table)
                        ->join('colleges', 'colleges.id', '=', 'college_review.college_id')
                        ->where('college_review.college_id', '=', $collegeId)
                        ->select('college_review.*','colleges.cname','colleges.url','colleges.full_url','colleges.thumb_url')
                        ->paginate(30);
        return $reviews;
    }
}