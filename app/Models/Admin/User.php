<?php

namespace App\Models\Admin;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class User extends Model{

    protected $table = 'users';
    protected $primarykey = 'id';
    public $timestamps = true;
    protected $fillable = [
        "id",
        "username",
        "email",
        "password",
        "role",
        "social_id",
        "social_token",
        "oauth",
        "oauth_json",
        "status"
    ];
    
    function fetch($userId){
        $user = User::find($userId);
        if(isset($user->id)):
            $user->member = Member::find($userId);
        else:
            return false;
        endif;
        return $user;
    }
    
    function fetchAll(){
        $users = User::paginate(30);
        foreach($users as &$user):
            $user->basicinfo = Member::find($user->id);
        endforeach;
        return $users;
    }
    
}
