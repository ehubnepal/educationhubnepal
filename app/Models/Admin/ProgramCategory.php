<?php

namespace App\Models\Admin;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\Affiliation;

/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class ProgramCategory extends Model {

    protected $table = 'categories';
    protected $primarykey = 'id';
    public $timestamps = false;
    protected $fillable = [
        "category",
        "published"
    ];

    function fetchAll() {
        //return ProgramCategory::paginate(50);
        $categories = $temp = ProgramCategory::paginate(50);
        foreach ($temp as &$category) {
            $category->program_count = $this->getProgramsCount($category->id);
        }
        $categories->all = $temp;

        return $categories;
    }

    function getProgramsCount($categoryId) {
        $sql = "select count(id) as program_count from programs where category_id=$categoryId";
        $rows = DB::select(DB::raw($sql));
        return $rows[0]->program_count;
    }

}
