<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class ArticleTag extends Model
{
    protected $primaryKey = 'tag_id'; 
    protected $table = 'tags';
    protected $fillable = array('name');

/* Get the article associated with the given tag.

*/

    public function articles(){

    	return $this->hasMany('App\Models\Admin\Article','article_tag','post_id','tag_id');
    }
}