<?php

namespace App\Models\Admin;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\MediaBrandRef;
use App\Models\Admin\Ad;
use App\Models\Admin\Brand;

/**
 * @author gaurabdahal
 */
class Bikefeature extends Model {

    protected $table = 'bikefeatures';
    protected $primarykey = 'id';
    public $timestamps = false;
    protected $fillable = [
        "model_id",
        "displacement",
        "gearbox",
        "seat_height",
        "ground_clearance",
        "tank_capacity",
        "top_speed",
        "mileage",
        "brake",
        "tyre",
        "engine"
    ];


    
}
