<?php

namespace App\Models\Admin;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class StatisticsModel extends Model {

    function get_users($start, $end) {
        $dates = $this->dateRange($start, $end);
        $google = array();
        $facebook = array();
        $ehn = array();
        $index = array();
        foreach ($dates as $date):
            $day = date('Mj', strtotime($date));
            $index[] = $day;
            $sql1 = "select id from users where date(created_at)='$date' and oauth='google'";
            $google_row = DB::select(DB::raw($sql1));
//            $google_row = DB::table("users")
//                    ->where('date(created_at)', '=', $date)
//                    ->where('oauth', '=', 'google')
//                    ->select('id')
//                    ->get();
            $google[] = count($google_row);

            $sql2 = "select id from users where date(created_at)='$date' and oauth='facebook'";
            $facebook_row = DB::select(DB::raw($sql2));
            $facebook[] = count($facebook_row);
            
            $sql3 = "select id from users where date(created_at)='$date' and oauth=''";
            $ehn_row = DB::select(DB::raw($sql3));
            $ehn[] = count($ehn_row);
        endforeach;
        $stat = new \stdClass();
        $stat->google = json_encode($google);
        $stat->facebook = json_encode($facebook);
        $stat->ehn = json_encode($ehn);
        $stat->index = json_encode($index);
        return $stat;
    }

    /**
     * creating between two date
     * @param string since
     * @param string until
     * @param string step
     * @param string date format
     * @return array
     */
    function dateRange($first, $last, $step = '+1 day', $format = 'Y-m-d') {
        $dates = array();
        $current = strtotime($first);
        $last = strtotime($last);
        while ($current <= $last) {

            $dates[] = date($format, $current);
            $current = strtotime($step, $current);
        }
        return $dates;
    }

}
