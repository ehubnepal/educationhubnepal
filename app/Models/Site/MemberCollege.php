<?php

namespace App\Models\Site;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Brand
 *
 * @author vaghawanojha
 */
class MemberCollege extends Model {

    protected $table = 'member_college';
    protected $primarykey = 'id';
    public $timestamps = false;
    protected $fillable = [
        "college_id",
        "user_id",
        "status"
    ];

    function isCurrentCollege($collegeId, $userId) {
        $ref = DB::table($this->table)
                ->select('status')
                ->where('college_id', '=', $collegeId)
                ->where('user_id', '=', $userId)
                ->first();
        if(isset($ref->status)):
            if($ref->status == "current"):
                return true;
            endif;
        endif;
        return false;
    }

    function isPastCollege($collegeId, $userId) {
        $ref = DB::table($this->table)
                ->select('status')
                ->where('college_id', '=', $collegeId)
                ->where('user_id', '=', $userId)
                ->first();
        if(isset($ref->status)):
            if($ref->status == "past"):
                return true;
            endif;
        endif;
        return false;
    }
    
    function fetchAll($userId){
         return  DB::table($this->table)
                ->select('member_college.status','colleges.url','colleges.cname')
                ->join('colleges', 'colleges.id', '=', 'member_college.college_id')
                ->where('member_college.user_id', '=', $userId)
                ->get();
    }

}
