<?php

namespace App\Models\Site;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\Models\Site\ProgramCategoryRef;

/**
 * Description of Brand
 *
 * @author vaghawanojha
 */
class Category extends Model {

    protected $table = 'categories';
    protected $primarykey = 'id';
    protected $fillable = [
        "category",
        "published"
    ];

    /* this scope function fetch all the faculties from the faculties table which
     *  has been set to be published. 
     */

    public function scopePublished($query) {
        return $query->where('published', '=', 1);
    }

    public function fetch_all() {
        $categories = Category::where('published', 1)->get();
        foreach ($categories as &$category) {
            $category->numprograms = $this->get_no_of_programs($category->id);
        }
        return $categories;
    }

    public function fetch_home() {
        $categories = Category::where('published', 1)->where('is_home', 1)->get();
        $cat = [];
        foreach ($categories as &$category) {
            $category->numprograms = $this->get_no_of_programs($category->id);
            $cat[] = $category->numprograms;
        }
        return $categories;
    }

    public function getCategorizedPrograms() {
        //$categories = Category::all()->where('published', 1);
        $categories =Category::where('published', 1)->get();
        $programRef = new ProgramCategoryRef();
        $sorted = array();
        foreach ($categories as $category) {
            $programs = $programRef->getProgramsMinimal($category->id);
            $category->programs = $programRef->groupProgramByLevel($programs);
            $category->program_count = count($programs);
        }
        return $categories;
    }

    public function get_no_of_programs($categoryId) {
        $programRef = new ProgramCategoryRef();
        $program = $programRef->getPrograms($categoryId);
        $count = count($program);
        return $count;
    }

    public function fetch($url) {
        $category = Category:: where('url', $url)->first();
        if (!empty($category)):
            $programRef = new ProgramCategoryRef();
            $category->programs = $programRef->getPrograms($category->id);

            foreach ($category->programs as $program):
                $program->affiliation = $this->getAffiliation($program->affiliation_id);
            endforeach;
        endif;

        return $category;
    }

    public function getAffiliation($affiliationId) {
        return DB::table('affiliations')->where('id', $affiliationId)->first();
        // we can later have level wise filter in these programs. 
    }

    public function getLevel($levelId) {
        return DB::table('levels')->where('id', $levelId)->first();
    }

}
