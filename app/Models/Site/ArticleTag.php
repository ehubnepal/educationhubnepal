<?php

namespace App\Models\Site;

use Illuminate\Database\Eloquent\Model;
use App\Models\Site\ArticleTagRef;

class ArticleTag extends Model
{
    protected $primaryKey = 'tag_id'; 
    protected $table = 'tags';
    protected $fillable = array('name');

public function fetch($id){
	$tag = ArticleTag::where('tag_id',$id)->first();
	if(!empty($tag)):
		$postRef = new ArticleTagRef();
		$tag->posts = $postRef->getPosts($tag->tag_id);
	endif;
	return $tag;
}
}
