<?php

namespace App\Models\Site;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class CollegeMediaRef extends Model {

    protected $table = 'college_media';
    protected $primarykey = 'id';
    protected $fillable = [
        "college_id",
        "media_id",
    ];

    function getMedia($collegeId) {
        return DB::table($this->table)
                        ->join('medias', 'medias.id', '=', 'college_media.media_id')
                        ->where('college_media.college_id', '=', $collegeId)
                        ->select('medias.*','college_media.*')
                        ->get();
    }

}