<?php

namespace App\Models\Site;

use DB;
use Illuminate\Database\Eloquent\Model;



/**
 * Description of Brand
 *
 * @author vaghawanojha
 */
class CollegeReview extends Model {

    protected $table = 'college_review';
    protected $primarykey = 'id';
     public $timestamps = true;
     protected $fillable = [
        "college_id",
        "user_id",
         "star",
         "review",
         "published"
    ];


		 /* this scope function fetch all the faculties from the faculties table which has been set to be published. 
		 */

    public function fetchAll($collegeId){
        $reviews = CollegeReview::where(['college_id'=>$collegeId,'published'=>1])->get();
        $memberObj = new \App\Models\Member\Member();
        foreach($reviews as &$review):
            $review->user = $memberObj->fetch($review->user_id);
        endforeach;
        return $reviews;
    }
    
    function fetchByUserId($collegeId,$userId){
        $review = CollegeReview::where(['college_id'=>$collegeId,'user_id'=>$userId,'published'=>'1'])->first();
        return $review;
    }
    
    function fetchAllByUserId($userId){
        //$reviews = CollegeReview::where(['user_id'=>$userId,'published'=>1])->get();
        
        $reviews = DB::table($this->table)
                        ->join('colleges', 'colleges.id', '=', 'college_review.college_id')
                        ->where('college_review.user_id', '=', $userId)
                        ->where('college_review.published', '=', 1)
                        ->select('college_review.*','colleges.cname','colleges.url','colleges.full_url','colleges.thumb_url')
                        ->get();
        return $reviews;
    }
}