<?php

namespace App\Models\Site;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\Models\Site\Affiliation;
use App\Models\Site\AffiliationCollegeRef;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\Site\ProgramCategoryRef;
use Auth;
/**
 * Description of College Model
 *
 * @author vaghawanojha
 */
class College extends Model {

    protected $table = 'colleges';
    protected $primarykey = 'id';
    public $timestamps = true;
    protected $fillable = [
        "cname",
        "published"
    ];

    /*
      @author Vaghawan Ojha
      @editedby Gaurab Dahal
     */


    public function fetchAll($column = false) {
        $data = DB::table($this->table)
                        ->select('id')
                        ->where('published', '=', '1')->get();
        return $this->getPaginatedResult($data);
    }
    
    function getPaginatedResult($data,$param=array()){
        $colleges = array();

        /* this block is little messy but it does exactly what we need
         * It will fetch detail of only 20 college for the current page. eg page=2
         * It will put blank array in details of remaining thoudands of college 
         * that are not in current page
         * It will determine which 20 college by the page query string i.e ?page=2
         * for exaple if page =3 , it will fetch detail of college having
         * indices of 30 to 50 in array. so from index 0 to 29 and after index 51,
         * it will contain blank array in detail so that unnecessary data are not fetched.
         * 
         * This technique reduces the number of query from more than 6000 to 113 only
         */
        $page = \Illuminate\Support\Facades\Input::get('page') == null ? 1 : \Illuminate\Support\Facades\Input::get('page');
        $counter = 0;
        foreach ($data as &$college):
            if ($counter >= ($page - 1) * 21 && $counter <= $page * 21):
                $colleges[] = $this->fetch($college->id);
            else:
                $colleges[] = array();
            endif;
            $counter++;
        endforeach;
        $collection = new Collection($colleges);
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $currentPageSearchResults = $collection->slice(($currentPage - 1) * 21, 21)->all();
        $temp = new LengthAwarePaginator($currentPageSearchResults, count($collection), 21);
       if(empty($param)){
           $temp->setPath('colleges');
       }else{
           $temp->setPath('searchcollege?searchby=college&institutiontype='.$param['query'].'&location='.$param['location'].'&level='.$param['level'].'&query=' . $param['query']);
       }
        
        return $temp;
    }

    public function scopeFeatured($query) {
        return $query->where('featured', 1);
    }

    public function scopePublished($query) {
        return $query->where('published', 1);
    }

    function fetchAffiliation($affiliationId) {
        return DB::table('affiliations')->where('id', $affiliationId)->select('affiliations.id', 'affiliations.aname', 'affiliations.url')->first();
    }

    function fetch($url) {
        if (intval($url)>0):
            $college = College:: find($url);
        else:
            $college = College:: where('url', $url)->first();
        endif;

        if (!empty($college)):
            $college->programs = $this->fetchPrograms($college->id);
            $affiliations = array();
            $affiliation_ids = array();
            foreach ($college->programs as $program):
                if (!in_array($program->affiliation_id, $affiliation_ids)):
                    $affiliation_ids[] = $program->affiliation_id;
                    $affiliations[] = Affiliation::find($program->affiliation_id);
                endif;
            endforeach;
            $college->affiliations = $affiliations;
            
            $collegeReviewObj = new CollegeReview();
            $memberCollegeObj = new MemberCollege();
            $college->reviews = $collegeReviewObj->fetchAll($college->id);
            if(Auth::user()):
                $college->myreview = $collegeReviewObj->fetchByUserId($college->id,Auth::user()->id);
                $college->isCurrentCollege = $memberCollegeObj->isCurrentCollege($college->id,Auth::user()->id);
                $college->isPastCollege = $memberCollegeObj->isPastCollege($college->id,Auth::user()->id);
            endif;
        endif;

        return $college;
    }


    function fetchPrograms($collegeId) {
        $programRef = new CollegeProgramRef();
        $programs = $programRef->getProgram($collegeId);
        if (!empty($programs)):
            $catRef = new ProgramCategoryRef();
            foreach ($programs as &$program):
                $program->category = $catRef->getCategory($program->id);
                $program->affiliation = $this->fetchAffiliation($program->affiliation_id);
                // $program->affiliation = $this->fetchAffiliation($program->affiliation_id);
            endforeach;
        endif;
        return $programs;
    }

    // this function is for college listing page
    function somePrograms($collegeId) {
        $programRef = new CollegeProgramRef();
        return $programRef->getTopProgram($collegeId);
    }

    function fetchMedia($collegeId) {
        
    }

    /* public function programs(){
      return $this->hasMany('App\Models\Site\Program');
      }
     */

    function unique_affiliation($id) {
        return Affiliation::where('id', $id)->get();
    }

    /* search from homepage */

    function search($param) {
        $query = $param['query'];
        $holder = array();
        /* Priority 1: get college where the query exactly matches the college name */
        $colleges = DB::table($this->table)
                ->where('cname', '=', $query)
                ->select('id')
                ->get();
        foreach ($colleges as $college):
            $holder[$college->id] = $college->id;
        endforeach;
        /* Priority 3: get colelge where the query partially matches the college name */
        $colleges = DB::table($this->table)
                ->where('cname', 'like', '%' . $query . '%')
                ->select('id')
                ->get();
        foreach ($colleges as $college):
            $holder[$college->id] = $college->id;
        endforeach;
        /* Priority 4: get colelge where the query matches any program offered by college */
        $colleges = DB::table($this->table)
                        ->join('college_program', 'college_program.college_id', '=', 'colleges.id')
                        ->join('programs', 'programs.id', '=', 'college_program.program_id')
                        ->where('programs.pname', 'like', '%' . $query . '%')
                        ->orwhere('programs.curriculum', 'like', '%' . $query . '%')
                        ->select('colleges.id')->get();

        foreach ($colleges as $college):
            $holder[$college->id] = $college->id;
        endforeach;

        if (empty($holder)):
//            $query_fragment = explode(' ', $query);
//            $counter = 0;
//            foreach ($query_fragment as $searchstring):

                /* Priority 5: if still no result found, break down the query and search 
                 * for every words in district */
                $temps = DB::table($this->table)
                                ->join('college_program', 'college_program.college_id', '=', 'colleges.id')
                                ->join('programs', 'programs.id', '=', 'college_program.program_id')
                                ->where('programs.pname', 'like', '%' . $query . '%')
                                ->orwhere('programs.curriculum', 'like', '%' . $query . '%')
                                ->orwhere('colleges.district', 'like', '%' . $query . '%')
                                ->orwhere('colleges.address', 'like', '%' . $query . '%')
                                ->select('colleges.id')->get();
//                foreach ($temps as $temp):
//                    $holder[$temp->id] = $temp->id;
//                endforeach;
//                if (++$counter == 4):
//                    break;
//                endif;
//            endforeach;
        endif;

        $adminCollegeObj = new \App\Models\Admin\College();
        foreach ($holder as $collegeid):
            $holder[$collegeid] = $adminCollegeObj->fetch($collegeid);
        endforeach;
        // return $holder;
        $collection = new Collection($holder);
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $currentPageSearchResults = $collection->slice(($currentPage - 1) * 20, 20)->all();
        $temp = new LengthAwarePaginator($currentPageSearchResults, count($collection), 20);
        $temp->setPath('search?searchby=' . $param['searchby'] . '&query=' . $param['query']);
        return $temp;
    }

}
