<?php

namespace App\Models\Site;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\Site\ProgramCategoryRef;
use App\Models\Site\Category;
use App\Models\Site\Program;
use App\Models\Site\Affiliation;


/**
 * Description of Brand
 *
 * @author vaghawanojha
 */
class Level extends Model {

    protected $table = 'levels';
    protected $primarykey = 'id';
    protected $fillable = [
        "lname",
        
    ];


 function fetch($url){
     $level = Level:: where('url',$url)->first();
        if(!empty($level)):
            $level->programs = $this->getPrograms($level->id);
        endif;
        return $level;
}
function fetch_all(){
    $levels = Level::all();
    foreach($levels as $level):
        $level->count = $this->getNumber($level->id);
    endforeach;
    return $levels;

}
 function getPrograms($levelId){
    $programs =DB::table('programs')->where('level_id',$levelId)->paginate(20);
    $categoryRef = new ProgramCategoryRef();
    foreach($programs as $program):
        $program->categories = $categoryRef->getCategory($program->id);
        $program->affiliation = $this->getAffiliation($program->affiliation_id);
    endforeach;
    return $programs;
    // we can later have level wise filter in these programs. 
}
function getAffiliation($affiliationId){
    return DB::table('affiliations')->where('id',$affiliationId)->first();
}
function getNumber($levelId){
    return DB::table('programs')->where('level_id',$levelId)->count();
}
function search($category,$university,$levelId){
    // first handle when university is set to all. 

    $holder = array();
    // get the programs inside a category.
    if($university=='' AND $category !==''):
    $cat = Category::where('category',$category)->first();
    if(!empty($cat)):
        $categoryRef = new ProgramCategoryRef();
        $programs = $categoryRef->getProgramsSearch($cat->id,$levelId);
        foreach($programs as $program):
            $program->affiliation = $this->getAffiliation($program->affiliation_id);
            $program->categories = $categoryRef->getCategory($program->id);
        endforeach;
        $holder['result'] = $programs;
    endif;
    
    

    // second handle when category is set to all. 
    elseif($category=='' AND $university !==''):
        $uni = Affiliation::where('aname',$university)->first();
        if(!empty($uni)):
            $programs = Program::where('affiliation_id',$uni->id)->where('level_id',$levelId)->get();
            $categoryRef = new ProgramCategoryRef();
            foreach($programs as &$program):
                $program->affiliation = $uni;
                $program->categories = $categoryRef->getCategory($program->id);
            endforeach;
            $holder['result'] = $programs;
            
        endif;

    // When both category and university has been selected.
    elseif($category!=='' AND $university !==''):
        $cat = Category::where('category',$category)->first();
        $uni = Affiliation::where('aname', $university)->first();
            if(!empty($cat) AND !empty($uni)):

                $categoryRef = new ProgramCategoryRef();
                $programs = $categoryRef->getProgramsSearchAffiliation($cat->id,$uni->id,$levelId);
                foreach($programs as &$program):
                    $program->affiliation = $uni;
                    $program->categories = $categoryRef->getCategory($program->id);
                endforeach;
                $holder['result'] = $programs;
                
            endif;
    endif;
    
        /*$collection = new Collection($holder);
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $currentPageSearchResults = $collection->slice(($currentPage - 1) * 20, 20)->all();
        $temp = new LengthAwarePaginator($currentPageSearchResults, count($collection), 20);
        $temp->setPath('search?level_id=' . $levelId . '&category=' . $category.'$affiliation='.$university);
        return $temp; */
        return $holder;
    
}
}