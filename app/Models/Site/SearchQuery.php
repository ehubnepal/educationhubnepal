<?php

namespace App\Models\Site;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;



/**
 * Description of Brand
 *
 * @author vaghawanojha
 */
class SearchQuery extends Model {

    protected $table = 'search_query';
    protected $primarykey = 'id';
    public $timestamps = true;
    protected $fillable = [
        "type",
        "query",
        "created_at"
    ];
}