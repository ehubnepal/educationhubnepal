<?php

namespace App\Models\Site;

use Illuminate\Database\Eloquent\Model;
use App\Models\Site\ArticleCategoryRef;

class ArticleCategory extends Model
{
	protected $primaryKey = 'category_id'; 
	protected $table = 'article_categories';


   public function fetch($slug){
   	$category = ArticleCategory::where('slug',$slug)->first();
   	if(!empty($category)):
   	$postRef = new ArticleCategoryRef();
   	$category->posts = $postRef->getPosts($category->category_id);
   	endif;
   	return $category;
   }
}
