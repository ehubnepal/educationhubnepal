<?php

namespace App\Models\Site;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class CollegeProgramRef extends Model {

    protected $table = 'college_program';
    protected $primarykey = 'id';
    public $timestamps = false;
    protected $fillable = [
        "college_id",
        "program_id",
        "no_of_seat",
        "monthly_fee",
        "total_fee",
        "eligibility_criteria",
        "scholorship_criteria",
        "additional_info",
        "program_since",
        "published",
        "ordering",
        "detail"
    ];

    function getProgram($collegeId) {
        return DB::table($this->table)
                        ->join('programs', 'programs.id', '=', 'college_program.program_id')
                        ->where('college_program.college_id', '=', $collegeId)
                        ->select('programs.*','college_program.*')
                        ->get();
    }


    function getCollege($programId){

        return DB::table($this->table)
                        ->join('colleges','colleges.id','=','college_program.college_id')
                        ->where('college_program.program_id','=',$programId)
                        ->select('colleges.*','college_program.*')
                        ->get();
    }
   
function getTopProgram($collegeId){
    return DB::table($this->table)
                        ->join('programs', 'programs.id', '=', 'college_program.program_id')
                        ->where('college_program.college_id', '=', $collegeId)
                        ->select('programs.pname','programs.url', 'college_program.*')
                        ->orderBy('programs.weight')
                        ->take(2)
                        ->get();
}
}
