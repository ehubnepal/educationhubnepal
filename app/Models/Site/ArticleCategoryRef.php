<?php

namespace App\Models\Site;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Brand
 *
 * @author vaghawan
 */
class ArticleCategoryRef extends Model {

    protected $table = 'article_category';
    protected $primarykey = 'id';
    public $timestamps = false;
    protected $fillable = [
        "category_id",
        "post_id"
    ];

     public function getCategories($articleId){
        return DB::table('article_category')
                        ->join('article_categories', 'article_categories.category_id', '=', 'article_category.category_id')
                        ->where('article_category.post_id', '=', $articleId)
                        ->select('article_categories.*')
                        ->get();
    }

     public function getPosts($categoryId){
        return DB::table('article_category')
                        ->join('articles', 'articles.post_id', '=', 'article_category.post_id')
                        ->where('article_category.category_id', '=', $categoryId)
                        ->select('articles.*')
                        ->paginate(10);
    }

}