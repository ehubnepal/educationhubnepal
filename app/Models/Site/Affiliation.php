<?php

namespace App\Models\Site;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\Models\Site\College;
use App\Models\Site\CollegeProgramRef;
use Illuminate\Http\RedirectResponse;



/**
 * Description of Brand
 *
 * @author vaghawanojha
 */
class Affiliation extends Model {

    protected $table = 'affiliations';
    protected $primarykey = 'id';
    


		 /* this scope function fetch all the faculties from the faculties table which has been set to be published. 
		 */

    public function fetchAll(){
        $affiliation = DB::table('affiliations')->get();
        
    }
    public function fetchByType($type){
        $affiliation= DB::table('affiliations')->where('education_body_type',$type)->orderBy('weight','desc')->get();
        foreach($affiliation as &$af){
            $af->programs = $this->getPrograms($af->id);
            $af->college = $this->getCollegeCount($af->id);
            

        }

        
    return $affiliation;
    }


public function fetch($url){

    $university = Affiliation::where('url',$url)->first();
    if(!empty($university)):
    $university->program = $this->getPrograms($university->id);
    
   // Here fetch no. of colleges
    $university->college = $this->getCollegeCount($university->id);
    
    endif;
    return $university;
    
}
public function getPrograms($affiliationId){

        $programs= DB::table('programs')->where('affiliation_id',$affiliationId)->get();
        $programRef = new ProgramCategoryRef();
        foreach($programs as &$program):
            $program->category = $programRef->getCategory($program->id);
        endforeach;
    return $programs;
    }
public function getCategory($affiliationId){
	
}

function getCollegeCount($affiliationId) {
        $sql = "select count(college_program.id) as college_count from college_program inner join programs p on college_program.program_id = p.id where p.affiliation_id=$affiliationId";
        $rows = DB::select(DB::raw($sql));
        return $rows[0]->college_count;
    }


}