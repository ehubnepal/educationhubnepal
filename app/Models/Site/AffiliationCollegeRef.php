<?php

namespace App\Models\Site;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Description Of the model
 *
 * @author vaghawanojha
 */
class AffiliationCollegeRef extends Model {

    protected $table = 'affiliation_college';
    protected $primarykey = 'id';
    protected $fillable = [
        "affiliation_id",
        "college_id",
        
    ];

    function getAffiliation($collegeId) {
        return DB::table($this->table)
                        ->join('affiliations', 'affiliations.id', '=', 'affiliation_college.affiliation_id')
                        ->where('affiliation_college.college_id', '=', $collegeId)
                        ->select('affiliations.aname','affiliation_college.*')
                        ->get();
    }


    function getCollege($affiliationId){

        return DB::table($this->table)
                        ->join('colleges','colleges.id','=','affiliation_college.college_id')
                        ->where('affiliation_college.affiliation_id','=',$affiliationId)
                        ->select('colleges.*','affiliation_college.*')
                        ->get();
    }
   

}