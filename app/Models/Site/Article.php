<?php 
namespace App\Models\Site;
use DB;
use Illuminate\Database\Eloquent\Model;
use App\Models\Site\ArticleCategoryRef;


/**
 * Article Model For Backend
 *
 * @author vaghawan
 */
class Article extends Model {

    protected $table = 'articles';
    protected $primaryKey = 'post_id';

    public function scopePublished(){
    	return Article::where('active',1);
    }
    public function fetch_all(){
    	$posts = Article::published()->latest('created_at')->paginate(5);
    	$categoryRef = new ArticleCategoryRef();
    	foreach($posts as $post):
    		$post->categories = $categoryRef->getCategories($post->post_id);
    	endforeach;
    	return $posts;

    }

    public function fetch($slug){
    	$post = Article::where('slug',$slug)->first();
        if(!empty($post)):
    	$categoryRef = new ArticleCategoryRef();
    	$post->categories = $categoryRef->getCategories($post->post_id);
        endif;
    	return $post;

    }
}