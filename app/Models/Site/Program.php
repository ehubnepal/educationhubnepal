<?php

namespace App\Models\Site;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\Site\ProgramCategoryRef;
use Auth;
/**
 * Description of Brand
 *
 * @author vaghawanojha
 */
class Program extends Model {

    protected $table = 'programs';
    protected $primarykey = 'id';

    /* this scope function fetch all the faculties from the faculties table which has been set to be published. 
     */
    

    public function scopeHomepage($query) {
        return $query->where('weight', '>=', 80);
    }

    public function fetch($url) {
        $program = Program::where('url', $url)->first();
        if(!empty($program)):
        $program->college = $this->fetchPrograms($program->id);
        $program->affiliation = $this->fetchAffiliation($program->affiliation_id);
        $program->level = $this->fetchLevel($program->level_id);
        $program->category = $this->fetchCategory($program->id);
        
        //$program->facutly = $this->fetchFacutly($program->facutly_id);
        if(!empty($program->category)){
         $program->related = $this->relatedProgram($program->category[0]->id,$program->level_id);
         }
        $program->college_count = $this->getCollegeCount($program->id);
        $programReviewObj = new ProgramReview();
        $memberProgramObj = new MemberProgram();
        $program->reviews = $programReviewObj->fetchAll($program->id);
            if(Auth::user()):
                $program->myreview = $programReviewObj->fetchByUserId($program->id,Auth::user()->id);
                $program->isCurrentProgram = $memberProgramObj->isCurrentProgram($program->id,Auth::user()->id);
                $program->isPastProgram = $memberProgramObj->isPastProgram($program->id,Auth::user()->id);
            endif;
        endif;
        return $program;
    }
   function fetchAll($allcolumn = true) {
    $programs = Program::where('published',1)->get();
    if(!empty($programs)):
        foreach($programs as $program):
            

        endforeach;


    endif;
        
    }
    function fetchPrograms($programId) {
        $programRef = new CollegeProgramRef();
        return $programRef->getCollege($programId);
    }

    function fetchAffiliation($affiliationId) {
        return DB::table('affiliations')->where('id', $affiliationId)->select('aname','url')->first();
    }
    function fetchLevel($levelId){
        return DB::table('levels')->where('id',$levelId)->first();
    }
    function fetchCategory($programId){
       $categoryRef = new ProgramCategoryRef();
       return $categoryRef->getCategory($programId);
    }
    
    function relatedProgram($categoryId,$levelId){
        $relatedRef = new ProgramCategoryRef();
         $relatedRef = $relatedRef->getRelatedPrograms($categoryId,$levelId);
         if(!empty($relatedRef)):
         foreach($relatedRef as &$rel):
            $rel->affiliation = $this->fetchAffiliation($rel->affiliation_id);
        endforeach;
        endif;
        return $relatedRef;
        /*$related= DB::table('programs')->where('category_id',$categoryId)->select('pname','url','affiliation_id','category_id')->take(5)->get();
        if(!empty($related)):
        foreach($related as &$rel):
            $rel->affiliation = 
            $this->fetchAffiliation($rel->affiliation_id);
        endforeach;
        endif;
        return $related;
        */
    }

    /* search from homepage */

    function search($query, $param) {
        $holder = array();
//        $query = explode('~', $query);
//        $query = $query[0];
        /* Priority 1: get course where the query exactly matches the program name */
        $courses = DB::table($this->table)
                ->where('pname', '=', $query)
                ->select('*')
                ->get();
        foreach ($courses as $course):
            $holder[$course->id] = $course;
        endforeach;
        /* Priority 2: get course where the query partially matches the program name */
        $courses = DB::table($this->table)
                ->where('pname', 'like', '%' . $query . '%')
                ->select('*')
                ->get();
        foreach ($courses as $course):
            $holder[$course->id] = $course;
        endforeach;
        /* Priority 3: get course that contains the search string in description
                 * or in curriculum. eg if user searches computer network, this
                 * query will return program that has the course computer network */
                $temps = DB::table($this->table)
                                ->where('course_description', 'like', '%' . $query . '%')
                                ->orwhere('curriculum', 'like', '%' . $query . '%')
                                ->select('*')->get();

                foreach ($temps as $temp):
                    $holder[$temp->id] = $temp;
                endforeach;
     /*   if (empty($holder)):
            $query_fragment = explode(' ', $query);

            foreach ($query_fragment as $searchstring):
                /* Priority 4: if still no result found, break down the query and search 
                 * for every words in description *-/
                $temps = DB::table($this->table)
                                ->where('course_description', 'like', '%' . $searchstring . '%')
                                ->orwhere('curriculum', 'like', '%' . $searchstring . '%')
                                ->select('*')->get();

                foreach ($temps as $temp):
                    $holder[$temp->id] = $temp;
                endforeach;
            endforeach;
        endif; */
        foreach ($holder as &$h):
            $h->collegecount = $this->getCollegeCount($h->id);
            //$affiliationObj =  new \App\Models\Admin\Affiliation;
            $h->affiliation = \App\Models\Admin\Affiliation::find($h->affiliation_id);
        endforeach;
        $collection = new Collection($holder);
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $currentPageSearchResults = $collection->slice(($currentPage - 1) * 20, 20)->all();
        $temp = new LengthAwarePaginator($currentPageSearchResults, count($collection), 20);
        $temp->setPath('search?searchby=' . $param['searchby'] . '&query=' . $param['query']);
        return $temp;
    }

    function getCollegeCount($programId) {
        $sql = "select count(id) as college_count from college_program where program_id=$programId";
        $rows = DB::select(DB::raw($sql));
        return $rows[0]->college_count;
    }

}
