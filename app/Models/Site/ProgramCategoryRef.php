<?php

namespace App\Models\Site;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Description Of the model
 *
 * @author vaghawanojha
 */
class ProgramCategoryRef extends Model {

    protected $table = 'programcategory_program';
    protected $primarykey = 'id';
    protected $fillable = [
        "program_id",
        "category_id",
    ];

    function getCategory($programId) {
        return DB::table($this->table)
                        ->join('categories', 'categories.id', '=', 'programcategory_program.category_id')
                        ->where('programcategory_program.program_id', '=', $programId)
                        ->select('categories.category', 'categories.url', 'categories.id')
                        ->get();
    }

    function getPrograms($categoryId) {
            return DB::table($this->table)
                            ->join('programs', 'programs.id', '=', 'programcategory_program.program_id')
                            ->where('programcategory_program.category_id', '=', $categoryId)
                            ->select('programs.*')
                            ->get();
    }
    function getProgramsMinimal($categoryId){
            $sql = "select p.id,levels.lname,p.pname,p.url "
                    . "FROM programs p "
                    . "INNER JOIN $this->table ref ON ref.program_id = p.id "
                    . "INNER JOIN levels ON levels.id = p.level_id "
                    . "WHERE ref.category_id = $categoryId";
            return DB::select(DB::raw($sql));
    }

    function groupProgramByLevel($programs) {
        $groupByLevel = array();
        foreach ($programs as $row):
            $groupByLevel[$row->lname][] = $row;
        endforeach;
        return $groupByLevel;
    }

    function getRelatedPrograms($categoryId,$levelId) {
        return DB::table($this->table)
                        ->join('programs', 'programs.id', '=', 'programcategory_program.program_id')
                        ->where('programs.level_id',$levelId)
                        ->where('programcategory_program.category_id', '=', $categoryId)
                        ->select('programs.pname', 'programs.id', 'programs.url', 'programs.affiliation_id')
                        ->take(5)
                        ->get();
    }

      function getProgramsSearch($categoryId,$levelId) {
            return DB::table($this->table)
                            ->join('programs', 'programs.id', '=', 'programcategory_program.program_id')
                            ->where('programs.level_id',$levelId)
                            ->where('programcategory_program.category_id', '=', $categoryId)
                            ->select('programs.*')
                            ->get();
    }

    function getProgramsSearchAffiliation($categoryId,$affiliationId,$levelId) {
            return DB::table($this->table)
                            ->join('programs', 'programs.id', '=', 'programcategory_program.program_id')
                            ->where('programs.affiliation_id',$affiliationId)
                            ->where('programs.level_id',$levelId)
                            ->where('programcategory_program.category_id', '=', $categoryId)
                            ->select('programs.*')
                            ->get();
    }

}
