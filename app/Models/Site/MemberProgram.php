<?php

namespace App\Models\Site;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Brand
 *
 * @author vaghawanojha
 */
class MemberProgram extends Model {

    protected $table = 'member_program';
    protected $primarykey = 'id';
    public $timestamps = false;
    protected $fillable = [
        "program_id",
        "user_id",
        "status"
    ];

    function isCurrentProgram($programId, $userId) {
        $ref = DB::table($this->table)
                ->select('status')
                ->where('program_id', '=', $programId)
                ->where('user_id', '=', $userId)
                ->first();
        if(isset($ref->status)):
            if($ref->status == "current"):
                return true;
            endif;
        endif;
        return false;
    }

    function isPastProgram($programId, $userId) {
        $ref = DB::table($this->table)
                ->select('status')
                ->where('program_id', '=', $programId)
                ->where('user_id', '=', $userId)
                ->first();
        if(isset($ref->status)):
            if($ref->status == "past"):
                return true;
            endif;
        endif;
        return false;
    }
    
    function fetchAll($userId){
         return  DB::table($this->table)
                ->select('member_program.status','programs.url','programs.pname')
                ->join('programs', 'programs.id', '=', 'member_program.program_id')
                ->where('member_program.user_id', '=', $userId)
                ->get();
    }

}
