<?php

namespace App\Models\Site;

use DB;
use Illuminate\Database\Eloquent\Model;



/**
 * Description of Brand
 *
 * @author vaghawanojha
 */
class ProgramReview extends Model {

    protected $table = 'program_review';
    protected $primarykey = 'id';
     public $timestamps = true;
     protected $fillable = [
        "program_id",
        "user_id",
         "star",
         "review",
         "published"
    ];


		 /* this scope function fetch all the faculties from the faculties table which has been set to be published. 
		 */

    public function fetchAll($programId){
        $reviews = ProgramReview::where(['program_id'=>$programId,'published'=>1])->get();
        $memberObj = new \App\Models\Member\Member();
        foreach($reviews as &$review):
            $review->user = $memberObj->fetch($review->user_id);
        endforeach;
        return $reviews;
    }
    
    function fetchByUserId($programId,$userId){
        $review = ProgramReview::where(['program_id'=>$programId,'user_id'=>$userId,'published'=>'1'])->first();
        return $review;
    }
    
    function fetchAllByUserId($userId){
        //$reviews = CollegeReview::where(['user_id'=>$userId,'published'=>1])->get();
        
        $reviews = DB::table($this->table)
                        ->join('programs', 'programs.id', '=', 'program_review.program_id')
                        ->where('program_review.user_id', '=', $userId)
                        ->where('program_review.published', '=', 1)
                        ->select('program_review.*','programs.pname','programs.url')
                        ->get();
        return $reviews;
    }
}