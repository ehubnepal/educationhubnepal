<?php

namespace App\Models\Site;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class ArticleTagRef extends Model {

    protected $table = 'article_tag';
    protected $primarykey = 'id';
    public $timestamps = false;
    protected $fillable = [
        "post_id",
        "tag_id"
    ];
      public function getTags($articleId){
        return DB::table('article_tag')
                        ->join('tags', 'tags.tag_id', '=', 'article_tag.tag_id')
                        ->where('article_tag.post_id', '=', $articleId)
                        ->select('tags.*')
                        ->get();
    }

    public function getPosts($tagId){
         return DB::table('article_tag')
                        ->join('articles', 'articles.post_id', '=', 'article_tag.post_id')
                        ->where('article_tag.tag_id', '=', $tagId)
                        ->select('articles.*')
                        ->paginate(10);
    }

}