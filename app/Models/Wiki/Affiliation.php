<?php

namespace App\Models\Wiki;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\Models\Wiki\Program;

/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class Affiliation extends Model {

    protected $table = 'affiliations';
    protected $primarykey = 'id';
    public $timestamps = true;
    protected $fillable = [
        "aname",
        "established",
        "education_body_type",
        "language_of_education",
        "vice_chancellor",
        "description",
        "published"
    ];

    function fetchAll($allcolumn = true) {
        if ($allcolumn):
            return Affiliation::paginate(30);
        else:
            return DB::table($this->table)
                        ->select('id','aname')->get();
        endif;
    }

    function fetch($id) {
        $affiliation = Affiliation::find($id);
        if (!empty($affiliation)):
            $affiliation->programs = $this->fetchPrograms($affiliation->id);
            $mediaRefObj = new AffiliationMediaRef();
            $affiliation->media = $mediaRefObj->getAllMedia($affiliation->id);
        endif;
        return $affiliation;
    }

    function fetchPrograms($affiliationId) {
        $programRef = new Program();
        return $programRef->getProgramByAffiliation($affiliationId);
    }
    
}
