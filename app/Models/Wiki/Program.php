<?php

namespace App\Models\Wiki;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\Models\Wiki\Affiliation;

/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class Program extends Model {

    protected $table = 'programs';
    protected $primarykey = 'id';
    public $timestamps = true;
    protected $fillable = [
        "pname",
        "affiliation_id",
        "program_since",
        "published",
        "level",
        "duration",
        "cost_range",
        "academic year",
        "evaluation_method",
        "faculty_id",
        "weight", // added by vaghawan
        "course_description",
        "eligibility",
        "curriculum",
        "published"
    ];

    function fetchAll($allcolumn = true) {
        if ($allcolumn):
            $programs = Program::paginate(30);
        else:
            $programs = DB::table($this->table)
                            ->select('id', 'pname', 'affiliation_id')->get();
        endif;
        foreach ($programs as &$program):
            $program->affiliation = Affiliation::find($program->affiliation_id);
        endforeach;
        return $programs;
    }

    function fetch($id) {
        if (is_int($id)):
            $program = Program:: find($id);
        else:
            //or it is a url
            $program = Program:: where('url', $id)->first();
        endif;
        $program->affiliation = Affiliation::find($program->affiliation_id);
        //$program->colleges = $this->fetchColleges($program->id);
        $programCategoryRef = new ProgramCategoryRef();
        $program->programcategory = $programCategoryRef->getCategoryIds($program->id);
        return $program;
    }

    function fetchProgramsByCollege($collegeId) {
        $programs = DB::table($this->table)
                        ->join('college_program', 'college_program.program_id', '=', 'programs.id')
                        ->join('affiliations', 'affiliations.id', '=', 'programs.affiliation_id')
                        ->where('college_program.college_id', $collegeId)
                        ->select('programs.id', 'pname', 'affiliation_id', 'aname')->get();
        return $programs;
    }

    function fetchColleges($programId) {
        $collegeRef = new CollegeProgramRef();
        return $collegeRef->getCollege($programId);
    }

    function getProgramByAffiliation($affiliationId) {
        return Program::select('*')->where("affiliation_id", $affiliationId)->get();
    }

    function search($query) {
        $programs = DB::table($this->table)
                ->where('pname', 'like', '%' . $query . '%')
                ->select('*')
                ->paginate(30);

        foreach ($programs as &$program):
            $program->affiliation = Affiliation::find($program->affiliation_id);
        endforeach;
        return $programs;
    }

    function searchAjax($query) {
        $programs = DB::table($this->table)
                ->join('affiliations', 'affiliations.id', '=', 'programs.affiliation_id')
                ->where('pname', 'like', '%' . $query . '%')
                ->select('programs.id', 'pname', 'aname')
                ->get();
        $items = array();
        foreach ($programs as $program):
            $items[] = $program;
        endforeach;
        return $items;
    }

}
