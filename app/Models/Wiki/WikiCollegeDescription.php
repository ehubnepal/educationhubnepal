<?php

namespace App\Models\Wiki;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class WikiCollegeDescription extends Model {

    protected $table = 'wiki_college_info';
    protected $primarykey = 'id';
    public $timestamps = true;
    protected $fillable = [
        "user_id",
        "college_id",
        "description",
        "published",
        "up_votes",
        "down_votes",
        "website",
        "phone",
        "email",
        "fax",
        "address"
    ];

}
