<?php

namespace App\Models\Wiki;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class College extends Model {

    protected $table = 'colleges';
    protected $primarykey = 'id';
    public $timestamps = true;
    protected $fillable = [
        "cname",
        "address",
        "longitude",
        "latitude",
        "district",
        "affiliation_ids",
        "phone",
        "fax",
        "website",
        "pobox",
        "email",
        "description",
        "country",
        "facilities",
        "published"
    ];

    function fetchAll($column=false) {
        return College::paginate(30);
    }
    
    function fetch($url) {
        if (is_int($url)):
            $college = College:: find($url);
        else:
            $college = College:: where('url', $url)->first();
        endif;
        return $college;
    }

    function fetchPrograms($collegeId) {
        $programRef = new CollegeProgramRef();
        return $programRef->getProgram($collegeId);
    }
 
    
    function search($query){
        return DB::table($this->table)
                        ->where('cname', 'like', '%'.$query.'%')
                        ->select('*')
                        ->paginate(30);
    }
    
    function searchAjax($query){
        $colleges = DB::table($this->table)
                        ->where('cname', 'like', '%'.$query.'%')
                        ->select('id','cname','address','full_url')
                        ->get();
        $items = array();
        foreach($colleges as $college):
            $items[] = $college;
        endforeach;
        return $items;
    }

    
}
