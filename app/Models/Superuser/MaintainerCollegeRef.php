<?php

namespace App\Models\Superuser;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class MaintainerCollegeRef extends Model {

    protected $table = 'maintainer_college';
    protected $primarykey = 'id';
    public $timestamps = false;
    protected $fillable = [
        "user_id",
        "college_id",
        "startdate",
        "enddate"
    ];
    
    function hello(){
        echo 'dead';
    }

    function fetchCollegeByUserId($userId) {
        $colleges = DB::table($this->table)
                ->join('colleges', 'colleges.id', '=', 'maintainer_college.college_id')
                ->where('maintainer_college.user_id', '=', $userId)
                ->select('colleges.*')
                ->get();
//        $programObj = new \App\Models\Admin\Program();
//        $maintainerCollegeProgramRef = new MaintainerCollegeProgramRef();
//        foreach ($colleges as &$college):
//            $college->programs = $programObj->fetchProgramsByCollege($college->id);
//            $maintainers_program = $maintainerCollegeProgramRef->getSelectedCollegePrograms($userId,$college->id);
//        foreach($college->programs as &$programRef):
//            if(in_array($programRef->id, $maintainers_program)):
//                $programRef->checked = 'checked';
//            else:
//                $programRef->checked = '';
//            endif;
//        endforeach;
//            endforeach;
        return $colleges;
    }

}
