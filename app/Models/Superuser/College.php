<?php

namespace App\Models\Superuser;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\Ad;

/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class College extends Model {

    protected $table = 'maintailer_edit_colleges';
    protected $primarykey = 'id';
    public $timestamps = true;
    protected $fillable = [
        "cname",
        "address",
        "longitude",
        "latitude",
        "district",
        "affiliation_ids",
        "phone",
        "fax",
        "website",
        "pobox",
        "email",
        "description",
        "country",
        "user_id",
        "facilities",
        "published"
    ];

    function fetchAll() {
        return College::paginate(30);
    }
    
    function fetch($id){
        $college = College::find($id);
        if(!empty($college)):
            //$college->programs = $this->fetchPrograms($college->id);
            $mediaRefObj = new MaintainerCollegeMediaRef();
            $college->media = $mediaRefObj->getAllMedia($college->id);
        endif;
        return $college;
    }

    function fetchPrograms($collegeId) {
        $programRef = new CollegeProgramRef();
        return $programRef->getProgram($collegeId);
    }
    
 
    
    function fetchMedia($brandId) {
        $mediaRef = new MediaBrandRef();
        return $mediaRef->getMedia($brandId);
    }

    function countAd($brandId) {
        return Ad::where('brand_id', $brandId)->get()->count();
    }
    
    function search($query){
        return DB::table($this->table)
                        ->where('cname', 'like', '%'.$query.'%')
                        ->select('*')
                        ->paginate(30);
    }
    
    function searchAjax($query){
        $colleges = DB::table($this->table)
                        ->where('cname', 'like', '%'.$query.'%')
                        ->select('id','cname','address','full_url')
                        ->get();
        $items = array();
        foreach($colleges as $college):
            $items[] = $college;
        endforeach;
        return $items;
    }

    
}
