<?php

namespace App\Models\Superuser;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class MaintainerCollegeMediaRef extends Model {

    protected $table = 'maintainercollege_media';
    protected $primarykey = 'id';
    public $timestamps = false;
    protected $fillable = [
        "college_id",
        "media_id",
        "published",
        "is_banner",
        "user_id",
        "ordering"
    ];
    
    function getAllMedia($collegeId){
        $medias =  DB::table($this->table)
                        ->join('medias', 'medias.id', '=', 'maintainercollege_media.media_id')
                        ->where('maintainercollege_media.college_id', '=', $collegeId)
                        ->select('medias.*','maintainercollege_media.id as refid','maintainercollege_media.published','maintainercollege_media.is_banner','maintainercollege_media.ordering')
                        ->get();
        return $medias;
    }
    
    function getMedia($mediaId){
        $medias =  DB::table($this->table)
                        ->join('medias', 'medias.id', '=', 'maintainercollege_media.media_id')
                        ->where('maintainercollege_media.media_id', '=', $mediaId)
                        ->select('medias.*','maintainercollege_media.id as refid','maintainercollege_media.published','maintainercollege_media.is_banner','maintainercollege_media.ordering')
                        ->first();
        return $medias;
    }
    
   
}
