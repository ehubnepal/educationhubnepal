<?php

namespace App\Models\Superuser;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\Ad;
use Auth;
/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class News extends Model {

    protected $table = 'news';
    protected $primarykey = 'id';
    public $timestamps = true;
    protected $fillable = [
        "college_id",
        "user_id",
        "title",
        "news",
        "published"
    ];

    function fetchAll() {
        //return News::where('user_id',Auth::user()->id)->paginate(30);
         return DB::table($this->table)
                        ->join('colleges', 'colleges.id', '=', 'news.college_id')
                        ->where('news.user_id', '=', Auth::user()->id)
                        ->select('news.*','colleges.cname')
                        ->paginate(30);
    }
    
    function fetch($id){
        $college = College::find($id);
       
        if(!empty($college)):
            //$college->programs = $this->fetchPrograms($college->id);
            $mediaRefObj = new MaintainerCollegeMediaRef();
            $college->media = $mediaRefObj->getAllMedia($college->id);
        endif;
        return $college;
    }

    function fetchPrograms($collegeId) {
        $programRef = new CollegeProgramRef();
        return $programRef->getProgram($collegeId);
    }
    
 
    
    function fetchMedia($brandId) {
        $mediaRef = new MediaBrandRef();
        return $mediaRef->getMedia($brandId);
    }

    function countAd($brandId) {
        return Ad::where('brand_id', $brandId)->get()->count();
    }
    
    function search($query){
        return DB::table($this->table)
                        ->where('cname', 'like', '%'.$query.'%')
                        ->select('*')
                        ->paginate(30);
    }
    
    function searchAjax($query){
        $colleges = DB::table($this->table)
                        ->where('cname', 'like', '%'.$query.'%')
                        ->select('id','cname','address','full_url')
                        ->get();
        $items = array();
        foreach($colleges as $college):
            $items[] = $college;
        endforeach;
        return $items;
    }

    
}
