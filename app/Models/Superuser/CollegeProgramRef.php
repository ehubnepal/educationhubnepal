<?php

namespace App\Models\Superuser;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\Affiliation;

/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class CollegeProgramRef extends Model {

    protected $table = 'm_college_program';
    protected $primarykey = 'id';
    public $timestamps = false;
    protected $fillable = [
        "college_id",
        "program_id",
        "no_of_seat",
        "monthly_fee",
        "total_fee",
        "eligibility_criteria",
        "scholorship_criteria",
        "additional_info",
        "program_since",
        "published",
        "ordering"
    ];

    function getProgram($collegeId) {
        $programs =  DB::table($this->table)
                        ->join('programs', 'programs.id', '=', 'm_college_program.program_id')
                        ->where('m_college_program.college_id', '=', $collegeId)
                        ->select('programs.*','m_college_program.*')
                        ->get();
        foreach($programs as &$program):
            $program->affiliation = Affiliation::find($program->affiliation_id);
        endforeach;
        return $programs;
    }
    
    function getCollege($programId){
        $colleges =  DB::table($this->table)
                        ->join('colleges', 'colleges.id', '=', 'college_program.college_id')
                        ->where('college_program.program_id', '=', $programId)
                        ->select('colleges.*')
                        ->get();
        return $colleges;
    }

   function fetch($collegeId, $programId) {
        $colleges = DB::table($this->table)
                ->join('colleges', 'colleges.id', '=', 'm_college_program.college_id')
                ->join('programs', 'programs.id', '=', 'm_college_program.program_id')
                ->where('program_id', '=', $programId)
                ->where('college_id', '=', $collegeId)
                ->select('m_college_program.*','cname','pname')
                ->first();
        return $colleges;
    }
}
