<?php

namespace App\Models\Superuser;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class MaintainerCollegeProgramRef extends Model {

    protected $table = 'maintainer_college_program';
    protected $primarykey = 'id';
    public $timestamps = false;
    protected $fillable = [
        "user_id",
        "college_id",
        "program_id"
    ];

    function getSelectedCollegePrograms($userId, $collegeId) {
        $refs = DB::table($this->table)
                ->where(['user_id' => $userId, 'college_id' => $collegeId])
                ->select('program_id')
                ->get();
        $ids = array();
        foreach ($refs as $ref):
            $ids[] = $ref->program_id;
        endforeach;
        return $ids;
    }
    
    function getCollegePrograms($userId, $collegeId) {
        $refs = DB::table($this->table)
                ->join('programs', 'programs.id', '=', 'maintainer_college_program.program_id')
                ->where(['maintainer_college_program.user_id' => $userId, 'maintainer_college_program.college_id' => $collegeId])
                ->select('programs.*','maintainer_college_program.college_id')
                ->get();
        return $refs;
    }

}
