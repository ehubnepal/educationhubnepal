<?php

namespace App\Models\Member;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class User extends Model implements \Illuminate\Contracts\Auth\Authenticatable{

    protected $table = 'users';
    protected $primarykey = 'id';
    public $timestamps = true;
    protected $fillable = [
        "id",
        "username",
        "email",
        "password",
        "role",
        "social_id",
        "social_token",
        "oauth",
        "oauth_json",
        "status"
    ];
    
    function user_exist($email){
        $user = User::where(['email' => $email,'status' => 'confirmed'])->first();
        return isset($user->id) && $user->id>0?true:false;
    }
    
    function user_exists_oauth($accounttype,$social_id){
        $user = User::where(['social_id' => $social_id,'oauth' => $accounttype])->first();
        return isset($user->id) && $user->id>0?$user:false;
    }
    
    function signUp($userdata){
        return User::create($userdata);
    }

    public function getAuthIdentifier() {
        return $this->getKey();
    }

    public function getAuthIdentifierName() {
        
    }

    public function getAuthPassword() {
        
    }

    public function getRememberToken() {
        
    }

    public function getRememberTokenName() {
        
    }

    public function setRememberToken($value) {
        
    }

}
