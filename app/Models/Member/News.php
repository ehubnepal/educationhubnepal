<?php

namespace App\Models\Member;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\Ad;
use Auth;
/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class News extends Model {

    protected $table = 'news';
    protected $primarykey = 'id';
    public $timestamps = true;
    protected $fillable = [
        "college_id",
        "user_id",
        "title",
        "news",
        "college",
        "program",
        "editable",
        "source",
        "published"
    ];

    function fetchAll() {
        //return News::where('user_id',Auth::user()->id)->paginate(30);
         return DB::table($this->table)
                     //   ->join('colleges', 'colleges.id', '=', 'news.college_id')
                        ->where('news.user_id', '=', Auth::user()->id)
                        ->select('news.*')
                        ->paginate(30);
    }
}
