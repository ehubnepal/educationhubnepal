<?php

namespace App\Models\Member;

use DB;
use Illuminate\Database\Eloquent\Model;
use Auth;
/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class College extends Model {

    protected $table = 'm_colleges';
    protected $primarykey = 'id';
    public $timestamps = true;
    protected $fillable = [
        "cname",
        "address",
        "longitude",
        "latitude",
        "district",
        "affiliation_ids",
        "phone",
        "fax",
        "website",
        "pobox",
        "email",
        "description",
        "country",
        "facilities",
        "user_id",
        "editable",
        "published"
    ];

    function fetchAll() {
       return \App\Models\Member\College::select('*')->where('user_id',Auth::user()->id)->get();
    }
    
    
}
