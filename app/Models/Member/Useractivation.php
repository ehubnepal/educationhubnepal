<?php

namespace App\Models\Member;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class Useractivation extends Model {

    protected $table = 'useractivation';
    protected $primarykey = 'id';
    public $timestamps = false;
    protected $fillable = [
        "user_id",
        "email",
        "salt",
        "token",
        "created_at"
    ];

    function activate($email, $salt, $token) {
        $data = Useractivation::where(['email' => $email, 'salt' => $salt, 'token' => $token])->first();
        if (isset($data->user_id) && $data->user_id > 0):
            $user = User::find($data->user_id);
            $user->status = "confirmed";
            $user->save();
            $this->blockUnverifiedAccounts($email);
            Useractivation::destroy($data->id);
            return true;
        else:
            return false;
        endif;
    }
     
    function blockUnverifiedAccounts($email){
        User::where(['email' => $email, 'status' => 'pending'])->update(["status"=>"blocked",'password'=>'']);
     /*   delete from members
        delete from users
        delete from user activation
        delete from member collge
        delete from member Program
        delete from maintainer collge
        delete from mainter Program */
        
        
    }
    

}
