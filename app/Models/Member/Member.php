<?php

namespace App\Models\Member;

use DB;
use Illuminate\Database\Eloquent\Model;
use Auth;
/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class Member extends Model {

    protected $table = 'members';
    protected $primarykey = 'id';
    public $timestamps = false;
    protected $fillable = [
        "id",
        "fullname",
        "address",
        "phone",
        "college_id",
        "program_id",
        "full_url",
        "thumb_url",
        "twitter",
        "social_image",
        "skype",
        "facebook"
    ];

    function fetch($id) {
        $member = Member::find($id);
        if (!empty($member)):
            $member->college = $member->college_id > 0 ? College::find($member->college_id) : false;
            $member->program = $member->program_id > 0 ? Program::find($member->program_id) : false;
        endif;
        return $member;
    }

}
