<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use PHPMailer;
/**
 * Description of Brand
 *
 * @author gaurabdahal
 */
class CommonModel extends Model{
    
     function sendmail($mailto,$subject,$message,$attachments = FALSE) {
       $replyto = "info@educationhubnepal.com";
        $uid = md5(uniqid(time()));

        $header = "From: Education Hub Nepal <info@educationhubnepal.com>\r\n"; //" . $eol;
        $header .= "Reply-To: ".$replyto."\r\n";
        $header .= "MIME-Version: 1.0\r\n";
        $header .= "Content-Type: multipart/mixed; boundary=\"" . $uid . "\"\r\n\r\n";

// message & attachment
        $nmessage = "--" . $uid . "\r\n";
        $nmessage .= "Content-type:text/html; charset=iso-8859-1\r\n";
        $nmessage .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
        $nmessage .= $message . "\r\n\r\n";
        $nmessage .= "--" . $uid . "\r\n";

        if ($attachments != FALSE):
            $file = '';
            if (is_array($attachments)) {
                $file = $attachments[0];
            } else {
                $file = $attachment;
            }
            $filename = $this->get_filename($file);
            $file_size = filesize($file);
            $handle = fopen($file, "r");
            $content = fread($handle, $file_size);
            fclose($handle);
            $content = chunk_split(base64_encode($content));
            $name = basename($file);

            $nmessage .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"\r\n";
            $nmessage .= "Content-Transfer-Encoding: base64\r\n";
            $nmessage .= "Content-Disposition: attachment; filename=\"" . $filename . "\"\r\n\r\n";
            $nmessage .= $content . "\r\n\r\n";
            $nmessage .= "--" . $uid . "--";
        endif;
        if (mail($mailto, $subject, $nmessage, $header)) {
            return 1;
        } else {
            return 0;
        }
    }
    
    
}
